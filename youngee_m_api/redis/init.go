package redis

import (
	"context"
	"fmt"
	"youngee_m_api/model/system_model"

	"github.com/go-redis/redis/v8"
)

var client *redis.Client

func Init(config *system_model.Redis) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%v:%v", config.Host, config.Port),
		Password: config.Auth,          // no password set
		DB:       int(config.Database), // use default DB
	})
	client = rdb
}

func GetRedisClient(ctx context.Context) *redis.Client {
	return client.WithContext(ctx)
}
