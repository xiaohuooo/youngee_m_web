package redis

import (
	"context"
	"time"
)

func Get(ctx context.Context, key string) (string, error) {
	val, err := client.Get(ctx, key).Result()
	if err != nil {
		return "", err
	}
	return val, nil
}

func GetOrDefault(ctx context.Context, key, defaultValue string) string {
	val, err := client.Get(ctx, key).Result()
	if err != nil {
		return defaultValue
	}
	return val
}

func Set(ctx context.Context, key, value string, expiration time.Duration) error {
	err := client.Set(ctx, key, value, expiration).Err()
	return err
}
