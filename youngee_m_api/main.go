package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	_ "net/http/pprof"
	"youngee_m_api/config"
	"youngee_m_api/route"
	"youngee_m_api/service"
)

func main() {
	r := gin.Default()
	route.InitRoute(r)
	config := config.Init()

	//s := &http.Server{
	//	Addr:           fmt.Sprintf(":%d", config.Port),
	//	Handler:        r,
	//	MaxHeaderBytes: 1 << 20,
	//}
	//
	//go func() {
	//	if err := s.ListenAndServe(); err != nil {
	//		log.Printf("Listen: %s\n", err)
	//	}
	//}()
	//
	//quit := make(chan os.Signal)
	//signal.Notify(quit, os.Interrupt)
	//<-quit
	//
	//log.Println("Shutdown Server ...")
	//
	//ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	//defer cancel()
	//if err := s.Shutdown(ctx); err != nil {
	//	log.Fatal("Server Shutdown:", err)
	//}
	//
	//log.Println("Server exiting")

	addr := fmt.Sprintf("%v:%v", config.Host, config.Port)
	err := service.AutoTask()
	if err != nil {
		log.Println("service AutoTask error:", err)
	}
	r.Run(addr)
}
