package service

import (
	"context"
	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"youngee_m_api/db"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
)

var Operate *operate

type operate struct {
}

func (*operate) SearchPricing(ctx context.Context, pagesize, pagenum int32, conditions *common_model.PricingConditions) (*http_model.SearchPricingData, error) {
	searchPricings, total, err := db.SearchPricing(ctx, pagesize, pagenum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[operate service] call SearchPricing error,err:%+v", err)
		return nil, err
	}
	searchPricingData := new(http_model.SearchPricingData)
	searchPricingData.SearchPricingPreview = pack.GormSearchPricingDataToHttpData(searchPricings)
	searchPricingData.Total = conv.MustString(total, "")
	return searchPricingData, nil
}

func (*operate) SearchYoungee(ctx context.Context, pagesize, pagenum int32, conditions *common_model.YoungeeConditions) (*http_model.SearchYoungeeData, error) {
	searchYoungees, total, err := db.SearchYoungee(ctx, pagesize, pagenum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[operate service] call SearchYoungee error,err:%+v", err)
		return nil, err
	}
	searchYoungeeData := new(http_model.SearchYoungeeData)
	searchYoungeeData.SearchYoungeePreview = pack.GormSearchYoungeeDataToHttpData(searchYoungees)
	searchYoungeeData.Total = conv.MustString(total, "")
	return searchYoungeeData, nil
}
