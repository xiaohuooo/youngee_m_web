package service

import (
	"context"
	"github.com/sirupsen/logrus"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
)

var Pay *pay

type pay struct {
}

func (*pay) GetPaysum(ctx context.Context, project http_model.PaySumRequest) (*http_model.PaySumResponce, error) {
	projectId := project.ProjectId
	tasks, err := db.GetTaskList(ctx, projectId)
	if err != nil {
		logrus.Infof("[GetPayTasklist] fail,err:%+v", err)
		return nil, err
	}
	recruitStrategy, err := db.GetRecruitStrategyByProjectId(ctx, projectId)
	if err != nil {
		logrus.Infof("[GetPayTasklist] fail,err:%+v", err)
		return nil, err
	}
	payMap := make(map[int]float64)
	for _, v := range recruitStrategy {
		payMap[int(v.StrategyID)] = 0
	}
	PaySum := http_model.PaySumResponce{}
	if tasks != nil {
		for _, task := range tasks {
			payMap[task.StrategyId] += task.AllPayment
		}
		for k, v := range payMap {
			ps := http_model.PaySum{StrategyId: k, AllPayment: v}
			PaySum.PaySum = append(PaySum.PaySum, ps)
		}
	}
	return &PaySum, nil
}
