package service

import (
	"context"
	"fmt"
	"strconv"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

var Product *product

type product struct {
}

func (*product) Create(ctx context.Context, newProduct http_model.CreateProductRequest, enterpriseID string) (*http_model.CreateProductData, error) {
	product := gorm_model.YounggeeProduct{
		ProductName:   newProduct.ProductName,
		ProductType:   newProduct.ProductType,
		ShopAddress:   newProduct.ShopAddress,
		ProductPrice:  newProduct.ProductPrice,
		ProductDetail: newProduct.ProductDetail,
		ProductUrl:    newProduct.ProductUrl,
		EnterpriseID:  enterpriseID,
		BrandName:     newProduct.BrandName,
	}
	productID, err := db.CreateProduct(ctx, product)
	if err != nil {
		return nil, err
	}
	if newProduct.ProductPhotos != nil {
		productPhotos := []gorm_model.YounggeeProductPhoto{}
		for _, photo := range newProduct.ProductPhotos {
			productPhoto := gorm_model.YounggeeProductPhoto{
				PhotoUrl:  photo.PhotoUrl,
				PhotoUid:  photo.PhotoUid,
				Symbol:    photo.Symbol,
				ProductID: *productID,
			}
			productPhotos = append(productPhotos, productPhoto)
		}
		err = db.CreateProductPhoto(ctx, productPhotos)
		if err != nil {
			return nil, err
		}
	}
	res := &http_model.CreateProductData{
		ProductID: *productID,
	}
	return res, nil
}

func (*product) FindByID(ctx context.Context, productID int64) (*http_model.FindProductData, error) {
	product, err := db.GetProductByID(ctx, productID)
	if err != nil {
		return nil, err
	}
	if product == nil {
		return nil, nil
	}
	productPhotos, err := db.GetProductPhotoByProductID(ctx, productID)
	if err != nil {
		return nil, err
	}
	productPrice, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", product.ProductPrice), 64)
	findProductData := http_model.FindProductData{
		ProductID:     product.ProductID,
		ProductName:   product.ProductName,
		ProductType:   product.ProductType,
		ShopAddress:   product.ShopAddress,
		ProductPrice:  productPrice,
		ProductDetail: product.ProductDetail,
		ProductUrl:    product.ProductUrl,
		EnterpriseID:  product.EnterpriseID,
		BrandName:     product.BrandName,
	}
	for _, photo := range productPhotos {
		productPhoto := http_model.ProductPhoto{
			PhotoUrl: photo.PhotoUrl,
			PhotoUid: photo.PhotoUid,
			Symbol:   photo.Symbol,
		}
		findProductData.ProductPhotos = append(findProductData.ProductPhotos, productPhoto)
	}
	return &findProductData, nil
}

func (*product) FindAll(ctx context.Context, enterpriseID string) (*http_model.FindAllProductData, error) {
	products, err := db.GetProductByEnterpriseID(ctx, enterpriseID)
	if err != nil {
		// 数据库查询error
		return nil, err
	}
	findAllProductData := http_model.FindAllProductData{}
	for _, product := range products {
		productData := http_model.ProductInfo{
			ProductID:   product.ProductID,
			BrandName:   product.BrandName,
			ProductName: product.ProductName,
			ProductType: consts.GetProjectTypes(product.ProductType),
		}
		findAllProductData.ProductInfos = append(findAllProductData.ProductInfos, productData)
	}
	return &findAllProductData, nil
}

func (*product) Update(ctx context.Context, newProduct http_model.CreateProductRequest, enterpriseID string) (*http_model.CreateProductData, error) {
	productPrice, _ := strconv.ParseFloat(fmt.Sprintf("%.2f", newProduct.ProductPrice), 64)
	product := gorm_model.YounggeeProduct{
		ProductID:     newProduct.ProductId,
		ProductName:   newProduct.ProductName,
		ProductType:   newProduct.ProductType,
		ShopAddress:   newProduct.ShopAddress,
		ProductPrice:  productPrice,
		ProductDetail: newProduct.ProductDetail,
		ProductUrl:    newProduct.ProductUrl,
		EnterpriseID:  enterpriseID,
		BrandName:     newProduct.BrandName,
	}
	productID, err := db.UpdateProduct(ctx, product)
	if err != nil {
		return nil, err
	}
	// 删除该商品之前的所有图片
	err = db.DeleteProductPhotoByProductID(ctx, *productID)
	if err != nil {
		return nil, err
	}
	if newProduct.ProductPhotos != nil {
		// 新增图片
		productPhotos := []gorm_model.YounggeeProductPhoto{}
		for _, photo := range newProduct.ProductPhotos {
			productPhoto := gorm_model.YounggeeProductPhoto{
				PhotoUrl:  photo.PhotoUrl,
				PhotoUid:  photo.PhotoUid,
				Symbol:    photo.Symbol,
				ProductID: *productID,
			}
			productPhotos = append(productPhotos, productPhoto)
		}
		err = db.CreateProductPhoto(ctx, productPhotos)
		if err != nil {
			return nil, err
		}
	}
	res := &http_model.CreateProductData{
		ProductID: *productID,
	}
	return res, nil
}
