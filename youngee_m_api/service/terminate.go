package service

import (
	"context"
	"fmt"
	"strings"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"

	"github.com/sirupsen/logrus"
)

var Terminate *terminate

type terminate struct {
}

// TaskTerminate 解约
func (*terminate) TaskTerminate(ctx context.Context, request http_model.TaskTerminateRequest) (*http_model.TaskTerminateData, error) {
	var TaskIDList []string
	TaskIDs := strings.Split(request.TaskIds, ",")
	for _, taskId := range TaskIDs {
		TaskIDList = append(TaskIDList, taskId)
	}
	fmt.Printf("acc request %+v", TaskIDList)
	err := db.Terminate(ctx, TaskIDList, request.ProjectIds)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Terminate service] call CreateTerminate error,err:%+v", err)
		return nil, err
	}

	res := &http_model.TaskTerminateData{
		TaskIds: TaskIDList,
	}
	return res, nil
}
