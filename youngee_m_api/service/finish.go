package service

import (
	"context"
	"github.com/sirupsen/logrus"
	"youngee_m_api/db"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
)

var Finish *finish

type finish struct {
}

// GetFinishData FinishOpinion 在上传脚本表上提交修改意见
func (*finish) GetFinishData(ctx context.Context, request http_model.GetFinishDataRequest) (*http_model.GetFinishData, error) {
	finishRecruitStrategyList, err := db.GetFinishData(ctx, request.ProjectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Finish service] call CreateFinish error,err:%+v", err)
		return nil, err
	}
	defaultMap := map[int64]int64{}
	for _, recruitStrategy := range finishRecruitStrategyList {
		var TerminateNumber int64
		tx := db.GetReadDB(ctx).Debug().Model(gorm_model.YoungeeTaskInfo{}).
			Where("project_id = ? and strategy_id = ? and task_status = 2", recruitStrategy.ProjectID, int(recruitStrategy.StrategyID))
		err = tx.Where("complete_status = 4").Count(&TerminateNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Finish service] error query mysql total, err:%+v", err)
			return nil, err
		}
		defaultMap[recruitStrategy.StrategyID] = TerminateNumber
	}

	res := new(http_model.GetFinishData)
	res.FinishRecruitStrategy = pack.MGormRecruitStrategyListToHttpGetFinishDataInfoList(finishRecruitStrategyList, defaultMap)

	return res, nil
}
