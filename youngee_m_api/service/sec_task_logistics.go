package service

import (
	"context"
	"github.com/sirupsen/logrus"
	"time"
	"youngee_m_api/db"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

var SecLogistics *secLogistics

type secLogistics struct {
}

func (*secLogistics) Create(ctx context.Context, request http_model.CreateSecTaskLogisticsRequest) (*http_model.CreateSecTaskLogisticsData, error) {
	ThingsType := request.ThingsType
	newLogistics := gorm_model.YoungeeTaskLogistics{
		TaskID:                request.TaskID,
		ThingsType:            int64(ThingsType),
		ExplorestoreStarttime: time.Now(),
		ExplorestoreEndtime:   time.Now(),
		DeliveryTime:          time.Now(),
	}
	//实物
	if ThingsType == 1 {
		newLogistics.CompanyName = request.CompanyName
		newLogistics.LogisticsNumber = request.LogisticsNumber
		newLogistics.DeliveryTime = time.Now()
	} else if ThingsType == 3 {
		ExplorestoreStarttime, _ := time.ParseInLocation("2006-01-02 15:04:05", request.ExplorestoreStarttime, time.Local)
		ExplorestoreEndtime, _ := time.ParseInLocation("2006-01-02 15:04:05", request.ExplorestoreEndtime, time.Local)
		newLogistics.ExplorestoreStarttime = ExplorestoreStarttime
		newLogistics.ExplorestoreEndtime = ExplorestoreEndtime
	}

	_, err := db.CreateSecTaskLogistics(ctx, newLogistics)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[newLogistics service] call CreatenewLogistics error,err:%+v", err)
		return nil, err
	}

	// 修改task_info中发货时间、任务阶段
	updatdSecTask := gorm_model.YounggeeSecTaskInfo{
		TaskID:          request.TaskID,
		LogisticsStatus: 2,
		TaskStage:       7,
		DeliveryDate:    time.Now(),
	}
	_, err = db.UpdateSecTask(ctx, updatdSecTask)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[sectask logistics service] call UpdateSecTask error,err:%+v", err)
		return nil, err
	}

	// 记录任务日志-发货
	err = db.CreateTaskLog(ctx, newLogistics.TaskID, "发货时间")
	if err != nil {
		logrus.WithContext(ctx).Errorf("[newLogistics service] call CreateTaskLog error,err:%+v", err)
		return nil, err
	}

	err = db.CreateMessageByTaskId(ctx, 8, 2, newLogistics.TaskID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[newLogistics service] call CreateMessageByTaskId error,err:%+v", err)
		return nil, err
	}

	data := http_model.CreateSecTaskLogisticsData{}

	return &data, nil
}

func (*secLogistics) Update(ctx context.Context, request http_model.UpdateSecTaskLogisticsRequest) (*http_model.UpdateSecTaskLogisticsData, error) {
	ThingsType := request.ThingsType
	newLogistics := gorm_model.YoungeeTaskLogistics{
		LogisticsID:           request.LogisticsId,
		TaskID:                request.TaskID,
		ThingsType:            int64(ThingsType),
		ExplorestoreStarttime: time.Now(),
		ExplorestoreEndtime:   time.Now(),
		DeliveryTime:          time.Now(),
	}
	//实物
	if ThingsType == 1 {
		newLogistics.CompanyName = request.CompanyName
		newLogistics.LogisticsNumber = request.LogisticsNumber
		newLogistics.DeliveryTime = time.Now()
	} else if ThingsType == 3 {
		ExplorestoreStarttime, _ := time.ParseInLocation("2006-01-02 15:04:05", request.ExplorestoreStarttime, time.Local)
		ExplorestoreEndtime, _ := time.ParseInLocation("2006-01-02 15:04:05", request.ExplorestoreEndtime, time.Local)
		newLogistics.ExplorestoreStarttime = ExplorestoreStarttime
		newLogistics.ExplorestoreEndtime = ExplorestoreEndtime
	}

	_, err := db.UpdateSecTaskLogistics(ctx, newLogistics)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[newLogistics service] call UpdatenewLogistics error,err:%+v", err)
		return nil, err
	}

	data := http_model.UpdateSecTaskLogisticsData{}

	return &data, nil
}
