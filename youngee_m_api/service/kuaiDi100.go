package service

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

type KdState struct {
	Message string `json:"message"`
	State   string `json:"state"`
	Status  string `json:"status"`
	IsCheck string `json:"ischeck"`
}

var KD100Flags = map[string]string{
	"ane66":          "安能快递",
	"debangwuliu":    "德邦物流",
	"debangkuaidi":   "德邦快递",
	"ems":            "EMS",
	"guotongkuaidi":  "国通快递",
	"huitongkuaidi":  "百世快递",
	"jd":             "京东物流",
	"kuayue":         "跨越速运",
	"pjbest":         "品骏快递",
	"shentong":       "申通快递",
	"shunfeng":       "顺丰速运",
	"suer":           "速尔快递",
	"xinfengwuliu":   "信丰物流",
	"youshuwuliu":    "优速物流",
	"youzhengguonei": "邮政快递包裹",
	"yuantong":       "圆通速递",
	"yuantongguoji":  "圆通国际",
	"yunda":          "韵达快递",
	"zhaijisong":     "宅急送",
	"zhongtong":      "中通快递",
	"ewe":            "EWE全球快递",
	"quanyikuaidi":   "全一快递",
	"tiantian":       "天天快递",
	"sxjdfreight":    "顺心捷达",
	"dhl":            "DHL-中国件",
	"tnt":            "TNT",
	"other":          "其它快递",
}

// GetKDStatus 获取快递跟踪信息
func GetKDStatus(com, num string) string {
	fmt.Printf("查询物流公司为 %s, 快递为编号为 %v 的快递\n", KD100Flags[com], num)
	key := "GsCwDkmq1520"                          //客户授权key
	customer := "F2E05F275D02E5344011B3DCD6FEB80D" //查询公司编号

	postUrl := "https://poll.kuaidi100.com/poll/query.do" //实时查询请求地址

	paramData := make(map[string]string)
	paramData["com"] = com //快递公司编码
	paramData["num"] = num //快递单号

	paramDataSlice, _ := json.Marshal(paramData)
	paramjson := string(paramDataSlice)

	sign := strings.ToUpper(GetMD5Encode(paramjson + key + customer))

	// POST请求需要三个参数，分别为customer(CustomerId)和sign(签名)和param(参数)
	fmt.Println("快递 sign:", sign)
	postRes, postErr := http.PostForm(postUrl, url.Values{"customer": {customer}, "sign": {sign}, "param": {paramjson}})
	if postErr != nil {
		fmt.Println("查询失败" + postErr.Error())
		return "查询失败"
	}
	postBody, err := ioutil.ReadAll(postRes.Body)
	if err != nil {
		fmt.Println("查询失败,请至快递公司官网自行查询" + err.Error())
		return "查询失败,请至快递公司官网自行查询"
	}
	resp := KdState{}
	err = json.Unmarshal(postBody, &resp)
	if err != nil {
		fmt.Println("json.Unmarshal error", err.Error())
		return "查询失败"
	}
	if resp.IsCheck == "1" {
		fmt.Println("查询成功，快递已签收")
	} else {
		fmt.Println("查询成功，快递尚未签收")
	}
	return resp.IsCheck
}

// GetMD5Encode 返回一个32位md5加密后的字符串
func GetMD5Encode(data string) string {
	h := md5.New()
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}
