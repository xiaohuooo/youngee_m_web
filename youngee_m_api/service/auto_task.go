package service

import (
	"context"
	"log"
	"time"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/gorm_model"

	"github.com/caixw/lib.go/conv"
	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
)

func AutoTask() error {
	// 新建一个定时任务对象
	// 根据cron表达式进行时间调度，cron可以精确到秒，大部分表达式格式也是从秒开始。
	// crontab := cron.New()  默认从分开始进行时间调度
	crontab := cron.New(cron.WithSeconds()) //精确到秒
	// 定义定时器调用的任务函数
	taskKDStatus := func() {
		logisticNums := db.GetLogisticsNum()
		for i := 0; i < len(logisticNums); i++ {
			logisticNum := logisticNums[i]
			status := GetKDStatus(consts.GetKD(logisticNum[0]), logisticNum[1])
			if status == "1" {
				taskId := db.SignLogistic(conv.MustInt64(logisticNum[2], 0))
				// 记录任务日志
				err := db.CreateTaskLog(context.Background(), taskId, "签收时间")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[logistics service] call CreateTaskLog error,err:%+v", err)
					return
				}
			}
		}
	}
	//	定时任务1 ，线下探店打卡的自动签收操作
	task1 := func() {
		DB := db.GetReadDB(context.Background())
		autoTaskTime := gorm_model.InfoAutoTask{}
		DB.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskTime)
		if autoTaskTime.SignInOffline != 0 {
			err := db.GetSignInOfflineTask(3)
			if err != nil {
				logrus.Error("[GetSignInOfflineTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetSignInOfflineTask is running ,Time :", time.Now())
	}
	// 定时任务2 ，虚拟产品测评的自动签收操作
	task2 := func() {
		DB := db.GetReadDB(context.Background())
		autoTaskTime := gorm_model.InfoAutoTask{}
		DB.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskTime)
		if autoTaskTime.SignInVirtual != 0 {
			err := db.GetSignInOfflineTask(2)
			if err != nil {
				logrus.Error("[GetSignInVirtualTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetSignInVirtualTask is running ,Time :", time.Now())
	}
	// 定时任务3 ，视频形式的审稿处理
	task3 := func() {
		DB := db.GetReadDB(context.Background())
		autoTaskTime := gorm_model.InfoAutoTask{}
		DB.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskTime)
		if autoTaskTime.ReviewInMv != 0 {
			err := db.GetAutoReviewTask(2)
			if err != nil {
				logrus.Error("[GetAutoReviewTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoReviewTask is running ,Time :", time.Now())
	}
	// 定时任务4 ，不限形式的审稿处理
	task4 := func() {
		DB := db.GetReadDB(context.Background())
		autoTaskTime := gorm_model.InfoAutoTask{}
		DB.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskTime)
		if autoTaskTime.ReviewUnlimited != 0 {
			err := db.GetAutoReviewTask(0)
			if err != nil {
				logrus.Error("[GetAutoReviewUnlimitedTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoReviewUnlimitedTask is running ,Time :", time.Now())
	}
	// 定时任务5 ，发布审核自动处理
	task5 := func() {
		DB := db.GetReadDB(context.Background())
		autoTaskTime := gorm_model.InfoAutoTask{}
		DB.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskTime)
		if autoTaskTime.Postreview != 0 {
			err := db.GetAutoPostReviewTask()
			if err != nil {
				logrus.Error("[GetAutoPostReviewTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoPostReviewTask is running ,Time :", time.Now())
	}
	// 定时任务6 ，结案自动处理
	task6 := func() {
		DB := db.GetReadDB(context.Background())
		autoTaskTime := gorm_model.InfoAutoTask{}
		DB.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskTime)
		if autoTaskTime.CaseClose != 0 {
			err := db.GetAutoCaseCloseTask()
			if err != nil {
				logrus.Error("[GetAutoCaseCloseTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoCaseCloseTask is running ,Time :", time.Now())
	}
	// 定时任务7  失效自动处理
	task7 := func() {
		DB := db.GetReadDB(context.Background())
		autoTaskTime := gorm_model.InfoAutoTask{}
		DB.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskTime)
		if autoTaskTime.Invalid != 0 {
			err := db.GetAutoInvalidTask()
			if err != nil {
				logrus.Error("[GetAutoInvalidTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoInvalidTask is running ,Time :", time.Now())
	}
	// 定时任务8.图文形式的初稿违约自动处理
	task8 := func() {
		DB := db.GetReadDB(context.Background())
		autoTaskTime := gorm_model.InfoAutoTask{}
		DB.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskTime)
		if autoTaskTime.DraftDefaultInPic != 0 {
			err := db.GetAutoDraftDefaultInPicTask()
			if err != nil {
				logrus.Error("[GetAutoDraftDefaultInPicTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoDraftDefaultInPicTask is running ,Time :", time.Now())
	}
	// 定时任务9.视频形式的初稿违约自动处理
	task9 := func() {
		DB := db.GetReadDB(context.Background())
		var DraftDefaultInMvTime int32
		DB.Select("draft_default_in_mv").Model(gorm_model.InfoAutoTask{}).Last(&DraftDefaultInMvTime)
		if DraftDefaultInMvTime != 0 {
			err := db.GetAutoDraftDefaultInMvTask()
			if err != nil {
				logrus.Error("[GetAutoDraftDefaultInMvTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoDraftDefaultInMvTask is running ,Time :", time.Now())
	}
	// 定时任务10.视频形式的脚本违约自动处理
	task10 := func() {
		DB := db.GetReadDB(context.Background())
		var scriptDefault int32
		DB.Select("script_default").Model(gorm_model.InfoAutoTask{}).Last(&scriptDefault)
		if scriptDefault != 0 {
			err := db.GetAutoScriptDefaultTask()
			if err != nil {
				logrus.Error("[GetAutoScriptDefaultTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoScriptDefaultTask is running ,Time :", time.Now())
	}
	// 定时任务11.链接违约自动处理
	task11 := func() {
		DB := db.GetReadDB(context.Background())
		var LinkBreach int32
		DB.Select("link_breach").Model(gorm_model.InfoAutoTask{}).Last(&LinkBreach)
		if LinkBreach != 0 {
			err := db.GetAutoLinkBreachTask()
			if err != nil {
				logrus.Error("[GetAutoLinkBreachTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoLinkBreachTask is running ,Time :", time.Now())
	}
	// 定时任务12.数据违约自动处理
	task12 := func() {
		DB := db.GetReadDB(context.Background())
		var LinkBreach int32
		DB.Select("case_close_default").Model(gorm_model.InfoAutoTask{}).Last(&LinkBreach)
		if LinkBreach != 0 {
			err := db.GetAutoCaseCloseDefaultTask()
			if err != nil {
				logrus.Error("[GetAutoCaseCloseDefaultTask] in DB error %+v", err)
				return
			}
		}
		log.Println("GetAutoCaseCloseDefaultTask is running ,Time :", time.Now())
	}
	//定时任务
	//spec := "*/30 * * * * ?" //cron表达式，每30秒一次
	//spec := "0 */1 * * * ?" //cron表达式，每一分钟执行一次
	spec := "0 0 * * * *" //每小时执行
	// 添加定时任务,
	_, err := crontab.AddFunc("0 0 * * * *", taskKDStatus) //每小时执行一次快递是否签收的查询
	//_, err := crontab.AddFunc("*/30 * * * * ?", taskKDStatus)
	if err != nil {
		return err
	}
	_, err1 := crontab.AddFunc(spec, task1)
	if err1 != nil {
		return err1
	}
	_, err2 := crontab.AddFunc(spec, task2)
	if err2 != nil {
		return err2
	}
	_, err3 := crontab.AddFunc(spec, task3)
	if err3 != nil {
		return err3
	}
	_, err4 := crontab.AddFunc(spec, task4)
	if err4 != nil {
		return err4
	}
	_, err5 := crontab.AddFunc(spec, task5)
	if err5 != nil {
		return err5
	}
	_, err6 := crontab.AddFunc(spec, task6)
	if err6 != nil {
		return err6
	}
	_, err7 := crontab.AddFunc(spec, task7)
	if err7 != nil {
		return err7
	}
	_, err8 := crontab.AddFunc(spec, task8)
	if err8 != nil {
		return err8
	}
	_, err9 := crontab.AddFunc(spec, task9)
	if err9 != nil {
		return err9
	}
	_, err10 := crontab.AddFunc(spec, task10)
	if err10 != nil {
		return err10
	}
	_, err11 := crontab.AddFunc(spec, task11)
	if err11 != nil {
		return err11
	}
	_, err12 := crontab.AddFunc(spec, task12)
	if err12 != nil {
		return err12
	}
	// 启动定时器
	crontab.Start()
	// 定时任务是另起协程执行的,这里使用 select 简单阻塞.需要根据实际情况进行控制
	//select {} //阻塞主线程停止
	return nil
}
