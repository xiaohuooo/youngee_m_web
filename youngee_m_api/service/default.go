package service

import (
	"github.com/caixw/lib.go/conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/db"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
)

var Default *defaults

type defaults struct {
}

func (*defaults) GetTaskDefaultReviewList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskDefaultReviewListData, error) {
	TaskDefaults, total, err := db.GetTaskDefaultReviewList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskDefaultList error,err:%+v", err)
		return nil, err
	}
	TaskDefaultListDefault := new(http_model.TaskDefaultReviewListData)
	TaskDefaultListDefault.TaskDefaultPreview = pack.MGormTaskDefaultReviewInfoListToHttpTaskDefaultReviewPreviewList(TaskDefaults)
	TaskDefaultListDefault.Total = conv.MustString(total, "")
	return TaskDefaultListDefault, nil
}

func (*defaults) GetTaskDefaultDataList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskDefaultDataListData, error) {
	TaskDefaults, total, err := db.GetTaskDefaultDataList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskDefaultList error,err:%+v", err)
		return nil, err
	}
	TaskDefaultListDefault := new(http_model.TaskDefaultDataListData)
	TaskDefaultListDefault.TaskDefaultPreview = pack.MGormTaskDefaultDataInfoListToHttpTaskDefaultDataPreviewList(TaskDefaults)
	TaskDefaultListDefault.Total = conv.MustString(total, "")
	return TaskDefaultListDefault, nil
}

func (*defaults) GetTaskTerminatingList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskTerminatingListData, error) {
	TaskDefaults, total, err := db.GetTaskTerminatingList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskDefaultList error,err:%+v", err)
		return nil, err
	}
	TaskDefaultListDefault := new(http_model.TaskTerminatingListData)
	TaskDefaultListDefault.TaskDefaultPreview = pack.MGormTaskTerminatingInfoListToHttpTaskTerminatingPreviewList(TaskDefaults)
	TaskDefaultListDefault.Total = conv.MustString(total, "")
	return TaskDefaultListDefault, nil
}

func (*defaults) GetTaskTerminatedList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskTerminatedListData, error) {
	TaskDefaults, total, err := db.GetTaskTerminatedList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskDefaultList error,err:%+v", err)
		return nil, err
	}
	TaskDefaultListDefault := new(http_model.TaskTerminatedListData)
	TaskDefaultListDefault.TaskDefaultPreview = pack.MGormTaskTerminatedInfoListToHttpTaskTerminatedPreviewList(TaskDefaults)
	TaskDefaultListDefault.Total = conv.MustString(total, "")
	return TaskDefaultListDefault, nil
}
