package service

import (
	"context"
	"strings"
	"youngee_m_api/db"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"

	"github.com/sirupsen/logrus"
)

var Data *data

type data struct {
}

// DataOpinion 在上传脚本表上提交修改意见
func (*data) DataOpinion(ctx context.Context, request http_model.DataOpinionRequest) (*http_model.DataOpinionData, error) {
	Data := gorm_model.YounggeeDataInfo{
		TaskID:        request.TaskID,
		ReviseOpinion: request.DataOpinion,
	}
	err := db.DataOpinion(ctx, Data.TaskID, Data.ReviseOpinion)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}
	// 记录任务日志
	err = db.CreateTaskLog(ctx, Data.TaskID, "数据驳回")
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Link service] call CreateTaskLog error,err:%+v", err)
		return nil, err
	}
	err = db.CreateMessageByTaskId(ctx, 19, 3, Data.TaskID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateMessageByTaskId error,err:%+v", err)
		return nil, err
	}

	res := &http_model.DataOpinionData{
		TaskID: Data.TaskID,
	}
	return res, nil
}

// 任务结案service
func (*data) AcceptData(ctx context.Context, request http_model.AcceptDataRequest) (*http_model.AcceptDataData, error) {
	var TaskIDList []string
	TaskIDs := strings.Split(request.TaskIds, ",")
	for _, taskId := range TaskIDs {
		TaskIDList = append(TaskIDList, taskId)
	}

	// 1. 更新YounggeeDataInfo表，通过数据
	//fmt.Printf("acc request %+v", TaskIDList)
	err := db.AcceptData(ctx, TaskIDList)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call AcceptData error,err:%+v", err)
		return nil, err
	}

	// 记录任务日志
	for _, taskId := range TaskIDList {
		err = db.CreateTaskLog(ctx, taskId, "数据通过")
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Data service] call CreateTaskLog error,err:%+v", err)
			return nil, err
		}
		err = db.CreateMessageByTaskId(ctx, 5, 1, taskId)
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Script service] call CreateTaskLog error,err:%+v", err)
			return nil, err
		}
	}

	res := &http_model.AcceptDataData{
		TaskIds: TaskIDList,
		IsEnd:   0,
	}
	if request.IsSpecial == 1 {
		// 2. 更新YoungeeTaskInfo表，将任务结案
		err = db.SetSpecialTaskFinish(ctx, TaskIDList)
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Data service] call SetTaskFinish error,err:%+v", err)
			return nil, err
		}
		// 3. 判断是否全部任务已结案，若已全部结案则触发项目结案
		// 查询task_stage<15的任务数量
		unFinishedTaskNumber, err1 := db.GetUnfinishedTaskNumber(ctx, request.ProjectId)
		if err1 != nil {
			logrus.WithContext(ctx).Errorf("[Data service] call GetUnfinishedTaskNumber error,err:%+v", err)
			return nil, err
		}
		if *unFinishedTaskNumber == 0 { // 若为0则触发项目结案
			err := db.SetSpecialProjectFinish(ctx, request.ProjectId)
			if err1 != nil {
				logrus.WithContext(ctx).Errorf("[Data service] call SetProjectFinish error,err:%+v", err)
				return nil, err
			}
			res.IsEnd = 1
		}
	} else {
		// 2. 更新YoungeeTaskInfo表，将任务结案
		//fmt.Printf("acc request %+v", TaskIDList)
		err = db.SetTaskFinish(ctx, TaskIDList)
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Data service] call SetTaskFinish error,err:%+v", err)
			return nil, err
		}

		err = db.SetTalentIncome(ctx, TaskIDList)
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Data service] call SetTalentIncome error,err:%+v", err)
			return nil, err
		}

		// 3. 判断是否全部任务已结案，若已全部结案则触发项目结案
		// 查询task_stage<15的任务数量
		unFinishedTaskNumber, err1 := db.GetUnfinishedTaskNumber(ctx, request.ProjectId)
		if err1 != nil {
			logrus.WithContext(ctx).Errorf("[Data service] call GetUnfinishedTaskNumber error,err:%+v", err)
			return nil, err
		}
		if *unFinishedTaskNumber == 0 { // 若为0则触发项目结案
			err := db.SetProjectFinish(ctx, request.ProjectId)
			if err1 != nil {
				logrus.WithContext(ctx).Errorf("[Data service] call SetProjectFinish error,err:%+v", err)
				return nil, err
			}
			res.IsEnd = 1
		}
	}

	return res, nil
}
