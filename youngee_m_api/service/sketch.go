package service

import (
	"context"
	"strings"
	"youngee_m_api/db"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"

	"github.com/caixw/lib.go/conv"

	"github.com/sirupsen/logrus"
)

var Sketch *sketch

type sketch struct {
}

// SketchOption ReviseOption 在上传初稿表上提交修改意见
func (*sketch) SketchOption(ctx context.Context, request http_model.SketchOpinionRequest) (*http_model.SketchOpinionData, error) {
	Sketch := gorm_model.YounggeeSketchInfo{
		TaskID:        request.TaskID,
		ReviseOpinion: request.SketchOpinion,
	}
	err := db.SketchOption(ctx, Sketch.TaskID, Sketch.ReviseOpinion)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Sketch service] call CreateSketch error,err:%+v", err)
		return nil, err
	}

	// 记录任务日志
	err = db.CreateTaskLog(ctx, Sketch.TaskID, "初稿驳回")
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Sketch service] call CreateTaskLog error,err:%+v", err)
		return nil, err
	}
	err = db.CreateMessageByTaskId(ctx, 17, 3, Sketch.TaskID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Sketch service] call CreateMessageByTaskId error,err:%+v", err)
		return nil, err
	}
	res := &http_model.SketchOpinionData{
		TaskID: Sketch.TaskID,
	}
	return res, nil
}

// AcceptSketch 同意初稿
func (*sketch) AcceptSketch(ctx context.Context, request http_model.AcceptSketchRequest) (*http_model.AcceptSketchData, error) {
	var TaskIDList []string
	TaskIDs := strings.Split(request.TaskIds, ",")
	for _, taskId := range TaskIDs {
		TaskIDList = append(TaskIDList, taskId)
	}
	//fmt.Printf("acc request %+v", TaskIDList)
	err := db.AcceptSketch(ctx, TaskIDList)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Sketch service] call CreateSketch error,err:%+v", err)
		return nil, err
	}
	// 记录任务日志
	for _, taskId := range TaskIDList {
		err = db.CreateTaskLog(ctx, taskId, "初稿通过")
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Sketch service] call CreateTaskLog error,err:%+v", err)
			return nil, err
		}
		err = db.CreateMessageByTaskId(ctx, 3, 1, taskId)
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Script service] call CreateTaskLog error,err:%+v", err)
			return nil, err
		}
	}

	res := &http_model.AcceptSketchData{
		TaskIds: TaskIDList,
	}
	return res, nil
}

// FindPhoto 查找初稿图片以及视频
func (*sketch) FindPhoto(ctx context.Context, request http_model.FindSketchPhotoRequest) ([]http_model.SketchPhoto, error) {
	var SketchPhotos []http_model.SketchPhoto
	res, err := db.FindPhoto(ctx, request.SketchID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Sketch service] call CreateSketch error,err:%+v", err)
		return nil, err
	}
	for _, photo := range res {
		sketchPhoto := http_model.SketchPhoto{
			PhotoUrl: photo.PhotoUrl,
			PhotoUid: photo.PhotoUid,
			Symbol:   conv.MustInt64(photo.Symbol, 0),
		}
		SketchPhotos = append(SketchPhotos, sketchPhoto)
	}
	return SketchPhotos, nil
}

// GetSketchInfo 获取初稿
func (*sketch) GetSketchInfo(ctx context.Context, request http_model.GetSketchInfoRequest) (*http_model.GetSketchInfoData, error) {
	var SketchPhotos []http_model.SketchPhotoInfo
	SketchInfo, err := db.FindSketchInfo(ctx, request.TaskID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Sketch service] call CreateSketch error,err:%+v", err)
		return nil, err
	}
	res, err := db.FindPhoto(ctx, int64(SketchInfo.SketchID))
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Sketch service] call CreateSketch error,err:%+v", err)
		return nil, err
	}
	for _, photo := range res {
		sketchPhoto := http_model.SketchPhotoInfo{
			PhotoUrl: photo.PhotoUrl,
			PhotoUid: photo.PhotoUid,
			Symbol:   conv.MustInt64(photo.Symbol, 0),
		}
		SketchPhotos = append(SketchPhotos, sketchPhoto)
	}
	SketchInfoData := http_model.GetSketchInfoData{
		Title:        SketchInfo.Title,
		Content:      SketchInfo.Content,
		SketchPhotos: SketchPhotos,
	}
	return &SketchInfoData, nil
}
