package service

import (
	"context"
	"strings"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"

	"github.com/caixw/lib.go/conv"

	"github.com/sirupsen/logrus"
)

var Number *number

type number struct {
}

func (*number) GetReviewNumberInfo(ctx context.Context, request http_model.GetReviewNumberInfoRequest) (*http_model.GetReviewNumberInfoData, error) {
	var StrategyIdList []int64
	StrategyIds := strings.Split(request.StrategyIds, ",")
	for _, strategyId := range StrategyIds {
		StrategyIdList = append(StrategyIdList, conv.MustInt64(strategyId, 0))
	}
	NumberData, err := db.GetReviewNumberInfo(ctx, request.ProjectId, StrategyIdList)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetLinkNumberInfo(ctx context.Context, request http_model.GetLinkNumberInfoRequest) (*http_model.GetLinkNumberInfoData, error) {
	var StrategyIdList []int64
	StrategyIds := strings.Split(request.StrategyIds, ",")
	for _, strategyId := range StrategyIds {
		StrategyIdList = append(StrategyIdList, conv.MustInt64(strategyId, 0))
	}
	NumberData, err := db.GetLinkNumberInfo(ctx, request.ProjectId, StrategyIdList)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetDataNumberInfo(ctx context.Context, request http_model.GetDataNumberInfoRequest) (*http_model.GetDataNumberInfoData, error) {
	var StrategyIdList []int64
	StrategyIds := strings.Split(request.StrategyIds, ",")
	for _, strategyId := range StrategyIds {
		StrategyIdList = append(StrategyIdList, conv.MustInt64(strategyId, 0))
	}
	NumberData, err := db.GetDataNumberInfo(ctx, request.ProjectId, StrategyIdList)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetDefaultNumberInfo(ctx context.Context, request http_model.GetDefaultNumberInfoRequest) (*http_model.GetDefaultNumberInfoData, error) {
	var StrategyIdList []int64
	StrategyIds := strings.Split(request.StrategyIds, ",")
	for _, strategyId := range StrategyIds {
		StrategyIdList = append(StrategyIdList, conv.MustInt64(strategyId, 0))
	}
	NumberData, err := db.GetDefaultNumberInfo(ctx, request.ProjectId, StrategyIdList)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetFinishNumberInfo(ctx context.Context, request http_model.GetFinishNumberInfoRequest) (*http_model.GetFinishNumberInfoData, error) {
	var StrategyIdList []int64
	StrategyIds := strings.Split(request.StrategyIds, ",")
	for _, strategyId := range StrategyIds {
		StrategyIdList = append(StrategyIdList, conv.MustInt64(strategyId, 0))
	}
	NumberData, err := db.GetFinishNumberInfo(ctx, request.ProjectId, StrategyIdList)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetLogisticsNumberInfo(ctx context.Context, request http_model.GetLogisticsNumberInfoRequest) (*http_model.GetLogisticsNumberInfoData, error) {
	var StrategyIdList []int64
	StrategyIds := strings.Split(request.StrategyIds, ",")
	for _, strategyId := range StrategyIds {
		StrategyIdList = append(StrategyIdList, conv.MustInt64(strategyId, 0))
	}
	NumberData, err := db.GetLogisticsNumberInfo(ctx, request.ProjectId, StrategyIdList)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}
	return NumberData, nil
}

func (*number) GetSpecialInviteNumber(ctx context.Context, request http_model.GetSpecialInviteNumberRequest) (*http_model.GetSpecialInviteNumberData, error) {
	projectId := request.ProjectId
	NumberData, err := db.GetSpecialInviteNumber(ctx, projectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call GetSpecialInviteNumber error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetSpecialLogisticNumber(ctx context.Context, request http_model.GetSpecialLogisticNumberRequest) (*http_model.GetSpecialLogisticNumberData, error) {
	projectId := request.ProjectId
	NumberData, err := db.GetSpecialLogisticNumber(ctx, projectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call GetSpecialLogisticNumber error,err:%+v", err)
		return nil, err
	}
	return NumberData, nil
}

func (*number) GetSpecialReviewNumber(ctx context.Context, request http_model.GetSpecialReviewNumberRequest) (*http_model.GetSpecialReviewNumberData, error) {
	projectId := request.ProjectId
	NumberData, err := db.GetSpecialReviewNumber(ctx, projectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetSpecialLinkNumber(ctx context.Context, request http_model.GetSpecialLinkNumberRequest) (*http_model.GetSpecialLinkNumberData, error) {
	projectId := request.ProjectId
	NumberData, err := db.GetSpecialLinkNumber(ctx, projectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetSpecialDataNumber(ctx context.Context, request http_model.GetSpecialDataNumberRequest) (*http_model.GetSpecialDataNumberData, error) {
	projectId := request.ProjectId
	NumberData, err := db.GetSpecialDataNumber(ctx, projectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}

func (*number) GetSpecialFinishData(ctx context.Context, request http_model.GetSpecialFinishDataRequest) (*http_model.GetSpecialFinishDataData, error) {
	projectId := request.ProjectId
	NumberData, err := db.GetSpecialFinishDataNumber(ctx, projectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Data service] call CreateData error,err:%+v", err)
		return nil, err
	}
	return NumberData, nil
}

func (*number) GetSpecialSettleNumber(ctx context.Context, request http_model.GetSpecialSettleNumberRequest) (*http_model.GetSpecialSettleNumberData, error) {
	projectId := request.ProjectId
	NumberData, err := db.GetSpecialSettleNumber(ctx, projectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Number service] call GetSpecialSettleNumber error,err:%+v", err)
		return nil, err
	}

	return NumberData, nil
}
