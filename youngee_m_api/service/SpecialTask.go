package service

import (
	"youngee_m_api/db"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"

	"github.com/caixw/lib.go/conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

var SpecialTask *specialTask

type specialTask struct {
}

func (p *specialTask) GetSpecialTaskInviteList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.SpecialTaskInviteListData, error) {
	TaskInvites, total, err := db.GetSpecialTaskInviteList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskInviteList error,err:%+v", err)
		return nil, err
	}
	TaskInviteListData := new(http_model.SpecialTaskInviteListData)
	TaskInviteListData.SpecialTaskInvitePreview = pack.MGormSpecialTaskInviteInfoListToHttpSpecialTaskInvitePreviewList(TaskInvites)
	TaskInviteListData.Total = conv.MustString(total, "")
	return TaskInviteListData, nil
}

func (p *specialTask) GetSpecialTaskScriptList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.SpecialTaskScriptListData, error) {
	TaskScripts, total, err := db.GetSpecialTaskScriptList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskScriptList error,err:%+v", err)
		return nil, err
	}
	TaskScriptListData := new(http_model.SpecialTaskScriptListData)
	TaskScriptListData.SpecialTaskScriptPreview = pack.MGormSpecialTaskScriptInfoListToHttpSpecialTaskScriptPreviewList(TaskScripts)
	TaskScriptListData.Total = conv.MustString(total, "")
	return TaskScriptListData, nil
}

func (p *specialTask) GetSpecialTaskSketchList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.SpecialTaskSketchListData, error) {
	TaskSketchs, total, err := db.GetSpecialTaskSketchList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskSketchList error,err:%+v", err)
		return nil, err
	}
	TaskSketchListData := new(http_model.SpecialTaskSketchListData)
	TaskSketchListData.SpecialTaskSketchPreview = pack.MGormSpecialTaskSketchInfoListToHttpSpecialTaskSketchPreviewList(TaskSketchs)
	TaskSketchListData.Total = conv.MustString(total, "")
	return TaskSketchListData, nil
}

func (p *specialTask) GetSpecialTaskLinkList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.SpecialTaskLinkListData, error) {
	TaskLinks, total, err := db.GetSpecialTaskLinkList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskLinkList error,err:%+v", err)
		return nil, err
	}
	TaskLinkListData := new(http_model.SpecialTaskLinkListData)
	TaskLinkListData.SpecialTaskLinkPreview = pack.MGormSpecialTaskLinkInfoListToHttpSpecialTaskLinkPreviewList(TaskLinks)
	TaskLinkListData.Total = conv.MustString(total, "")
	return TaskLinkListData, nil
}

func (p *specialTask) GetSpecialTaskDataList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.SpecialTaskDataListData, error) {
	TaskDatas, total, err := db.GetSpecialTaskDataList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskDataList error,err:%+v", err)
		return nil, err
	}
	TaskDataListData := new(http_model.SpecialTaskDataListData)
	TaskDataListData.SpecialTaskDataPreview = pack.MGormSpecialTaskDataInfoListToHttpSpecialTaskDataPreviewList(TaskDatas)
	TaskDataListData.Total = conv.MustString(total, "")
	return TaskDataListData, nil
}

func (p *specialTask) GetSpecialTaskFinishDataList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.SpecialTaskFinishDataListData, error) {
	TaskFinishDatas, total, err := db.GetSpecialTaskFinishList(ctx, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskFinishList error,err:%+v", err)
		return nil, err
	}
	TaskFinishListData := new(http_model.SpecialTaskFinishDataListData)
	TaskFinishListData.SpecialTaskFinishDataPreview = pack.MGormSpecialTaskFinishDataInfoListToHttpSpecialTaskFinishDataPreviewList(TaskFinishDatas)
	TaskFinishListData.Total = conv.MustString(total, "")
	return TaskFinishListData, nil
}

func (p *specialTask) GetSpecialTaskSettleList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.SpecialTaskSettleListData, error) {
	TaskSettles, total, err := db.GetSpecialTaskSettleList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskSettleList error,err:%+v", err)
		return nil, err
	}
	TaskSettleListData := new(http_model.SpecialTaskSettleListData)
	TaskSettleListData.SpecialTaskSettlePreview = pack.MGormSpecialTaskSettleInfoListToHttpSpecialTaskSettlePreviewList(TaskSettles)
	TaskSettleListData.Total = conv.MustString(total, "")
	return TaskSettleListData, nil
}
