package service

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	"strings"
	"time"
	"youngee_m_api/db"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

var Project *project

type project struct {
}

func (*project) GetFullProjectList(ctx context.Context, pageSize, pageNum int32, condition *common_model.ProjectCondition, projectType string) (*http_model.FullProjectListData, error) {
	fullProjects, total, err := db.GetFullProjectList(ctx, pageSize, pageNum, condition, projectType)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetFullProjectList error,err:%+v", err)
		return nil, err
	}
	fullProjectListData := new(http_model.FullProjectListData)
	fullProjectListData.FullProjectPreview = pack.MGormFullProjectToHttpFullProjectPreview(fullProjects)
	fullProjectListData.Total = conv.MustString(total, "")
	return fullProjectListData, nil
}

func (*project) GetProjectDetail(ctx context.Context, projectID string) (*http_model.ShowProjectData, error) {
	project, err := db.GetProjectDetail(ctx, projectID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetPorjectDetail error,err:%+v", err)
		return nil, err
	}
	enterprise, err := db.GetEnterpriseByEnterpriseID(ctx, project.EnterpriseID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetEnterpriseByEnterpriseID error,err:%+v", err)
		return nil, err
	}
	user, err := db.GetUserByID(ctx, enterprise.UserID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetUserByID error,err:%+v", err)
		return nil, err
	}
	ProjectDetail := http_model.ShowProjectData{
		ProjectID:        conv.MustString(project.ProjectID, ""),
		ProjectName:      conv.MustString(project.ProjectName, ""),
		ProjectStatus:    conv.MustString(project.ProjectStatus, ""),
		ProjectType:      conv.MustString(project.ProjectType, ""),
		ProjectPlatform:  conv.MustString(project.ProjectPlatform, ""),
		ProjectForm:      conv.MustString(project.ProjectForm, ""),
		TalentType:       conv.MustString(project.TalentType, ""),
		RecruitDdl:       util.GetTimePointer(project.RecruitDdl),
		ContentType:      conv.MustString(project.ContentType, ""),
		ProjectDetail:    conv.MustString(project.ProjectDetail, ""),
		ProductID:        conv.MustString(project.ProductID, ""),
		EnterpriseID:     conv.MustString(project.EnterpriseID, ""),
		Balance:          conv.MustString(enterprise.Balance, ""),
		AvailableBalance: conv.MustString(enterprise.AvailableBalance, ""),
		EstimatedCost:    conv.MustString(project.EstimatedCost, ""),
		FailReason:       conv.MustString(project.FailReason, ""),
		CreateAt:         util.GetTimePointer(project.CreatedAt),
		UpdateAt:         util.GetTimePointer(project.UpdatedAt),
		Phone:            user.Phone,
		FinishAt:         util.GetTimePointer(project.FinishAt),
		PassAt:           util.GetTimePointer(project.PassAt),
		PayAt:            util.GetTimePointer(project.PayAt),
		ProductInfo:      conv.MustString(project.ProductSnap, ""),
		ProductPhotoInfo: conv.MustString(project.ProductPhotoSnap, ""),
		AutoFailAt:       util.GetTimePointer(project.AutoFailAt),
		SubmitAt:         util.GetTimePointer(project.SubmitAt),
	}
	Strategys, err := db.GetRecruitStrategys(ctx, projectID)
	if err != nil {
		logrus.WithContext(ctx).Error()
		return nil, err
	}
	for _, strategy := range Strategys {
		RecruitStrategy := http_model.ShowRecruitStrategy{
			RecruitStrategyID: conv.MustString(strategy.RecruitStrategyID, ""),
			FeeForm:           conv.MustString(strategy.FeeForm, ""),
			StrategyID:        conv.MustString(strategy.StrategyID, ""),
			FollowersLow:      conv.MustString(strategy.FollowersLow, ""),
			FollowersUp:       conv.MustString(strategy.FollowersUp, ""),
			RecruitNumber:     conv.MustString(strategy.RecruitNumber, ""),
			Offer:             conv.MustString(strategy.Offer, ""),
			ServiceCharge:     conv.MustString(strategy.ServiceCharge, ""),
			SelectedNumber:    strategy.SelectedNumber,
			WaitingNumber:     strategy.WaitingNumber,
			DeliveredNumber:   strategy.DeliveredNumber,
			SignedNumber:      strategy.SignedNumber,
		}
		ProjectDetail.RecruitStrategys = append(ProjectDetail.RecruitStrategys, RecruitStrategy)
	}
	Photos, err := db.GetProjectPhoto(ctx, projectID)
	if err != nil {
		logrus.WithContext(ctx).Error()
		return nil, err
	}
	for _, Photo := range Photos {
		ProjectPhoto := http_model.ShowProjectPhoto{
			PhotoUrl: Photo.PhotoUrl,
			PhotoUid: Photo.PhotoUid,
			FileName: Photo.FileName,
		}
		ProjectDetail.ProjectPhotos = append(ProjectDetail.ProjectPhotos, ProjectPhoto)
	}
	return &ProjectDetail, nil
}

func (*project) ApproveProject(ctx *gin.Context, data http_model.ApproveProjectRequest) (error, string) {
	//fmt.Println("data.IsApprove：", data.IsApprove)
	err, message := db.ApproveProject(ctx, data.ProjectId, data.IsApprove)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call ChangeTaskStatus error,err:%+v", err)
		return err, ""
	}
	return nil, message
}

func (*project) GetAllProject(ctx context.Context, pageSize, pageNum int32) (*http_model.GetAllProjectData, error) {
	allProjectPreviews, total, err := db.GetAllProject(ctx, pageSize, pageNum)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetAllProject error,err:%+v", err)
		return nil, err
	}
	allProjects := new(http_model.GetAllProjectData)
	allProjects.AllProjectPreview = pack.MGormAllProjectToHttpAllProjectPreview(allProjectPreviews)
	allProjects.Total = conv.MustString(total, "")
	return allProjects, nil
}

func (*project) GetProjectTaskList(ctx context.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TaskConditions) (*http_model.ProjectTaskListData, error) {
	projectTasks, total, err := db.GetProjectTaskList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetProjectTaskList error,err:%+v", err)
		return nil, err
	}
	projectTaskListData := new(http_model.ProjectTaskListData)
	projectTaskListData.ProjectTaskPreview = pack.MGormProjectTaskToHttpProjectTaskPreview(projectTasks)
	projectTaskListData.Total = conv.MustString(total, "")
	return projectTaskListData, nil
}

func (*project) Create(ctx context.Context, newProject http_model.CreateProjectRequest, enterpriseID string) (*http_model.CreateProjectData, error) {
	fmt.Printf("newProject:%+v\n", newProject)
	fmt.Println("newProject.RecruitDdl:", newProject.RecruitDdl)
	RecruitDdl := time.Time{} //赋零值
	if newProject.RecruitDdl != "" {
		RecruitDdl, _ = time.ParseInLocation("2006-01-02 15:04:05", newProject.RecruitDdl, time.Local)
	}
	// 查询关联商品信息
	product, err := db.GetProductByID(ctx, newProject.ProductID)
	if err != nil {
		return nil, err
	}
	fmt.Println("product:", product)
	productPhotos, err := db.GetProductPhotoByProductID(ctx, newProject.ProductID)
	productInfoToJson, _ := json.Marshal(product)
	productPhotosToJson, _ := json.Marshal(productPhotos)
	// 按照品牌名-商品名对项目进行命名
	AutoTaskID, err := db.GetLastAutoTaskID()
	if err != nil {
		return nil, err
	}
	AutoDefaultID, err := db.GetLastAutoDefaultID()
	if err != nil {
		return nil, err
	}
	projectName := product.BrandName + "-" + product.ProductName
	var feeFrom []string
	for _, strategy := range newProject.RecruitStrategys {
		feeFrom = append(feeFrom, strconv.FormatInt(strategy.FeeForm, 10))
	}
	var ECost float64 = 0
	if newProject.ProjectType == int64(1) {
		for _, strategy := range newProject.RecruitStrategys {
			// 计算预估成本
			var tmpCost float64 = 0
			if strategy.FeeForm == 1 {
				tmpCost = strategy.ServiceCharge * float64(strategy.RecruitNumber)
			} else if strategy.FeeForm == 2 {
				tmpCost = strategy.Offer * float64(strategy.RecruitNumber)
			}
			ECost += tmpCost
		}
	}
	feeForms := strings.Join(feeFrom, ",")
	projectInfo := gorm_model.ProjectInfo{}
	rand.Seed(time.Now().UnixNano())
	td := conv.MustString(time.Now().Day(), "")
	for {
		if len(td) == 3 {
			break
		}
		td = "0" + td
	}
	fmt.Printf("RecruitDdl:%+v\n", RecruitDdl)
	if newProject.ProjectType == int64(1) {
		if newProject.RecruitDdl == "" {
			projectInfo = gorm_model.ProjectInfo{
				ProjectID:        conv.MustString(time.Now().Year(), "")[2:] + td + conv.MustString(rand.Intn(100000-10000)+10000, ""),
				ProjectName:      projectName,
				ProjectStatus:    1,
				ProjectType:      newProject.ProjectType,
				TalentType:       newProject.TalentType,
				ProjectPlatform:  newProject.ProjectPlatform,
				ProjectForm:      newProject.ProjectForm,
				ProjectDetail:    newProject.ProjectDetail,
				ContentType:      newProject.ContentType,
				EnterpriseID:     enterpriseID,
				ProductID:        newProject.ProductID,
				FeeForm:          feeForms,
				AutoTaskID:       conv.MustInt64(AutoTaskID, 0),
				AutoDefaultID:    conv.MustInt64(AutoDefaultID, 0),
				EstimatedCost:    ECost,
				IsRead:           0,
				ProductSnap:      string(productInfoToJson),
				ProductPhotoSnap: string(productPhotosToJson),
			}
		} else {
			projectInfo = gorm_model.ProjectInfo{
				ProjectID:        conv.MustString(time.Now().Year(), "")[2:] + td + conv.MustString(rand.Intn(100000-10000)+10000, ""),
				ProjectName:      projectName,
				ProjectStatus:    1,
				ProjectType:      newProject.ProjectType,
				TalentType:       newProject.TalentType,
				ProjectPlatform:  newProject.ProjectPlatform,
				ProjectForm:      newProject.ProjectForm,
				RecruitDdl:       &RecruitDdl,
				ProjectDetail:    newProject.ProjectDetail,
				ContentType:      newProject.ContentType,
				EnterpriseID:     enterpriseID,
				ProductID:        newProject.ProductID,
				FeeForm:          feeForms,
				AutoTaskID:       conv.MustInt64(AutoTaskID, 0),
				AutoDefaultID:    conv.MustInt64(AutoDefaultID, 0),
				EstimatedCost:    ECost,
				IsRead:           0,
				ProductSnap:      string(productInfoToJson),
				ProductPhotoSnap: string(productPhotosToJson),
			}
		}
	} else {
		projectInfo = gorm_model.ProjectInfo{
			ProjectID:        conv.MustString(time.Now().Year(), "")[2:] + td + conv.MustString(rand.Intn(100000-10000)+10000, ""),
			ProjectName:      projectName,
			ProjectStatus:    1,
			ProjectType:      newProject.ProjectType,
			TalentType:       "[]",
			ProjectPlatform:  newProject.ProjectPlatform,
			ProjectForm:      newProject.ProjectForm,
			ProjectDetail:    newProject.ProjectDetail,
			ContentType:      newProject.ContentType,
			EnterpriseID:     enterpriseID,
			ProductID:        newProject.ProductID,
			FeeForm:          feeForms,
			AutoTaskID:       conv.MustInt64(AutoTaskID, 0),
			AutoDefaultID:    conv.MustInt64(AutoDefaultID, 0),
			EstimatedCost:    ECost,
			ProductSnap:      string(productInfoToJson),
			ProductPhotoSnap: string(productPhotosToJson),
		}
	}
	// db create ProjectInfo
	projectID, err := db.CreateProject(ctx, projectInfo)
	if err != nil {
		return nil, err
	}
	if len(newProject.ProjectPhotos) != 0 {
		var projectPhotos []gorm_model.ProjectPhoto
		for _, photo := range newProject.ProjectPhotos {
			projectPhoto := gorm_model.ProjectPhoto{
				PhotoUrl:  photo.PhotoUrl,
				PhotoUid:  photo.PhotoUid,
				ProjectID: projectID,
			}
			projectPhotos = append(projectPhotos, projectPhoto)
		}
		// db create ProjectPhoto
		err = db.CreateProjectPhoto(ctx, projectPhotos)
		if err != nil {
			return nil, err
		}
	}
	// build
	if newProject.ProjectType == int64(1) && newProject.RecruitStrategys != nil {
		var recruitStrategys []gorm_model.RecruitStrategy
		for _, strategy := range newProject.RecruitStrategys {
			// 查询对应定价策略
			pricingStrategy, err := db.GetPricingStrategy(ctx, strategy.FollowersLow, strategy.FollowersUp, strategy.FeeForm, newProject.ProjectPlatform)
			if err != nil {
				return nil, err
			}
			// 根据定价策略计算达人所见报价
			if strategy.FeeForm == 2 {
				strategy.TOffer = strategy.Offer * (1 - conv.MustFloat64(pricingStrategy.ServiceRate, 0)/1000)
			}
			recruitStrategy := gorm_model.RecruitStrategy{
				FeeForm:       strategy.FeeForm,
				StrategyID:    strategy.StrategyID,
				FollowersLow:  strategy.FollowersLow,
				FollowersUp:   strategy.FollowersUp,
				RecruitNumber: strategy.RecruitNumber,
				ServiceCharge: strategy.ServiceCharge,
				Offer:         strategy.Offer,
				TOffer:        strategy.TOffer,
				ProjectID:     projectID,
			}
			recruitStrategys = append(recruitStrategys, recruitStrategy)
		}
		err = db.CreateRecruitStrategy(ctx, recruitStrategys)
		if err != nil {
			return nil, err
		}
	}
	res := &http_model.CreateProjectData{
		ProjectID: projectID,
	}
	return res, nil
}

func (*project) Update(ctx context.Context, newProject http_model.UpdateProjectRequest, enterpriseID string) (*http_model.UpdateProjectData, error) {
	//RecruitDdl, _ := time.ParseInLocation("2006-01-02 15:04:05", newProject.RecruitDdl, time.Local)
	RecruitDdl := time.Time{} //赋零值
	if newProject.RecruitDdl != "" {
		RecruitDdl, _ = time.ParseInLocation("2006-01-02 15:04:05", newProject.RecruitDdl, time.Local)
	}
	oldProject, err3 := db.GetProjectDetail(ctx, newProject.ProjectID)
	if err3 != nil {
		return nil, err3
	}
	var feeFrom []string
	for _, strategy := range newProject.RecruitStrategys {
		feeFrom = append(feeFrom, strconv.FormatInt(strategy.FeeForm, 10))
	}
	var ECost float64 = 0
	if newProject.ProjectType == int64(1) {
		for _, strategy := range newProject.RecruitStrategys {
			// 计算预估成本
			var tmpCost float64 = 0
			if strategy.FeeForm == 1 {
				tmpCost = strategy.ServiceCharge * float64(strategy.RecruitNumber)
			} else if strategy.FeeForm == 2 {
				tmpCost = strategy.Offer * float64(strategy.RecruitNumber)
			}
			ECost += tmpCost
		}
	}
	feeForms := strings.Join(feeFrom, ",")
	t := time.Now()
	project := gorm_model.ProjectInfo{}
	if newProject.RecruitDdl == "" {
		project = gorm_model.ProjectInfo{
			ProjectType:   newProject.ProjectType,
			ProjectID:     newProject.ProjectID,
			TalentType:    newProject.TalentType,
			ContentType:   conv.MustInt64(newProject.ContentType, 0),
			ProjectDetail: newProject.ProjectDetail,
			ProjectForm:   conv.MustInt64(newProject.ProjectForm, 0),
			EnterpriseID:  enterpriseID,
			ProjectStatus: newProject.ProjectStatus,
			FeeForm:       feeForms,
			EstimatedCost: ECost,
			SubmitAt:      &t,
		}
	} else {
		project = gorm_model.ProjectInfo{
			ProjectType:   newProject.ProjectType,
			ProjectID:     newProject.ProjectID,
			RecruitDdl:    &RecruitDdl,
			TalentType:    newProject.TalentType,
			ContentType:   conv.MustInt64(newProject.ContentType, 0),
			ProjectDetail: newProject.ProjectDetail,
			ProjectForm:   conv.MustInt64(newProject.ProjectForm, 0),
			EnterpriseID:  enterpriseID,
			ProjectStatus: newProject.ProjectStatus,
			FeeForm:       feeForms,
			EstimatedCost: ECost,
			SubmitAt:      &t,
		}
	}
	projectID, err := db.UpdateProject(ctx, project)
	if err != nil {
		return nil, err
	}
	// 删除该项目之前的所有图片
	err = db.DeleteProjectPhotoByProjectID(ctx, *projectID)
	if err != nil {
		return nil, err
	}
	fmt.Printf("照片:\t %+v\n", newProject.ProjectPhotos)
	if len(newProject.ProjectPhotos) != 0 {
		// 新增图片
		var projectPhotos []gorm_model.ProjectPhoto
		for _, photo := range newProject.ProjectPhotos {
			projectPhoto := gorm_model.ProjectPhoto{
				ProjectID: project.ProjectID,
				PhotoUrl:  photo.PhotoUrl,
				PhotoUid:  photo.PhotoUid,
				FileName:  photo.FileName,
			}
			projectPhotos = append(projectPhotos, projectPhoto)
		}
		err = db.CreateProjectPhoto(ctx, projectPhotos)
		if err != nil {
			return nil, err
		}
	}
	// 删除该项目之前的所有策略
	err = db.DeleteRecruitStrategyByProjectID(ctx, *projectID)
	if err != nil {
		return nil, err
	}
	fmt.Printf("招募策略:%+v \n", newProject.RecruitStrategys)
	if len(newProject.RecruitStrategys) != 0 && newProject.ProjectType == 1 {
		// 新增策略
		var RecruitStrategys []gorm_model.RecruitStrategy
		for _, Strategy := range newProject.RecruitStrategys {
			// 查询对应定价策略
			pricingStrategy, err := db.GetPricingStrategy(ctx, Strategy.FollowersLow, Strategy.FollowersUp, Strategy.FeeForm, oldProject.ProjectPlatform)
			if err != nil {
				return nil, err
			}
			// 根据定价策略计算达人所见报价
			if Strategy.FeeForm == 2 {
				Strategy.TOffer = Strategy.Offer * (1 - conv.MustFloat64(pricingStrategy.ServiceRate, 0)/1000)
			}
			RecruitStrategy := gorm_model.RecruitStrategy{
				FeeForm:       Strategy.FeeForm,
				StrategyID:    Strategy.StrategyID,
				FollowersLow:  Strategy.FollowersLow,
				FollowersUp:   Strategy.FollowersUp,
				RecruitNumber: Strategy.RecruitNumber,
				Offer:         Strategy.Offer,
				TOffer:        Strategy.TOffer,
				ServiceCharge: Strategy.ServiceCharge,
				ProjectID:     project.ProjectID,
			}
			fmt.Printf("Offer:\t %+v", Strategy.Offer)
			RecruitStrategys = append(RecruitStrategys, RecruitStrategy)
		}
		err = db.CreateRecruitStrategy(ctx, RecruitStrategys)
		if err != nil {
			return nil, err
		}
	}
	res := &http_model.UpdateProjectData{
		ProjectID: *projectID,
	}
	return res, nil
}

func (*project) GetPorjectDetail(ctx context.Context, projectID string) (*http_model.ShowProjectData, error) {
	project, err := db.GetProjectDetail(ctx, projectID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetPorjectDetail error,err:%+v", err)
		return nil, err
	}
	enterprise, err := db.GetEnterpriseByEnterpriseID(ctx, project.EnterpriseID)
	// fmt.Println("%+v", enterprise.UserID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetEnterpriseByEnterpriseID error,err:%+v", err)
		return nil, err
	}
	user, err := db.GetUserByID(ctx, enterprise.UserID)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetUserByID error,err:%+v", err)
		return nil, err
	}
	ProjectDetail := http_model.ShowProjectData{
		ProjectID:       project.ProjectID,
		ProjectName:     conv.MustString(project.ProjectName, ""),
		ProjectStatus:   conv.MustString(project.ProjectStatus, ""),
		ProjectType:     conv.MustString(project.ProjectType, ""),
		ProjectPlatform: conv.MustString(project.ProjectPlatform, ""),
		ProjectForm:     conv.MustString(project.ProjectForm, ""),
		TalentType:      conv.MustString(project.TalentType, ""),
		RecruitDdl:      util.GetTimePointer(project.RecruitDdl),
		ContentType:     conv.MustString(project.ContentType, ""),
		ProjectDetail:   conv.MustString(project.ProjectDetail, ""),
		ProductID:       conv.MustString(project.ProductID, ""),
		EnterpriseID:    conv.MustString(project.EnterpriseID, ""),
		Balance:         conv.MustString(enterprise.Balance, ""),
		FailReason:      conv.MustString(project.FailReason, ""),
		CreateAt:        util.GetTimePointer(project.CreatedAt),
		UpdateAt:        util.GetTimePointer(project.UpdatedAt),
		Phone:           user.Phone,
		FinishAt:        util.GetTimePointer(project.FinishAt),
		PassAt:          util.GetTimePointer(project.PassAt),
		PayAt:           util.GetTimePointer(project.PayAt),
	}
	Strategys, err := db.GetRecruitStrategys(ctx, projectID)
	fmt.Println("招募策略：", Strategys)
	if err != nil {
		logrus.WithContext(ctx).Error()
		return nil, err
	}
	for _, strategy := range Strategys {
		RecruitStrategy := http_model.ShowRecruitStrategy{
			RecruitStrategyID: conv.MustString(strategy.RecruitStrategyID, ""),
			FeeForm:           conv.MustString(strategy.FeeForm, ""),
			StrategyID:        conv.MustString(strategy.StrategyID, ""),
			FollowersLow:      conv.MustString(strategy.FollowersLow, ""),
			FollowersUp:       conv.MustString(strategy.FollowersUp, ""),
			RecruitNumber:     conv.MustString(strategy.RecruitNumber, ""),
			Offer:             conv.MustString(strategy.Offer, ""),
			ServiceCharge:     conv.MustString(strategy.ServiceCharge, ""),
			SelectedNumber:    strategy.SelectedNumber,
			WaitingNumber:     strategy.WaitingNumber,
			DeliveredNumber:   strategy.DeliveredNumber,
			SignedNumber:      strategy.SignedNumber,
		}
		ProjectDetail.RecruitStrategys = append(ProjectDetail.RecruitStrategys, RecruitStrategy)
	}
	Photos, err := db.GetProjectPhoto(ctx, projectID)
	if err != nil {
		logrus.WithContext(ctx).Error()
		return nil, err
	}
	for _, Photo := range Photos {
		ProjectPhoto := http_model.ShowProjectPhoto{
			PhotoUrl: Photo.PhotoUrl,
			PhotoUid: Photo.PhotoUid,
		}
		ProjectDetail.ProjectPhotos = append(ProjectDetail.ProjectPhotos, ProjectPhoto)
	}
	return &ProjectDetail, nil
}

func (*project) GetTaskLogisticsList(ctx context.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskLogisticsListData, error) {
	TaskLogisticss, total, err := db.GetTaskLogisticsList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskLogisticsList error,err:%+v", err)
		return nil, err
	}
	TaskLogisticsListData := new(http_model.TaskLogisticsListData)
	TaskLogisticsListData.TaskLogisticsPreview = pack.MGormTaskLogisticsInfoListToHttpTaskLogisticsPreviewList(TaskLogisticss)
	TaskLogisticsListData.Total = conv.MustString(total, "")
	return TaskLogisticsListData, nil
}

func (*project) GetSpecialProjectTaskList(ctx context.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.SpecialTaskLogisticsListData, error) {
	SpecialTaskLogisticsListDatas, total, err := db.GetSpecialTaskLogisticsList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetSpecialProjectTaskList error,err:%+v", err)
		return nil, err
	}
	TaskLogisticsListData := new(http_model.SpecialTaskLogisticsListData)
	TaskLogisticsListData.SpecialTaskLogisticsPreview = pack.MGormSpecialTaskLogisticsInfoListToHttpTaskLogisticsPreviewList(SpecialTaskLogisticsListDatas)
	TaskLogisticsListData.Total = conv.MustString(total, "")
	return TaskLogisticsListData, nil
}

func (*project) ChangeTaskStatus(ctx *gin.Context, data http_model.ProjectChangeTaskStatusRequest) interface{} {
	fmt.Println("taskIds :", data.TaskIds)
	fmt.Println("task_status :", data.TaskStatus)
	RecruitStrategyIDs, err := db.ChangeTaskStatus(ctx, data.TaskIds, data.TaskStatus)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call ChangeTaskStatus error,err:%+v", err)
		return err
	}
	if data.TaskStatus == "2" {
		for _, RecruitStrategyID := range RecruitStrategyIDs {
			err = db.CalculateSelectedNumberByRecruitStrategyID(ctx, RecruitStrategyID, 1)
			if err != nil {
				logrus.WithContext(ctx).Errorf("[project service] call ChangeTaskStatus error,err:%+v", err)
				return err
			}
		}
	} else if data.TaskStatus == "3" && data.ClickIndex != "0" {
		for _, RecruitStrategyID := range RecruitStrategyIDs {
			err = db.CalculateSelectedNumberByRecruitStrategyID(ctx, RecruitStrategyID, -1)
			if err != nil {
				logrus.WithContext(ctx).Errorf("[project service] call ChangeTaskStatus error,err:%+v", err)
				return err
			}
		}
	}
	return nil
}

func (*project) ChangeSpecialTaskStatus(ctx *gin.Context, data http_model.ProjectChangeTaskStatusRequest) interface{} {
	err := db.ChangeSpecialTaskStatus(ctx, data.TaskIds, data.TaskStatus, data.TaskStage)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call ChangeSpecialTaskStatus error,err:%+v", err)
		return err
	}
	for _, taskId := range data.TaskIds {
		err = db.CreateMessageByTaskId(ctx, 7, 2, taskId)
		if err != nil {
			logrus.WithContext(ctx).Errorf("[project service] call CreateMessageByTaskId error,err:%+v", err)
			return err
		}
	}
	err = db.SetSpecialProjectFinish(ctx, data.ProjectId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call CreateMessageByTaskId error,err:%+v", err)
		return err
	}
	return nil
}

func (p *project) GetTaskScriptList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskScriptListData, error) {
	TaskScripts, total, err := db.GetTaskScriptList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskScriptList error,err:%+v", err)
		return nil, err
	}
	TaskScriptListData := new(http_model.TaskScriptListData)
	TaskScriptListData.TaskScriptPreview = pack.MGormTaskScriptInfoListToHttpTaskScriptPreviewList(TaskScripts)
	TaskScriptListData.Total = conv.MustString(total, "")
	return TaskScriptListData, nil
}

func (p *project) GetTaskSketchList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskSketchListData, error) {
	TaskSketchs, total, err := db.GetTaskSketchList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskSketchList error,err:%+v", err)
		return nil, err
	}
	TaskSketchListData := new(http_model.TaskSketchListData)
	TaskSketchListData.TaskSketchPreview = pack.MGormTaskSketchInfoListToHttpTaskSketchPreviewList(TaskSketchs)
	TaskSketchListData.Total = conv.MustString(total, "")
	return TaskSketchListData, nil
}

func (p *project) GetTaskLinkList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskLinkListData, error) {
	TaskLinks, total, err := db.GetTaskLinkList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskLinkList error,err:%+v", err)
		return nil, err
	}
	TaskLinkListData := new(http_model.TaskLinkListData)
	TaskLinkListData.TaskLinkPreview = pack.MGormTaskLinkInfoListToHttpTaskLinkPreviewList(TaskLinks)
	TaskLinkListData.Total = conv.MustString(total, "")
	return TaskLinkListData, nil
}

func (p *project) GetTaskDataList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskDataListData, error) {
	TaskDatas, total, err := db.GetTaskDataList(ctx, projectID, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskDataList error,err:%+v", err)
		return nil, err
	}
	TaskDataListData := new(http_model.TaskDataListData)
	TaskDataListData.TaskDataPreview = pack.MGormTaskDataInfoListToHttpTaskDataPreviewList(TaskDatas)
	TaskDataListData.Total = conv.MustString(total, "")
	return TaskDataListData, nil
}

func (p *project) GetTaskFinishList(ctx *gin.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) (*http_model.TaskFinishListData, error) {
	TaskFinishs, total, err := db.GetTaskFinishList(ctx, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project service] call GetTaskFinishList error,err:%+v", err)
		return nil, err
	}
	TaskFinishListData := new(http_model.TaskFinishListData)
	TaskFinishListData.TaskFinishPreview = pack.MGormTaskFinishInfoListToHttpTaskFinishPreviewList(TaskFinishs)
	TaskFinishListData.Total = conv.MustString(total, "")
	return TaskFinishListData, nil
}

func (p *project) GetServiceCharge(ctx *gin.Context, data http_model.GetServiceChargeRequest) (*http_model.ServiceChargeData, error) {
	pricingStrategy, err := db.GetPricingStrategy(ctx, data.FollowersLow, data.FollowersUp, data.FeeForm, data.Platform)
	if err != nil {
		return nil, err
	}
	serviceFee := http_model.ServiceChargeData{
		ServiceCharge: pricingStrategy.ServiceCharge,
	}
	return &serviceFee, nil
}
