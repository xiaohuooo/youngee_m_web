package service

import (
	"context"
	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"youngee_m_api/db"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

var User *user

type user struct {
}

func (*user) EnterpriseUserList(ctx context.Context, pageSize, pageNum int32, condition *common_model.EnterpriseUserConditions) (*http_model.EnterpriseUserData, error) {
	enterpriseUsers, total, err := db.GetEnterpriseUserList(ctx, pageSize, pageNum, condition)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[user service] call GetEnterpriseUserList error,err:%+v", err)
		return nil, err
	}
	EnterpriseUserData := new(http_model.EnterpriseUserData)
	EnterpriseUserData.EnterpriseUserPreview = enterpriseUsers
	EnterpriseUserData.Total = conv.MustString(total, "")
	return EnterpriseUserData, nil
}

func (*user) CreatorList(ctx context.Context, pageSize, pageNum int32, conditions *common_model.CreatorListConditions) (*http_model.CreatorListData, error) {
	CreatorList, total, err := db.GetCreatorList(ctx, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[user service] call GetEnterpriseUserList error,err:%+v", err)
		return nil, err
	}
	CreatorListData := new(http_model.CreatorListData)
	CreatorListData.CreatorListPreview = CreatorList
	CreatorListData.Total = conv.MustString(total, "")
	return CreatorListData, nil
}

func (u *user) AccountInfo(ctx context.Context, pageSize int32, pageNum int32, conditions *common_model.AccountInfoConditions) (*http_model.AccountInfoPreView, error) {
	accountInfo, total, err := db.AccountInfo(ctx, pageSize, pageNum, conditions)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[user service] call AccountInfo error,err:%+v", err)
		return nil, err
	}
	accountInfoPreView := new(http_model.AccountInfoPreView)
	accountInfoPreView.AccountInfoData = accountInfo
	accountInfoPreView.Total = conv.MustString(total, "")
	return accountInfoPreView, nil
}

func (u *user) GetTaskRecord(ctx context.Context, talentId string) (*http_model.GetTaskRecordResponse, error) {
	data, err := db.GetTaskRecord(ctx, talentId)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[user service] call GetTaskRecord error,err:%+v", err)
		return nil, err
	}
	return data, nil
}
