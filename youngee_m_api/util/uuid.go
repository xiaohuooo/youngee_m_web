package util

import (
	"github.com/GUAIK-ORG/go-snowflake/snowflake"
	"github.com/google/uuid"
	"github.com/issue9/conv"
	"math/rand"
	"time"
)

var snowflakeInstance *snowflake.Snowflake

func init() {
	s, err := snowflake.NewSnowflake(int64(0), int64(0))
	if err != nil {
		panic(err)
	}
	snowflakeInstance = s
}
func GetUUID() string {
	return uuid.New().String()
}
func GetSnowflakeID() int64 {
	return snowflakeInstance.NextVal()
}

func GetSelectionID() string {
	rand.Seed(time.Now().UnixNano())
	td := conv.MustString(time.Now().Day())
	for {
		if len(td) == 3 {
			break
		}
		td = "0" + td
	}
	selectionId := conv.MustString(time.Now().Year())[2:] + td + conv.MustString(rand.Intn(100000-10000)+10000)
	return selectionId
}
