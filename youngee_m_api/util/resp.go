package util

import (
	"net/http"
	"youngee_m_api/consts"

	"youngee_m_api/model/http_model"

	"github.com/gin-gonic/gin"
)

func PackErrorResp(c *gin.Context, status int32) {
	resp := http_model.CommonResponse{
		Status:  status,
		Message: consts.GetErrorToast(status),
		Data:    nil,
	}
	c.JSON(http.StatusOK, resp)
}
func HandlerPackErrorResp(resp *http_model.CommonResponse, status int32, message string) {
	resp.Status = status
	if message != consts.DefaultToast {
		resp.Message = message
	} else {
		resp.Message = consts.GetErrorToast(status)
	}
}
