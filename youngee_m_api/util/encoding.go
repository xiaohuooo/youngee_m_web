package util

import (
	"crypto/md5"
	"encoding/hex"
)

func MD5(keys ...string) string {
	finalKey := ""
	for _, key := range keys {
		finalKey += key
	}
	s := md5.New()
	s.Write([]byte(finalKey))
	return hex.EncodeToString(s.Sum(nil))
}
