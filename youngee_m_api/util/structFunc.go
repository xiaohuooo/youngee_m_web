package util

import "reflect"

/*
	合并两个结构体的值，遍历结构体s1，若s1中某字段值为空，则将s2该字段的值赋给s1，否则不变
	参数：interface{} -> &struct, 结构体指针
	返回值：interface{} -> &struct, 结构体指针
*/
func MergeStructValue(s1 interface{}, s2 interface{}) interface{} {
	v1 := reflect.ValueOf(s1).Elem()
	v2 := reflect.ValueOf(s2).Elem()
	for i := 0; i < v1.NumField(); i++ {
		field := v1.Field(i)
		name := v1.Type().Field(i).Name
		if field.Interface() == reflect.Zero(field.Type()).Interface() {
			v1.FieldByName(name).Set(v2.FieldByName(name))
		}
	}

	return v1
}
