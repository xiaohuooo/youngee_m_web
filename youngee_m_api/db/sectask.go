package db

import (
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"strings"
	"time"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
)

func GetSecTaskById(ctx context.Context, secTaskId string) (*gorm_model.YounggeeSecTaskInfo, error) {
	db := GetWriteDB(ctx)
	secTaskInfo := gorm_model.YounggeeSecTaskInfo{}
	whereCondition := gorm_model.YounggeeSecTaskInfo{TaskID: secTaskId}
	result := db.Where(&whereCondition).First(&secTaskInfo)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, nil
		} else {
			return nil, result.Error
		}
	}
	return &secTaskInfo, nil
}

func GetSecTaskList(ctx context.Context, selectionId string, taskStatus int, searchValue string, pageSize, pageNum int64) ([]*http_model.SecTaskInfo, int64, error) {
	db := GetReadDB(ctx)
	whereCondition := gorm_model.YounggeeSecTaskInfo{
		SelectionID: selectionId,
		TaskStatus:  taskStatus,
	}
	db = db.Model(gorm_model.YounggeeSecTaskInfo{}).Where(whereCondition)

	// 查询总数
	var total int64
	var secTaskInfoList []*gorm_model.YounggeeSecTaskInfo
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetSelectionList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("create_date desc").Limit(int(limit)).Offset(int(offset)).Find(&secTaskInfoList).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetSelectionList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	newSecTaskInfoList := pack.GormSecTaskListToHttpSecTaskList(secTaskInfoList)
	var resSecTaskInfoList []*http_model.SecTaskInfo
	if searchValue != "" {
		for _, v := range newSecTaskInfoList {
			if strings.Contains(v.SelectionId, searchValue) {
				resSecTaskInfoList = append(resSecTaskInfoList, v)
			} else if strings.Contains(v.PlatformNickname, searchValue) {
				resSecTaskInfoList = append(resSecTaskInfoList, v)
			} else {
				total--
			}
		}
	} else {
		resSecTaskInfoList = newSecTaskInfoList
	}
	return resSecTaskInfoList, total, nil
}

func PassSecTaskCoop(ctx context.Context, selectionId string, taskIds []string) (bool, error) {
	db := GetWriteDB(ctx)
	// 1. 校验
	var count int64
	err := db.Model(gorm_model.YounggeeSecTaskInfo{}).Where("task_id IN ? AND task_stage = 3", taskIds).Count(&count).Error
	if err != nil {
		return false, err
	}
	if int64(len(taskIds)) == 0 || count != int64(len(taskIds)) {
		return false, errors.New("任务id有误")
	}

	// 2. 查询任务对应达人id（用于生成达人消息）
	var talentIds []string
	err = db.Model(gorm_model.YounggeeSecTaskInfo{}).Where("task_id IN ?", taskIds).Select("talent_id").Find(talentIds).Error
	if err != nil {
		return false, err
	}
	// 3. 查询任务对应选品名称（用于生成达人消息）
	var selection gorm_model.YounggeeSelectionInfo
	err = db.Model(gorm_model.YounggeeSelectionInfo{}).Where("selection_id = ?", selectionId).Find(selection).Error
	if err != nil {
		return false, err
	}

	err = db.Transaction(func(tx *gorm.DB) error {
		// 2. 修改任务状态和任务阶段
		// 若选品不提供样品，则直接跳转执行阶段，否则进入发货阶段
		if selection.SampleMode == 3 {
			updateData := gorm_model.YounggeeSecTaskInfo{
				TaskStatus:      2,
				TaskStage:       6,
				SelectDate:      time.Now(),
				LogisticsStatus: 1,
			}
			err = tx.Model(gorm_model.YounggeeSecTaskInfo{}).Where("task_id IN ? AND task_stage = 3", taskIds).Updates(updateData).Error
			if err != nil {
				return err
			}
		} else {
			updateData := gorm_model.YounggeeSecTaskInfo{
				TaskStatus:       2,
				TaskStage:        8,
				SelectDate:       time.Now(),
				LogisticsStatus:  3,
				AssignmentStatus: 1,
			}
			err = tx.Model(gorm_model.YounggeeSecTaskInfo{}).Where("task_id IN ? AND task_stage = 3", taskIds).Updates(updateData).Error
			if err != nil {
				return err
			}
		}
		// 3. 生成达人消息
		for _, talendId := range talentIds {
			err = CreateMessage(ctx, 1, 1, talendId, selection.SelectionName)
			if err != nil {
				return err
			}
		}
		// 返回 nil 提交事务
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

func RefuseSecTaskCoop(ctx context.Context, taskIds []string) (bool, error) {
	db := GetWriteDB(ctx)
	// 1. 校验
	var count int64
	err := db.Model(gorm_model.YounggeeSecTaskInfo{}).Where("task_id IN ? AND task_stage = 3", taskIds).Count(&count).Error
	if err != nil {
		return false, err
	}
	if count != int64(len(taskIds)) {
		return false, errors.New("任务id有误")
	}

	// 查询任务对应达人id
	var talentIds []string
	err = db.Model(gorm_model.YounggeeSecTaskInfo{}).Where("task_id IN ?", taskIds).Select("talent_id").Find(talentIds).Error
	if err != nil {
		return false, err
	}

	err = db.Transaction(func(tx *gorm.DB) error {
		// 2. 修改任务状态和任务阶段
		updateData := gorm_model.YounggeeSecTaskInfo{
			TaskStatus:     3,
			TaskStage:      5,
			CompleteDate:   time.Now(),
			CompleteStatus: 3,
		}
		err = tx.Model(gorm_model.YounggeeSecTaskInfo{}).Where("task_id IN ? AND task_stage = 3", taskIds).Updates(updateData).Error
		if err != nil {
			return err
		}

		// 返回 nil 提交事务
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

func UpdateSecTask(ctx context.Context, updateData gorm_model.YounggeeSecTaskInfo) (bool, error) {
	db := GetWriteDB(ctx)
	whereCondition := gorm_model.YounggeeSecTaskInfo{
		TaskID: updateData.TaskID,
	}
	err := db.Where(whereCondition).Updates(&updateData).Error
	if err != nil {
		return false, err
	}
	return true, nil
}
