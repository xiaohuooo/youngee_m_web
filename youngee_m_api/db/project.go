package db

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"strings"
	"time"
	"youngee_m_api/consts"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func GetFullProjectList(ctx context.Context, pageSize, pageNum int32, condition *common_model.ProjectCondition, projectType string) ([]*gorm_model.ProjectInfo, int64, error) {
	db := GetReadDB(ctx)
	if projectType == "2" {
		db = db.Debug().Model(gorm_model.ProjectInfo{}).Where("project_type = 2")
	} else {
		db = db.Debug().Model(gorm_model.ProjectInfo{}).Where("project_type = 1")
	}
	// 根据Project条件过滤
	conditionType := reflect.TypeOf(condition).Elem()
	conditionValue := reflect.ValueOf(condition).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if tag == "project_status" && util.IsBlank(value) {
			db = db.Where(fmt.Sprintf("project_status != 3"))
			db = db.Where(fmt.Sprintf("project_status != 5"))
			db = db.Where(fmt.Sprintf("project_status != 7"))
		}
		if !util.IsBlank(value) && tag != "updated_at" && tag != "project_name" && tag != "project_id" {
			db = db.Debug().Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		}
		if tag == "updated_at" && value.Interface() != "0" {
			db = db.Where(fmt.Sprintf("updated_at like '%s%%'", value.Interface()))
		}
		if tag == "project_name" && !util.IsBlank(value) {
			//db = db.Where(fmt.Sprintf("%s = ?", tag), value.Interface())
			db = db.Where(fmt.Sprintf("project_name like '%%%s%%'", value.Interface()))
		}
		if tag == "project_id" && value.Interface() != "0" {
			db = db.Debug().Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		}
	}
	// 查询总数
	var total int64
	var fullProjects []*gorm_model.ProjectInfo
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetFullProjectList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("updated_at desc").Limit(int(limit)).Offset(int(offset)).Find(&fullProjects).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetFullProjectList] error query mysql find, err:%+v", err)
		return nil, 0, err
	}
	return fullProjects, total, nil
}

func GetProjectDetail(ctx context.Context, projectID string) (*gorm_model.ProjectInfo, error) {
	db := GetReadDB(ctx)
	var ProjectDetail *gorm_model.ProjectInfo
	err := db.Where("project_id = ?", projectID).First(&ProjectDetail).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		} else {
			return nil, err
		}
	}
	return ProjectDetail, nil
}

func UpdateProjectStatus(ctx context.Context, projectId string, status int64) error {
	db := GetReadDB(ctx)
	t := time.Now()
	err := db.Model(gorm_model.ProjectInfo{}).
		Where("project_id = ?", projectId).Updates(gorm_model.ProjectInfo{
		ProjectStatus: status,
		PayAt:         &t,
	}).Error
	if err != nil {
		log.Println("DB UpdateProjectStatus error :", err)
		return err
	}
	return nil
}

func GetRecruitStrategys(ctx context.Context, ProjectID string) ([]gorm_model.RecruitStrategy, error) {
	db := GetReadDB(ctx)
	var RecruitStrategys []gorm_model.RecruitStrategy
	err := db.Where("project_id=?", ProjectID).Find(&RecruitStrategys).Error
	if err != nil {
		return nil, err
	}
	return RecruitStrategys, nil
}

func GetProjectPhoto(ctx context.Context, ProjectID string) ([]gorm_model.ProjectPhoto, error) {
	db := GetReadDB(ctx)
	var ProjectPhoto []gorm_model.ProjectPhoto
	err := db.Where("project_id=?", ProjectID).Find(&ProjectPhoto).Error
	if err != nil {
		return nil, err
	}
	return ProjectPhoto, nil
}

func UpdateProject(ctx context.Context, project gorm_model.ProjectInfo) (*string, error) {
	db := GetReadDB(ctx)
	err := db.Model(&project).Updates(project).Error
	if err != nil {
		return nil, err
	}
	return &project.ProjectID, nil
}

func ApproveProject(ctx context.Context, projectId string, isApprove int64) (error error, message string) {
	db := GetReadDB(ctx)
	projectInfo := gorm_model.ProjectInfo{}
	db = db.Debug().Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).First(&projectInfo)
	t := time.Now()
	if isApprove == 1 && projectInfo.ProjectStatus == 2 {
		err := db.Updates(gorm_model.ProjectInfo{
			ProjectStatus: 4,
			PassAt:        &t,
		}).Error
		message = "审核通过"
		if err != nil {
			logrus.Println("DB AutoUpdateStatus error :", err)
			return err, ""
		}
	} else if isApprove == 2 && projectInfo.ProjectStatus == 2 {
		err := db.Updates(gorm_model.ProjectInfo{
			ProjectStatus: 8,
			FinishAt:      &t,
			FailReason:    2,
		}).Error
		message = "项目存在风险已禁止发布"
		if err != nil {
			logrus.Println("DB AutoUpdateStatus error :", err)
			return err, ""
		}
	} else {
		return nil, "操作失败"
	}
	return nil, message
}

func GetAllProject(ctx context.Context, pageSize, pageNum int32) ([]*http_model.GetAllProjectPreview, int64, error) {
	db := GetReadDB(ctx)
	db = db.Debug().Model(gorm_model.ProjectInfo{}).Where("project_status = 1 or project_status = 6")
	db = db.Where("is_read = 0")
	var allProjects []*http_model.GetAllProjectPreview
	// 查询总数
	var total int64
	var fullProjects []*gorm_model.ProjectInfo
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetAllProject] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("updated_at desc").Limit(int(limit)).Offset(int(offset)).Find(&fullProjects).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetAllProject] error query mysql find, err:%+v", err)
		return nil, 0, err
	}
	var EnterpriseIDs []string
	for _, fullProject := range fullProjects {
		EnterpriseIDs = append(EnterpriseIDs, fullProject.EnterpriseID)
	}
	EnterpriseIDs = util.RemoveStrRepByMap(EnterpriseIDs)
	userMemo := map[string]string{}
	phoneMemo := map[string]string{}
	for _, EnterpriseID := range EnterpriseIDs {
		userMemo[EnterpriseID] = GetUsernameEnterpriseID(ctx, EnterpriseID)
	}
	for _, EnterpriseID := range EnterpriseIDs {
		phoneMemo[EnterpriseID] = GetPhoneByEnterpriseID(ctx, EnterpriseID)
	}
	for _, fullProject := range fullProjects {
		allProject := new(http_model.GetAllProjectPreview)
		allProject.ProjectUpdated = conv.MustString(fullProject.UpdatedAt, "")
		allProject.ProjectStatus = conv.MustString(fullProject.ProjectStatus, "")
		allProject.ProjectId = conv.MustString(fullProject.ProjectID, "")
		allProject.EnterpriseID = fullProject.EnterpriseID
		allProject.Username = userMemo[fullProject.EnterpriseID]
		allProject.Phone = phoneMemo[fullProject.EnterpriseID]
		allProjects = append(allProjects, allProject)
	}
	return allProjects, total, nil
}

// GetPhoneByEnterpriseID 根据企业ID查找用户联系方式
func GetPhoneByEnterpriseID(ctx context.Context, enterpriseID string) string {
	db := GetReadDB(ctx)
	var Enterprise gorm_model.Enterprise
	// 通过企业ID获得用户ID
	db = db.Model([]gorm_model.Enterprise{}).Where("enterprise_id", enterpriseID).First(&Enterprise)
	userId := Enterprise.UserID
	db1 := GetReadDB(ctx)
	var user gorm_model.YounggeeUser
	db1 = db1.Model([]gorm_model.YounggeeUser{}).Where("id", userId).First(&user)
	phone := user.Phone
	return phone
}

// GetUsernameEnterpriseID 根据企业ID查找用户名称
func GetUsernameEnterpriseID(ctx context.Context, enterpriseID string) string {
	db := GetReadDB(ctx)
	var Enterprise gorm_model.Enterprise
	// 通过企业ID获得用户ID
	db = db.Model([]gorm_model.Enterprise{}).Where("enterprise_id", enterpriseID).First(&Enterprise)
	userId := Enterprise.UserID
	db1 := GetReadDB(ctx)
	var user gorm_model.YounggeeUser
	db1 = db1.Model([]gorm_model.YounggeeUser{}).Where("id", userId).First(&user)
	username := user.Username
	return username
}

func GetProjectTaskList(ctx context.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TaskConditions) ([]*http_model.ProjectTaskInfo, int64, error) {
	db := GetReadDB(ctx)
	// 查询task表信息
	db = db.Debug().Model(gorm_model.YoungeeTaskInfo{})
	// 根据Project条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	var platform_nickname string = ""
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if !util.IsBlank(value) && tag != "platform_nickname" {
			db = db.Debug().Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		} else if tag == "platform_nickname" {
			platform_nickname = fmt.Sprintf("%v", value.Interface())
			continue
		}
	}
	var taskInfos []gorm_model.YoungeeTaskInfo
	db = db.Model(gorm_model.YoungeeTaskInfo{})
	// 查询总数
	var totalTask int64
	if err := db.Count(&totalTask).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	db.Order("task_id").Find(&taskInfos)

	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("task_id").Limit(int(limit)).Offset(int(offset)).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	var taskDatas []*http_model.ProjectTaskInfo
	var newTaskDatas []*http_model.ProjectTaskInfo

	taskDatas = pack.TaskAccountToTaskInfo(taskInfos)

	for _, v := range taskDatas {
		if platform_nickname == "" {
			newTaskDatas = append(newTaskDatas, v)
		} else if strings.Contains(v.PlatformNickname, platform_nickname) {
			newTaskDatas = append(newTaskDatas, v)
		} else if strings.Contains(v.TaskID, platform_nickname) {
			newTaskDatas = append(newTaskDatas, v)
		} else {
			totalTask--
		}
	}
	return newTaskDatas, totalTask, nil
}

func CreateProject(ctx context.Context, projectInfo gorm_model.ProjectInfo) (string, error) {
	db := GetWriteDB(ctx)
	err := db.Create(&projectInfo).Error
	if err != nil {
		return "", err
	}
	return projectInfo.ProjectID, nil
}

func ProjectHandle(ctx context.Context, data http_model.ProjectHandleRequest) error {
	err := ChangeProjectStatus(ctx, data.ProjectID, data.ProjectStatus)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project] call ChangeProjectStatus error,err:%+v", err)
		return err
	}
	return nil
}

func ChangeProjectStatus(ctx context.Context, projectID string, projectStatus string) error {
	db := GetReadDB(ctx)
	projectInfo := gorm_model.ProjectInfo{}
	if err := db.Debug().Model(&projectInfo).
		Where("project_id = ?", projectID).
		Updates(gorm_model.ProjectInfo{IsRead: 1}).
		Error; err != nil {
		logrus.WithContext(ctx).Errorf("[ChangeProjectStatus] error query mysql total, err:%+v", err)
		return err
	}
	return nil
}

//// 取消拉黑
//func unBlack(ctx context.Context, ID string) error {
//	db := GetReadDB(ctx)
//	talentInfo := gorm_model.YoungeeTalentInfo{}
//	if err := db.Debug().Model(&talentInfo).
//		Where("id = ?", ID).
//		Updates(gorm_model.YoungeeTalentInfo{InBlacklist: 0}).
//		Error; err != nil {
//		logrus.WithContext(ctx).Errorf("[ChangeProjectStatus] error query mysql total, err:%+v", err)
//		return err
//	}
//	return nil
//}

func GetProjectRecords(ctx context.Context, userId int64, pageSize, pageNum int32) (*http_model.ProjectRecordsData, error) {
	db := GetReadDB(ctx)
	enterpriseID := GetEnterpriseIDByUserId(ctx, userId)
	var projectInfos []*gorm_model.ProjectInfo
	db = db.Model(&gorm_model.ProjectInfo{}).Where("enterprise_id = ?", enterpriseID)
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectRecords] error query mysql total, err:%+v", err)
		return nil, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("updated_at desc").Limit(int(limit)).Offset(int(offset)).Find(&projectInfos).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectRecords] error query mysql limit, err:%+v", err)
		return nil, err
	}
	var projectRecords []*http_model.ProjectRecordsPreview
	for _, projectInfo := range projectInfos {
		projectRecordsPreview := new(http_model.ProjectRecordsPreview)
		projectRecordsPreview.ProjectID = projectInfo.ProjectID
		projectRecordsPreview.ProjectName = projectInfo.ProjectName
		projectRecordsPreview.ProjectType = consts.GetProjectType(projectInfo.ProjectType)
		projectRecordsPreview.UpdatedAt = conv.MustString(projectInfo.UpdatedAt, "")[:19]
		projectRecordsPreview.ProjectStatus = consts.GetProjectStatus(projectInfo.ProjectStatus)
		projectRecords = append(projectRecords, projectRecordsPreview)
	}
	projectRecordsData := http_model.ProjectRecordsData{}
	projectRecordsData.ProjectRecordsPreview = projectRecords
	projectRecordsData.Total = strconv.FormatInt(total, 10)
	return &projectRecordsData, nil
}

func SetProjectFinish(ctx context.Context, projectId string) error {
	db := GetReadDB(ctx)
	var finishedNum int64
	var unfinishedNum int64
	err1 := db.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = 2 AND task_stage < 15", projectId).Count(&unfinishedNum).Error
	if err1 != nil {
		logrus.WithContext(ctx).Errorf("[Data db] Count YoungeeTaskInfo error,err:%+v", err1)
		return err1
	}
	db.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage >= 15", projectId).Count(&finishedNum)
	if unfinishedNum == 0 && finishedNum != 0 {

		// 2. 释放企业账户因项目冻结的资金
		// 1） 计算剩余资金
		db1 := GetReadDB(ctx)
		var allPayment float64
		var realPayment float64
		err := db1.Model(gorm_model.YoungeeTaskInfo{}).
			Where("project_id = ? and task_status = 2 and task_stage >= 15", projectId).Pluck("COALESCE(SUM(all_payment), 0) as allPayment", &allPayment).Error
		if err != nil {
			log.Println("DB GetAutoCaseCloseTask error in data:", err)
			return err
		}
		err = db1.Model(gorm_model.YoungeeTaskInfo{}).Select("sum(real_payment) as realPayment").
			Where("project_id = ? and task_status = 2 and task_stage >= 15", projectId).Pluck("COALESCE(SUM(real_payment), 0) as realPayment", &realPayment).Error
		if err != nil {
			log.Println("DB GetAutoCaseCloseTask error in data:", err)
			return err
		}
		fmt.Println("企业应支付金额总计：", allPayment, "实际支付总计：", realPayment)
		db2 := GetReadDB(ctx)
		var enterpriseID int64
		db2.Model(gorm_model.ProjectInfo{}).Select("enterprise_id").Where("project_id = ?", projectId).Find(&enterpriseID)
		// 	2）. 释放剩余资金
		err = db1.Model(gorm_model.Enterprise{}).Where("enterprise_id = ?", enterpriseID).Updates(
			map[string]interface{}{
				"frozen_balance":    gorm.Expr("frozen_balance - ?", allPayment),
				"balance":           gorm.Expr("balance - ?", realPayment),
				"available_balance": gorm.Expr("available_balance + ?", allPayment-realPayment)}).Error
		if err != nil {
			log.Println("DB GetAutoCaseCloseTask error in data:", err)
			return err
		}
		// 1. 更新项目状态为已结束
		t := time.Now()
		err = db.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).
			Updates(map[string]interface{}{"project_status": 10, "payment_amount": realPayment, "finish_at": &t}).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Project db] Update ProjectInfo error,err:%+v", err)
			return err
		}
	}
	return nil
}

func SetSpecialProjectFinish(ctx context.Context, projectId string) error {
	db := GetReadDB(ctx)
	var finishedNum int64
	var unfinishedNum int64
	db.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = 2 AND task_stage < 15", projectId).Count(&unfinishedNum)
	db.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage >= 15", projectId).Count(&finishedNum)
	if unfinishedNum == 0 && finishedNum != 0 {
		// 1. 更新项目状态为已结束
		t := time.Now()
		err := db.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).Updates(gorm_model.ProjectInfo{ProjectStatus: 10, FinishAt: &t}).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[Project db] Update ProjectInfo error,err:%+v", err)
			return err
		}
	}
	return nil
}
