package db

import (
	"context"
	"gorm.io/gorm"
	"youngee_m_api/model/gorm_model"
)

func CreateIncome(ctx context.Context, income gorm_model.YounggeeTalentIncome, tx *gorm.DB) error {
	if tx != nil {
		err := tx.Create(&income).Error
		if err != nil {
			return err
		}
	} else {
		db := GetWriteDB(ctx)
		err := db.Create(&income).Error
		if err != nil {
			return err
		}
	}
	return nil
}
