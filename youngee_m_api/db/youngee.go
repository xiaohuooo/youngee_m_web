package db

import (
	"context"
	"github.com/sirupsen/logrus"
	"strconv"
	"youngee_m_api/consts"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

func GetYoungeeRecords(ctx context.Context, talentId string) (*http_model.YoungeeRecordsData, error) {
	db := GetReadDB(ctx)
	var youngeeInfos []*gorm_model.YounggeeTalentTeam
	db = db.Model(&gorm_model.YounggeeTalentTeam{}).Where("talent_id = ?", talentId)
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetyoungeeRecords] error query mysql total, err:%+v", err)
		return nil, err
	}
	// 查询该页数据
	//limit := pageSize
	//offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("create_at desc").Find(&youngeeInfos).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetyoungeeRecords] error query mysql total, err:%+v", err)
	}
	var youngeeRecords []*http_model.YoungeeRecordsPreview
	for _, youngeeInfo := range youngeeInfos {
		pointIncome := strconv.FormatInt(youngeeInfo.PointIncome, 10)
		youngeeRecordsPreview := new(http_model.YoungeeRecordsPreview)
		youngeeRecordsPreview.ProjectId = youngeeInfo.ProjectID
		youngeeRecordsPreview.ProjectName = youngeeInfo.ProjectName
		youngeeRecordsPreview.ProjectType = consts.GetyoungeeType(youngeeInfo.ProjectType)
		youngeeRecordsPreview.ProjectPlatform = consts.GetyoungeeType(youngeeInfo.Platform)
		youngeeRecordsPreview.TeamId = youngeeInfo.TeamID
		youngeeRecordsPreview.TeamStatus = consts.GetTeamStage(youngeeInfo.TeamStatus)
		youngeeRecordsPreview.PintIncome = pointIncome
		youngeeRecordsPreview.MoneyIncome = youngeeInfo.MoneyIncome
		youngeeRecords = append(youngeeRecords, youngeeRecordsPreview)
	}
	youngeeRecordsData := http_model.YoungeeRecordsData{}
	youngeeRecordsData.YoungeeRecordsPreview = youngeeRecords
	youngeeRecordsData.Total = strconv.FormatInt(total, 10)
	return &youngeeRecordsData, nil
}
