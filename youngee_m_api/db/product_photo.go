package db

import (
	"context"
	"youngee_m_api/model/gorm_model"
)

func GetProductPhotoByProductID(ctx context.Context, productID int64) ([]gorm_model.YounggeeProductPhoto, error) {
	db := GetReadDB(ctx)
	var productPhotos []gorm_model.YounggeeProductPhoto
	err := db.Where("product_id = ?", productID).Find(&productPhotos).Error
	if err != nil {
		return nil, err
	}
	return productPhotos, nil
}

func CreateProjectPhoto(ctx context.Context, projectPhotos []gorm_model.ProjectPhoto) error {
	db := GetReadDB(ctx)
	err := db.Create(&projectPhotos).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteProjectPhotoByProjectID(ctx context.Context, projectId string) error {
	db := GetReadDB(ctx)
	err := db.Where("project_id = ?", projectId).Delete(&gorm_model.ProjectPhoto{}).Error
	if err != nil {
		return err
	}
	return nil
}

func DeleteProductPhotoByProductID(ctx context.Context, productID int64) error {
	db := GetReadDB(ctx)
	err := db.Where("product_id = ?", productID).Delete(&gorm_model.YounggeeProductPhoto{}).Error
	if err != nil {
		return err
	}
	return nil
}
func CreateProductPhoto(ctx context.Context, productPhotos []gorm_model.YounggeeProductPhoto) error {
	db := GetReadDB(ctx)
	err := db.Create(&productPhotos).Error
	if err != nil {
		return err
	}
	return nil
}

func DeletePhotoUrl(ctx context.Context, photoUrl string) error {
	db := GetReadDB(ctx)
	err := db.Where("photo_url = ?", photoUrl).Delete(&gorm_model.YounggeeProductPhoto{}).Error
	if err != nil {
		return err
	}
	return nil
}
