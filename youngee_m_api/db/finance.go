package db

import (
	"context"
	"fmt"
	"reflect"
	"strconv"
	"strings"
	"time"
	"youngee_m_api/consts"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func GetWithdrawRecords(ctx context.Context, pageSize, pageNum int32, req *http_model.WithdrawalRecordsRequest, condition *common_model.WithdrawRecordsCondition) (*http_model.WithdrawalRecordsPreview, error) {
	db := GetReadDB(ctx)
	var withdrawRecords []*gorm_model.YounggeeWithdrawRecord
	db = db.Debug().Model(gorm_model.YounggeeWithdrawRecord{})
	conditionType := reflect.TypeOf(condition).Elem()
	conditionValue := reflect.ValueOf(condition).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if tag == "submit_at" && value.Interface() != "" {
			db = db.Where(fmt.Sprintf("submit_at like '%s%%'", value.Interface()))
		}
		if tag == "withdraw_at" && value.Interface() != "" {
			db = db.Where(fmt.Sprintf("withdraw_at like '%s%%'", value.Interface()))
		}
		if !util.IsBlank(value) && tag != "submit_at" && tag != "withdraw_at" {
			db = db.Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		}
	}
	if req.TalentName != "" {
		fmt.Println("TalentName:", req.TalentName)
		db1 := GetReadDB(ctx)
		var talentId string
		db1.Model(gorm_model.YoungeeTalentInfo{}).Select("id").Where(fmt.Sprintf("talent_wx_nickname like '%%%s%%'", req.TalentName)).Find(&talentId)
		db = db.Where("talent_id = ?", talentId)
	}
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetWithdrawRecords] error query mysql total, err:%+v", err)
		return nil, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("submit_at").Limit(int(limit)).Offset(int(offset)).Find(&withdrawRecords).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetWithdrawRecords] error query mysql limit, err:%+v", err)
		return nil, err
	}
	var talentIds []string
	for _, withdrawRecord := range withdrawRecords {
		talentIds = append(talentIds, withdrawRecord.TalentID)
	}
	talentIdToTalentInfoMap := make(map[string]gorm_model.YoungeeTalentInfo)
	for _, talentId := range talentIds {
		db1 := GetReadDB(ctx)
		talentInfo := gorm_model.YoungeeTalentInfo{}
		db1.Model(gorm_model.YoungeeTalentInfo{}).Where("id = ?", talentId).Find(&talentInfo)
		talentIdToTalentInfoMap[talentId] = talentInfo
	}
	var withdrawRecordsDatas []*http_model.WithdrawalRecordsData
	for _, withdrawRecord := range withdrawRecords {
		withdrawRecordsData := new(http_model.WithdrawalRecordsData)
		withdrawRecordsData.WithdrawId = withdrawRecord.WithdrawID
		withdrawRecordsData.TalentId = withdrawRecord.TalentID
		withdrawRecordsData.TalentName = talentIdToTalentInfoMap[withdrawRecord.TalentID].TalentWxNickname
		withdrawRecordsData.WithdrawAmount = float32(withdrawRecord.WithdrawAmount)
		withdrawRecordsData.AmountPayable = float32(withdrawRecord.AmountPayable)
		withdrawRecordsData.ReceiveInfo = withdrawRecord.ReceiveInfo
		withdrawRecordsData.BankType = withdrawRecord.BankType
		withdrawRecordsData.Phone = talentIdToTalentInfoMap[withdrawRecord.TalentID].TalentPhoneNumber
		withdrawRecordsData.SubmitAt = conv.MustString(withdrawRecord.SubmitAt, "")[0:19]
		withdrawRecordsData.WithdrawAt = conv.MustString(withdrawRecord.WithdrawAt, "")[0:19]
		withdrawRecordsDatas = append(withdrawRecordsDatas, withdrawRecordsData)
	}
	var withdrawRecordsPreview http_model.WithdrawalRecordsPreview
	withdrawRecordsPreview.WithdrawalRecordsData = withdrawRecordsDatas
	withdrawRecordsPreview.Total = total
	return &withdrawRecordsPreview, nil
}

func GetWithdrawRecord(ctx context.Context, req *http_model.GetWithdrawalRecordRequest) (*http_model.WithdrawalRecordPreview, error) {
	db := GetReadDB(ctx)
	//fmt.Println("talentId:", req.TalentId)
	var withdrawRecords []*gorm_model.YounggeeWithdrawRecord
	db = db.Debug().Model(gorm_model.YounggeeWithdrawRecord{}).Where("talent_id = ? AND  status = ?", req.TalentId, 2).Find(&withdrawRecords)
	var withdrawRecordsPreview http_model.WithdrawalRecordPreview
	var withdrawRecordsDatas []*http_model.WithdrawalRecordData
	for _, withdrawRecord := range withdrawRecords {
		withdrawRecordData := new(http_model.WithdrawalRecordData)
		withdrawRecordData.WithdrawAmount = float32(withdrawRecord.WithdrawAmount)
		withdrawRecordData.WithdrawAt = conv.MustString(withdrawRecord.WithdrawAt, "")[0:19]
		withdrawRecordsDatas = append(withdrawRecordsDatas, withdrawRecordData)
	}
	withdrawRecordsPreview.WithdrawalRecordData = withdrawRecordsDatas
	return &withdrawRecordsPreview, nil
}

func GetWithdrawNums(ctx context.Context) (*http_model.WithdrawNums, error) {
	var withdrawNums int64
	db := GetReadDB(ctx)
	err := db.Model(gorm_model.YounggeeWithdrawRecord{}).Where("status = 1").Count(&withdrawNums).Error
	if err != nil {
		return nil, err
	}
	WithdrawNums := new(http_model.WithdrawNums)
	WithdrawNums.WithdrawNums = int(withdrawNums)
	return WithdrawNums, err
}

func GetInvoiceNums(ctx context.Context) (*http_model.InvoiceNums, error) {
	var invoiceNums int64
	db := GetReadDB(ctx)
	err := db.Model(gorm_model.YounggeeInvoiceRecord{}).Where("status = 1").Count(&invoiceNums).Error
	if err != nil {
		return nil, err
	}
	InvoiceNums := new(http_model.InvoiceNums)
	InvoiceNums.InvoiceNums = int(invoiceNums)
	return InvoiceNums, err
}

func GetRechargeNums(ctx context.Context) (*http_model.RechargeNums, error) {
	var rechargeNums int64
	db := GetReadDB(ctx)
	err := db.Model(gorm_model.YounggeeRechargeRecord{}).Where("status = 1").Count(&rechargeNums).Error
	if err != nil {
		return nil, err
	}
	RechargeNums := new(http_model.RechargeNums)
	RechargeNums.RechargeNums = rechargeNums
	return RechargeNums, err
}

func ConfirmWithdrawal(ctx context.Context, withdrawId string) error {
	db := GetReadDB(ctx)
	db2 := GetReadDB(ctx)
	withdrawInfo := gorm_model.YounggeeWithdrawRecord{}
	db = db.Debug().Model(gorm_model.YounggeeWithdrawRecord{}).Where("withdraw_id = ?", withdrawId).Find(&withdrawInfo)
	db2.Debug().Where("withdraw_id = ?", withdrawId).Updates(gorm_model.YounggeeWithdrawRecord{Status: 2, WithdrawAt: time.Now()})
	taskIdLists := strings.Split(withdrawInfo.TaskIDList, ",")
	for _, taskId := range taskIdLists {
		db1 := GetReadDB(ctx)
		err := db1.Debug().Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(gorm_model.YoungeeTaskInfo{WithdrawStatus: 4}).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[finance db] Update YoungeeTaskInfo error,err:%+v", err)
			return err
		}

		err = CreateMessageByTaskId(ctx, 6, 1, taskId)
		if err != nil {
			logrus.WithContext(ctx).Errorf("[ConfirmWithdrawal] call CreateMessageByTaskId error,err:%+v", err)
			return err
		}
	}
	db3 := GetReadDB(ctx)
	db3.Debug().Model(gorm_model.YoungeeTalentInfo{}).Where("id = ?", withdrawInfo.TalentID).Updates(
		map[string]interface{}{
			"withdrawing": gorm.Expr("withdrawing - ?", withdrawInfo.WithdrawAmount),
			"withdrawed":  gorm.Expr("withdrawed + ?", withdrawInfo.WithdrawAmount)})
	return nil
}

func GetBankInfo(ctx context.Context, req *http_model.GetBankInfoRequest) (*http_model.BankInfo, error) {
	//db := GetReadDB(ctx)
	//if req.BankId == "" {
	//	return nil, nil
	//}
	//var infoBank *gorm_model.InfoBank
	//db.Debug().Model(gorm_model.InfoBank{}).Where("id = ?", req.BankId).First(&infoBank)
	db1 := GetReadDB(ctx)
	var infoRegion *gorm_model.InfoRegion
	db1.Debug().Model(gorm_model.InfoRegion{}).Where("self_code = ?", conv.MustInt(req.BankOpenAddress, 0)).First(&infoRegion)

	provinceCode := conv.MustString(req.BankOpenAddress, "")[0:2] + "0000"
	var province *gorm_model.InfoRegion
	db1.Debug().Model(gorm_model.InfoRegion{}).Where("self_code = ?", conv.MustInt(provinceCode, 0)).First(&province)

	cityCode := conv.MustString(req.BankOpenAddress, "")[0:4] + "00"
	var city *gorm_model.InfoRegion
	db1.Debug().Model(gorm_model.InfoRegion{}).Where("self_code = ?", conv.MustInt(cityCode, 0)).First(&city)
	data := new(http_model.BankInfo)
	//data.BankName = infoBank.Name
	data.BankOpenAddress = province.RegionName + city.RegionName + infoRegion.RegionName
	//db.Model(gorm_model.InfoBank{}).Where("")
	return data, nil
}

// GetEnterpriseIDByBusiness 根据企业名称查找企业ID
func GetEnterpriseIDByBusiness(ctx context.Context, BusinessName string) int64 {
	db := GetReadDB(ctx)
	var EnterpriseID int64
	db = db.Model([]gorm_model.Enterprise{}).Select("enterprise_id").Where("business_name", BusinessName).First(&EnterpriseID)
	return EnterpriseID
}

// GetEnterpriseIDByUserId 根据企业名称查找企业ID
func GetEnterpriseIDByUserId(ctx context.Context, UserId int64) int64 {
	db := GetReadDB(ctx)
	var EnterpriseID int64
	db = db.Model([]gorm_model.Enterprise{}).Select("enterprise_id").Where("user_id", UserId).First(&EnterpriseID)
	return EnterpriseID
}

// GetUserIDByUsername 根据用户名称查UserID
func GetUserIDByUsername(ctx context.Context, Username string) int64 {
	db := GetReadDB(ctx)
	var UserID int64
	db = db.Model([]gorm_model.YounggeeUser{}).Select("id").Where(fmt.Sprintf("username like '%%%s%%'", Username)).First(&UserID)
	return UserID
}

func GetInvoiceRecords(ctx context.Context, req *http_model.InvoiceRecordsRequest, condition *common_model.InvoiceRecordsCondition) (*http_model.InvoiceRecordsData, error) {
	db := GetReadDB(ctx)
	var invoiceRecords []*gorm_model.YounggeeInvoiceRecord
	db = db.Debug().Model(gorm_model.YounggeeInvoiceRecord{}).Where("status = ?", req.InvoiceStatus)
	conditionType := reflect.TypeOf(condition).Elem()
	conditionValue := reflect.ValueOf(condition).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if tag == "submit_at" && value.Interface() != "" {
			db = db.Where(fmt.Sprintf("submit_at like '%s%%'", value.Interface()))
		}
		if tag == "billing_at" && value.Interface() != "" {
			db = db.Where(fmt.Sprintf("billing_at like '%s%%'", value.Interface()))
		}
	}
	if req.Username != "" {
		UserID := GetUserIDByUsername(ctx, req.Username)
		enterpriseId := GetEnterpriseIDByUserId(ctx, UserID)
		db = db.Where("enterprise_id = ?", enterpriseId)
	}
	if req.UserId != 0 {
		enterpriseId := GetEnterpriseIDByUserId(ctx, req.UserId)
		db = db.Where("enterprise_id = ?", enterpriseId)
	}
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetInvoiceRecords] error query mysql total, err:%+v", err)
		return nil, err
	}
	if req.InvoiceStatus != 3 {
		db.Order("submit_at")
	} else {
		db.Order("billing_at desc")
	}
	// 查询该页数据
	limit := req.PageSize
	offset := req.PageSize * req.PageNum // assert pageNum start with 0
	err := db.Limit(int(limit)).Offset(int(offset)).Find(&invoiceRecords).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetInvoiceRecords] error query mysql limit, err:%+v", err)
		return nil, err
	}
	enterpriseIdToUserInfoMap := make(map[string]gorm_model.Enterprise)
	regionAddressMap := make(map[string]string)
	for _, invoiceRecord := range invoiceRecords {
		if _, ok := enterpriseIdToUserInfoMap[invoiceRecord.EnterpriseID]; !ok {
			db1 := GetReadDB(ctx)
			enterpriseInfo := gorm_model.Enterprise{}
			db1.Model(gorm_model.Enterprise{}).Where("enterprise_id = ?", invoiceRecord.EnterpriseID).Find(&enterpriseInfo)
			enterpriseIdToUserInfoMap[invoiceRecord.EnterpriseID] = enterpriseInfo
		}
		if _, ok := regionAddressMap[invoiceRecord.EnterpriseID]; !ok {
			db1 := GetReadDB(ctx)
			var regionCode string
			db1.Model(gorm_model.YounggeeInvoiceAddress{}).Select("region_code").Where("enterprise_id = ?", invoiceRecord.EnterpriseID).Find(&regionCode)
			regionAddressMap[invoiceRecord.EnterpriseID] = GetRegion(ctx, conv.MustInt(regionCode, 0))
		}
	}
	var InvoiceRecords []*http_model.InvoiceRecordsPreviews
	for _, invoiceRecord := range invoiceRecords {
		InvoiceRecord := new(http_model.InvoiceRecordsPreviews)
		InvoiceRecord.BillingId = invoiceRecord.BillingID
		InvoiceRecord.InvoiceInfo = invoiceRecord.InvoiceSnap
		InvoiceRecord.AddressInfo = invoiceRecord.AddressSnap
		InvoiceRecord.InvoiceAddress = regionAddressMap[invoiceRecord.EnterpriseID]
		InvoiceRecord.InvoiceType = invoiceRecord.InvoiceType
		InvoiceRecord.Amount = invoiceRecord.InvoiceAmount
		InvoiceRecord.Phone = invoiceRecord.Phone
		InvoiceRecord.ShipmentNumber = invoiceRecord.ShipmentNumber
		InvoiceRecord.BusinessName = enterpriseIdToUserInfoMap[invoiceRecord.EnterpriseID].BusinessName
		InvoiceRecord.UserId = invoiceRecord.EnterpriseID
		InvoiceRecord.Username = GetUsernameByUserID(ctx, enterpriseIdToUserInfoMap[invoiceRecord.EnterpriseID].UserID)
		InvoiceRecord.SubmitAt = conv.MustString(invoiceRecord.SubmitAt, "")[:19]
		InvoiceRecord.BillingAt = conv.MustString(invoiceRecord.BillingAt, "")[:19]
		InvoiceRecords = append(InvoiceRecords, InvoiceRecord)
	}
	var InvoiceRecordsData http_model.InvoiceRecordsData
	InvoiceRecordsData.InvoiceRecordsPreviews = InvoiceRecords
	InvoiceRecordsData.Total = strconv.FormatInt(total, 10)
	return &InvoiceRecordsData, nil
}

func ConfirmInvoice(ctx context.Context, request *http_model.ConfirmInvoiceRequest) error {
	db := GetReadDB(ctx)
	return db.Model(gorm_model.YounggeeInvoiceRecord{}).Where("billing_id = ?", request.BillingId).Updates(
		gorm_model.YounggeeInvoiceRecord{
			BillingAt:      time.Now(),
			ShipmentNumber: request.ShipmentNumber,
			Status:         2,
		}).Error
}

func GetUsernameByUserID(ctx context.Context, UserID int64) (username string) {
	db := GetReadDB(ctx)
	db = db.Model([]gorm_model.YounggeeUser{}).Select("username").Where("id", UserID).First(&username)
	return username
}

func GetRechargeRecords(ctx context.Context, req *http_model.GetRechargeRecordsRequest, condition *common_model.RechargeRecordsCondition) (*http_model.RechargeRecordsData, error) {
	db := GetReadDB(ctx)
	var rechargeRecords []*gorm_model.YounggeeRechargeRecord
	db = db.Debug().Model(gorm_model.YounggeeRechargeRecord{}).Where("status = ?", req.Status)
	conditionType := reflect.TypeOf(condition).Elem()
	conditionValue := reflect.ValueOf(condition).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if tag == "commit_at" && value.Interface() != "" {
			db = db.Where(fmt.Sprintf("commit_at like '%s%%'", value.Interface()))
		}
		if tag == "confirm_at" && value.Interface() != "" {
			db = db.Where(fmt.Sprintf("confirm_at like '%s%%'", value.Interface()))
		}
	}
	if req.Username != "" {
		UserID := GetUserIDByUsername(ctx, req.Username)
		enterpriseId := GetEnterpriseIDByUserId(ctx, UserID)
		db = db.Where("enterprise_id = ?", enterpriseId)
	}
	if req.UserId != 0 {
		enterpriseId := GetEnterpriseIDByUserId(ctx, req.UserId)
		db = db.Where("enterprise_id = ?", enterpriseId)
	}
	if req.RechargeMethod == 1 {
		db = db.Where("recharge_method = ?", 1)
	} else if req.RechargeMethod == 2 {
		db = db.Where("recharge_method = ?", 2)
	} else if req.RechargeMethod == 3 {
		db = db.Where("recharge_method = ?", 3)
	}
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetRechargeRecords] error query mysql total, err:%+v", err)
		return nil, err
	}
	if req.Status == 1 {
		db = db.Order("commit_at")
	} else {
		db = db.Order("confirm_at desc")
	}
	// 查询该页数据
	limit := req.PageSize
	offset := req.PageSize * req.PageNum // assert pageNum start with 0
	err := db.Limit(int(limit)).Offset(int(offset)).Find(&rechargeRecords).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetRechargeRecords] error query mysql limit, err:%+v", err)
		return nil, err
	}
	var enterpriseIds []string
	for _, rechargeRecord := range rechargeRecords {
		enterpriseIds = append(enterpriseIds, rechargeRecord.EnterpriseID)
	}
	util.RemoveStrRepByMap(enterpriseIds)
	enterpriseIdToUserInfoMap := make(map[string]gorm_model.Enterprise)
	db1 := GetReadDB(ctx)
	for _, v := range enterpriseIds {
		enterpriseInfo := gorm_model.Enterprise{}
		db1.Model(gorm_model.Enterprise{}).Where("enterprise_id = ?", v).Find(&enterpriseInfo)
		enterpriseIdToUserInfoMap[v] = enterpriseInfo
	}
	var RechargeRecords []*http_model.RechargeRecordsPreview
	for _, rechargeRecord := range rechargeRecords {
		RechargeRecord := new(http_model.RechargeRecordsPreview)
		RechargeRecord.RechargeId = rechargeRecord.RechargeID
		RechargeRecord.EnterpriseID = rechargeRecord.EnterpriseID
		RechargeRecord.RechargeAmount = rechargeRecord.RechargeAmount
		RechargeRecord.ConfirmAt = conv.MustString(rechargeRecord.ConfirmAt, "")[:19]
		RechargeRecord.CommitAt = conv.MustString(rechargeRecord.CommitAt, "")[:19]
		RechargeRecord.Phone = rechargeRecord.Phone
		RechargeRecord.TransferVoucher = rechargeRecord.TransferVoucherUrl
		RechargeRecord.RechargeMethod = consts.GetRechargeMethod(rechargeRecord.RechargeMethod)
		RechargeRecord.UserId = rechargeRecord.EnterpriseID
		RechargeRecord.Username = GetUsernameByUserID(ctx, enterpriseIdToUserInfoMap[rechargeRecord.EnterpriseID].UserID)
		RechargeRecord.BusinessName = enterpriseIdToUserInfoMap[rechargeRecord.EnterpriseID].BusinessName
		RechargeRecords = append(RechargeRecords, RechargeRecord)
	}
	var RechargeRecordsData http_model.RechargeRecordsData
	RechargeRecordsData.RechargeRecordsPreview = RechargeRecords
	RechargeRecordsData.Total = conv.MustString(total, "")
	return &RechargeRecordsData, nil
}

func OperateRecharge(ctx context.Context, req *http_model.OperateRechargeRequest) error {
	db := GetReadDB(ctx)
	db1 := GetReadDB(ctx)
	err := db1.Model(gorm_model.Enterprise{}).Where("enterprise_id = ?", req.EnterpriseId).Updates(map[string]interface{}{
		"balance":           gorm.Expr("balance + ?", req.RechargeAmount),
		"available_balance": gorm.Expr("available_balance + ?", req.RechargeAmount)}).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[OperateRecharge] error Updates balance, err:%+v", err)
		return err
	}
	err1 := db.Model(gorm_model.YounggeeRechargeRecord{}).Where("recharge_id = ?", req.RechargeId).Updates(gorm_model.YounggeeRechargeRecord{
		Status:        2,
		InvoiceStatus: 2,
		ConfirmAt:     time.Now(),
	}).Error
	if err1 != nil {
		logrus.WithContext(ctx).Errorf("[OperateRecharge] error Updates Status, err:%+v", err)
		return err1
	}
	if req.Method == 1 {
		db2 := GetReadDB(ctx)
		db2.Model(gorm_model.YounggeeRechargeRecord{}).Where("recharge_id = ?", req.RechargeId).Updates(gorm_model.YounggeeRechargeRecord{
			RechargeAmount: req.RechargeAmount,
		})
	}
	return nil
}
