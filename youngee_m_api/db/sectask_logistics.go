package db

import (
	"context"
	"youngee_m_api/model/gorm_model"
)

func CreateSecTaskLogistics(ctx context.Context, logistics gorm_model.YoungeeTaskLogistics) (*int64, error) {
	db := GetWriteDB(ctx)
	err := db.Create(&logistics).Error
	if err != nil {
		return nil, err
	}

	return &logistics.LogisticsID, nil
}

func UpdateSecTaskLogistics(ctx context.Context, updateData gorm_model.YoungeeTaskLogistics) (*int64, error) {
	db := GetWriteDB(ctx)
	whereCondition := gorm_model.YoungeeTaskLogistics{
		LogisticsID: updateData.LogisticsID,
	}
	err := db.Where(whereCondition).Updates(&updateData).Error
	if err != nil {
		return nil, err
	}

	return &updateData.LogisticsID, nil
}
