package db

import (
	"context"
	"fmt"
	"strconv"
	"time"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"

	"github.com/sirupsen/logrus"
)

func GetLogisticsNumberInfo(ctx context.Context, projectId string, strategyIds []int64) (*http_model.GetLogisticsNumberInfoData, error) {
	var LogisticsNumberInfoDataList http_model.GetLogisticsNumberInfoData
	for _, strategyId := range strategyIds {
		var LogisticsNumberInfoData http_model.LogisticsNumberInfo
		LogisticsNumberInfoData.StrategyId = strategyId
		db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err := db.Where("logistics_status = 1").Count(&LogisticsNumberInfoData.UndeliveredNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("logistics_status = 2").Count(&LogisticsNumberInfoData.DeliveredNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("logistics_status = 3").Count(&LogisticsNumberInfoData.SignedNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Count(&LogisticsNumberInfoData.DeliverNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		LogisticsNumberInfoDataList.LogisticsNumberInfoList = append(LogisticsNumberInfoDataList.LogisticsNumberInfoList, &LogisticsNumberInfoData)
	}
	return &LogisticsNumberInfoDataList, nil
}

func GetReviewNumberInfo(ctx context.Context, projectId string, strategyIds []int64) (*http_model.GetReviewNumberInfoData, error) {
	var ReviewNumberInfoDataList http_model.GetReviewNumberInfoData

	for _, strategyId := range strategyIds {
		var ReviewNumberInfoData http_model.ReviewNumberInfo
		ReviewNumberInfoData.StrategyId = strategyId
		db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err := db.Where("task_stage = 8").Count(&ReviewNumberInfoData.ScriptUnreviewNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("task_stage > 8").Count(&ReviewNumberInfoData.ScriptPassedNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("task_stage = 10").Count(&ReviewNumberInfoData.SketchUnreviewNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("task_stage > 10").Count(&ReviewNumberInfoData.SketchPassedNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Count(&ReviewNumberInfoData.ReviewNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}

		ReviewNumberInfoDataList.ReviewNumberInfoList = append(ReviewNumberInfoDataList.ReviewNumberInfoList, &ReviewNumberInfoData)
	}
	return &ReviewNumberInfoDataList, nil
}

func GetLinkNumberInfo(ctx context.Context, projectId string, strategyIds []int64) (*http_model.GetLinkNumberInfoData, error) {
	var LinkNumberInfoDataList http_model.GetLinkNumberInfoData

	for _, strategyId := range strategyIds {
		var LinkNumberInfoData http_model.LinkNumberInfo
		LinkNumberInfoData.StrategyId = strategyId
		db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err := db.Where("task_stage = 12").Count(&LinkNumberInfoData.LinkUnreviewNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("task_stage > 12").Count(&LinkNumberInfoData.LinkPassedNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Count(&LinkNumberInfoData.LinkNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}

		LinkNumberInfoDataList.LinkNumberInfoList = append(LinkNumberInfoDataList.LinkNumberInfoList, &LinkNumberInfoData)
	}
	return &LinkNumberInfoDataList, nil
}

func GetDataNumberInfo(ctx context.Context, projectId string, strategyIds []int64) (*http_model.GetDataNumberInfoData, error) {
	var DataNumberInfoDataList http_model.GetDataNumberInfoData

	for _, strategyId := range strategyIds {
		var DataNumberInfoData http_model.DataNumberInfo
		DataNumberInfoData.StrategyId = strategyId
		db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err := db.Where("task_stage = 14").Count(&DataNumberInfoData.DataUnreviewNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("task_stage = 15").Count(&DataNumberInfoData.DataPassedNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2 and task_stage < 16", projectId, strategyId)
		err = db.Count(&DataNumberInfoData.DataNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}

		DataNumberInfoDataList.DataNumberInfoList = append(DataNumberInfoDataList.DataNumberInfoList, &DataNumberInfoData)
	}
	return &DataNumberInfoDataList, nil
}

func GetDefaultNumberInfo(ctx context.Context, projectId string, strategyIds []int64) (*http_model.GetDefaultNumberInfoData, error) {
	var DefaultNumberInfoDataList http_model.GetDefaultNumberInfoData
	for _, strategyId := range strategyIds {
		var DefaultNumberInfoData http_model.DefaultNumberInfo
		DefaultNumberInfoData.StrategyId = strategyId
		db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err := db.Where("complete_status <> 4").Count(&DefaultNumberInfoData.ShouldFinishNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("cur_default_type = 1").Count(&DefaultNumberInfoData.UnuploadScriptNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("cur_default_type = 3").Count(&DefaultNumberInfoData.UnuploadSketchNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("cur_default_type = 5").Count(&DefaultNumberInfoData.UnuploadLinkNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("cur_default_type = 7").Count(&DefaultNumberInfoData.UnuploadDataNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("complete_status = 4").Count(&DefaultNumberInfoData.TerminateNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}

		DefaultNumberInfoDataList.DefaultNumberInfoList = append(DefaultNumberInfoDataList.DefaultNumberInfoList, &DefaultNumberInfoData)
	}
	return &DefaultNumberInfoDataList, nil
}

func GetFinishNumberInfo(ctx context.Context, projectId string, strategyIds []int64) (*http_model.GetFinishNumberInfoData, error) {
	var FinishNumberInfoDataList http_model.GetFinishNumberInfoData

	for _, strategyId := range strategyIds {
		var FinishNumberInfoData http_model.FinishNumberInfo
		FinishNumberInfoData.StrategyId = strategyId
		db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err := db.Count(&FinishNumberInfoData.ShouldFinishNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and strategy_id = ? and task_status = 2", projectId, strategyId)
		err = db.Where("task_stage = 15").Count(&FinishNumberInfoData.FinishedNumber).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
			return nil, err
		}
		FinishNumberInfoDataList.FinishNumberInfoList = append(FinishNumberInfoDataList.FinishNumberInfoList, &FinishNumberInfoData)
	}
	return &FinishNumberInfoDataList, nil
}

func GetLogisticsNum() [][]string {
	var logisticNumsInfos [][]string
	db := GetReadDB(context.Background())
	var logisticInfos []*gorm_model.YoungeeTaskLogistics
	db.Model(gorm_model.YoungeeTaskLogistics{}).Where("things_type = 1 AND status = 0").Find(&logisticInfos)
	for _, logisticInfo := range logisticInfos {
		var logisticNumsInfo []string
		logisticNumsInfo = append(logisticNumsInfo, logisticInfo.CompanyName, logisticInfo.LogisticsNumber, strconv.FormatInt(logisticInfo.LogisticsID, 10))
		logisticNumsInfos = append(logisticNumsInfos, logisticNumsInfo)
	}
	fmt.Println("logisticNumsInfos:", logisticNumsInfos)
	return logisticNumsInfos
}

func SignLogistic(logisticId int64) string {
	db := GetReadDB(context.Background())
	var taskId, projectId string
	var contentType int64
	db.Model(gorm_model.YoungeeTaskLogistics{}).Select("task_id").Where("logistics_id = ?", logisticId).Find(&taskId)
	db.Model(gorm_model.YoungeeTaskInfo{}).Select("project_id").Where("task_id = ?", taskId).Find(&projectId)
	db.Model(gorm_model.ProjectInfo{}).Select("content_type").Where("project_id = ?", projectId).Find(&contentType)
	t := time.Now()
	db.Model(gorm_model.YoungeeTaskLogistics{}).Where("logistics_id = ?", logisticId).Updates(&gorm_model.YoungeeTaskLogistics{SignedTime: &t, Status: 1})
	if contentType == 1 {
		db.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(&gorm_model.YoungeeTaskInfo{LogisticsStatus: 3, TaskStage: 9})
		fmt.Printf("任务 %v 物流状态为已签收，已更新任务状态为 待传初稿\n", taskId)
	} else {
		db.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(&gorm_model.YoungeeTaskInfo{LogisticsStatus: 3, TaskStage: 7})
		fmt.Printf("任务 %v 物流状态为已签收，已更新任务状态为 待传脚本\n", taskId)
	}
	return taskId
}

func GetSpecialInviteNumber(ctx context.Context, projectId string) (*http_model.GetSpecialInviteNumberData, error) {
	var specialInviteNumberData http_model.GetSpecialInviteNumberData
	db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_stage != 3", projectId)
	err := db.Where("task_status <> 2").Count(&specialInviteNumberData.UnpassNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ?", projectId)
	err = db.Where("task_status = 2").Count(&specialInviteNumberData.PassNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	return &specialInviteNumberData, nil
}

func GetSpecialLogisticNumber(ctx context.Context, projectId string) (*http_model.GetSpecialLogisticNumberData, error) {
	var specialLogisticNumberData http_model.GetSpecialLogisticNumberData
	db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_stage != 3", projectId)
	err := db.Where("task_status = 2").Count(&specialLogisticNumberData.DeliverNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetSpecialLogisticNumber] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("logistics_status = 3").Count(&specialLogisticNumberData.SignedNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetSpecialLogisticNumber] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("logistics_status = 1").Count(&specialLogisticNumberData.UndeliveredNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetSpecialLogisticNumber] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("logistics_status = 2").Count(&specialLogisticNumberData.DeliveredNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetSpecialLogisticNumber] error query mysql total, err:%+v", err)
		return nil, err
	}
	return &specialLogisticNumberData, nil
}

func GetSpecialReviewNumber(ctx context.Context, projectId string) (*http_model.GetSpecialReviewNumberData, error) {
	var specialReviewNumber http_model.GetSpecialReviewNumberData
	db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err := db.Where("").Count(&specialReviewNumber.ReviewNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("task_stage = 8").Count(&specialReviewNumber.ScriptUnreviewNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("task_stage > 8").Count(&specialReviewNumber.ScriptPassedNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("task_stage = 10").Count(&specialReviewNumber.SketchUnreviewNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("task_stage > 10").Count(&specialReviewNumber.SketchPassedNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	return &specialReviewNumber, nil
}

func GetSpecialLinkNumber(ctx context.Context, projectId string) (*http_model.GetSpecialLinkNumberData, error) {
	var specialLinkNumber http_model.GetSpecialLinkNumberData
	db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err := db.Where("").Count(&specialLinkNumber.LinkNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("task_stage = 12").Count(&specialLinkNumber.LinkUnreviewNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("task_stage > 12").Count(&specialLinkNumber.LinkPassedNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	return &specialLinkNumber, nil
}

func GetSpecialDataNumber(ctx context.Context, projectId string) (*http_model.GetSpecialDataNumberData, error) {
	var specialDataNumber http_model.GetSpecialDataNumberData
	db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err := db.Where("").Count(&specialDataNumber.DataNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("task_stage = 14").Count(&specialDataNumber.DataUnreviewNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2", projectId)
	err = db.Where("task_stage > 14").Count(&specialDataNumber.DataPassedNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, err
	}
	return &specialDataNumber, nil
}

func GetSpecialSettleNumber(ctx context.Context, projectId string) (*http_model.GetSpecialSettleNumberData, error) {
	var specialSettleNumber http_model.GetSpecialSettleNumberData
	db := GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage = 15", projectId)
	err := db.Where("").Count(&specialSettleNumber.SettleNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetSpecialSettleNumber] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage = 15 and settle_status = 1", projectId)
	err = db.Where("task_stage = 14").Count(&specialSettleNumber.UnsettleNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetSpecialSettleNumber] error query mysql total, err:%+v", err)
		return nil, err
	}
	db = GetReadDB(ctx).Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage = 15 and settle_status = 2", projectId)
	err = db.Where("task_stage > 14").Count(&specialSettleNumber.SettledNumber).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetSpecialSettleNumber] error query mysql total, err:%+v", err)
		return nil, err
	}
	return &specialSettleNumber, nil
}
