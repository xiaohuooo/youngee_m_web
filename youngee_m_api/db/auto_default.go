package db

import (
	"context"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/model/gorm_model"
)

func GetLastAutoDefaultID() (int, error) {
	db := GetReadDB(context.Background())
	// 查找最后一个
	LastDefault := gorm_model.InfoAutoDefaultHandle{}
	result := db.Last(&LastDefault)
	err := result.Error
	if err != nil {
		log.Println("DB GetLastAutoDefaultID:", err)
		return 0, err
	}
	//fmt.Printf("auto task %+v %+v", result, LastTask)
	return LastDefault.AutoDefaultID, nil
}
