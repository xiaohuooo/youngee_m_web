package db

import (
	"context"
	"fmt"
	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"reflect"
	"time"
	"youngee_m_api/consts"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/util"
)

// GetUser 查找用户，查不到返回空
func GetUser(ctx context.Context, User string) (*gorm_model.YounggeeUser, error) {
	db := GetReadDB(ctx)
	user := &gorm_model.YounggeeUser{}
	err := db.Model(user).Where("user = ?", User).First(user).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			fmt.Println("record not found")
			return nil, nil
		}
		return nil, err
	}
	return user, nil
}

// GetUserByID 查不到返回空
func GetUserByID(ctx context.Context, ID int64) (*gorm_model.YounggeeUser, error) {
	db := GetReadDB(ctx)
	user := &gorm_model.YounggeeUser{}
	err := db.Model(user).Where("id = ?", ID).First(user).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			fmt.Println("record not found")
			return nil, nil
		}
		return nil, err
	}
	return user, nil
}

// GetAllUser 查找所有用户
func GetAllUser(ctx context.Context) ([]string, error) {
	db := GetReadDB(ctx)
	db = db.Debug().Model([]gorm_model.YounggeeUser{}).Where("role = ? or role = ?", "1", "2")
	var user []gorm_model.YounggeeUser
	db.Find(&user)
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetAllUser] error query mysql total, err:%+v", err)
		return nil, err
	}
	var userList []string
	for _, User := range user {
		userList = append(userList, User.User)
	}
	return userList, nil
}

// GetUserList 获取员工用户列表
func GetUserList(ctx context.Context, pageNum, pageSize int32) (*http_model.UserListData, error) {
	db := GetReadDB(ctx)
	db = db.Debug().Model([]gorm_model.YounggeeUser{}).Where("role = ? or role = ?", "1", "2")
	var user []gorm_model.YounggeeUser
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetUserList] error query mysql total, err:%+v", err)
		return nil, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("created_at desc").Limit(int(limit)).Offset(int(offset)).Find(&user).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetUserList] error query mysql find, err:%+v", err)
		return nil, err
	}
	userList := new(http_model.UserListData)
	userList.UserListPreview = pack.MGormUserListToHttpUserListPreview(user)
	userList.Total = conv.MustString(total, "")
	return userList, nil
}

func UpdateUserInfo(ctx context.Context, req *http_model.UpdateUserInfoRequest) (string, error) {
	db := GetReadDB(ctx)
	user, username, role, password, email, phone := req.User, req.Username, req.Role, req.Password, req.Email, req.Phone
	db = db.Debug().Model([]gorm_model.YounggeeUser{}).Where("user = ?", user)
	err := db.Updates(map[string]interface{}{
		"username": username, "password": password, "email": email, "phone": phone, "role": role}).Error
	if err != nil {
		return "", err
	}
	return "更新成功", nil
}

func CreateUser(ctx context.Context, req *http_model.CreateUserRequest) (string, error) {
	db := GetReadDB(ctx)
	username, role, password, email, phone := req.Username, req.Role, req.Password, req.Email, req.Phone
	var IsUserPhone gorm_model.YounggeeUser
	err := db.Debug().Where(" phone = ?", req.Phone).First(&IsUserPhone).Error
	if err == nil {
		return "手机号已存在，不能再次创建", nil
	}
	var user gorm_model.YounggeeUser
	var userInfo gorm_model.YounggeeUser
	getMonth := time.Now().Format("01") //获取月
	getDay := time.Now().Day()          //获取日
	dayString := ""
	if getDay < 10 {
		dayString = conv.MustString(getDay, "")
		dayString = "0" + dayString
	} else {
		dayString = conv.MustString(getDay, "")
	}
	monthAndDay := conv.MustString(getMonth, "") + dayString
	err = db.Debug().Where(fmt.Sprintf(" user like '%s%%'", monthAndDay)).Last(&userInfo).Error

	num := 1
	if userInfo.User != "" {
		num = conv.MustInt(userInfo.User[4:6], 0)
		num = num + 1 //获取日
	} else {
		num = 1
	}
	numString := ""
	if num < 10 {
		numString = conv.MustString(num, "")
		numString = "0" + numString
	} else {
		numString = conv.MustString(num, "")
	}

	user.Username = username
	user.RealName = username
	user.User = monthAndDay + numString
	user.Role = role
	user.Password = password
	user.Email = email
	user.Phone = phone
	user.LastLoginTime = time.Now()
	user.CreatedAt = time.Now()
	user.UpdatedAt = time.Now()
	err = db.Debug().Create(&user).Error
	if err != nil {
		return "创建失败", err
	}
	return "创建成功", nil
}

func DisabledUser(ctx context.Context, user string) (string, error) {
	db := GetReadDB(ctx)
	err := db.Debug().Model(gorm_model.YounggeeUser{}).Where("user = ?", user).Update("user_state", "0").Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[DisabledUser] error Update mysql find, err:%+v", err)
		return "禁用失败", err
	}
	return "账号已禁止使用", nil
}

func GetEnterpriseUserList(ctx context.Context, pageSize, pageNum int32, conditions *common_model.EnterpriseUserConditions) ([]*http_model.EnterpriseUserPreview, int64, error) {
	db := GetReadDB(ctx)
	// 查询user表信息，筛选出企业用户
	db = db.Model([]gorm_model.YounggeeUser{}).Where("role = ? ", "3")
	if conditions.User != "" {
		userId := GetUserIDByEnterpriseID(ctx, conditions.User)
		db = db.Where("id = ?", userId)
	}
	// 根据 查询条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if !util.IsBlank(value) && tag != "created_at" && tag != "username" && tag != "user" {
			db = db.Debug().Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		} else if tag == "created_at" && value.Interface() != nil {
			db = db.Where(fmt.Sprintf("created_at like '%s%%'", value.Interface()))
		} else if tag == "username" && value.Interface() != nil {
			db = db.Debug().Where(fmt.Sprintf("username like '%%%s%%'", value.Interface()))
		}
	}
	var totalUser int64
	if err := db.Count(&totalUser).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetEnterpriseUserList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	var users []gorm_model.YounggeeUser
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("created_at desc").Limit(int(limit)).Offset(int(offset)).Find(&users).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetEnterpriseUserList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	// 查询 用户自增的id
	var userIds []int64
	userMap := make(map[int64]gorm_model.YounggeeUser)
	for _, user := range users {
		userIds = append(userIds, user.ID)
		userMap[user.ID] = user
	}

	db1 := GetReadDB(ctx)
	var enterprises []gorm_model.Enterprise
	db1 = db1.Model(gorm_model.Enterprise{}).Where("user_id IN ?", userIds).Find(&enterprises)

	enterpriseMap := make(map[int64]gorm_model.Enterprise)
	for _, enterprise := range enterprises {
		enterpriseMap[enterprise.UserID] = enterprise
	}

	var enterpriseUsers []*http_model.EnterpriseUser
	for _, userId := range userIds {
		enterpriseUser := new(http_model.EnterpriseUser)
		_, ok1 := userMap[userId]
		_, ok2 := enterpriseMap[userId]
		if ok1 && ok2 {
			enterpriseUser.Enterprise = enterpriseMap[userId]
			enterpriseUser.YoungeeUser = userMap[userId]
		}
		enterpriseUsers = append(enterpriseUsers, enterpriseUser)
	}

	var enterpriseUserDatas []*http_model.EnterpriseUserPreview
	enterpriseUserDatas = pack.EnterpriseUserToEnterpriseUserData(enterpriseUsers)
	return enterpriseUserDatas, totalUser, nil
}

func GetCreatorList(ctx context.Context, pageSize, pageNum int32, conditions *common_model.CreatorListConditions) ([]*http_model.CreatorListPreview, int64, error) {
	db := GetReadDB(ctx)
	db = db.Debug().Model(gorm_model.YoungeeTalentInfo{}).Where("in_blacklist = ?", conditions.InBlacklist)
	// 根据 条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if !util.IsBlank(value) && tag != "create_date" && tag != "talent_wx_nickname" && tag != "id" {
			db = db.Debug().Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		} else if tag == "create_date" && value.Interface() != nil {
			db = db.Where(fmt.Sprintf("create_date like '%s%%'", value.Interface()))
		} else if tag == "talent_wx_nickname" && value.Interface() != nil {
			db = db.Where(fmt.Sprintf("talent_wx_nickname like '%%%s%%'", value.Interface()))
		} else if tag == "id" && value.Interface() != nil {
			db = db.Where(fmt.Sprintf("id like '%%%s%%'", value.Interface()))
		}
	}

	// 查询总数
	var total int64
	var talentList []gorm_model.YoungeeTalentInfo
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetCreatorList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("create_date desc").Limit(int(limit)).Offset(int(offset)).Find(&talentList).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetCreatorList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	var CreatorList []*http_model.CreatorListPreview

	CreatorList = pack.TalentListToCreatorListData(talentList)
	return CreatorList, total, nil
}

func AccountInfo(ctx context.Context, pageSize, pageNum int32, conditions *common_model.AccountInfoConditions) ([]*http_model.AccountInfoData, int64, error) {
	db := GetReadDB(ctx)
	db = db.Debug().Model(gorm_model.YoungeePlatformAccountInfo{})
	// 根据 条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if !util.IsBlank(value) && tag != "bind_date" && tag != "fans_low" && tag != "fans_high" && tag != "platform_nickname" {
			db = db.Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		} else if tag == "bind_date" && value.Interface() != nil {
			db = db.Where(fmt.Sprintf("bind_date like '%s%%'", value.Interface()))
		}
		if !util.IsBlank(value) && tag == "fans_low" {
			db = db.Where(fmt.Sprintf("%s >= ?", "fans_count"), value.Interface())
		}
		if !util.IsBlank(value) && tag == "fans_high" {
			db = db.Where(fmt.Sprintf("%s <= ?", "fans_count"), value.Interface())
		}
		if !util.IsBlank(value) && tag == "platform_nickname" {
			db = db.Where(fmt.Sprintf("platform_nickname like '%%%s%%'", value.Interface()))
		}
	}
	db = db.Debug().Where("deleted = ?", 0)
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetAccountInfo] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	var PlatformAccountInfos []*gorm_model.YoungeePlatformAccountInfo
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("bind_date desc").Limit(int(limit)).Offset(int(offset)).Find(&PlatformAccountInfos).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetAccountInfo] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	phoneMap := map[string]string{}
	for _, PlatformAccountInfo := range PlatformAccountInfos {
		if _, ok := phoneMap[PlatformAccountInfo.TalentID]; !ok {
			phoneMap[PlatformAccountInfo.TalentID] = getPhoneByTalentID(ctx, PlatformAccountInfo.TalentID)
		}
	}
	var accountInfoDatas []*http_model.AccountInfoData
	for _, PlatformAccountInfo := range PlatformAccountInfos {
		accountInfo := new(http_model.AccountInfoData)
		accountInfo.BindDate = conv.MustString(PlatformAccountInfo.BindDate, "")[0:19]
		accountInfo.Platform = consts.GetProjectPlatform(PlatformAccountInfo.PlatformID)
		accountInfo.PlatformNickname = PlatformAccountInfo.PlatformNickname
		accountInfo.PlatformType = PlatformAccountInfo.PlatformType
		accountInfo.TalentId = PlatformAccountInfo.TalentID
		accountInfo.Phone = phoneMap[PlatformAccountInfo.TalentID]
		accountInfo.Fans = util.GetNumString(PlatformAccountInfo.FansCount)
		accountInfo.HomePageUrl = PlatformAccountInfo.HomePageUrl
		accountInfo.HomePageCaptureUrl = PlatformAccountInfo.HomePageCaptureUrl
		accountInfoDatas = append(accountInfoDatas, accountInfo)
	}
	return accountInfoDatas, total, nil
}

func getPhoneByTalentID(ctx context.Context, talentID string) string {
	db := GetReadDB(ctx)
	//talentInfo := gorm_model.YoungeeTalentInfo{}
	phoneNumber := ""
	err := db.Debug().Model(&gorm_model.YoungeeTalentDeliveryAddress{}).Select("phone_number").Where("talent_id = ?", talentID).First(&phoneNumber).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return ""
		} else {
			return ""
		}
	}
	return phoneNumber
}

func DeleteAccount(ctx context.Context, PlatformID, PlatformNickname, TalentId string) error {
	db := GetReadDB(ctx)
	accountInfo := gorm_model.YoungeePlatformAccountInfo{}
	db = db.Debug().Where("talent_id = ? "+
		"AND platform_nickname = ? "+
		"AND platform_id = ?",
		TalentId, PlatformNickname, PlatformID).Find(&accountInfo)
	err := CreateMessage(context.Background(), 15, 2, accountInfo.TalentID, "")
	if err != nil {
		logrus.WithContext(context.Background()).Errorf("[user db] call CreateMessageByTaskId error,err:%+v", err)
	}
	err = db.Delete(&accountInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[user db] call DeleteAccount error,err:%+v", err)
		return err
	}
	return nil
}

func GetEnterpriseIds(ctx context.Context) (*http_model.EnterPriseIds, error) {
	var enterpriseIds []string
	db := GetReadDB(ctx)
	err := db.Model(gorm_model.Enterprise{}).Select("enterprise_id").Find(&enterpriseIds).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[user db] call GetEnterpriseIds error,err:%+v", err)
		return nil, err
	}
	EnterpriseIds := http_model.EnterPriseIds{}
	EnterpriseIds.EnterPriseIds = enterpriseIds
	return &EnterpriseIds, nil
}

func ModifyAccInfo(ctx context.Context, req *http_model.ModifyAccInfoRequest) error {
	db := GetReadDB(ctx)
	return db.Model(gorm_model.YoungeePlatformAccountInfo{}).Where("account_id = ?", req.AccountId).Updates(
		gorm_model.YoungeePlatformAccountInfo{
			FansCount:          req.Fans,
			PlatformNickname:   req.PlatformNickname,
			HomePageUrl:        req.HomePageUrl,
			HomePageCaptureUrl: req.HomePageCaptureUrl,
			UpdatedPerson:      1,
			UpdatedAdminID:     req.User,
		}).Error
}

// 拉黑创作者
func Block(ctx context.Context, data http_model.BlockRequest) error {
	err := Black(ctx, data.ID, data.InBlacklist)
	if err != nil {
		logrus.WithContext(ctx).Errorf("[project] call ChangeProjectStatus error,err:%+v", err)
		return err
	}
	return nil
}

func Black(ctx context.Context, ID string, InBlacklist uint) error {
	db := GetReadDB(ctx)
	talentInfo := gorm_model.YoungeeTalentInfo{}
	if err := db.Debug().Model(&talentInfo).
		Where("id = ?", ID).
		//Updates(gorm_model.YoungeeTalentInfo{InBlacklist: InBlacklist}). //这种方法置0不生效
		UpdateColumn("in_blacklist", InBlacklist).
		Error; err != nil {
		logrus.WithContext(ctx).Errorf("[ChangeProjectStatus] error query mysql total, err:%+v", err)
		return err
	}
	return nil
}
