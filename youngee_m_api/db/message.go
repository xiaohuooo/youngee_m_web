package db

import (
	"context"
	"fmt"
	"time"
	"youngee_m_api/model/gorm_model"

	"github.com/sirupsen/logrus"
)

// 通过taskId查询talentId，插入新消息
func CreateMessageByTaskId(ctx context.Context, messageId int, messageType int, taskId string) error {
	db := GetReadDB(ctx)
	taskInfo := gorm_model.YoungeeTaskInfo{}
	err := db.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Find(&taskInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[CreateMessageByTask] error read mysql, err:%+v", err)
		return err
	}

	db1 := GetReadDB(ctx)
	var projectName string
	err = db1.Model(gorm_model.ProjectInfo{}).Select("project_name").Where("project_id = ?", taskInfo.ProjectId).Find(&projectName).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[CreateMessageByTask] error read mysql, err:%+v", err)
		return err
	}
	fmt.Printf("project_name: %+v, project_id: %+v\n", projectName, taskInfo.ProjectId)
	messageInfo := gorm_model.YounggeeMessageInfo{
		MessageID:   messageId,
		MessageType: messageType,
		CreatedAt:   time.Now(),
		TalentID:    taskInfo.TalentId,
		ProjectName: projectName,
		IsReaded:    0,
		IsDeleted:   0,
	}
	db2 := GetReadDB(ctx)
	err = db2.Model(gorm_model.YounggeeMessageInfo{}).Create(&messageInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[CreateMessageByTask] error create mysql, err:%+v", err)
		return err
	}
	return nil
}

// 插入新消息
func CreateMessage(ctx context.Context, messageId int, messageType int, talentId string, projectId string) error {
	db := GetReadDB(ctx)
	var projectName string
	if projectId != "" {
		err := db.Model(gorm_model.ProjectInfo{}).Select("project_name").Where("project_id = ?", projectId).Find(&projectName).Error
		if err != nil {
			logrus.WithContext(ctx).Errorf("[CreateMessageByTask] error read mysql, err:%+v", err)
			return err
		}
	}

	messageInfo := gorm_model.YounggeeMessageInfo{
		MessageID:   messageId,
		MessageType: messageType,
		CreatedAt:   time.Now(),
		TalentID:    talentId,
		ProjectName: projectName,
		IsReaded:    0,
		IsDeleted:   0,
	}
	db1 := GetReadDB(ctx)
	err := db1.Model(gorm_model.YounggeeMessageInfo{}).Create(&messageInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[CreateMessageByTask] error create mysql, err:%+v", err)
		return err
	}
	return nil
}
