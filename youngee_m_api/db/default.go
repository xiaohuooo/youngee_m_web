package db

import (
	"context"
	"fmt"
	"reflect"
	"strings"
	"time"
	"youngee_m_api/consts"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

func CountDefaultNum(ctx context.Context) (error, *http_model.CountNumOfDefaultsResponse) {
	db := GetReadDB(ctx)
	var contractInfos []gorm_model.YoungeeContractInfo
	err := db.Debug().Model(gorm_model.YoungeeContractInfo{}).Where("default_status = 3").Find(&contractInfos).Error
	if err != nil {
		return err, nil
	}
	DraftDefaultNum, ScriptDefaultNum, LinkDefaultNum, DataDefaultNum := 0, 0, 0, 0
	for _, contractInfo := range contractInfos {
		if contractInfo.BreakType == 1 {
			ScriptDefaultNum++
		} else if contractInfo.BreakType == 2 {
			DraftDefaultNum++
		} else if contractInfo.BreakType == 3 {
			LinkDefaultNum++
		} else {
			DataDefaultNum++
		}
	}
	data := &http_model.CountNumOfDefaultsResponse{}
	data.ScriptDefaultNum = int32(ScriptDefaultNum)
	data.DraftDefaultNum = int32(DraftDefaultNum)
	data.LinkDefaultNum = int32(LinkDefaultNum)
	data.DataDefaultNum = int32(DataDefaultNum)
	return nil, data
}

func BreachPending(ctx context.Context, pageSize, pageNum int32, req *http_model.BreachPendingRequest) (*http_model.BreachPendingData, error) {
	db := GetReadDB(ctx)
	var contractInfos []*gorm_model.YoungeeContractInfo
	db = db.Model(gorm_model.YoungeeContractInfo{}).Where("default_status = 3")
	if req.DefaultType != 0 {
		db = db.Where("break_type = ?", req.DefaultType)
	}
	if req.TaskId != "" {
		db = db.Debug().Where("task_id = ?", req.TaskId)
	}
	var findProjectIds []int64
	if req.ProjectName != "" {
		db1 := GetReadDB(ctx)
		db1.Model(gorm_model.ProjectInfo{}).Select("project_id").Where("project_name = ?", req.ProjectName).Find(&findProjectIds)
		var findTaskIds []int
		db2 := GetReadDB(ctx)
		db2.Model(gorm_model.YoungeeTaskInfo{}).Select("task_id").Where("project_id IN ?", findProjectIds).Find(&findTaskIds)
		db = db.Where("task_id IN ?", findTaskIds)
	}
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[BreachPending] error query mysql total, err:%+v", err)
		return nil, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("terminate_at").Limit(int(limit)).Offset(int(offset)).Find(&contractInfos).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[BreachPending] error query mysql total, err:%+v", err)
		return nil, err
	}
	var taskIds []string
	for _, contractInfo := range contractInfos {
		taskIds = append(taskIds, contractInfo.TaskID)
	}
	taskIds = util.RemoveStrRepByMap(taskIds)
	taskIdToProjectMap := make(map[string]string)
	taskIdToTalentIdMap := make(map[string]string)
	taskIdToTaskInfoMap := make(map[string]gorm_model.YoungeeTaskInfo)
	var projectIds []string
	for _, taskId := range taskIds {
		db1 := GetReadDB(ctx)
		var taskInfo gorm_model.YoungeeTaskInfo
		db1.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Find(&taskInfo)
		taskIdToProjectMap[taskId] = taskInfo.ProjectId
		taskIdToTalentIdMap[taskId] = taskInfo.TalentId
		taskIdToTaskInfoMap[taskId] = taskInfo
		projectIds = append(projectIds, taskInfo.ProjectId)
	}
	projectIds = util.RemoveStrRepByMap(projectIds)
	var enterpriseIds []string
	projectIdToProjectInfoMap := make(map[string]gorm_model.ProjectInfo)
	for _, projectId := range projectIds {
		db1 := GetReadDB(ctx)
		projectInfo := gorm_model.ProjectInfo{}
		db1.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).Find(&projectInfo)
		projectIdToProjectInfoMap[projectInfo.ProjectID] = projectInfo
		enterpriseIds = append(enterpriseIds, projectInfo.EnterpriseID)
	}
	enterpriseIds = util.RemoveStrRepByMap(enterpriseIds)
	enterpriseIdToUserId := make(map[string]int64)
	var userIds []int64
	for _, enterpriseId := range enterpriseIds {
		db1 := GetReadDB(ctx)
		var userId int64
		db1.Model(gorm_model.Enterprise{}).Select("user_id").Where("enterprise_id = ?", enterpriseId).Find(&userId)
		enterpriseIdToUserId[enterpriseId] = userId
		userIds = append(userIds, userId)
	}
	userIdToUserPhone := make(map[int64]string)
	for _, userId := range userIds {
		db1 := GetReadDB(ctx)
		var userPhone string
		db1.Model(gorm_model.YounggeeUser{}).Select("phone").Where("id = ?", userId).Find(&userPhone)
		userIdToUserPhone[userId] = userPhone
	}
	talentIdToTalentPhoneMap := make(map[string]string)
	for _, v := range taskIdToTalentIdMap {
		if len(talentIdToTalentPhoneMap) == 0 {
			db1 := GetReadDB(ctx)
			var talentPhoneNumber string
			db1.Model(gorm_model.YoungeeTalentInfo{}).Select("talent_phone_number").Where("id = ?", v).Find(&talentPhoneNumber)
			talentIdToTalentPhoneMap[v] = talentPhoneNumber
		}
		if _, ok := talentIdToTalentPhoneMap[v]; !ok {
			db1 := GetReadDB(ctx)
			var talentPhoneNumber string
			db1.Model(gorm_model.YoungeeTalentInfo{}).Select("talent_phone_number").Where("id = ?", v).Find(&talentPhoneNumber)
			talentIdToTalentPhoneMap[v] = talentPhoneNumber
		}
	}
	taskIdToDefaultInfo := make(map[string]string)
	if req.DefaultType == 4 {
		for _, taskId := range taskIds {
			db1 := GetReadDB(ctx)
			var link string
			db1.Debug().Model(gorm_model.YounggeeLinkInfo{}).Select("link_url").Where("task_id = ? AND is_ok = 1", taskId).Find(&link)
			taskIdToDefaultInfo[taskId] = link
		}
	}
	var BreachPendingPreviews []*http_model.BreachPendingPreview
	for _, contractInfo := range contractInfos {
		fmt.Println(contractInfo.TerminateAt)
		BreachPendingPreview := new(http_model.BreachPendingPreview)
		BreachPendingPreview.ContractId = int32(contractInfo.ContractID)
		BreachPendingPreview.ProjectId = taskIdToProjectMap[contractInfo.TaskID]
		BreachPendingPreview.UserId = projectIdToProjectInfoMap[taskIdToProjectMap[contractInfo.TaskID]].EnterpriseID
		BreachPendingPreview.ProjectName = projectIdToProjectInfoMap[taskIdToProjectMap[contractInfo.TaskID]].ProjectName
		BreachPendingPreview.UserPhone = userIdToUserPhone[enterpriseIdToUserId[projectIdToProjectInfoMap[taskIdToProjectMap[contractInfo.TaskID]].EnterpriseID]]
		BreachPendingPreview.TaskId = contractInfo.TaskID
		BreachPendingPreview.TalentId = taskIdToTalentIdMap[contractInfo.TaskID]
		BreachPendingPreview.TalentPhone = talentIdToTalentPhoneMap[taskIdToTalentIdMap[contractInfo.TaskID]]
		BreachPendingPreview.LinkInfo = taskIdToDefaultInfo[contractInfo.TaskID]
		BreachPendingPreview.Price = taskIdToTaskInfoMap[contractInfo.TaskID].AllPayment
		BreachPendingPreview.SettlementAmount = taskIdToTaskInfoMap[contractInfo.TaskID].RealPayment
		BreachPendingPreview.DefaultAt = conv.MustString(contractInfo.BreakAt, "")[0:19]
		BreachPendingPreview.TerminateAt = conv.MustString(contractInfo.TerminateAt, "")[0:19]
		BreachPendingPreviews = append(BreachPendingPreviews, BreachPendingPreview)
	}
	var BreachPendingData http_model.BreachPendingData
	BreachPendingData.BreachPendingPreview = BreachPendingPreviews
	BreachPendingData.Total = total
	return &BreachPendingData, nil
}

func ContractBreach(ctx context.Context, req *http_model.ContractBreachRequest) error {
	db := GetReadDB(ctx)
	var breakType int
	db.Model(gorm_model.YoungeeContractInfo{}).Select("break_type").Where("contract_id IN ?", req.ContractIds).Find(&breakType)
	t := time.Now()
	err := db.Debug().Where("contract_id IN ?", req.ContractIds).Updates(&gorm_model.YoungeeContractInfo{DefaultStatus: int(req.DefaultStatus), HandleAt: &t}).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[ContractBreach] error query mysql total, err:%+v", err)
		return err
	}
	var taskIds []string
	db.Model(gorm_model.YoungeeContractInfo{}).Select("task_id").Where("contract_id IN ?", req.ContractIds).Find(&taskIds)
	for _, taskId := range taskIds {
		if req.DefaultStatus == 5 {
			var taskInfo gorm_model.YoungeeTaskInfo
			var enterpriseId string

			db1 := GetReadDB(ctx)
			db1.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Find(&taskInfo)

			db2 := GetReadDB(ctx)
			db2.Model(gorm_model.ProjectInfo{}).Select("enterprise_id").Where("project_id = ?", taskInfo.ProjectId).Find(&enterpriseId)

			db3 := GetReadDB(ctx)
			err := db3.Model(gorm_model.Enterprise{}).Where("enterprise_id = ?", enterpriseId).Updates(
				map[string]interface{}{
					"frozen_balance":    gorm.Expr("frozen_balance - ?", taskInfo.RealPayment),
					"available_balance": gorm.Expr("available_balance + ?", taskInfo.RealPayment)}).Error
			if err != nil {
				logrus.WithContext(ctx).Errorf("[ContractBreach] error update Enterprise, err:%+v", err)
				return err
			}

			db = GetReadDB(ctx)
			db = db.Model(gorm_model.RecruitStrategy{}).Where("project_id = ? and strategy_id = ?", taskInfo.ProjectId, taskInfo.StrategyId)
			err = db.Updates(map[string]interface{}{
				"total_offer": gorm.Expr("total_offer + ?", taskInfo.RealPayment)}).Error
			if err != nil {
				logrus.WithContext(ctx).Errorf("[ContractBreach] error update RecruitStrategy, err:%+v", err)
				return err
			}

			err = CreateTaskLog(context.Background(), taskInfo.TaskId, "解约时间")
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[ContractBreach] call CreateTaskLog error,err:%+v", err)
			}

			db4 := GetReadDB(ctx)
			if breakType == 1 {
				err = db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(
					map[string]interface{}{
						"cur_default_type":  2,
						"task_stage":        16,
						"script_break_rate": 0}).Error
				taskInfo.ScriptBreakRate = 0
			} else if breakType == 2 {
				err = db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(
					map[string]interface{}{
						"cur_default_type":  4,
						"task_stage":        16,
						"sketch_break_rate": 0}).Error
				taskInfo.SketchBreakRate = 0
			} else if breakType == 3 {
				err = db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(
					map[string]interface{}{
						"cur_default_type": 6,
						"task_stage":       16,
						"link_break_rate":  0}).Error
				taskInfo.LinkBreakRate = 0
			} else if breakType == 4 {
				err = db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(
					map[string]interface{}{
						"cur_default_type": 8,
						"task_stage":       16,
						"data_break_rate":  0}).Error
				taskInfo.DataBreakRate = 0
			}
			if err != nil {
				logrus.WithContext(ctx).Errorf("[ContractBreach] error update YoungeeTaskInfo, err:%+v", err)
				return err
			}
			settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.LinkBreakRate+taskInfo.DataBreakRate+taskInfo.SketchBreakRate+taskInfo.ScriptBreakRate+taskInfo.ErrBreakRate)/100)
			if settleAmount <= 0 {
				settleAmount = 0.0
			}
			db5 := GetReadDB(ctx)
			err = db5.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(
				map[string]interface{}{"settle_amount": settleAmount, "withdraw_status": 2, "complete_status": 4, "complete_date": time.Now()}).Error
			if err != nil {
				logrus.WithContext(ctx).Errorf("[ContractBreach] error update YoungeeTaskInfo, err:%+v", err)
				return err
			}

			err = SetProjectFinish(context.Background(), taskInfo.ProjectId)
			if err != nil {
				logrus.WithContext(ctx).Errorf("[ContractBreach] call SetProjectFinish error, err:%+v", err)
				return err
			}
		} else {
			db4 := GetReadDB(ctx)
			if breakType == 1 {
				err = db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(gorm_model.YoungeeTaskInfo{CurDefaultType: 1}).Error
			} else if breakType == 2 {
				err = db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(gorm_model.YoungeeTaskInfo{CurDefaultType: 3}).Error
			} else if breakType == 3 {
				err = db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(gorm_model.YoungeeTaskInfo{CurDefaultType: 5}).Error
			} else if breakType == 4 {
				err = db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Updates(gorm_model.YoungeeTaskInfo{CurDefaultType: 7}).Error
			}
			if err != nil {
				logrus.WithContext(ctx).Errorf("[ContractBreach] error update YoungeeTaskInfo, err:%+v", err)
				return err
			}
		}
	}
	if req.DefaultStatus == 5 {
		err = SetTalentIncome(context.Background(), taskIds)
		if err != nil {
			logrus.WithContext(ctx).Errorf("[ContractBreach] call SetTalentIncome error, err:%+v", err)
			return err
		}
	}
	return nil
}

func GetSketchInfoByTaskId(ctx context.Context, request *http_model.GetSketchInfoByTaskIdRequest) (*http_model.SketchInfoResponse, error) {
	db := GetReadDB(ctx)
	var sketchInfo gorm_model.YounggeeSketchInfo
	db.Debug().Model(gorm_model.YounggeeSketchInfo{}).Where("task_id = ? AND is_ok = 1", request.TaskId).Find(&sketchInfo)
	db2 := GetReadDB(ctx)
	sketchPhotoInfo := gorm_model.YounggeeSketchPhoto{}
	db2.Debug().Model(gorm_model.YounggeeSketchPhoto{}).Where("sketch_id = ?", sketchInfo.SketchID).Find(&sketchPhotoInfo)
	data := new(http_model.SketchInfoResponse)
	data.Type = int32(sketchPhotoInfo.Symbol)
	data.PhotoUrl = sketchPhotoInfo.PhotoUrl
	data.Content = sketchInfo.Content
	data.Title = sketchInfo.Title
	return data, nil
}

func BreachHandled(ctx context.Context, pageSize, pageNum int32, req *http_model.BreachHandledRequest, conditions *common_model.BreachHandledConditions) (*http_model.BreachHandledData, error) {
	db := GetReadDB(ctx)
	var contractInfos []*gorm_model.YoungeeContractInfo
	db = db.Model(gorm_model.YoungeeContractInfo{}).Where("default_status = 4 OR default_status = 5")
	// 根据Project条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if !util.IsBlank(value) {
			db = db.Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		}
	}
	if req.TaskId != "" {
		db = db.Where("task_id = ?", req.TaskId)
	}
	var findProjectIds []string
	if req.ProjectName != "" {
		db1 := GetReadDB(ctx)
		db1.Model(gorm_model.ProjectInfo{}).Select("project_id").Where("project_name = ?", req.ProjectName).Find(&findProjectIds)
		var findTaskIds []string
		db2 := GetReadDB(ctx)
		db2.Model(gorm_model.YoungeeTaskInfo{}).Select("task_id").Where("project_id IN ?", findProjectIds).Find(&findTaskIds)
		db = db.Where("task_id IN ?", findTaskIds)
	}
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[BreachHandled] error query mysql total, err:%+v", err)
		return nil, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("terminate_at").Limit(int(limit)).Offset(int(offset)).Find(&contractInfos).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[BreachHandled] error query mysql total, err:%+v", err)
		return nil, err
	}

	var taskIds []string
	for _, contractInfo := range contractInfos {
		taskIds = append(taskIds, contractInfo.TaskID)
	}
	taskIds = util.RemoveStrRepByMap(taskIds)
	taskIdToProjectMap := make(map[string]string)
	taskIdToTalentIdMap := make(map[string]string)
	taskIdToTaskInfoMap := make(map[string]gorm_model.YoungeeTaskInfo)
	var projectIds []string
	for _, taskId := range taskIds {
		db1 := GetReadDB(ctx)
		var taskInfo gorm_model.YoungeeTaskInfo
		db1.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).Find(&taskInfo)
		taskIdToProjectMap[taskId] = taskInfo.ProjectId
		taskIdToTalentIdMap[taskId] = taskInfo.TalentId
		taskIdToTaskInfoMap[taskId] = taskInfo
		projectIds = append(projectIds, taskInfo.ProjectId)
	}
	projectIds = util.RemoveStrRepByMap(projectIds)
	var enterpriseIds []string
	projectIdToProjectInfoMap := make(map[string]gorm_model.ProjectInfo)
	for _, projectId := range projectIds {
		db1 := GetReadDB(ctx)
		projectInfo := gorm_model.ProjectInfo{}
		db1.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).Find(&projectInfo)
		projectIdToProjectInfoMap[projectInfo.ProjectID] = projectInfo
		enterpriseIds = append(enterpriseIds, projectInfo.EnterpriseID)
	}
	enterpriseIds = util.RemoveStrRepByMap(enterpriseIds)
	enterpriseIdToUserId := make(map[string]int64)
	var userIds []int64
	for _, enterpriseId := range enterpriseIds {
		db1 := GetReadDB(ctx)
		var userId int64
		db1.Model(gorm_model.Enterprise{}).Select("user_id").Where("enterprise_id = ?", enterpriseId).Find(&userId)
		enterpriseIdToUserId[enterpriseId] = userId
		userIds = append(userIds, userId)
	}
	userIdToUserPhone := make(map[int64]string)
	for _, userId := range userIds {
		db1 := GetReadDB(ctx)
		var userPhone string
		db1.Model(gorm_model.YounggeeUser{}).Select("phone").Where("id = ?", userId).Find(&userPhone)
		userIdToUserPhone[userId] = userPhone
	}
	talentIdToTalentPhoneMap := make(map[string]string)
	for _, v := range taskIdToTalentIdMap {
		if len(talentIdToTalentPhoneMap) == 0 {
			db1 := GetReadDB(ctx)
			var talentPhoneNumber string
			db1.Model(gorm_model.YoungeeTalentInfo{}).Select("talent_phone_number").Where("id = ?", v).Find(&talentPhoneNumber)
			talentIdToTalentPhoneMap[v] = talentPhoneNumber
		}
		if _, ok := talentIdToTalentPhoneMap[v]; !ok {
			db1 := GetReadDB(ctx)
			var talentPhoneNumber string
			db1.Model(gorm_model.YoungeeTalentInfo{}).Select("talent_phone_number").Where("id = ?", v).Find(&talentPhoneNumber)
			talentIdToTalentPhoneMap[v] = talentPhoneNumber
		}
	}
	var BreachHandledPreviews []*http_model.BreachHandledPreview
	for _, contractInfo := range contractInfos {
		BreachHandledPreview := new(http_model.BreachHandledPreview)
		BreachHandledPreview.ContractId = int32(contractInfo.ContractID)
		BreachHandledPreview.ProjectId = taskIdToProjectMap[contractInfo.TaskID]
		BreachHandledPreview.UserId = projectIdToProjectInfoMap[taskIdToProjectMap[contractInfo.TaskID]].EnterpriseID
		BreachHandledPreview.ProjectName = projectIdToProjectInfoMap[taskIdToProjectMap[contractInfo.TaskID]].ProjectName
		BreachHandledPreview.UserPhone = userIdToUserPhone[enterpriseIdToUserId[projectIdToProjectInfoMap[taskIdToProjectMap[contractInfo.TaskID]].EnterpriseID]]
		BreachHandledPreview.TaskId = contractInfo.TaskID
		BreachHandledPreview.TalentId = taskIdToTalentIdMap[contractInfo.TaskID]
		BreachHandledPreview.TalentPhone = talentIdToTalentPhoneMap[taskIdToTalentIdMap[contractInfo.TaskID]]
		BreachHandledPreview.TerminateReason = consts.GetBreakType(contractInfo.BreakType)
		BreachHandledPreview.HandleResult = consts.GetHandleResult(contractInfo.DefaultStatus)
		BreachHandledPreview.HandleAt = conv.MustString(contractInfo.HandleAt, "")[0:19]
		BreachHandledPreviews = append(BreachHandledPreviews, BreachHandledPreview)
	}
	var BreachHandledData http_model.BreachHandledData
	BreachHandledData.BreachHandledPreview = BreachHandledPreviews
	BreachHandledData.Total = total
	return &BreachHandledData, nil
}

func GetTaskDefaultReviewList(ctx context.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) ([]*http_model.TaskDefaultReviewInfo, int64, error) {
	db := GetReadDB(ctx)
	// 查询Task表信息
	db = db.Debug().Model(gorm_model.YoungeeTaskInfo{}).Where("task_status = 2")
	// 根据Project条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	var platform_nickname string = ""
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if tag == "default_status" {
			fmt.Printf("default %+v", value.Interface() == int64(0))
			if value.Interface() == int64(0) {
				db = db.Where("cur_default_type = 1")
			} else if value.Interface() == int64(1) {
				db = db.Where("cur_default_type = 3")
			} else if value.Interface() == int64(2) {
				db = db.Where("cur_default_type = 5")
			}
			continue
		} else if !util.IsBlank(value) {
			if tag == "platform_nickname" {
				platform_nickname = fmt.Sprintf("%v", value.Interface())
				continue
			} else if tag == "project_id" || tag == "strategy_id" {
				db = db.Where(fmt.Sprintf("%s = ?", tag), value.Interface())
			}
		}
	}
	var taskInfos []gorm_model.YoungeeTaskInfo
	db = db.Model(gorm_model.YoungeeTaskInfo{})
	// 查询总数
	var totalTask int64
	if err := db.Count(&totalTask).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	db.Order("task_id").Find(&taskInfos)

	// 查询任务id
	var taskIds []string
	taskMap := make(map[string]gorm_model.YoungeeTaskInfo)
	for _, taskInfo := range taskInfos {
		taskIds = append(taskIds, taskInfo.TaskId)
		taskMap[taskInfo.TaskId] = taskInfo
	}
	db1 := GetReadDB(ctx)
	db1 = db1.Debug().Model(gorm_model.YoungeeContractInfo{})

	var DefaultReviewInfos []gorm_model.YoungeeContractInfo
	db1 = db1.Model(gorm_model.YoungeeContractInfo{}).Where("task_id IN ? AND (default_status = 1 OR default_status = 4)", taskIds)
	err := db1.Find(&DefaultReviewInfos).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	DefaultReviewMap := make(map[string]gorm_model.YoungeeContractInfo)
	for _, DefaultReviewInfo := range DefaultReviewInfos {
		DefaultReviewMap[DefaultReviewInfo.TaskID] = DefaultReviewInfo
	}
	// 查询总数
	var totalDefaultReview int64
	if err := db1.Count(&totalDefaultReview).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTalentList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err = db.Order("task_id").Limit(int(limit)).Offset(int(offset)).Error

	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	var TaskDefaultReviews []*http_model.TaskDefaultReview
	var taskDefaultReviews []*http_model.TaskDefaultReviewInfo
	var newTaskDefaultReviews []*http_model.TaskDefaultReviewInfo
	for _, taskId := range taskIds {
		TaskDefaultReview := new(http_model.TaskDefaultReview)
		TaskDefaultReview.Talent = taskMap[taskId]
		TaskDefaultReview.Default = DefaultReviewMap[taskId]
		TaskDefaultReviews = append(TaskDefaultReviews, TaskDefaultReview)
	}

	taskDefaultReviews = pack.TaskDefaultReviewToTaskInfo(TaskDefaultReviews)

	for _, v := range taskDefaultReviews {
		if platform_nickname == "" {
			newTaskDefaultReviews = append(newTaskDefaultReviews, v)
		} else if strings.Contains(v.PlatformNickname, platform_nickname) {
			newTaskDefaultReviews = append(newTaskDefaultReviews, v)
		} else if strings.Contains(conv.MustString(v.TaskID, ""), platform_nickname) {
			newTaskDefaultReviews = append(newTaskDefaultReviews, v)
		} else {
			totalTask--
		}
	}
	return newTaskDefaultReviews, totalTask, nil
}

func GetTaskDefaultDataList(ctx context.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) ([]*http_model.TaskDefaultDataInfo, int64, error) {
	db := GetReadDB(ctx)
	// 查询Task表信息
	db = db.Debug().Model(gorm_model.YoungeeTaskInfo{}).Where("task_status = 2")
	// 根据Project条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	var platform_nickname string = ""
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if tag == "default_status" {
			if value.Interface() == int64(3) {
				db = db.Where("cur_default_type = 7")
			}
			continue
		} else if !util.IsBlank(value) {
			if tag == "platform_nickname" {
				platform_nickname = fmt.Sprintf("%v", value.Interface())
				continue
			} else if tag == "project_id" || tag == "strategy_id" {
				db = db.Where(fmt.Sprintf("%s = ?", tag), value.Interface())
			}
		}
	}
	var taskInfos []gorm_model.YoungeeTaskInfo
	db = db.Model(gorm_model.YoungeeTaskInfo{})
	// 查询总数
	var totalTask int64
	if err := db.Count(&totalTask).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	db.Order("task_id").Find(&taskInfos)

	// 查询任务id
	var taskIds []string
	taskMap := make(map[string]gorm_model.YoungeeTaskInfo)
	for _, taskInfo := range taskInfos {
		taskIds = append(taskIds, taskInfo.TaskId)
		taskMap[taskInfo.TaskId] = taskInfo
	}
	db1 := GetReadDB(ctx)
	db1 = db1.Debug().Model(gorm_model.YoungeeContractInfo{})

	var DefaultDataInfos []gorm_model.YoungeeContractInfo
	db1 = db1.Model(gorm_model.YoungeeContractInfo{}).Where("task_id IN ? AND (default_status = 1 OR default_status = 4)", taskIds)
	err := db1.Find(&DefaultDataInfos).Error
	DefaultDataMap := make(map[string]gorm_model.YoungeeContractInfo)
	for _, DefaultDataInfo := range DefaultDataInfos {
		DefaultDataMap[DefaultDataInfo.TaskID] = DefaultDataInfo
	}

	var LinkInfos []gorm_model.YounggeeLinkInfo
	db2 := GetReadDB(ctx)
	db2 = db2.Model(gorm_model.YounggeeLinkInfo{}).Where("task_id IN ? AND is_ok = 1", taskIds).Find(&LinkInfos)
	LinkMap := make(map[string]gorm_model.YounggeeLinkInfo)
	for _, LinkInfo := range LinkInfos {
		LinkMap[LinkInfo.TaskID] = LinkInfo
	}

	// 查询总数
	var totalDefaultData int64
	if err := db2.Count(&totalDefaultData).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTalentList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err = db.Order("task_id").Limit(int(limit)).Offset(int(offset)).Error

	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProjectTaskList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	var TaskDefaultDatas []*http_model.TaskDefaultData
	var taskDefaultDatas []*http_model.TaskDefaultDataInfo
	var newTaskDefaultDatas []*http_model.TaskDefaultDataInfo
	for _, taskId := range taskIds {
		TaskDefaultData := new(http_model.TaskDefaultData)
		TaskDefaultData.Talent = taskMap[taskId]
		TaskDefaultData.Default = DefaultDataMap[taskId]
		TaskDefaultData.Link = LinkMap[taskId]
		TaskDefaultDatas = append(TaskDefaultDatas, TaskDefaultData)
	}

	taskDefaultDatas = pack.TaskDefaultDataToTaskInfo(TaskDefaultDatas)

	for _, v := range taskDefaultDatas {
		if platform_nickname == "" {
			newTaskDefaultDatas = append(newTaskDefaultDatas, v)
		} else if strings.Contains(v.PlatformNickname, platform_nickname) {
			newTaskDefaultDatas = append(newTaskDefaultDatas, v)
		} else if strings.Contains(conv.MustString(v.TaskID, ""), platform_nickname) {
			newTaskDefaultDatas = append(newTaskDefaultDatas, v)
		} else {
			totalTask--
		}
	}
	return newTaskDefaultDatas, totalTask, nil
}

func GetTaskTerminatingList(ctx context.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) ([]*http_model.TaskTerminatingInfo, int64, error) {
	db := GetReadDB(ctx)
	var taskIds1 []string
	var totalTerminating int64
	var TerminatingInfos []gorm_model.YoungeeContractInfo
	db = db.Model(gorm_model.YoungeeContractInfo{})
	err := db.Where("default_status = 3 AND project_id = ?", projectID).Find(&TerminatingInfos).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskTerminatingList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	TerminatingMap := make(map[string]gorm_model.YoungeeContractInfo)
	for _, TerminatingInfo := range TerminatingInfos {
		taskIds1 = append(taskIds1, TerminatingInfo.TaskID)
		TerminatingMap[TerminatingInfo.TaskID] = TerminatingInfo
	}

	if err := db.Count(&totalTerminating).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskTerminatingList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	db1 := GetReadDB(ctx)
	// 查询Task表信息
	db1 = db1.Model(gorm_model.YoungeeTaskInfo{}).Where("task_status = 2 and task_id in ?", taskIds1)
	// 根据Project条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	var platform_nickname string = ""
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if tag == "default_status" {
			if value.Interface() == int64(4) {
				db1 = db1.Where("cur_default_type = 9")
			}
			continue
		} else if !util.IsBlank(value) {
			if tag == "platform_nickname" {
				platform_nickname = fmt.Sprintf("%v", value.Interface())
				continue
			} else if tag == "project_id" || tag == "strategy_id" {
				if tag == "strategy_id" {
					fmt.Println("strategy_id:", value.Interface())
				}
				db1 = db1.Debug().Where(fmt.Sprintf("%s = ?", tag), value.Interface())
			}
		}
	}
	var taskInfos []gorm_model.YoungeeTaskInfo
	// db1 = db1.Model(gorm_model.YoungeeTaskInfo{})
	// 查询总数
	var totalTask int64
	if err := db1.Count(&totalTask).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskTerminatingList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	db1.Order("task_id").Find(&taskInfos)

	// 查询任务id
	var taskIds []string
	taskMap := make(map[string]gorm_model.YoungeeTaskInfo)
	for _, taskInfo := range taskInfos {
		taskIds = append(taskIds, taskInfo.TaskId)
		taskMap[taskInfo.TaskId] = taskInfo
	}
	var misNum int64
	if totalTerminating > totalTask {
		misNum = totalTerminating - totalTask
	} else {
		misNum = totalTask - totalTerminating
	}
	//logrus.Println("totalTerminating,totalTalent,misNum:", totalTerminating, totalTask, misNum)

	// 查询该页数据
	limit := pageSize + misNum
	offset := pageSize * pageNum // assert pageNum start with 0
	err = db1.Order("task_id").Limit(int(limit)).Offset(int(offset)).Error

	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskTerminatingList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	var TaskTerminatings []*http_model.TaskTerminating
	var taskTerminatings []*http_model.TaskTerminatingInfo
	var newTaskTerminatings []*http_model.TaskTerminatingInfo
	for _, taskId := range taskIds {
		TaskTerminating := new(http_model.TaskTerminating)
		TaskTerminating.Talent = taskMap[taskId]
		TaskTerminating.Default = TerminatingMap[taskId]
		TaskTerminatings = append(TaskTerminatings, TaskTerminating)
	}

	taskTerminatings = pack.TaskTerminatingToTaskInfo(TaskTerminatings)

	for _, v := range taskTerminatings {
		if platform_nickname == "" {
			newTaskTerminatings = append(newTaskTerminatings, v)
		} else if strings.Contains(v.PlatformNickname, platform_nickname) {
			newTaskTerminatings = append(newTaskTerminatings, v)
		} else if strings.Contains(conv.MustString(v.TaskID, ""), platform_nickname) {
			newTaskTerminatings = append(newTaskTerminatings, v)
		} else {
			totalTask--
		}
	}
	return newTaskTerminatings, totalTask, nil
}

func GetTaskTerminatedList(ctx context.Context, projectID string, pageSize, pageNum int64, conditions *common_model.TalentConditions) ([]*http_model.TaskTerminatedInfo, int64, error) {
	db := GetReadDB(ctx)
	// 查询Task表信息
	db = db.Debug().Model(gorm_model.YoungeeTaskInfo{}).Where("task_status = 2")
	// 根据Project条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	var platformNickname string = ""
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if tag == "default_status" {
			fmt.Printf("default %+v", value.Interface() == int64(0))
			if value.Interface() == int64(5) {
				db = db.Where("complete_status = 4")
			}
			continue
		} else if !util.IsBlank(value) {
			if tag == "platform_nickname" {
				platformNickname = fmt.Sprintf("%v", value.Interface())
				continue
			} else if tag == "project_id" || tag == "strategy_id" {
				db = db.Where(fmt.Sprintf("%s = ?", tag), value.Interface())
			}
		}
	}
	var taskInfos []gorm_model.YoungeeTaskInfo
	db = db.Model(gorm_model.YoungeeTaskInfo{})
	// 查询总数
	var totalTask int64
	if err := db.Count(&totalTask).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskTerminatedList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	db.Order("task_id").Find(&taskInfos)

	// 查询任务id
	var taskIds []string
	taskMap := make(map[string]gorm_model.YoungeeTaskInfo)
	for _, taskInfo := range taskInfos {
		taskIds = append(taskIds, taskInfo.TaskId)
		taskMap[taskInfo.TaskId] = taskInfo
	}
	db1 := GetReadDB(ctx)
	db1 = db1.Debug().Model(gorm_model.YoungeeContractInfo{})
	var TerminatedInfos []gorm_model.YoungeeContractInfo
	db1 = db1.Model(gorm_model.YoungeeContractInfo{}).Where("task_id IN ? AND (default_status = 1 OR default_status = 5)", taskIds)
	err1 := db1.Find(&TerminatedInfos).Error
	if err1 != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskTerminatedList] error query mysql find, err:%+v", err1)
		return nil, 0, err1
	}
	TerminatedMap := make(map[string]gorm_model.YoungeeContractInfo)
	for _, TerminatedInfo := range TerminatedInfos {
		fmt.Printf("TerminatedInfo%#v", TerminatedInfo)
		TerminatedMap[TerminatedInfo.TaskID] = TerminatedInfo
	}
	// 查询总数
	var totalTerminated int64
	if err := db1.Count(&totalTerminated).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskTerminatedList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("task_id").Limit(int(limit)).Offset(int(offset)).Error

	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskTerminatedList] error query mysql total, err:%+v", err)
		return nil, 0, err
	}

	var TaskTerminateds []*http_model.TaskTerminated
	var taskTerminateds []*http_model.TaskTerminatedInfo
	var newTaskTerminateds []*http_model.TaskTerminatedInfo
	for _, taskId := range taskIds {
		TaskTerminated := new(http_model.TaskTerminated)
		TaskTerminated.Talent = taskMap[taskId]
		TaskTerminated.Default = TerminatedMap[taskId]
		TaskTerminateds = append(TaskTerminateds, TaskTerminated)
	}

	taskTerminateds = pack.TaskTerminatedToTaskInfo(TaskTerminateds)

	for _, v := range taskTerminateds {
		if platformNickname == "" {
			newTaskTerminateds = append(newTaskTerminateds, v)
		} else if strings.Contains(v.PlatformNickname, platformNickname) {
			newTaskTerminateds = append(newTaskTerminateds, v)
		} else if strings.Contains(conv.MustString(v.TaskID, ""), platformNickname) {
			newTaskTerminateds = append(newTaskTerminateds, v)
		} else {
			totalTask--
		}
	}
	return newTaskTerminateds, totalTask, nil
}
