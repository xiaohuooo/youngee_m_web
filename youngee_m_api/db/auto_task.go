package db

import (
	"context"
	"youngee_m_api/model/gorm_model"

	log "github.com/sirupsen/logrus"
)

func GetLastAutoTaskID() (int32, error) {
	db := GetReadDB(context.Background())
	// 查找最后一个
	LastTask := gorm_model.InfoAutoTask{}
	result := db.Last(&LastTask)
	err := result.Error
	if err != nil {
		log.Println("DB GetLastAutoTaskID:", err)
		return 0, err
	}
	//fmt.Printf("auto task %+v %+v", result, LastTask)
	return LastTask.AutoTaskID, nil
}
