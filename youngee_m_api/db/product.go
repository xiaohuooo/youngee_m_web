package db

import (
	"context"
	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"strconv"
	"youngee_m_api/model/gorm_model"
)

func CreateProduct(ctx context.Context, product gorm_model.YounggeeProduct) (*int64, error) {
	db := GetReadDB(ctx)
	err := db.Create(&product).Error
	if err != nil {
		return nil, err
	}
	return &product.ProductID, nil
}

func GetProductByID(ctx context.Context, productID int64) (*gorm_model.YounggeeProduct, error) {
	db := GetReadDB(ctx)
	product := &gorm_model.YounggeeProduct{}
	err := db.First(&product, productID).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		} else {
			return nil, err
		}
	}
	return product, nil
}

func GetEnterpriseIDByUserID(ctx context.Context, UserId string) string {
	db := GetReadDB(ctx)
	enterpriseInfo := gorm_model.Enterprise{}
	userId := conv.MustInt64(UserId, 0)
	err := db.Where("user_id = ?", userId).Find(&enterpriseInfo).Error
	if err != nil {
		return ""
	}
	enterpriseID := enterpriseInfo.EnterpriseID
	return enterpriseID
}

func GetUserIDByEnterpriseID(ctx context.Context, enterpriseId string) string {
	db := GetReadDB(ctx)
	enterpriseInfo := gorm_model.Enterprise{}
	err := db.Where("enterprise_id = ?", enterpriseId).Find(&enterpriseInfo).Error
	if err != nil {
		return ""
	}
	enterpriseID := enterpriseInfo.UserID
	return strconv.FormatInt(enterpriseID, 10)
}

func GetProductByEnterpriseID(ctx context.Context, enterpriseID string) ([]gorm_model.YounggeeProduct, error) {
	db := GetReadDB(ctx)
	var products []gorm_model.YounggeeProduct
	err := db.Where("enterprise_id = ?", enterpriseID).Find(&products).Error
	if err != nil {
		return nil, err
	}
	return products, nil
}

func GetEnterpriseIDByProductID(ctx context.Context, ProductID int64) string {
	db := GetReadDB(ctx)
	ProjectInfo := gorm_model.ProjectInfo{}
	err := db.Where("product_id = ?", ProductID).Find(&ProjectInfo).Error
	if err != nil {
		return ""
	}
	enterpriseID := ProjectInfo.EnterpriseID
	return enterpriseID
}

func UpdateProduct(ctx context.Context, product gorm_model.YounggeeProduct) (*int64, error) {
	db := GetReadDB(ctx)
	err := db.Model(&product).Updates(product).Error
	if err != nil {
		return nil, err
	}
	return &product.ProductID, nil
}

func GetProductInfoBySelectionId(ctx context.Context, selectionId string) (*gorm_model.YounggeeProduct, error) {
	db := GetReadDB(ctx)
	productId := 0
	err := db.Model(gorm_model.YounggeeSelectionInfo{}).Select("product_id").Where("selection_id = ?", selectionId).Find(&productId).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProductInfo] error query mysql, err:%+v", err)
		return nil, err
	}
	productInfo := gorm_model.YounggeeProduct{}
	err = db.Model(gorm_model.YounggeeProduct{}).Where("product_id = ?", productId).Find(&productInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProductInfo] error query mysql, err:%+v", err)
		return nil, err
	}
	return &productInfo, nil
}

func GetProductPhotoInfoBySelectionId(ctx context.Context, selectionId string) ([]*gorm_model.YounggeeProductPhoto, error) {
	db := GetReadDB(ctx)
	productId := 0
	err := db.Model(gorm_model.YounggeeSelectionInfo{}).Select("product_id").Where("selection_id = ?", selectionId).Find(&productId).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProductInfo] error query mysql, err:%+v", err)
		return nil, err
	}
	var productPhotoInfo []*gorm_model.YounggeeProductPhoto
	err = db.Model(gorm_model.YounggeeProductPhoto{}).Where("product_id = ?", productId).Find(&productPhotoInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[GetProductInfo] error query mysql, err:%+v", err)
		return nil, err
	}
	return productPhotoInfo, nil
}
