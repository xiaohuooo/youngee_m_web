package db

import (
	"context"
	"fmt"
	"youngee_m_api/model/system_model"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var client *gorm.DB

func Init(config *system_model.Mysql) {
	dsn := "%v:%v@tcp(%v:%v)/%v?charset=utf8mb4&parseTime=True&loc=Local"
	dsn = fmt.Sprintf(dsn, config.User, config.Password, config.Host, config.Port, config.Database)
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	client = db
}

func GetReadDB(ctx context.Context) *gorm.DB {
	return client.WithContext(ctx)
}

func GetWriteDB(ctx context.Context) *gorm.DB {
	return client.WithContext(ctx)
}
