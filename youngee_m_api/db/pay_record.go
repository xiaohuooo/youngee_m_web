package db

import (
	"context"
	"time"
	"youngee_m_api/model/gorm_model"

	"github.com/sirupsen/logrus"
)

// CreatePayRecord 新增
func CreatePayRecord(ctx context.Context, enterpriseId string, payment float64, balance float64, payType int64, projectId string) (*int64, error) {
	db := GetReadDB(ctx)
	payRecord := gorm_model.EnterprisePayRecord{
		EnterpriseID: enterpriseId,
		Payment:      payment,
		Balance:      balance,
		PayType:      payType,
		PayAt:        time.Now(),
		ProjectID:    projectId,
	}
	err := db.Create(&payRecord).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[logistics db] call CreatePayRecord error,err:%+v", err)
		return nil, err
	}

	return &payRecord.ID, nil
}
