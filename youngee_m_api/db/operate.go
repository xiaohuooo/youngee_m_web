package db

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"strconv"
	"time"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"github.com/tidwall/gjson"
	"gorm.io/gorm"
)

func CreatePricingStrategy(ctx context.Context, req *http_model.AddPricingRequest) (string, string, error) {
	db := GetReadDB(ctx)
	project_type, fee_form, platform, fans_low, fans_up, service_charge, service_rate := req.ProjectType, req.ManuscriptForm, req.Platform, req.FansLow, req.FansHigh, req.ServiceCharge, req.ServiceRate*10
	var IsExclusive, IsExclusive2, IsExclusive3, IsExclusive1 gorm_model.InfoPricingStrategy
	err := db.Where(" fee_form = ? && platform = ? && fans_low <= ? && fans_up >= ?", fee_form, platform, fans_low, fans_up).First(&IsExclusive).Error
	if err == nil {
		return "该策略与已经运行的策略互斥，请修改相关字段后重新创建", "", nil
	}
	err = db.Where(" fee_form = ? && platform = ? && fans_low < ? && fans_up > ?", fee_form, platform, fans_low, fans_low).First(&IsExclusive1).Error
	if err == nil {
		return "该策略与已经运行的策略互斥，请修改相关字段后重新创建", "", nil
	}
	err = db.Where(" fee_form = ? && platform = ? && fans_low < ? && fans_up > ?", fee_form, platform, fans_up, fans_up).First(&IsExclusive2).Error
	if err == nil {
		return "该策略与已经运行的策略互斥，请修改相关字段后重新创建", "", nil
	}
	err = db.Where(" fee_form = ? && platform = ? && fans_low >= ? && fans_up <= ?", fee_form, platform, fans_low, fans_up).First(&IsExclusive3).Error
	if err == nil {
		return "该策略与已经运行的策略互斥，请修改相关字段后重新创建", "", nil
	}
	var newStrategy gorm_model.InfoPricingStrategy
	var strategyInfo gorm_model.InfoPricingStrategy
	getMonth := time.Now().Format("01") //获取月
	getDay := time.Now().Day()          //获取日
	dayString := ""
	if getDay < 10 {
		dayString = conv.MustString(getDay, "")
		dayString = "0" + dayString
	} else {
		dayString = conv.MustString(getDay, "")
	}
	monthAndDay := conv.MustString(getMonth, "") + dayString
	err = db.Where(fmt.Sprintf(" strategyId like '%s%%'", monthAndDay)).Last(&strategyInfo).Error

	num := 1
	if strategyInfo.StrategyId != "" {
		num = conv.MustInt(strategyInfo.StrategyId[4:6], 0)
		num = num + 1 //获取日
	} else {
		num = 1
	}
	numString := ""
	if num < 10 {
		numString = conv.MustString(num, "")
		numString = "0" + numString
	} else {
		numString = conv.MustString(num, "")
	}
	newStrategy.StrategyId = monthAndDay + numString
	newStrategy.Status = 0
	newStrategy.FansLow = fans_low
	newStrategy.FansUp = fans_up
	newStrategy.ProjectType = conv.MustInt64(project_type, 0)
	newStrategy.ServiceCharge = conv.MustFloat64(service_charge, 0)
	newStrategy.Platform = conv.MustInt64(platform, 0)
	newStrategy.FeeForm = conv.MustInt64(fee_form, 0)
	newStrategy.ServiceRate = conv.MustInt64(service_rate, 0)
	newStrategy.CreateAt = time.Now()
	newStrategy.UpdateAt = time.Now()
	err = db.Create(&newStrategy).Error
	if err != nil {
		return "创建失败", "", err
	}
	return "创建成功", monthAndDay + numString, nil
}

func CreateYoungeeStrategy(ctx context.Context, req *http_model.AddYoungeeRequest) (string, string, error) {
	db := GetReadDB(ctx)
	project_type, task_type, platform, content_type, reason, cash := req.ProjectType, req.TaskType, req.Platform, req.ContentType, req.Reason, req.Cash
	var IsExclusive gorm_model.InfoYoungeeStrategy
	err := db.Where(" platform = ? ").First(&IsExclusive).Error
	if err == nil {
		return "该策略与已经运行的策略互斥，请修改相关字段后重新创建", "", nil
	}
	var newStrategy gorm_model.InfoYoungeeStrategy
	var strategyInfo gorm_model.InfoYoungeeStrategy
	getMonth := time.Now().Format("01") //获取月
	getDay := time.Now().Day()          //获取日
	dayString := ""
	if getDay < 10 {
		dayString = conv.MustString(getDay, "")
		dayString = "0" + dayString
	} else {
		dayString = conv.MustString(getDay, "")
	}
	monthAndDay := conv.MustString(getMonth, "") + dayString
	err = db.Where(fmt.Sprintf(" strategyId like '%s%%'", monthAndDay)).Last(&strategyInfo).Error

	num := 1
	if strategyInfo.StrategyId != "" {
		num = conv.MustInt(strategyInfo.StrategyId[4:6], 0)
		num = num + 1 //获取日
	} else {
		num = 1
	}
	numString := ""
	if num < 10 {
		numString = conv.MustString(num, "")
		numString = "0" + numString
	} else {
		numString = conv.MustString(num, "")
	}
	newStrategy.StrategyId = monthAndDay + numString
	newStrategy.Status = 0
	newStrategy.ProjectType = conv.MustInt64(project_type, 0)
	newStrategy.TaskType = conv.MustInt64(task_type, 0)
	newStrategy.Platform = conv.MustInt64(platform, 0)
	newStrategy.ContentType = conv.MustInt(content_type, 0)
	newStrategy.Reason = conv.MustInt(reason, 0)
	//newStrategy.Points = conv.MustFloat64(points, 0)
	newStrategy.Cash = conv.MustFloat64(cash, 0)
	newStrategy.CreateAt = time.Now()
	err = db.Create(&newStrategy).Error
	if err != nil {
		return "创建失败", "", err
	}
	return "创建成功", monthAndDay + numString, nil
}

func SearchPricing(ctx context.Context, pageSize, pageNum int32, conditions *common_model.PricingConditions) ([]*gorm_model.InfoPricingStrategy, int64, error) {
	db := GetReadDB(ctx)
	db = db.Model(gorm_model.InfoPricingStrategy{})
	// 根据 查询条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if !util.IsBlank(value) && tag != "update_at" {
			db = db.Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		} else if tag == "update_at" && value.Interface() != nil {
			db = db.Where(fmt.Sprintf("update_at like '%s%%'", value.Interface()))
		}
	}
	// 查询总数
	var total int64
	var PricingDatas []*gorm_model.InfoPricingStrategy
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[SearchPricing] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("update_at desc").Limit(int(limit)).Offset(int(offset)).Find(&PricingDatas).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[SearchPricing] error query mysql find, err:%+v", err)
		return nil, 0, err
	}
	return PricingDatas, total, nil
}

func SearchYoungee(ctx context.Context, pageSize, pageNum int32, conditions *common_model.YoungeeConditions) ([]*gorm_model.InfoYoungeeStrategy, int64, error) {
	db := GetReadDB(ctx)
	db = db.Model(gorm_model.InfoYoungeeStrategy{})
	// 根据 查询条件过滤
	conditionType := reflect.TypeOf(conditions).Elem()
	conditionValue := reflect.ValueOf(conditions).Elem()
	for i := 0; i < conditionType.NumField(); i++ {
		field := conditionType.Field(i)
		tag := field.Tag.Get("condition")
		value := conditionValue.FieldByName(field.Name)
		if !util.IsBlank(value) {
			fmt.Println("tag:", tag)
			db = db.Debug().Where(fmt.Sprintf("%s = ?", tag), value.Interface())
		}
	}
	// 查询总数
	var total int64
	var YoungeeDatas []*gorm_model.InfoYoungeeStrategy
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[SearchYoungee] error query mysql total, err:%+v", err)
		return nil, 0, err
	}
	// 查询该页数据
	limit := pageSize
	offset := pageSize * pageNum // assert pageNum start with 0
	err := db.Order("content_type desc").Limit(int(limit)).Offset(int(offset)).Find(&YoungeeDatas).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[SearchYoungee] error query mysql find, err:%+v", err)
		return nil, 0, err
	}
	return YoungeeDatas, total, nil
}

func ModifyPricing(ctx context.Context, req *http_model.ModifyPricingRequest) (int, error) {
	db := GetReadDB(ctx)
	strategyId := req.StrategyId
	db = db.Model(gorm_model.InfoPricingStrategy{})
	project_type, fee_form, platform, fans_low, fans_up, service_charge, service_rate := req.ProjectType, req.FeeForm, req.Platform, req.FansLow, req.FansHigh, req.ServiceCharge, req.ServiceRate*10
	var IsExclusive, IsExclusive2, IsExclusive3, IsExclusive1 int64
	err := db.Debug().Where("strategyId <> ? && fee_form = ? && platform = ? && fans_low <= ? && fans_up >= ?", strategyId, fee_form, platform, fans_low, fans_up).Count(&IsExclusive).Error
	if IsExclusive != 0 {
		return 1, nil
	}
	err = db.Debug().Where("strategyId <> ? && fee_form = ? && platform = ? && fans_low < ? && fans_up > ?", strategyId, fee_form, platform, fans_low, fans_low).Count(&IsExclusive1).Error
	if IsExclusive1 != 0 {
		return 1, nil
	}
	err = db.Debug().Where("strategyId <> ? && fee_form = ? && platform = ? && fans_low < ? && fans_up > ?", strategyId, fee_form, platform, fans_up, fans_up).Count(&IsExclusive2).Error
	if IsExclusive2 != 0 {
		return 1, nil
	}
	err = db.Debug().Where("strategyId <> ? && fee_form = ? && platform = ? && fans_low >= ? && fans_up <= ?", strategyId, fee_form, platform, fans_low, fans_up).Count(&IsExclusive3).Error
	if IsExclusive3 != 0 {
		return 1, nil
	}
	if err != nil {
		fmt.Printf("[ModifyPricing] error query mysql find, err:%+v", err)
		logrus.WithContext(ctx).Errorf("[ModifyPricing] error query mysql find, err:%+v", err)
		return 2, err
	}
	//fmt.Printf("service_rate:%+v", service_rate)
	db1 := GetReadDB(ctx).Model(gorm_model.InfoPricingStrategy{}).Where("strategyId = ?", strategyId)
	err1 := db1.Updates(map[string]interface{}{
		"project_type": project_type, "service_rate": service_rate, "fee_form": fee_form,
		"platform": platform, "fans_low": fans_low, "fans_up": fans_up, "service_charge": service_charge,
	}).Error
	if err1 != nil {
		fmt.Printf("[ModifyPricing] error query mysql update, err:%+v", err)
		logrus.WithContext(ctx).Errorf("[ModifyPricing] error query mysql update, err:%+v", err)
		return 2, err
	}
	return 0, nil
}

func UpdateAutoTaskTime(ctx context.Context, time int32, num int32) error {
	db := GetReadDB(ctx)
	var autoTaskInfo gorm_model.InfoAutoTask
	err := db.Model(gorm_model.InfoAutoTask{}).Last(&autoTaskInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[UpdateAutoTaskTime] error get Last mysql, err:%+v", err)
		return err
	}
	switch num {
	case 1:
		autoTaskInfo.SignInOffline = time
	case 2:
		autoTaskInfo.SignInVirtual = time
	case 3:
		autoTaskInfo.ReviewInMv = time
	case 4:
		autoTaskInfo.ReviewUnlimited = time
	case 5:
		autoTaskInfo.Postreview = time
	case 6:
		autoTaskInfo.CaseClose = time
	case 7:
		autoTaskInfo.Invalid = time
	case 8:
		autoTaskInfo.DraftDefaultInPic = time
	case 9:
		autoTaskInfo.DraftDefaultInMv = time
	case 10:
		autoTaskInfo.ScriptDefault = time
	case 11:
		autoTaskInfo.LinkBreach = time
	case 12:
		autoTaskInfo.CaseCloseDefault = time
	}
	autoTaskInfo.AutoTaskID += 1
	err = db.Model(gorm_model.InfoAutoTask{}).Create(&autoTaskInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[UpdateAutoTaskTime] error Create mysql, err:%+v", err)
		return err
	}
	return nil
}

func UpdateAutoDefaultRate(ctx context.Context, rate int, num int32) error {
	db := GetReadDB(ctx)
	autoDefaultRateInfo := gorm_model.InfoAutoDefaultHandle{}
	err := db.Model(&gorm_model.InfoAutoDefaultHandle{}).Last(&autoDefaultRateInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[UpdateAutoDefaultRate] error get Last mysql, err:%+v", err)
		return err
	}
	switch num {
	case 1:
		autoDefaultRateInfo.SketchReplaceNotUpload = rate
	case 2:
		autoDefaultRateInfo.SketchReplaceTimeOut = rate
	case 3:
		autoDefaultRateInfo.SketchOtherNotUpload = rate
	case 4:
		autoDefaultRateInfo.SketchOtherTimeOut = rate
	case 5:
		autoDefaultRateInfo.ScriptReplaceNotUpload = rate
	case 6:
		autoDefaultRateInfo.ScriptReplaceTimeOut = rate
	case 7:
		autoDefaultRateInfo.ScriptOtherNotUpload = rate
	case 8:
		autoDefaultRateInfo.ScriptOtherTimeOut = rate
	case 9:
		autoDefaultRateInfo.LinkReplaceNotUpload = rate
	case 10:
		autoDefaultRateInfo.LinkReplaceTimeOut = rate
	case 11:
		autoDefaultRateInfo.LinkOtherNotUpload = rate
	case 12:
		autoDefaultRateInfo.LinkOtherTimeOut = rate
	case 13:
		autoDefaultRateInfo.DataReplaceNotUpload = rate
	case 14:
		autoDefaultRateInfo.DataReplaceTimeOut = rate
	case 15:
		autoDefaultRateInfo.DataOtherNotUpload = rate
	case 16:
		autoDefaultRateInfo.DataOtherTimeOut = rate
	}
	autoDefaultRateInfo.AutoDefaultID += 1
	err = db.Model(&gorm_model.InfoAutoDefaultHandle{}).Create(autoDefaultRateInfo).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[UpdateAutoDefaultRate] error Create mysql, err:%+v", err)
		return err
	}
	return nil
}

func GetSignInOfflineTask(projectForm int32) error {
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND project_form = ? AND project_status = 9", 1, projectForm).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	projectIdMap := make(map[string]int64)
	taskIDs := make(map[string]string)
	var projectIds []string
	var taskIds []string
	var t int32
	for _, projectInfo := range projectInfos {
		autoTaskId := projectInfo.AutoTaskID
		dbStart := GetReadDB(context.Background())
		var autoTaskInfo gorm_model.InfoAutoTask
		dbStart.Model(gorm_model.InfoAutoTask{}).Where("auto_task_id = ?", autoTaskId).First(&autoTaskInfo)
		// fmt.Println("t:", autoTaskInfo.SignInOffline)
		if projectForm == 3 {
			t = autoTaskInfo.SignInOffline
		} else {
			t = autoTaskInfo.SignInVirtual
		}
		projectIds = append(projectIds, projectInfo.ProjectID)
		if _, ok := projectIdMap[projectInfo.ProjectID]; !ok {
			projectIdMap[projectInfo.ProjectID] = projectInfo.ContentType
			db1 := GetReadDB(context.Background())
			var taskInfos []gorm_model.YoungeeTaskInfo
			db1.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND logistics_status = ? AND task_stage = 5", projectInfo.ProjectID, 2).Find(&taskInfos)
			//fmt.Println("taskInfos", taskInfos)
			for _, taskInfo := range taskInfos {
				//fmt.Println("TaskId", taskInfo.TaskId)
				if _, ok1 := taskIDs[taskInfo.TaskId]; !ok1 {
					taskIds = append(taskIds, taskInfo.TaskId)
					taskIDs[taskInfo.TaskId] = taskInfo.ProjectId
					var logisticInfo gorm_model.YoungeeTaskLogistics
					db2 := GetReadDB(context.Background())
					db2.Model(gorm_model.YoungeeTaskLogistics{}).Where("task_id = ?", taskInfo.TaskId).Find(&logisticInfo)
					dd, _ := time.ParseDuration(conv.MustString(t, "") + "h")
					if logisticInfo.AutoSignAt == nil || logisticInfo.AutoSignAt.IsZero() {
						var t1 time.Time
						if logisticInfo.ThingsType == 3 {
							t1 = logisticInfo.ExplorestoreEndtime.Add(dd)
						} else {
							t1 = logisticInfo.DeliveryTime.Add(dd)
						}
						db5 := GetReadDB(context.Background())
						db5.Model(gorm_model.YoungeeTaskLogistics{}).Where("logistics_id = ?", logisticInfo.LogisticsID).
							Updates(&gorm_model.YoungeeTaskLogistics{AutoSignAt: &t1})
						fmt.Println("已添加自动签收时间")
					}
				}
			}
		}
	}
	var logisticInfos []gorm_model.YoungeeTaskLogistics
	db3 := GetReadDB(context.Background())
	err = db3.Model(gorm_model.YoungeeTaskLogistics{}).Where("auto_sign_at <= ? AND task_id IN ?", time.Now(), taskIds).Find(&logisticInfos).Error
	if err != nil {
		log.Println("DB AutoGetSignInOfflineTask error :", err)
		return err
	}
	var signedTaskId []string
	for _, logisticInfo := range logisticInfos {
		signedTaskId = append(signedTaskId, logisticInfo.TaskID)
		if logisticInfo.Status == 0 {
			if logisticInfo.ThingsType == 3 {
				t := time.Now()
				db6 := GetReadDB(context.Background())
				db6.Model(gorm_model.YoungeeTaskLogistics{}).Where("logistics_id = ?", logisticInfo.LogisticsID).Updates(
					&gorm_model.YoungeeTaskLogistics{SignedTime: &t, Status: 1})
			} else {
				db6 := GetReadDB(context.Background())
				t := time.Now()
				db6.Model(gorm_model.YoungeeTaskLogistics{}).Where("logistics_id = ?", logisticInfo.LogisticsID).Updates(
					&gorm_model.YoungeeTaskLogistics{SignedTime: &t, Status: 1})
			}
			err = CreateTaskLog(context.Background(), logisticInfo.TaskID, "签收时间")
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
			}

			err = CreateMessageByTaskId(context.Background(), 9, 2, logisticInfo.TaskID)
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
			}
			fmt.Println("已更新签收时间")
		}
	}
	signedTaskId = util.RemoveStrRepByMap(signedTaskId)
	//fmt.Println("signedTaskId", signedTaskId)
	for _, taskID := range signedTaskId {
		//fmt.Println("taskID", taskID)
		db4 := GetReadDB(context.Background())
		taskNeedMod := gorm_model.YoungeeTaskInfo{}
		db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskID).Find(&taskNeedMod)
		if projectIdMap[taskIDs[taskID]] == 1 {
			if taskNeedMod.LogisticsStatus == 2 && taskNeedMod.TaskStage == 5 {
				db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskID).Updates(&gorm_model.YoungeeTaskInfo{LogisticsStatus: 3, TaskStage: 9})
				fmt.Println("内容形式为图片的项目 已修改任务状态为待传初稿 及物流状态为已签收")
			}
		} else if projectIdMap[taskIDs[taskID]] == 2 {
			if taskNeedMod.LogisticsStatus == 2 && taskNeedMod.TaskStage == 5 {
				db4.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskID).Updates(&gorm_model.YoungeeTaskInfo{LogisticsStatus: 3, TaskStage: 7})
				fmt.Println("内容形式为视频的项目 已修改任务状态为待传脚本 及物流状态为已签收")
			}
		}
	}
	return nil
}

func GetAutoReviewTask(contentType int32) error {
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	if contentType == 2 {
		// 项目内容形式为视频
		// 1. 补全scriptInfo表中的自动审核脚本时间
		err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND content_type = ? AND project_status = 9", 1, contentType).Find(&projectInfos).Error
		if err != nil {
			return err
		}
		var projectIds []string
		projectIdToAutoTaskIdMap := map[string]int{}
		for _, projectInfo := range projectInfos {
			projectIds = append(projectIds, projectInfo.ProjectID)
			projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
		}
		//fmt.Println("projectIds:", projectIds)
		db1 := GetReadDB(context.Background())
		for _, projectId := range projectIds {
			// 获取 autoTaskId 及其对应的限制时间、
			autoTaskId := projectIdToAutoTaskIdMap[projectId]
			dbStart := GetReadDB(context.Background())
			var ReviewInMv int32
			dbStart.Model(gorm_model.InfoAutoTask{}).Select("review_in_mv").Where("auto_task_id = ?", autoTaskId).First(&ReviewInMv)
			//fmt.Println("t:", autoTaskInfo.ReviewInMv)
			// 获取 projectId 对应的 taskId 及添加脚本自动审核时间
			var taskInfos []gorm_model.YoungeeTaskInfo
			db1.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = 2 AND task_stage = 8", projectId).Find(&taskInfos)
			for _, taskInfo := range taskInfos {
				//fmt.Println("TaskID:", taskInfo.TaskId)
				db2 := GetReadDB(context.Background())
				var scriptInfos gorm_model.YounggeeScriptInfo
				db2.Model(gorm_model.YounggeeScriptInfo{}).Where("task_id = ? AND is_submit = ? AND is_review = ? ", taskInfo.TaskId, 1, 0).Find(&scriptInfos)
				dd, _ := time.ParseDuration(conv.MustString(ReviewInMv, "") + "h")
				if scriptInfos.AutoAgreeAt.IsZero() {
					db3 := GetReadDB(context.Background())
					db3.Model(gorm_model.YounggeeScriptInfo{}).Where("task_id = ?", taskInfo.TaskId).
						Updates(&gorm_model.YounggeeScriptInfo{AutoAgreeAt: scriptInfos.SubmitAt.Add(dd)})
					fmt.Println("已添加脚本自动审核时间")
				}
			}
		}
		//fmt.Println("taskId:", taskId)

		// 2. 查询scriptInfo表中应该被自动审稿的脚本
		var newScriptInfos []gorm_model.YounggeeScriptInfo
		db3 := GetReadDB(context.Background())
		err = db3.Model(gorm_model.YounggeeScriptInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Find(&newScriptInfos).Error
		if err != nil {
			log.Println("DB GetAutoReviewTask error in script:", err)
			return err
		}
		// 记录其task_id
		var taskIds []string
		for _, scriptInfo := range newScriptInfos {
			taskIds = append(taskIds, scriptInfo.TaskID)
		}
		// 3. 执行审核脚本操作
		// 1）. 更新scriptInfo表
		db4 := GetReadDB(context.Background())
		err = db4.Model(gorm_model.YounggeeScriptInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Updates(
			&gorm_model.YounggeeScriptInfo{AgreeAt: time.Now(), IsReview: 1, IsOk: 1}).Error
		if err != nil {
			log.Println("DB GetAutoReviewTask error in script:", err)
			return err
		}
		var num int64
		db4.Model(gorm_model.YounggeeScriptInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Count(&num)
		if num != 0 {
			fmt.Println("自动审核脚本：已更新scriptInfo表")
		}
		// 2）.更新taskInfo表
		db5 := GetReadDB(context.Background())
		err = db5.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id in ?", taskIds).Updates(
			&gorm_model.YoungeeTaskInfo{ScriptStatus: 5, TaskStage: 9}).Error
		if err != nil {
			log.Println("DB GetAutoReviewTask error in script:", err)
			return err
		}
		for _, taskId := range taskIds {
			err = CreateTaskLog(context.Background(), taskId, "脚本通过")
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
			}

			err = CreateMessageByTaskId(context.Background(), 2, 1, taskId)
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
			}
		}
		if len(taskIds) != 0 {
			fmt.Println("以下任务状态需要变更:", taskIds)
			fmt.Println("自动审核脚本：已更新taskInfo表，任务阶段为待传链接 及脚本上传状态为已通过")
		}
		return nil
	} else {
		// 项目内容形式为图文
		// 1. 补全sketchInfo表中的自动审核初稿时间
		err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND project_status = 9", 1).Find(&projectInfos).Error
		if err != nil {
			return err
		}
		var projectIds []string
		projectIdToAutoTaskIdMap := map[string]int{}
		for _, projectInfo := range projectInfos {
			projectIds = append(projectIds, projectInfo.ProjectID)
			projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
		}
		//fmt.Println("projectIds:", projectIds)
		db1 := GetReadDB(context.Background())
		for _, projectId := range projectIds {
			// 获取 autoTaskId 及其对应的限制时间、
			autoTaskId := projectIdToAutoTaskIdMap[projectId]
			dbStart := GetReadDB(context.Background())
			var ReviewUnlimited int32
			dbStart.Model(gorm_model.InfoAutoTask{}).Select("review_unlimited").Where("auto_task_id = ?", autoTaskId).First(&ReviewUnlimited)
			//fmt.Println("t:", autoTaskInfo.ReviewUnlimited)
			var taskInfos []gorm_model.YoungeeTaskInfo
			db1.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = 2 AND task_stage = 10", projectId).Find(&taskInfos)
			for _, taskInfo := range taskInfos {
				db2 := GetReadDB(context.Background())
				var sketchInfo gorm_model.YounggeeSketchInfo
				db2.Model(gorm_model.YounggeeSketchInfo{}).Where("task_id = ? AND is_submit = ? AND is_review = ? ", taskInfo.TaskId, 1, 0).Find(&sketchInfo)
				dd, _ := time.ParseDuration(conv.MustString(ReviewUnlimited, "") + "h")
				if sketchInfo.AutoAgreeAt.IsZero() {
					db3 := GetReadDB(context.Background())
					db3.Model(gorm_model.YounggeeSketchInfo{}).Where("task_id = ?", sketchInfo.TaskID).Updates(&gorm_model.YounggeeSketchInfo{
						AutoAgreeAt: sketchInfo.SubmitAt.Add(dd),
					})
					fmt.Println("已添加初稿自动审核时间")
				}
			}
		}
		// 2. 查询sketchInfo表中应该被自动审稿的初稿
		var newSketchInfos []gorm_model.YounggeeSketchInfo
		db3 := GetReadDB(context.Background())
		err = db3.Model(gorm_model.YounggeeSketchInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Find(&newSketchInfos).Error
		if err != nil {
			log.Println("DB GetAutoReviewTask error in sketch:", err)
			return err
		}
		// 记录其task_id
		var taskIds []string
		for _, sketchInfo := range newSketchInfos {
			taskIds = append(taskIds, sketchInfo.TaskID)
		}
		// 3. 执行审核初稿操作
		// 1）. 更新sketchInfo表
		db4 := GetReadDB(context.Background())
		err = db4.Model(gorm_model.YounggeeSketchInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Updates(
			&gorm_model.YounggeeSketchInfo{AgreeAt: time.Now(), IsReview: 1, IsOk: 1}).Error
		if err != nil {
			log.Println("DB GetAutoReviewTask error in sketch:", err)
			return err
		}
		var num int64
		db4.Model(gorm_model.YounggeeSketchInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Count(&num)
		if num != 0 {
			fmt.Println("自动审核初稿：已更新sketchInfo表")
		}
		// 2）.更新taskInfo表
		db5 := GetReadDB(context.Background())
		err = db5.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id in ?", taskIds).Updates(
			&gorm_model.YoungeeTaskInfo{SketchStatus: 5, TaskStage: 11}).Error
		if err != nil {
			log.Println("DB GetAutoReviewTask error in sketch:", err)
			return err
		}
		for _, taskId := range taskIds {
			err = CreateTaskLog(context.Background(), taskId, "初稿通过")
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
			}
			err = CreateMessageByTaskId(context.Background(), 3, 1, taskId)
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
			}
		}
		if len(taskIds) != 0 {
			fmt.Println("以下任务状态需要变更:", taskIds)
			fmt.Println("自动审核初稿：已更新taskInfo表，任务阶段为待传链接 及初稿上传状态为已通过")
		}
		return nil
	}
}

func GetAutoPostReviewTask() error {
	// 1. 补全linkInfo表中的自动审核链接时间
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND project_status = 9", 1).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	var projectIds []string
	projectIdToAutoTaskIdMap := map[string]int{}
	for _, projectInfo := range projectInfos {
		projectIds = append(projectIds, projectInfo.ProjectID)
		projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
	}
	//fmt.Println("projectIds:", projectIds)
	db1 := GetReadDB(context.Background())
	for _, projectId := range projectIds {
		// 获取 autoTaskId 及其对应的限制时间、
		autoTaskId := projectIdToAutoTaskIdMap[projectId]
		dbStart := GetReadDB(context.Background())
		var Postreview int32
		dbStart.Model(gorm_model.InfoAutoTask{}).Select("postreview").Where("auto_task_id = ?", autoTaskId).First(&Postreview)
		//fmt.Println("t:", autoTaskInfo.Postreview)
		var taskInfos []gorm_model.YoungeeTaskInfo
		db1.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = 2 AND task_stage = 12", projectId).Find(&taskInfos)
		for _, taskInfo := range taskInfos {
			db2 := GetReadDB(context.Background())
			var linkInfos gorm_model.YounggeeLinkInfo
			db2.Model(gorm_model.YounggeeLinkInfo{}).Where("task_id = ? AND is_submit = ? AND is_review = ? ", taskInfo.TaskId, 1, 0).Find(&linkInfos)
			dd, _ := time.ParseDuration(conv.MustString(Postreview, "") + "h")
			if linkInfos.AutoAgreeAt.IsZero() && linkInfos.IsSubmit == 1 {
				db3 := GetReadDB(context.Background())
				db3.Model(gorm_model.YounggeeLinkInfo{}).Where("task_id = ?", linkInfos.TaskID).Updates(&gorm_model.YounggeeLinkInfo{AutoAgreeAt: linkInfos.SubmitAt.Add(dd)})
				fmt.Println("已添加链接自动审核时间")
			}
		}
	}

	// 2. 查询linkInfo表中应该被自动审稿的链接
	var newLinkInfos []gorm_model.YounggeeLinkInfo
	db3 := GetReadDB(context.Background())
	err = db3.Model(gorm_model.YounggeeLinkInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Find(&newLinkInfos).Error
	if err != nil {
		log.Println("DB GetAutoPostReviewTask error in link:", err)
		return err
	}
	// 记录其task_id
	var taskIds []string
	for _, linkInfo := range newLinkInfos {
		taskIds = append(taskIds, linkInfo.TaskID)
	}
	// 3. 执行审核链接操作
	// 1）. 更新linkInfo表
	db4 := GetReadDB(context.Background())
	err = db4.Model(gorm_model.YounggeeLinkInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Updates(
		&gorm_model.YounggeeLinkInfo{AgreeAt: time.Now(), IsReview: 1, IsOk: 1}).Error
	if err != nil {
		log.Println("DB GetAutoPostReviewTask error in link:", err)
		return err
	}
	var num int64
	db4.Model(gorm_model.YounggeeLinkInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Count(&num)
	if num != 0 {
		fmt.Println("自动审核链接：已更新linkInfo表")
	}
	// 2）.更新taskInfo表
	db5 := GetReadDB(context.Background())
	err = db5.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id in ?", taskIds).Updates(
		&gorm_model.YoungeeTaskInfo{LinkStatus: 5, TaskStage: 13}).Error
	if err != nil {
		log.Println("DB GetAutoPostReviewTask error in link:", err)
		return err
	}
	for _, taskId := range taskIds {
		err = CreateTaskLog(context.Background(), taskId, "链接通过")
		if err != nil {
			logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
		}
		err = CreateMessageByTaskId(context.Background(), 4, 1, taskId)
		if err != nil {
			logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
		}
	}
	if len(taskIds) != 0 {
		fmt.Println("以下任务状态需要变更:", taskIds)
		fmt.Println("自动审核链接：已更新taskInfo表，任务阶段为待传链接 及链接上传状态为已通过")
	}
	return nil
}

func GetAutoCaseCloseTask() error {
	// 1. 补全dataInfo表中的自动审核数据时间
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND project_status = 9", 1).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	var projectIds []string
	projectIdToAutoTaskIdMap := map[string]int{}
	for _, projectInfo := range projectInfos {
		projectIds = append(projectIds, projectInfo.ProjectID)
		projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
	}
	//fmt.Println("projectIds:", projectIds)
	db1 := GetReadDB(context.Background())
	for _, projectId := range projectIds {
		// 获取 autoTaskId 及其对应的限制时间、
		autoTaskId := projectIdToAutoTaskIdMap[projectId]
		dbStart := GetReadDB(context.Background())
		var CaseClose int32
		dbStart.Model(gorm_model.InfoAutoTask{}).Select("case_close").Where("auto_task_id = ?", autoTaskId).First(&CaseClose)
		//fmt.Println("t:", autoTaskInfo.CaseClose)
		var taskInfos []gorm_model.YoungeeTaskInfo
		db1.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = 2 AND task_stage = 14", projectId).Find(&taskInfos)
		for _, taskInfo := range taskInfos {
			db2 := GetReadDB(context.Background())
			var dataInfos gorm_model.YounggeeDataInfo
			db2.Model(gorm_model.YounggeeDataInfo{}).Where("task_id = ? AND is_submit = ? AND is_review = ? ", taskInfo.TaskId, 1, 0).Find(&dataInfos)
			dd, _ := time.ParseDuration(conv.MustString(CaseClose, "") + "h")
			if dataInfos.AutoAgreeAt.IsZero() && dataInfos.IsSubmit == 1 {
				db3 := GetReadDB(context.Background())
				db3.Model(gorm_model.YounggeeDataInfo{}).Where("task_id = ?", dataInfos.TaskID).Updates(&gorm_model.YounggeeDataInfo{AutoAgreeAt: dataInfos.SubmitAt.Add(dd)})
				fmt.Println("已添加数据自动审核时间")
			}
		}
	}

	// 2. 查询dataInfo表中应该被自动审稿的数据
	var newDataInfos []gorm_model.YounggeeDataInfo
	db3 := GetReadDB(context.Background())
	err = db3.Model(gorm_model.YounggeeDataInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Find(&newDataInfos).Error
	if err != nil {
		log.Println("DB GetAutoCaseCloseTask error in data:", err)
		return err
	}
	// 记录其task_id
	taskIdToDataInfoMap := map[string]gorm_model.YounggeeDataInfo{}
	taskIdToTaskInfoMap := map[string]gorm_model.YoungeeTaskInfo{}
	var taskIds []string
	db6 := GetReadDB(context.Background())
	for _, dataInfo := range newDataInfos {
		taskIdToDataInfoMap[dataInfo.TaskID] = dataInfo
		taskIds = append(taskIds, dataInfo.TaskID)
		taskInfo := gorm_model.YoungeeTaskInfo{}
		err = db6.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", dataInfo.TaskID).Find(&taskInfo).Error
		if err != nil {
			log.Println("DB GetAutoCaseCloseTask error in data:", err)
			return err
		}
		taskIdToTaskInfoMap[dataInfo.TaskID] = taskInfo
	}
	// 3. 执行审核数据操作
	// 1）. 更新dataInfo表
	db4 := GetReadDB(context.Background())
	err = db4.Model(gorm_model.YounggeeDataInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Updates(
		&gorm_model.YounggeeDataInfo{AgreeAt: time.Now(), IsReview: 1, IsOk: 1}).Error
	if err != nil {
		log.Println("DB GetAutoCaseCloseTask error in data:", err)
		return err
	}
	var num int64
	db4.Model(gorm_model.YounggeeDataInfo{}).Where("auto_agree_at <= ? AND is_submit = ? AND is_review = ? ", time.Now(), 1, 0).Count(&num)
	if num != 0 {
		fmt.Println("自动审核数据：已更新dataInfo表")
	}
	// 2）.更新taskInfo表
	db5 := GetReadDB(context.Background())
	err = db5.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id in ?", taskIds).Updates(
		&gorm_model.YoungeeTaskInfo{DataStatus: 5, TaskStage: 15, CompleteDate: time.Now(), CompleteStatus: 2, WithdrawStatus: 2}).Error
	if err != nil {
		log.Println("DB GetAutoCaseCloseTask error in data:", err)
		return err
	}
	for _, taskId := range taskIds {
		err = CreateTaskLog(context.Background(), taskId, "数据通过")
		if err != nil {
			logrus.WithContext(context.Background()).Errorf("[auto] call CreateTaskLog error,err:%+v", err)
		}
		err = CreateMessageByTaskId(context.Background(), 5, 1, taskId)
		if err != nil {
			logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
		}
	}
	err = SetTalentIncome(context.Background(), taskIds)
	if err != nil {
		logrus.WithContext(context.Background()).Errorf("[auto] call SetTalentIncome error,err:%+v", err)
	}
	if len(taskIds) != 0 {
		fmt.Println("以下任务状态需要变更:", taskIds)
		fmt.Println("自动审核数据：已更新taskInfo表，任务阶段为待传数据 及数据上传状态为已通过")
	}

	// 3）. 更新recruitStrategy表
	for _, taskId := range taskIds {
		// 更新招募策略
		taskInfo := taskIdToTaskInfoMap[taskId]
		dataInfo := taskIdToDataInfoMap[taskId]
		db7 := GetReadDB(context.Background())
		db7 = db7.Model(gorm_model.RecruitStrategy{}).Where("project_id = ? and strategy_id = ?", taskInfo.ProjectId, taskInfo.StrategyId)
		fansCount, _ := strconv.Atoi(conv.MustString(gjson.Get(taskInfo.TalentPlatformInfoSnap, "fans_count"), ""))
		err = db7.Updates(map[string]interface{}{
			"fan_number":     gorm.Expr("fan_number + ?", fansCount),
			"play_number":    gorm.Expr("play_number + ?", dataInfo.PlayNumber),
			"like_number":    gorm.Expr("like_number + ?", dataInfo.LikeNumber),
			"collect_number": gorm.Expr("collect_number + ?", dataInfo.CollectNumber),
			"comment_number": gorm.Expr("comment_number + ?", dataInfo.CommentNumber),
			"finish_number":  gorm.Expr("finish_number + 1"),
			"total_offer":    gorm.Expr("total_offer + ?", taskInfo.RealPayment)}).Error
		if err != nil {
			log.Println("DB GetAutoCaseCloseTask error in data:", err)
			return err
		}
	}

	// 4. 判断是否全部任务已结案，若已全部结案则触发项目结案
	// 查询 task_stage < 15 的任务数量
	for _, projectInfo := range projectInfos {
		var unFinishedTaskNumber int64
		db8 := GetReadDB(context.Background())
		err = db8.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage < 15", projectInfo.ProjectID).Count(&unFinishedTaskNumber).Error
		if err != nil {
			log.Println("DB GetAutoCaseCloseTask error in data:", err)
			return err
		}
		var finishedNum int64
		db8.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage >= 15", projectInfo.ProjectID).Count(&finishedNum)
		if unFinishedTaskNumber == 0 && finishedNum != 0 {
			// 若为0则触发项目结案
			db9 := GetReadDB(context.Background())
			// 2. 释放企业账户因项目冻结的资金
			// 	1）. 计算剩余资金
			var allPayment float64
			var realPayment float64
			err = db9.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage >= 15", projectInfo.ProjectID).
				Pluck("COALESCE(SUM(all_payment), 0) as allPayment", &allPayment).Error
			if err != nil {
				log.Println("DB GetAutoCaseCloseTask error in data:", err)
				return err
			}
			err = db9.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? and task_status = 2 and task_stage >= 15", projectInfo.ProjectID).
				Pluck("COALESCE(SUM(real_payment), 0) as realPayment", &realPayment).Error
			if err != nil {
				log.Println("DB GetAutoCaseCloseTask error in data:", err)
				return err
			}
			fmt.Println("企业应支付金额总计：", allPayment, "实际支付总计：", realPayment)

			// 	2）. 释放剩余资金
			err = db9.Model(gorm_model.Enterprise{}).Where("enterprise_id = ?", projectInfo.EnterpriseID).Updates(
				map[string]interface{}{
					"frozen_balance":    gorm.Expr("frozen_balance - ?", allPayment),
					"balance":           gorm.Expr("balance - ?", realPayment),
					"available_balance": gorm.Expr("available_balance + ?", allPayment-realPayment)}).Error
			if err != nil {
				log.Println("DB GetAutoCaseCloseTask error in data:", err)
				return err
			}
			// 1. 更新项目状态为已结束
			t := time.Now()
			err := db9.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectInfo.ProjectID).
				Updates(map[string]interface{}{"project_status": 10, "payment_amount": realPayment, "finish_at": &t}).Error
			if err != nil {
				log.Println("DB GetAutoCaseCloseTask error in data:", err)
				return err
			}
		}
	}
	return nil
}

func GetAutoInvalidTask() error {
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND project_status = ? ", 1, 6).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	var projectIds []string
	projectIdToAutoTaskIdMap := map[string]int{}
	for _, projectInfo := range projectInfos {
		projectIds = append(projectIds, projectInfo.ProjectID)
		projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
	}
	for _, projectId := range projectIds {
		// 获取 autoTaskId 及其对应的限制时间
		autoTaskId := projectIdToAutoTaskIdMap[projectId]
		dbStart := GetReadDB(context.Background())
		var Invalid int32
		dbStart.Model(gorm_model.InfoAutoTask{}).Select("invalid").Where("auto_task_id = ?", autoTaskId).First(&Invalid)
		//fmt.Println("t:", autoTaskInfo.Invalid)
		db1 := GetReadDB(context.Background())
		project := gorm_model.ProjectInfo{}
		db1.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).First(&project)
		//if project.AutoFailAt.IsZero() {
		//	dd, _ := time.ParseDuration(conv.MustString(Invalid, "") + "h")
		//	t := project.RecruitDdl.Add(dd)
		//	db2 := GetReadDB(context.Background())
		//	db2.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).Updates(&gorm_model.ProjectInfo{AutoFailAt: &t})
		//	fmt.Println("已添加失效自动处理时间")
		//}

		dd, _ := time.ParseDuration(conv.MustString(Invalid, "") + "h")
		project.RecruitDdl.Add(dd)
		t := project.RecruitDdl.Add(dd)
		//fmt.Println("已存在的失效时间是否跟要添加的相等", project.AutoFailAt.Equal(t))
		if project.AutoFailAt == nil || project.AutoFailAt.IsZero() {
			db3 := GetReadDB(context.Background())
			db3.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).Updates(&gorm_model.ProjectInfo{AutoFailAt: &t})
			fmt.Println("已添加失效自动处理时间")
		}

		projectNeedMod := gorm_model.ProjectInfo{}
		db2 := GetReadDB(context.Background())
		db2 = db2.Where("project_id = ?", projectId).First(&projectNeedMod)
		fmt.Println("失效自动处理时间为：", projectNeedMod.AutoFailAt)
		if !projectNeedMod.AutoFailAt.IsZero() || projectNeedMod.AutoFailAt != nil {
			fmt.Printf("项目 %v 失效自动处理时间不为空\n", projectNeedMod.ProjectID)
			fmt.Println("距离失效自动处理时间还有：,项目当前状态为", projectNeedMod.AutoFailAt.Sub(time.Now()), projectNeedMod.ProjectStatus)
			if projectNeedMod.AutoFailAt.Sub(time.Now()) <= 0 && projectNeedMod.ProjectStatus < 8 {
				db4 := GetReadDB(context.Background())
				t := time.Now()
				err1 := db4.Model(gorm_model.ProjectInfo{}).Where("project_id = ?", projectId).Updates(&gorm_model.ProjectInfo{ProjectStatus: 8, FinishAt: &t, FailReason: 1}).Error
				if err1 != nil {
					return err1
				}
				fmt.Println("已更新项目状态为超时未支付的失效状态")
				dbT := GetReadDB(context.TODO())
				dbT.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ?", projectId).Updates(gorm_model.YoungeeTaskInfo{TaskStage: 3, CompleteStatus: 3, CompleteDate: time.Now()})
			}
		}
	}
	return nil
}

// 图文-初稿超时违约判断
func GetAutoDraftDefaultInPicTask() error {
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND content_type = ? ", 1, 1).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	var projectIds []string
	projectIdToAutoTaskIdMap := map[string]int{}    // 项目id 对 定时任务id 的map
	projectIdToAutoDefaultIdMap := map[string]int{} // 项目id 对 违约扣款设置id 的map
	taskIdToFeeFormMap := make(map[string]int)      // taskId 对 稿费形式的 map
	TaskIdToProjectId := make(map[string]string)    // taskId 对 项目id的 map
	var taskNeedModIds []string                     // 首次提交初稿的任务记录id
	var submitTaskNeedModIds []string               // 修改后提交初稿的任务记录id
	// 构造map及list
	for _, projectInfo := range projectInfos {
		projectIds = append(projectIds, projectInfo.ProjectID)
		projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
		projectIdToAutoDefaultIdMap[projectInfo.ProjectID] = int(projectInfo.AutoDefaultID)
	}
	for _, projectId := range projectIds {
		db1 := GetReadDB(context.Background())

		var taskInfos []gorm_model.YoungeeTaskInfo
		db1.Model(gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = ? AND task_stage = ? AND cur_default_type = ? AND logistics_status = 3", projectId, 2, 9, 0).Find(&taskInfos)
		for _, taskInfo := range taskInfos {
			TaskIdToProjectId[taskInfo.TaskId] = projectId
			taskNeedMod := gorm_model.YoungeeTaskInfo{}
			db2 := GetReadDB(context.Background())
			// 保存所有满足物流状态为 3 且 初稿上传状态为 1或 3 的任务记录的id
			db2.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskInfo.TaskId).First(&taskNeedMod)
			taskIdToFeeFormMap[taskInfo.TaskId] = taskInfo.FeeForm
			if taskNeedMod.TaskStage == 9 && taskNeedMod.SketchStatus == 1 {
				taskNeedModIds = append(taskNeedModIds, taskInfo.TaskId)
			}
			if taskNeedMod.TaskStage == 9 && taskNeedMod.SketchStatus == 3 {
				submitTaskNeedModIds = append(submitTaskNeedModIds, taskInfo.TaskId)
			}
		}
	}
	// 判断应首次上传初稿的任务
	for _, taskNeedModId := range taskNeedModIds {
		// 获取 autoTaskId 及其对应的限制时间
		autoTaskId := projectIdToAutoTaskIdMap[TaskIdToProjectId[taskNeedModId]]
		dbStart := GetReadDB(context.Background())
		var DraftDefaultInPic int32
		dbStart.Model(gorm_model.InfoAutoTask{}).Select("draft_default_in_pic").Where("auto_task_id = ?", autoTaskId).First(&DraftDefaultInPic)
		db3 := GetReadDB(context.Background())

		taskLogisticNeedMod := gorm_model.YoungeeTaskLogistics{}
		db3.Model(gorm_model.YoungeeTaskLogistics{}).Where("task_id = ?", taskNeedModId).First(&taskLogisticNeedMod)

		// 查询违约扣款比例
		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[TaskIdToProjectId[taskNeedModId]]).Find(&autoDefaultHandle)

		var sketchDefaultRate int
		var sketchErrRate int
		if taskIdToFeeFormMap[taskNeedModId] == 1 { // 稿费形式为产品置换
			sketchDefaultRate = autoDefaultHandle.SketchReplaceTimeOut
			sketchErrRate = autoDefaultHandle.SketchReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("sketch_replace_time_out", "sketch_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[TaskIdToProjectId[taskNeedModId]]).Find(&autoDefaultHandle)
		} else { // 稿费形式为其他
			sketchDefaultRate = autoDefaultHandle.SketchOtherTimeOut
			sketchErrRate = autoDefaultHandle.SketchOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("sketch_other_time_out", "sketch_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[TaskIdToProjectId[taskNeedModId]]).Find(&sketchDefaultRate, &sketchErrRate)
		}

		//	添加初稿违约自动处理时间
		if taskLogisticNeedMod.Status == 1 && taskLogisticNeedMod.TaskID != "" && taskLogisticNeedMod.AutoSketchBreakAt == nil || taskLogisticNeedMod.AutoSketchBreakAt.IsZero() {
			dd, _ := time.ParseDuration(conv.MustString(DraftDefaultInPic, "") + "h")
			db4 := GetReadDB(context.Background())
			t := taskLogisticNeedMod.SignedTime.Add(dd)
			db4.Where("task_id = ?", taskNeedModId).Updates(&gorm_model.YoungeeTaskLogistics{
				AutoSketchBreakAt: &t,
			})
			taskLogisticNeedMod.AutoSketchBreakAt = &t
			fmt.Println("已添加图片初稿违约自动处理时间")
		}

		// 判断是否超时违约
		if taskLogisticNeedMod.TaskID != "" && taskLogisticNeedMod.AutoSketchBreakAt.Sub(time.Now()) <= 0 {
			// 超时违约处理
			db5 := GetReadDB(context.Background())
			err1 := db5.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
				TaskID: taskLogisticNeedMod.TaskID, ProjectID: TaskIdToProjectId[taskNeedModId], BreakType: 2, BreakAt: time.Now(), DefaultStatus: 1}).Error
			if err1 != nil {
				return err1
			}
			taskInfo := gorm_model.YoungeeTaskInfo{}
			dbt := GetReadDB(context.Background())
			err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskLogisticNeedMod.TaskID).Find(&taskInfo).Error
			if err2 != nil {
				return err2
			}
			settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.LinkBreakRate+taskInfo.DataBreakRate+sketchDefaultRate+taskInfo.ScriptBreakRate)/100)
			if settleAmount <= 0 {
				settleAmount = 0.0
			}
			realPayment := taskInfo.AllPayment * (1.0 - float64(sketchErrRate)/100)
			if realPayment <= 0 {
				realPayment = 0.0
			}
			db8 := GetReadDB(context.Background())
			db8.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskLogisticNeedMod.TaskID).Updates(
				map[string]interface{}{
					"cur_default_type":  3,
					"sketch_break_rate": sketchDefaultRate,
					"settle_amount":     settleAmount,
					"err_break_rate":    sketchErrRate,
					"real_payment":      realPayment,
				})

			err = CreateTaskLog(context.Background(), taskLogisticNeedMod.TaskID, "初稿逾期")
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
			}
			err = CreateMessageByTaskId(context.Background(), 22, 4, taskLogisticNeedMod.TaskID)
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
			}
			fmt.Println("已创建图文类型的初稿违约记录")
		}
	}
	// 判断应修改后上传初稿的任务
	for _, submitTaskNeedModId := range submitTaskNeedModIds {
		// 获取 autoTaskId 及其对应的限制时间
		db2 := GetReadDB(context.Background())
		var DraftDefaultInMv int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("draft_default_in_mv").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[TaskIdToProjectId[submitTaskNeedModId]]).First(&DraftDefaultInMv)
		dd, _ := time.ParseDuration(conv.MustString(DraftDefaultInMv, "") + "h")

		// 查询违约扣款比例
		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[TaskIdToProjectId[submitTaskNeedModId]]).Find(&autoDefaultHandle)

		var sketchDefaultRate int
		var sketchErrRate int
		if taskIdToFeeFormMap[submitTaskNeedModId] == 1 {
			sketchDefaultRate = autoDefaultHandle.SketchReplaceTimeOut
			sketchErrRate = autoDefaultHandle.SketchReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("sketch_replace_time_out", "sketch_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[TaskIdToProjectId[submitTaskNeedModId]]).Find(&sketchDefaultRate, &sketchErrRate)
		} else {
			sketchDefaultRate = autoDefaultHandle.SketchOtherTimeOut
			sketchErrRate = autoDefaultHandle.SketchOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("sketch_other_time_out", "sketch_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[TaskIdToProjectId[submitTaskNeedModId]]).Find(&sketchDefaultRate, &sketchErrRate)
		}
		//	添加初稿违约自动处理时间
		db1 := GetReadDB(context.Background())
		var taskSketchInfo gorm_model.YounggeeSketchInfo
		db1.Model(gorm_model.YounggeeSketchInfo{}).Where("task_id = ? and is_review = 1", submitTaskNeedModId).Order("reject_at desc").First(&taskSketchInfo)
		if taskSketchInfo.AutoSketchBreakAt.IsZero() {
			err4 := db1.Where("task_id = ?", submitTaskNeedModId).Updates(&gorm_model.YounggeeSketchInfo{AutoSketchBreakAt: taskSketchInfo.RejectAt.Add(dd)}).Error
			if err4 != nil {
				return err4
			}
			taskSketchInfo.AutoSketchBreakAt = taskSketchInfo.RejectAt.Add(dd)
			fmt.Println("已添加图文形式的初稿违约自动处理时间")
		}

		// 判断是否违约
		if taskSketchInfo.TaskID != "" && taskSketchInfo.AutoSketchBreakAt.Sub(time.Now()) <= 0 {
			db4 := GetReadDB(context.Background())
			err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
				TaskID: submitTaskNeedModId, ProjectID: TaskIdToProjectId[submitTaskNeedModId], BreakType: 2, BreakAt: time.Now(), DefaultStatus: 1}).Error
			if err1 != nil {
				return err1
			}
			taskInfo := gorm_model.YoungeeTaskInfo{}
			dbt := GetReadDB(context.Background())
			err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Find(&taskInfo).Error
			if err2 != nil {
				return err2
			}
			settleAmount := taskInfo.TaskReward * (1.0 - float64(sketchDefaultRate+taskInfo.DataBreakRate+taskInfo.LinkBreakRate+taskInfo.ScriptBreakRate)/100)
			if settleAmount <= 0 {
				settleAmount = 0.
			}
			realPayment := taskInfo.AllPayment * (1.0 - float64(sketchErrRate)/100)
			if realPayment <= 0 {
				realPayment = 0.0
			}
			db3 := GetReadDB(context.Background())
			err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Updates(
				map[string]interface{}{
					"cur_default_type":  3,
					"sketch_break_rate": sketchDefaultRate,
					"settle_amount":     settleAmount,
					"err_break_rate":    sketchErrRate,
					"real_payment":      realPayment,
				}).Error
			if err2 != nil {
				return err2
			}
			err = CreateTaskLog(context.Background(), submitTaskNeedModId, "初稿逾期")
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
			}
			err = CreateMessageByTaskId(context.Background(), 22, 4, submitTaskNeedModId)
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
			}
			fmt.Println("创建已提交初稿的图文类型的初稿违约记录")
		}
	}
	return nil
}

// GetAutoDraftDefaultInMvTask 视频-初稿超时违约判断
func GetAutoDraftDefaultInMvTask() error {
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND content_type = ? ", 1, 2).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	var projectIds []string
	// 任务id 对 项目id 的map
	taskIdToProjectIdMap := map[string]string{}
	// 项目id 对 定时任务id 的map
	projectIdToAutoTaskIdMap := map[string]int{}
	// 项目 id 对 违约定时任务 id 的 map
	projectIdToAutoDefaultIdMap := map[string]int{}
	// taskId 对 稿费形式的 map
	taskIdToFeeFormMap := make(map[string]int)
	var videoTaskIds []string
	// 构造map及list
	for _, projectInfo := range projectInfos {
		projectIds = append(projectIds, projectInfo.ProjectID)
		projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
		projectIdToAutoDefaultIdMap[projectInfo.ProjectID] = int(projectInfo.AutoDefaultID)

		var videoTaskInfos []*gorm_model.YoungeeTaskInfo
		db1 := GetReadDB(context.Background())
		db1.Model(&gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = ? AND task_stage = ? AND cur_default_type = ? AND logistics_status = 3",
			projectInfo.ProjectID, 2, 9, 0).Find(&videoTaskInfos)
		for _, videoTaskInfo := range videoTaskInfos {
			taskIdToProjectIdMap[videoTaskInfo.TaskId] = projectInfo.ProjectID
			taskIdToFeeFormMap[videoTaskInfo.TaskId] = videoTaskInfo.FeeForm
			videoTaskIds = append(videoTaskIds, videoTaskInfo.TaskId)
		}
	}

	// 首次提交初稿的任务记录id
	var videoTaskNeedModIds []string
	// 已提交初稿的任务记录id
	var submitVideoTaskNeedModIds []string
	for _, videoTaskId := range videoTaskIds {
		var taskInfo gorm_model.YoungeeTaskInfo
		db3 := GetReadDB(context.Background())
		// 保存所有初稿上传状态为 1  且任务阶段为9 的任务记录的id
		db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", videoTaskId).First(&taskInfo)
		if taskInfo.SketchStatus == 1 && taskInfo.TaskStage == 9 {
			videoTaskNeedModIds = append(videoTaskNeedModIds, videoTaskId)
		}
		// 保存所有初稿上传状态为 3  且任务阶段为9 的任务记录的id
		if taskInfo.SketchStatus == 3 && taskInfo.TaskStage == 9 {
			submitVideoTaskNeedModIds = append(submitVideoTaskNeedModIds, videoTaskId)
		}
	}
	// 判断应首次提交初稿的任务
	for _, videoTaskNeedModId := range videoTaskNeedModIds {
		db2 := GetReadDB(context.Background())
		var DraftDefaultInMv int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("draft_default_in_mv").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[taskIdToProjectIdMap[videoTaskNeedModId]]).First(&DraftDefaultInMv)
		dd, _ := time.ParseDuration(conv.MustString(DraftDefaultInMv, "") + "h")

		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[videoTaskNeedModId]]).Find(&autoDefaultHandle)

		var sketchDefaultRate int
		var sketchErrRate int
		if taskIdToFeeFormMap[videoTaskNeedModId] == 1 {
			sketchDefaultRate = autoDefaultHandle.SketchReplaceTimeOut
			sketchErrRate = autoDefaultHandle.SketchReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("sketch_replace_time_out", "sketch_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[videoTaskNeedModId]]).Find(&sketchDefaultRate, &sketchErrRate)
		} else {
			sketchDefaultRate = autoDefaultHandle.SketchOtherTimeOut
			sketchErrRate = autoDefaultHandle.SketchOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("sketch_other_time_out", "sketch_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[videoTaskNeedModId]]).Find(&sketchDefaultRate, &sketchErrRate)
		}
		db1 := GetReadDB(context.Background())
		var taskScriptInfo gorm_model.YounggeeScriptInfo
		db1.Model(gorm_model.YounggeeScriptInfo{}).Where("task_id = ? AND is_ok = ?", videoTaskNeedModId, 1).Find(&taskScriptInfo)
		if taskScriptInfo.AutoSketchBreakAt.IsZero() {
			db1.Where("task_id = ?", videoTaskNeedModId).Updates(&gorm_model.YounggeeScriptInfo{AutoSketchBreakAt: taskScriptInfo.AgreeAt.Add(dd)})
			taskScriptInfo.AutoSketchBreakAt = taskScriptInfo.AgreeAt.Add(dd)
			fmt.Println("已添加视频形式的初稿违约自动处理时间")
		}

		if taskScriptInfo.TaskID != "" && taskScriptInfo.AutoSketchBreakAt.Sub(time.Now()) <= 0 {
			db4 := GetReadDB(context.Background())
			err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
				TaskID: videoTaskNeedModId, ProjectID: taskIdToProjectIdMap[videoTaskNeedModId], BreakType: 2, BreakAt: time.Now(), DefaultStatus: 1}).Error
			if err1 != nil {
				return err1
			}
			taskInfo := gorm_model.YoungeeTaskInfo{}
			dbt := GetReadDB(context.Background())
			err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", videoTaskNeedModId).Find(&taskInfo).Error
			if err2 != nil {
				return err2
			}
			settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.LinkBreakRate+taskInfo.DataBreakRate+sketchDefaultRate+taskInfo.ScriptBreakRate)/100)
			if settleAmount <= 0 {
				settleAmount = 0.0
			}
			realPayment := taskInfo.AllPayment * (1.0 - float64(sketchErrRate)/100)
			if realPayment <= 0 {
				realPayment = 0.0
			}
			db3 := GetReadDB(context.Background())
			err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", videoTaskNeedModId).Updates(
				map[string]interface{}{
					"cur_default_type":  3,
					"sketch_break_rate": sketchDefaultRate,
					"settle_amount":     settleAmount,
					"err_break_rate":    sketchErrRate,
					"real_payment":      realPayment,
				}).Error
			if err2 != nil {
				return err2
			}
			err = CreateTaskLog(context.Background(), videoTaskNeedModId, "初稿逾期")
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
			}
			err = CreateMessageByTaskId(context.Background(), 22, 4, videoTaskNeedModId)
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
			}
			fmt.Println("已创建视频类型的初稿违约记录")
		}
	}

	// 判断应修改后上传初稿的任务
	for _, submitVideoTaskNeedModId := range submitVideoTaskNeedModIds {
		db2 := GetReadDB(context.Background())
		var DraftDefaultInMv int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("draft_default_in_mv").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[taskIdToProjectIdMap[submitVideoTaskNeedModId]]).First(&DraftDefaultInMv)
		dd, _ := time.ParseDuration(conv.MustString(DraftDefaultInMv, "") + "h")

		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitVideoTaskNeedModId]]).Find(&autoDefaultHandle)

		var sketchDefaultRate int
		var sketchErrRate int
		if taskIdToFeeFormMap[submitVideoTaskNeedModId] == 1 {
			sketchDefaultRate = autoDefaultHandle.SketchReplaceTimeOut
			sketchErrRate = autoDefaultHandle.SketchReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("sketch_replace_time_out", "sketch_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitVideoTaskNeedModId]]).Find(&sketchDefaultRate, &sketchErrRate)
		} else {
			sketchDefaultRate = autoDefaultHandle.SketchOtherTimeOut
			sketchErrRate = autoDefaultHandle.SketchOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("sketch_other_time_out", "sketch_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitVideoTaskNeedModId]]).Find(&sketchDefaultRate, &sketchErrRate)
		}
		db1 := GetReadDB(context.Background())
		var taskSketchInfo gorm_model.YounggeeSketchInfo
		db1.Model(gorm_model.YounggeeSketchInfo{}).Where("task_id = ? and is_review = 1", submitVideoTaskNeedModId).Order("reject_at desc").First(&taskSketchInfo)
		if taskSketchInfo.AutoSketchBreakAt.IsZero() {
			err4 := db1.Where("task_id = ?", submitVideoTaskNeedModId).Updates(&gorm_model.YounggeeSketchInfo{AutoSketchBreakAt: taskSketchInfo.RejectAt.Add(dd)}).Error
			if err4 != nil {
				return err4
			}
			taskSketchInfo.AutoSketchBreakAt = taskSketchInfo.RejectAt.Add(dd)
			fmt.Println("已添加视频形式的初稿违约自动处理时间")
		}
		if taskSketchInfo.TaskID != "" && taskSketchInfo.AutoSketchBreakAt.Sub(time.Now()) <= 0 {
			db4 := GetReadDB(context.Background())
			err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
				TaskID: submitVideoTaskNeedModId, ProjectID: taskIdToProjectIdMap[submitVideoTaskNeedModId], BreakType: 2, BreakAt: time.Now(), DefaultStatus: 1}).Error
			if err1 != nil {
				return err1
			}
			taskInfo := gorm_model.YoungeeTaskInfo{}
			dbt := GetReadDB(context.Background())
			err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitVideoTaskNeedModId).Find(&taskInfo).Error
			if err2 != nil {
				return err2
			}
			settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.LinkBreakRate+taskInfo.DataBreakRate+sketchDefaultRate+taskInfo.ScriptBreakRate)/100)
			if settleAmount <= 0 {
				settleAmount = 0.0
			}
			realPayment := taskInfo.AllPayment * (1.0 - float64(sketchErrRate)/100)
			if realPayment <= 0 {
				realPayment = 0.0
			}
			db3 := GetReadDB(context.Background())
			err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitVideoTaskNeedModId).Updates(
				map[string]interface{}{
					"cur_default_type":  3,
					"sketch_break_rate": sketchDefaultRate,
					"settle_amount":     settleAmount,
					"err_break_rate":    sketchErrRate,
					"real_payment":      realPayment,
				}).Error
			if err2 != nil {
				return err2
			}
			err = CreateTaskLog(context.Background(), submitVideoTaskNeedModId, "初稿逾期")
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
			}
			err = CreateMessageByTaskId(context.Background(), 22, 4, submitVideoTaskNeedModId)
			if err != nil {
				logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
			}
			fmt.Println("创建已提交初稿的视频类型的初稿违约记录")
		}
	}
	return nil
}

// GetAutoScriptDefaultTask 脚本超时违约判断
func GetAutoScriptDefaultTask() error {
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ? AND content_type = ?", 1, 2).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	var projectIds []string
	// 任务id 对 项目id 的map
	taskIdToProjectIdMap := map[string]string{}
	// 项目id 对 定时任务id 的map
	projectIdToAutoTaskIdMap := map[string]int{}
	// 项目id 对 违约定时任务id 的map
	projectIdToAutoDefaultIdMap := map[string]int{}
	// taskId 对 稿费形式的 map
	taskIdToFeeFormMap := make(map[string]int)
	// 首次提交脚本的任务记录id
	var videoTaskNeedModIds []string
	// 已提交脚本的任务记录id
	var submitVideoTaskNeedModIds []string
	var videoTaskIds []string
	// 构造map及list
	for _, projectInfo := range projectInfos {
		projectIds = append(projectIds, projectInfo.ProjectID)
		projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
		projectIdToAutoDefaultIdMap[projectInfo.ProjectID] = int(projectInfo.AutoDefaultID)

		var videoTaskInfos []*gorm_model.YoungeeTaskInfo
		db1 := GetReadDB(context.Background())
		db1.Model(&gorm_model.YoungeeTaskInfo{}).
			Where("project_id = ?  AND task_status = ? AND task_stage = ? AND cur_default_type = ? AND logistics_status = 3", projectInfo.ProjectID, 2, 7, 0).Find(&videoTaskInfos)
		for _, videoTaskInfo := range videoTaskInfos {
			taskIdToProjectIdMap[videoTaskInfo.TaskId] = projectInfo.ProjectID
			taskIdToFeeFormMap[videoTaskInfo.TaskId] = videoTaskInfo.FeeForm
			videoTaskIds = append(videoTaskIds, videoTaskInfo.TaskId)
		}
	}
	for _, videoTaskId := range videoTaskIds {
		var taskInfo gorm_model.YoungeeTaskInfo
		db3 := GetReadDB(context.Background())
		// 保存所有脚本上传状态为 1  且任务状态为8 的任务记录的id
		db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", videoTaskId).First(&taskInfo)
		if taskInfo.ScriptStatus == 1 && taskInfo.TaskStage == 7 {
			videoTaskNeedModIds = append(videoTaskNeedModIds, videoTaskId)
		}
		// 保存所有脚本上传状态为 3  且任务状态为8 的任务记录的id
		if taskInfo.ScriptStatus == 3 && taskInfo.TaskStage == 7 {
			submitVideoTaskNeedModIds = append(submitVideoTaskNeedModIds, videoTaskId)
		}
	}
	// 判断应首次上传脚本的任务
	for _, videoTaskNeedModId := range videoTaskNeedModIds {
		db2 := GetReadDB(context.Background())
		var ScriptDefault int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("script_default").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[taskIdToProjectIdMap[videoTaskNeedModId]]).First(&ScriptDefault)
		dd, _ := time.ParseDuration(conv.MustString(ScriptDefault, "") + "h")
		db1 := GetReadDB(context.Background())
		var taskLogisticInfo gorm_model.YoungeeTaskLogistics
		db1.Model(gorm_model.YoungeeTaskLogistics{}).Where("task_id = ?", videoTaskNeedModId).Find(&taskLogisticInfo)

		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[videoTaskNeedModId]]).Find(&autoDefaultHandle)
		//fmt.Printf("autoDefaultHandle%+v\n", autoDefaultHandle)
		var scriptDefaultRate int
		var scriptErrRate int
		if taskIdToFeeFormMap[videoTaskNeedModId] == 1 {
			scriptDefaultRate = autoDefaultHandle.ScriptReplaceTimeOut
			scriptErrRate = autoDefaultHandle.ScriptReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("script_replace_time_out", "script_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[videoTaskNeedModId]]).Find(&scriptDefaultRate, &scriptErrRate)
		} else {
			scriptDefaultRate = autoDefaultHandle.ScriptOtherTimeOut
			scriptErrRate = autoDefaultHandle.ScriptOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("script_other_time_out", "script_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[videoTaskNeedModId]]).Find(&scriptDefaultRate, &scriptErrRate)
		}
		if taskLogisticInfo.AutoScriptBreakAt == nil || taskLogisticInfo.AutoScriptBreakAt.IsZero() {
			t := taskLogisticInfo.SignedTime.Add(dd)
			db1.Where("task_id = ?", videoTaskNeedModId).Updates(&gorm_model.YoungeeTaskLogistics{AutoScriptBreakAt: &t})
			fmt.Println("已添加视频形式的脚本违约自动处理时间")
			if taskLogisticInfo.Status == 1 && taskLogisticInfo.TaskID != "" && taskLogisticInfo.SignedTime.Add(dd).Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: videoTaskNeedModId, ProjectID: taskIdToProjectIdMap[videoTaskNeedModId], BreakType: 1, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", videoTaskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.SketchBreakRate+taskInfo.DataBreakRate+taskInfo.LinkBreakRate+scriptDefaultRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(scriptErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", videoTaskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type":  1,
						"sketch_break_rate": scriptDefaultRate,
						"settle_amount":     settleAmount,
						"err_break_rate":    scriptErrRate,
						"real_payment":      realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), videoTaskNeedModId, "脚本逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 21, 4, videoTaskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建时已违约则创建视频类型的脚本违约记录")
			}
		} else {
			if taskLogisticInfo.Status == 1 && taskLogisticInfo.TaskID != "" && taskLogisticInfo.AutoScriptBreakAt.Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: videoTaskNeedModId, ProjectID: taskIdToProjectIdMap[videoTaskNeedModId], BreakType: 1, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", videoTaskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.SketchBreakRate+taskInfo.DataBreakRate+taskInfo.LinkBreakRate+scriptDefaultRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(scriptErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				fmt.Printf("脚本未上传扣款率： %+v\nrealPayment：%+v\n", scriptErrRate, realPayment)
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", videoTaskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type":  1,
						"script_break_rate": scriptDefaultRate,
						"settle_amount":     settleAmount,
						"err_break_rate":    scriptErrRate,
						"real_payment":      realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), videoTaskNeedModId, "脚本逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 21, 4, videoTaskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("已创建视频类型的脚本违约记录")
			}
		}
	}
	// 根据最近一次拒绝时间判断当前是否违约
	for _, submitVideoTaskNeedModId := range submitVideoTaskNeedModIds {
		db2 := GetReadDB(context.Background())
		var ScriptDefault int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("script_default").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[taskIdToProjectIdMap[submitVideoTaskNeedModId]]).First(&ScriptDefault)
		dd, _ := time.ParseDuration(conv.MustString(ScriptDefault, "") + "h")
		db1 := GetReadDB(context.Background())
		var taskScriptInfo gorm_model.YounggeeScriptInfo
		db1.Model(gorm_model.YounggeeScriptInfo{}).Where("task_id = ? and is_review = 1", submitVideoTaskNeedModId).Order("reject_at desc").First(&taskScriptInfo)

		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitVideoTaskNeedModId]]).Find(&autoDefaultHandle)

		var scriptDefaultRate int
		var scriptErrRate int
		if taskIdToFeeFormMap[submitVideoTaskNeedModId] == 1 {
			scriptDefaultRate = autoDefaultHandle.ScriptReplaceTimeOut
			scriptErrRate = autoDefaultHandle.ScriptReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("script_replace_time_out", "script_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitVideoTaskNeedModId]]).Find(&scriptDefaultRate, &scriptErrRate)
		} else {
			scriptDefaultRate = autoDefaultHandle.ScriptOtherTimeOut
			scriptErrRate = autoDefaultHandle.ScriptOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("script_other_time_out", "script_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitVideoTaskNeedModId]]).Find(&scriptDefaultRate, &scriptErrRate)
		}
		if taskScriptInfo.AutoScriptBreakAt.IsZero() {
			err4 := db1.Where("task_id = ?", submitVideoTaskNeedModId).Updates(&gorm_model.YounggeeScriptInfo{AutoScriptBreakAt: taskScriptInfo.RejectAt.Add(dd)}).Error
			if err4 != nil {
				return err4
			}
			fmt.Println("已添加视频形式的脚本违约自动处理时间")
			if taskScriptInfo.TaskID != "" && taskScriptInfo.RejectAt.Add(dd).Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: submitVideoTaskNeedModId, ProjectID: taskIdToProjectIdMap[submitVideoTaskNeedModId], BreakType: 1, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitVideoTaskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.SketchBreakRate+taskInfo.DataBreakRate+taskInfo.LinkBreakRate+scriptDefaultRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(scriptErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err3 := db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitVideoTaskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type":  1,
						"script_break_rate": scriptDefaultRate,
						"settle_amount":     settleAmount,
						"err_break_rate":    scriptErrRate,
						"real_payment":      realPayment,
					}).Error
				if err3 != nil {
					return err3
				}
				err = CreateTaskLog(context.Background(), submitVideoTaskNeedModId, "脚本逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 21, 4, submitVideoTaskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建时已违约则创建已提交脚本的视频类型的脚本违约记录")
			}
		} else {
			if taskScriptInfo.TaskID != "" && taskScriptInfo.AutoScriptBreakAt.Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: submitVideoTaskNeedModId, ProjectID: taskIdToProjectIdMap[submitVideoTaskNeedModId], BreakType: 1, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitVideoTaskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.SketchBreakRate+taskInfo.DataBreakRate+taskInfo.LinkBreakRate+scriptDefaultRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(scriptErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err3 := db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitVideoTaskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type":  1,
						"sketch_break_rate": scriptDefaultRate,
						"settle_amount":     settleAmount,
						"err_break_rate":    scriptErrRate,
						"real_payment":      realPayment,
					}).Error
				if err3 != nil {
					return err3
				}
				err = CreateTaskLog(context.Background(), submitVideoTaskNeedModId, "脚本逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 21, 4, submitVideoTaskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建已提交脚本的视频类型的脚本违约记录")
			}
		}
	}
	return nil
}

// GetAutoLinkBreachTask 链接超时违约判断
func GetAutoLinkBreachTask() error {
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ?", 1).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	var projectIds []string
	// 任务id 对 项目id 的map
	taskIdToProjectIdMap := map[string]string{}
	// 项目id 对 定时任务id 的map
	projectIdToAutoTaskIdMap := map[string]int{}
	// 项目id 对 违约定时任务id 的map
	projectIdToAutoDefaultIdMap := map[string]int{}
	var taskIds []string
	// taskId 对 稿费形式的 map
	taskIdToFeeFormMap := make(map[string]int)
	for _, projectInfo := range projectInfos {
		projectIds = append(projectIds, projectInfo.ProjectID)
		projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
		projectIdToAutoDefaultIdMap[projectInfo.ProjectID] = int(projectInfo.AutoDefaultID)

		var taskInfos []*gorm_model.YoungeeTaskInfo
		db1 := GetReadDB(context.Background())
		db1.Model(&gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = ? AND task_stage = ? AND cur_default_type = ? ", projectInfo.ProjectID, 2, 11, 0).Find(&taskInfos)
		for _, taskInfo := range taskInfos {
			taskIdToProjectIdMap[taskInfo.TaskId] = projectInfo.ProjectID
			taskIds = append(taskIds, taskInfo.TaskId)
			taskIdToFeeFormMap[taskInfo.TaskId] = taskInfo.FeeForm
		}
	}
	// 首次提交链接的任务记录id
	var taskNeedModIds []string
	// 已提交链接的任务记录id
	var submitTaskNeedModIds []string
	for _, taskId := range taskIds {
		var taskInfo gorm_model.YoungeeTaskInfo
		db3 := GetReadDB(context.Background())
		// 保存所有链接上传状态为 1  且任务状态为12 的任务记录的id
		db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).First(&taskInfo)
		if taskInfo.LinkStatus == 1 && taskInfo.TaskStage == 11 {
			taskNeedModIds = append(taskNeedModIds, taskId)
		}
		// 保存所有链接上传状态为 3  且任务状态为12 的任务记录的id
		if taskInfo.LinkStatus == 3 && taskInfo.TaskStage == 11 {
			submitTaskNeedModIds = append(submitTaskNeedModIds, taskId)
		}
	}
	for _, taskNeedModId := range taskNeedModIds {
		db2 := GetReadDB(context.Background())
		var linkBreach int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("link_breach").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[taskIdToProjectIdMap[taskNeedModId]]).First(&linkBreach)
		dd, _ := time.ParseDuration(conv.MustString(linkBreach, "") + "h")

		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[taskNeedModId]]).Find(&autoDefaultHandle)

		var linkDefaultRate int
		var linkErrRate int
		if taskIdToFeeFormMap[taskNeedModId] == 1 {
			linkDefaultRate = autoDefaultHandle.LinkReplaceTimeOut
			linkErrRate = autoDefaultHandle.LinkReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("link_replace_time_out", "link_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[taskNeedModId]]).Find(&linkDefaultRate, &linkErrRate)
		} else {
			linkDefaultRate = autoDefaultHandle.LinkOtherTimeOut
			linkErrRate = autoDefaultHandle.LinkOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("link_other_time_out", "link_other_not_uploadsg").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[taskNeedModId]]).Find(&linkDefaultRate, &linkErrRate)
		}
		db1 := GetReadDB(context.Background())
		var taskSketchInfo gorm_model.YounggeeSketchInfo
		db1.Model(gorm_model.YounggeeSketchInfo{}).Where("task_id = ? AND is_ok = ?", taskNeedModId, 1).Find(&taskSketchInfo)
		if taskSketchInfo.AutoLinkBreakAt.IsZero() {
			db1.Where("task_id = ?", taskNeedModId).Updates(&gorm_model.YounggeeSketchInfo{AutoLinkBreakAt: taskSketchInfo.AgreeAt.Add(dd)})
			fmt.Println("已添加链接违约自动处理时间")
			if taskSketchInfo.TaskID != "" && taskSketchInfo.AgreeAt.Add(dd).Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: taskNeedModId, ProjectID: taskIdToProjectIdMap[taskNeedModId], BreakType: 3, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(linkDefaultRate+taskInfo.DataBreakRate+taskInfo.SketchBreakRate+taskInfo.ScriptBreakRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(linkErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type": 5,
						"link_break_rate":  linkDefaultRate,
						"settle_amount":    settleAmount,
						"err_break_rate":   linkErrRate,
						"real_payment":     realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), taskNeedModId, "链接逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 23, 4, taskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建时已违约则创建链接违约记录")
			}
		} else {
			if taskSketchInfo.TaskID != "" && taskSketchInfo.AutoLinkBreakAt.Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: taskNeedModId, ProjectID: taskIdToProjectIdMap[taskNeedModId], BreakType: 3, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(linkDefaultRate+taskInfo.DataBreakRate+taskInfo.SketchBreakRate+taskInfo.ScriptBreakRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(linkErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type": 5,
						"link_break_rate":  linkDefaultRate,
						"settle_amount":    settleAmount,
						"err_break_rate":   linkErrRate,
						"real_payment":     realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), taskNeedModId, "链接逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 23, 4, taskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("已创建链接违约记录")
			}
		}
	}
	// 根据最近一次拒绝时间判断当前是否违约
	for _, submitTaskNeedModId := range submitTaskNeedModIds {
		db2 := GetReadDB(context.Background())
		var LinkBreach int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("link_breach").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[taskIdToProjectIdMap[submitTaskNeedModId]]).First(&LinkBreach)
		dd, _ := time.ParseDuration(conv.MustString(LinkBreach, "") + "h")

		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitTaskNeedModId]]).Find(&autoDefaultHandle)

		var linkDefaultRate int
		var linkErrRate int
		if taskIdToFeeFormMap[submitTaskNeedModId] == 1 {
			linkDefaultRate = autoDefaultHandle.LinkReplaceTimeOut
			linkErrRate = autoDefaultHandle.LinkReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("link_replace_time_out", "link_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitTaskNeedModId]]).Find(&linkDefaultRate, &linkErrRate)
		} else {
			linkDefaultRate = autoDefaultHandle.LinkOtherTimeOut
			linkErrRate = autoDefaultHandle.LinkOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("link_other_time_out", "link_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitTaskNeedModId]]).Find(&linkDefaultRate, &linkErrRate)
		}
		db1 := GetReadDB(context.Background())
		var taskLinkInfo gorm_model.YounggeeLinkInfo
		db1.Model(gorm_model.YounggeeLinkInfo{}).Where("task_id = ?", submitTaskNeedModId).Order("reject_at desc").First(&taskLinkInfo)
		if taskLinkInfo.AutoLinkBreakAt.IsZero() {
			err4 := db1.Where("task_id = ?", submitTaskNeedModId).Updates(&gorm_model.YounggeeLinkInfo{AutoLinkBreakAt: taskLinkInfo.RejectAt.Add(dd)}).Error
			if err4 != nil {
				return err4
			}
			fmt.Println("已添加链接违约自动处理时间")
			if taskLinkInfo.TaskID != "" && taskLinkInfo.RejectAt.Add(dd).Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: submitTaskNeedModId, ProjectID: taskIdToProjectIdMap[submitTaskNeedModId], BreakType: 3, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(linkDefaultRate+taskInfo.DataBreakRate+taskInfo.SketchBreakRate+taskInfo.ScriptBreakRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(linkErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type": 5,
						"link_break_rate":  linkDefaultRate,
						"settle_amount":    settleAmount,
						"err_break_rate":   linkErrRate,
						"real_payment":     realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), submitTaskNeedModId, "链接逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 23, 4, submitTaskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建时已违约则创建已提交链接的链接违约记录")
			}
		} else {
			if taskLinkInfo.TaskID != "" && taskLinkInfo.AutoLinkBreakAt.Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: submitTaskNeedModId, ProjectID: taskIdToProjectIdMap[submitTaskNeedModId], BreakType: 3, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(linkDefaultRate+taskInfo.DataBreakRate+taskInfo.SketchBreakRate+taskInfo.ScriptBreakRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(linkErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type":  5,
						"sketch_break_rate": linkDefaultRate,
						"settle_amount":     settleAmount,
						"err_break_rate":    linkErrRate,
						"real_payment":      realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), submitTaskNeedModId, "链接逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 23, 4, submitTaskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建已提交链接的链接违约记录")
			}
		}
	}
	return nil
}

// GetAutoCaseCloseDefaultTask 数据超时违约判断
func GetAutoCaseCloseDefaultTask() error {
	db := GetReadDB(context.Background())
	var projectInfos []*gorm_model.ProjectInfo
	err := db.Model(gorm_model.ProjectInfo{}).Where("project_type = ?", 1).Find(&projectInfos).Error
	if err != nil {
		return err
	}
	var projectIds []string
	// 任务id 对 项目id 的map
	taskIdToProjectIdMap := map[string]string{}
	// 项目id 对 定时任务id 的map
	projectIdToAutoTaskIdMap := map[string]int{}
	// 项目id 对 违约定时任务id 的map
	projectIdToAutoDefaultIdMap := map[string]int{}
	var taskIds []string
	for _, projectInfo := range projectInfos {
		projectIds = append(projectIds, projectInfo.ProjectID)
		projectIdToAutoTaskIdMap[projectInfo.ProjectID] = int(projectInfo.AutoTaskID)
		projectIdToAutoDefaultIdMap[projectInfo.ProjectID] = int(projectInfo.AutoDefaultID)

		var taskInfos []string
		db1 := GetReadDB(context.Background())
		db1.Select("task_id").Model(&gorm_model.YoungeeTaskInfo{}).Where("project_id = ? AND task_status = ? AND task_stage = ? AND cur_default_type = ? ", projectInfo.ProjectID, 2, 13, 0).Find(&taskInfos)
		for _, taskInfo := range taskInfos {
			taskIdToProjectIdMap[taskInfo] = projectInfo.ProjectID
			taskIds = append(taskIds, taskInfo)
		}
	}
	// 首次提交链接的任务记录id
	var taskNeedModIds []string
	// 已提交链接的任务记录id
	var submitTaskNeedModIds []string
	// taskId 对 稿费形式的 map
	taskIdToFeeFormMap := make(map[string]int)
	for _, taskId := range taskIds {
		var taskInfo gorm_model.YoungeeTaskInfo
		db3 := GetReadDB(context.Background())
		// 保存所有数据上传状态为 1  且任务状态为14 的任务记录的id
		db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskId).First(&taskInfo)
		if taskInfo.DataStatus == 1 && taskInfo.TaskStage == 13 {
			taskNeedModIds = append(taskNeedModIds, taskId)
		}
		// 保存所有数据上传状态为 3  且任务状态为14 的任务记录的id
		if taskInfo.DataStatus == 3 && taskInfo.TaskStage == 13 {
			submitTaskNeedModIds = append(submitTaskNeedModIds, taskId)
		}
		taskIdToFeeFormMap[taskId] = taskInfo.FeeForm
	}
	for _, taskNeedModId := range taskNeedModIds {
		db2 := GetReadDB(context.Background())
		var CaseCloseDefault int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("case_close_default").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[taskIdToProjectIdMap[taskNeedModId]]).First(&CaseCloseDefault)
		dd, _ := time.ParseDuration(conv.MustString(CaseCloseDefault, "") + "h")

		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[taskNeedModId]]).Find(&autoDefaultHandle)

		var dataDefaultRate int
		var dataErrRate int
		if taskIdToFeeFormMap[taskNeedModId] == 1 {
			dataDefaultRate = autoDefaultHandle.DataReplaceTimeOut
			dataErrRate = autoDefaultHandle.DataReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("data_replace_time_out", "data_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[taskNeedModId]]).Find(&dataDefaultRate, &dataErrRate)
		} else {
			dataDefaultRate = autoDefaultHandle.DataOtherTimeOut
			dataErrRate = autoDefaultHandle.DataOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("data_other_time_out", "data_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[taskNeedModId]]).Find(&dataDefaultRate, &dataErrRate)
		}
		//fmt.Println("dataDefaultRate:", dataDefaultRate)
		db1 := GetReadDB(context.Background())
		var taskLinkInfo gorm_model.YounggeeLinkInfo
		db1.Model(gorm_model.YounggeeLinkInfo{}).Where("task_id = ? AND is_ok = ?", taskNeedModId, 1).Find(&taskLinkInfo)
		if taskLinkInfo.AutoDataBreakAt.IsZero() {
			db1.Where("task_id = ?", taskNeedModId).Updates(&gorm_model.YounggeeLinkInfo{AutoDataBreakAt: taskLinkInfo.AgreeAt.Add(dd)})
			fmt.Println("已添加数据违约自动处理时间")
			if taskLinkInfo.TaskID != "" && taskLinkInfo.AgreeAt.Add(dd).Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: taskNeedModId, ProjectID: taskIdToProjectIdMap[taskNeedModId], BreakType: 4, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.SketchBreakRate+dataDefaultRate+taskInfo.LinkBreakRate+taskInfo.ScriptBreakRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(dataErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type": 7,
						"data_break_rate":  dataDefaultRate,
						"settle_amount":    settleAmount,
						"err_break_rate":   dataErrRate,
						"real_payment":     realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), taskNeedModId, "数据逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 24, 4, taskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建时已违约则创建数据违约记录")
			}
		} else {
			if taskLinkInfo.TaskID != "" && taskLinkInfo.AutoDataBreakAt.Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: taskNeedModId, ProjectID: taskIdToProjectIdMap[taskNeedModId], BreakType: 4, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.SketchBreakRate+dataDefaultRate+taskInfo.LinkBreakRate+taskInfo.ScriptBreakRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(dataErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				fmt.Println("settleAmount: ", settleAmount)
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", taskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type":  7,
						"sketch_break_rate": dataDefaultRate,
						"settle_amount":     settleAmount,
						"err_break_rate":    dataErrRate,
						"real_payment":      realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), taskNeedModId, "数据逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 24, 4, taskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("已创建数据违约记录")
			}
		}
	}
	// 根据最近一次拒绝时间判断当前是否违约
	for _, submitTaskNeedModId := range submitTaskNeedModIds {
		db2 := GetReadDB(context.Background())
		var LinkBreach int32
		db2.Model(&gorm_model.InfoAutoTask{}).Select("case_close_default").Where("auto_task_id = ?", projectIdToAutoTaskIdMap[taskIdToProjectIdMap[submitTaskNeedModId]]).First(&LinkBreach)
		dd, _ := time.ParseDuration(conv.MustString(LinkBreach, "") + "h")

		autoDefaultHandle := gorm_model.InfoAutoDefaultHandle{}
		db6 := GetReadDB(context.Background())
		db6.Model(gorm_model.InfoAutoDefaultHandle{}).Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitTaskNeedModId]]).Find(&autoDefaultHandle)

		var dataDefaultRate int
		var dataErrRate int
		if taskIdToFeeFormMap[submitTaskNeedModId] == 1 {
			dataDefaultRate = autoDefaultHandle.DataReplaceTimeOut
			dataErrRate = autoDefaultHandle.DataReplaceNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("data_replace_time_out", "data_replace_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitTaskNeedModId]]).Find(&dataDefaultRate, &dataErrRate)
		} else {
			dataDefaultRate = autoDefaultHandle.DataOtherTimeOut
			dataErrRate = autoDefaultHandle.DataOtherNotUpload
			// db6 := GetReadDB(context.Background())
			// db6.Model(gorm_model.InfoAutoDefaultHandle{}).Select("data_other_time_out", "data_other_not_upload").Where("auto_default_id = ?", projectIdToAutoDefaultIdMap[taskIdToProjectIdMap[submitTaskNeedModId]]).Find(&dataDefaultRate, &dataErrRate)
		}
		db1 := GetReadDB(context.Background())
		var taskDataInfo gorm_model.YounggeeDataInfo
		db1.Model(gorm_model.YounggeeDataInfo{}).Where("task_id = ? and is_review = 1", submitTaskNeedModId).Order("reject_at desc").First(&taskDataInfo)
		if taskDataInfo.AutoDataBreakAt.IsZero() {
			err4 := db1.Where("task_id = ?", submitTaskNeedModId).Updates(&gorm_model.YounggeeDataInfo{AutoDataBreakAt: taskDataInfo.RejectAt.Add(dd)}).Error
			if err4 != nil {
				return err4
			}
			fmt.Println("已添加数据违约自动处理时间")
			if taskDataInfo.TaskID != "" && taskDataInfo.RejectAt.Add(dd).Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: submitTaskNeedModId, ProjectID: taskIdToProjectIdMap[submitTaskNeedModId], BreakType: 4, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.SketchBreakRate+dataDefaultRate+taskInfo.LinkBreakRate+taskInfo.ScriptBreakRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(dataErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type": 7,
						"data_break_rate":  dataDefaultRate,
						"settle_amount":    settleAmount,
						"err_break_rate":   dataErrRate,
						"real_payment":     realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), submitTaskNeedModId, "数据逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 24, 4, submitTaskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建时已违约则创建已提交数据的数据违约记录")
			}
		} else {
			if taskDataInfo.TaskID != "" && taskDataInfo.AutoDataBreakAt.Sub(time.Now()) <= 0 {
				db4 := GetReadDB(context.Background())
				err1 := db4.Model(gorm_model.YoungeeContractInfo{}).Create(&gorm_model.YoungeeContractInfo{
					TaskID: submitTaskNeedModId, ProjectID: taskIdToProjectIdMap[submitTaskNeedModId], BreakType: 4, BreakAt: time.Now(), DefaultStatus: 1}).Error
				if err1 != nil {
					return err1
				}
				taskInfo := gorm_model.YoungeeTaskInfo{}
				dbt := GetReadDB(context.Background())
				err2 := dbt.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Find(&taskInfo).Error
				if err2 != nil {
					return err2
				}
				settleAmount := taskInfo.TaskReward * (1.0 - float64(taskInfo.SketchBreakRate+dataDefaultRate+taskInfo.LinkBreakRate+taskInfo.ScriptBreakRate)/100)
				if settleAmount <= 0 {
					settleAmount = 0.0
				}
				realPayment := taskInfo.AllPayment * (1.0 - float64(dataErrRate)/100)
				if realPayment <= 0 {
					realPayment = 0.0
				}
				db3 := GetReadDB(context.Background())
				err2 = db3.Model(gorm_model.YoungeeTaskInfo{}).Where("task_id = ?", submitTaskNeedModId).Updates(
					map[string]interface{}{
						"cur_default_type": 7,
						"data_break_rate":  dataDefaultRate,
						"settle_amount":    settleAmount,
						"err_break_rate":   dataErrRate,
						"real_payment":     realPayment,
					}).Error
				if err2 != nil {
					return err2
				}
				err = CreateTaskLog(context.Background(), submitTaskNeedModId, "数据逾期")
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateTaskLog error,err:%+v", err)
				}
				err = CreateMessageByTaskId(context.Background(), 24, 4, submitTaskNeedModId)
				if err != nil {
					logrus.WithContext(context.Background()).Errorf("[operate db] call CreateMessageByTaskId error,err:%+v", err)
				}
				fmt.Println("创建已提交数据的数据违约记录")
			}
		}
	}
	return nil
}

func GetHours(ctx context.Context) (*http_model.Hours, error) {
	db := GetReadDB(ctx)
	infoAuto := gorm_model.InfoAutoTask{}
	err := db.Model(gorm_model.InfoAutoTask{}).Last(&infoAuto).Error
	if err != nil {
		return nil, err
	}
	Hours := http_model.Hours{}
	Hours.DraftDefaultInPic = infoAuto.DraftDefaultInPic
	Hours.DraftDefaultInMv = infoAuto.DraftDefaultInMv
	Hours.ScriptDefault = infoAuto.ScriptDefault
	Hours.Invalid = infoAuto.Invalid
	Hours.LinkBreach = infoAuto.LinkBreach
	Hours.CaseClose = infoAuto.CaseClose
	Hours.CaseCloseDefault = infoAuto.CaseCloseDefault
	Hours.ReviewInMv = infoAuto.ReviewInMv
	Hours.ReviewUnlimited = infoAuto.ReviewUnlimited
	Hours.PostReview = infoAuto.Postreview
	Hours.SignInVirtual = infoAuto.SignInVirtual
	Hours.SignInOffline = infoAuto.SignInOffline
	return &Hours, err
}

func GetPercents(ctx context.Context) (*http_model.Percents, error) {
	db := GetReadDB(ctx)
	percentsInfo := gorm_model.InfoAutoDefaultHandle{}
	err := db.Model(gorm_model.InfoAutoDefaultHandle{}).Last(&percentsInfo).Error
	if err != nil {
		return nil, err
	}
	Percents := http_model.Percents{}
	Percents.DataReplaceNotUpload = percentsInfo.DataReplaceNotUpload
	Percents.DataReplaceTimeOut = percentsInfo.DataReplaceTimeOut
	Percents.DataOtherTimeOut = percentsInfo.DataOtherTimeOut
	Percents.DataOtherNotUpload = percentsInfo.DataOtherNotUpload
	Percents.LinkReplaceTimeOut = percentsInfo.LinkReplaceTimeOut
	Percents.LinkReplaceNotUpload = percentsInfo.LinkReplaceNotUpload
	Percents.LinkOtherTimeOut = percentsInfo.LinkOtherTimeOut
	Percents.LinkOtherNotUpload = percentsInfo.LinkOtherNotUpload
	Percents.ScriptReplaceTimeOut = percentsInfo.ScriptReplaceTimeOut
	Percents.ScriptReplaceNotUpload = percentsInfo.ScriptReplaceNotUpload
	Percents.ScriptOtherNotUpload = percentsInfo.ScriptOtherNotUpload
	Percents.ScriptOtherTimeOut = percentsInfo.ScriptOtherTimeOut
	Percents.SketchReplaceTimeOut = percentsInfo.SketchReplaceTimeOut
	Percents.SketchOtherNotUpload = percentsInfo.SketchOtherNotUpload
	Percents.SketchOtherTimeOut = percentsInfo.SketchOtherTimeOut
	Percents.SketchReplaceNotUpload = percentsInfo.SketchReplaceNotUpload
	return &Percents, err
}
