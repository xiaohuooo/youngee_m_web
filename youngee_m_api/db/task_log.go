package db

import (
	"context"
	"time"
	"youngee_m_api/model/gorm_model"

	"github.com/sirupsen/logrus"
)

func CreateTaskLog(ctx context.Context, taskId string, log string) error {
	db := GetReadDB(ctx)
	taskLog := gorm_model.YounggeeTaskLog{
		TaskID:  taskId,
		Content: log,
		LogAt:   time.Now(),
	}
	err := db.Model(gorm_model.YounggeeTaskLog{}).Create(&taskLog).Error
	if err != nil {
		logrus.WithContext(ctx).Errorf("[Sketch db] Insert YounggeeTaskLog error,err:%+v", err)
		return err
	}
	return nil
}
