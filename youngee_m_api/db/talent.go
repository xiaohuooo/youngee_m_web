package db

import (
	"context"
	"fmt"
	"github.com/caixw/lib.go/conv"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"youngee_m_api/consts"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
)

func PlatformAccInfo(ctx context.Context, talentId string) (*http_model.PlatformAccInfoPreView, error) {
	db := GetReadDB(ctx)
	var gormPlatform []*gorm_model.YoungeePlatformAccountInfo
	// 根据 talent_id 进行筛选
	db = db.Debug().Model(gorm_model.YoungeePlatformAccountInfo{}).Where("talent_id = ?", talentId)
	err := db.Find(&gormPlatform).Order("bind_date").Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		} else {
			return nil, err
		}
	}
	// 查询总数
	var total int64
	if err = db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[PlatformAccInfo] error query mysql total, err:%+v", err)
		return nil, err
	}
	platform := new(http_model.PlatformAccInfoPreView)
	platform.PlatformAccInfoData = pack.GormPlatformToHttpPlatform(gormPlatform)
	platform.Total = conv.MustString(total, "")
	return platform, err
}

func AccountIncome(ctx context.Context, talentId string) (*http_model.TalentInfoResponse, error) {
	db := GetReadDB(ctx)
	var talentInfo *gorm_model.YoungeeTalentInfo
	// 根据 talent_id 进行筛选
	err := db.Model(gorm_model.YoungeeTalentInfo{}).Where("id = ?", talentId).
		First(&talentInfo).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		} else {
			return nil, err
		}
	}
	data := new(http_model.TalentInfoResponse)
	data.Income = float32(talentInfo.Income)
	data.Withdrawed = float32(talentInfo.Withdrawed)
	data.Canwithdraw = float32(talentInfo.Canwithdraw)
	data.Withdrawing = float32(talentInfo.Withdrawing)

	// 查询是否绑定物流地址
	IsBindLocation := talentInfo.IsBindLocation
	if IsBindLocation == 1 {
		var deliveryInfo *gorm_model.YoungeeTalentDeliveryAddress
		db1 := GetReadDB(ctx)
		err = db1.Debug().Model(gorm_model.YoungeeTalentDeliveryAddress{}).
			Where("talent_id = ? AND default_tag = ?", talentId, 1).
			First(&deliveryInfo).Error
		//fmt.Printf("deliveryInfo%+v\n", deliveryInfo)
		if err != nil {
			if err == gorm.ErrRecordNotFound {
				return nil, nil
			} else {
				return nil, err
			}
		}
		region := GetRegion(ctx, deliveryInfo.RegionCode)
		data.ReceiverName = deliveryInfo.ReceiverName
		data.PhoneNumber = deliveryInfo.PhoneNumber
		data.DetailAddr = region + deliveryInfo.DetailAddr
	} else {
		data.ReceiverName = "暂无"
		data.PhoneNumber = "暂无"
		data.DetailAddr = "暂无"
	}
	region := ""
	if talentInfo.IsBindBank == 1 {
		db2 := GetReadDB(ctx)
		var talentBankInfo *gorm_model.YounggeeTalentBank
		err = db2.Model(gorm_model.YounggeeTalentBank{}).Where("talent_id = ?", talentId).
			First(&talentBankInfo).Error
		if err != nil {
			if err == gorm.ErrRecordNotFound {
				return nil, nil
			} else {
				return nil, err
			}
		}
		if talentBankInfo.BankOpenAddress != 0 {
			region = GetRegion(ctx, talentBankInfo.BankOpenAddress)
		}
		data.Bank = talentBankInfo.Bank
		data.BankOpenAddress = region
		data.BankCardNumber = talentBankInfo.BankCardNumber
		data.Name = talentBankInfo.Name
		data.AliPayNumber = "暂无"
		data.AliPayRealName = "暂无"
	} else {
		data.Bank = "暂无"
		data.BankCardNumber = "暂无"
		data.BankOpenAddress = "暂无"
		data.Name = "暂无"
		data.AliPayNumber = "暂无"
		data.AliPayRealName = "暂无"
	}
	return data, err
}

func GetRegion(ctx context.Context, regionCode int) string {
	db4 := GetReadDB(ctx)
	var infoRegion *gorm_model.InfoRegion
	db4.Debug().Model(gorm_model.InfoRegion{}).Where("self_code = ?", regionCode).First(&infoRegion)

	provinceCode := conv.MustString(regionCode, "")[0:2] + "0000"
	var province *gorm_model.InfoRegion
	db4.Debug().Model(gorm_model.InfoRegion{}).Where("self_code = ?", conv.MustInt(provinceCode, 0)).First(&province)

	cityCode := conv.MustString(regionCode, "")[0:4] + "00"
	var city *gorm_model.InfoRegion
	db4.Debug().Model(gorm_model.InfoRegion{}).Where("self_code = ?", conv.MustInt(cityCode, 0)).First(&city)
	fmt.Println("province,city,infoRegion Code :", provinceCode, cityCode, regionCode)
	fmt.Println("province,city,infoRegion RegionName :", province.RegionName, city.RegionName, infoRegion.RegionName)
	return province.RegionName + city.RegionName + infoRegion.RegionName
}

func GetTaskRecord(ctx context.Context, talentId string) (*http_model.GetTaskRecordResponse, error) {
	db := GetReadDB(ctx)
	var taskInfos []*gorm_model.YoungeeTaskInfo
	// 根据 talent_id 进行筛选
	db = db.Debug().Model(gorm_model.YoungeeTaskInfo{}).Where("talent_id = ?", talentId)
	// 查询总数
	var total int64
	if err := db.Count(&total).Error; err != nil {
		logrus.WithContext(ctx).Errorf("[GetTaskRecord] error query mysql total, err:%+v", err)
		return nil, err
	}
	err := db.Order("update_at desc").Find(&taskInfos).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, nil
		} else {
			return nil, err
		}
	}
	projectIdMap := map[string]gorm_model.ProjectInfo{}
	for _, taskInfo := range taskInfos {
		if _, ok := projectIdMap[taskInfo.ProjectId]; !ok {
			db1 := GetReadDB(ctx)
			projectInfo := gorm_model.ProjectInfo{}
			db1.Where("project_id = ?", taskInfo.ProjectId).First(&projectInfo)
			projectIdMap[taskInfo.ProjectId] = projectInfo
		}
	}
	var taskRecordDatas []*http_model.TaskRecordData
	for _, taskInfo := range taskInfos {
		updateAt := conv.MustString(taskInfo.UpdateAt, "")
		updateAt = updateAt[0:19]
		taskRecordData := http_model.TaskRecordData{
			ProjectId:       conv.MustString(taskInfo.ProjectId, ""),
			ProjectName:     projectIdMap[taskInfo.ProjectId].ProjectName,
			ProjectType:     consts.GetProjectType(projectIdMap[taskInfo.ProjectId].ProjectType),
			ProjectPlatform: consts.GetProjectPlatform(projectIdMap[taskInfo.ProjectId].ProjectPlatform),
			TaskId:          conv.MustString(taskInfo.TaskId, ""),
			TaskStage:       consts.GetTaskStage(taskInfo.TaskStage),
			UpdatedAt:       updateAt,
		}
		taskRecordDatas = append(taskRecordDatas, &taskRecordData)
	}
	taskRecordResponse := new(http_model.GetTaskRecordResponse)
	taskRecordResponse.TaskRecordData = taskRecordDatas
	taskRecordResponse.Total = conv.MustString(total, "")
	return taskRecordResponse, nil
}
