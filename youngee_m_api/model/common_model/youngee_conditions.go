package common_model

type YoungeeConditions struct {
	ProjectType int64 `condition:"project_type"` // 项目类型
	TaskType    int64 `condition:"task_type"`    // 任务形式
	Platform    int64 `condition:"platform"`     // 社媒平台
	ContentType int64 `condition:"content_type"` // 内容形式
}
