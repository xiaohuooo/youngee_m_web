package common_model

type EnterpriseUserConditions struct {
	User      string `condition:"user"`       // 企业用户ID
	Username  string `condition:"username"`   // 企业名称
	CreatedAt string `condition:"created_at"` // 创建时间
}
