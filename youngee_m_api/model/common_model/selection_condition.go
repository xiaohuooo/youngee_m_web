package common_model

type SelectionConditions struct {
	SelectionStatus int8   `condition:"selection_status"` // 选品阶段
	Platform        int8   `condition:"platform"`         // 社媒平台
	SampleMode      int8   `condition:"sample_mode"`      // 领样形式
	ContentType     int8   `condition:"content_type"`     // 内容形式
	TaskMode        int8   `condition:"task_mode"`        // 任务形式
	SearchValue     string `condition:"search_value"`     // 项目id或项目名称
	SubmitAt        string `condition:"submit_at"`        // 提交审核时间
	TaskDdl         string `condition:"task_ddl"`         // 任务截止时间
}
