package common_model

type ProjectCondition struct {
	ProjectId          string `condition:"project_id"`       // 项目ID
	ProjectName        string `condition:"project_name"`     // 项目名
	ProjectStatus      int64  `condition:"project_status"`   // 项目状态
	ProjectPlatform    int64  `condition:"project_platform"` // 项目平台
	ProjectForm        int64  `condition:"project_form"`     // 项目形式
	ProjectContentType int64  `condition:"content_type"`     // 内容形式
	ProjectUpdated     string `condition:"updated_at"`       // 最后操作时间
}
