package common_model

type PricingConditions struct {
	ProjectType int64  `condition:"project_type"` //项目类型
	FeeForm     int64  `condition:"fee_form"`     //稿费形式
	Platform    int64  `condition:"platform"`     //社媒平台
	UpdateAt    string `condition:"update_at"`    // 更新时间
}
