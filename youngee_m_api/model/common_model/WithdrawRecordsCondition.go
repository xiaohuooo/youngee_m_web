package common_model

type WithdrawRecordsCondition struct {
	TalentId   string `condition:"talent_id"`   // 达人ID
	Status     int32  `condition:"status"`      //提现状态
	SubmitAt   string `condition:"submit_at"`   // 申请提交时间
	WithdrawAt string `condition:"withdraw_at"` // 提现时间
}
