package common_model

type RechargeRecordsCondition struct {
	CommitAt  string `condition:"commit_at"`  // 充值申请提交时间
	ConfirmAt string `condition:"confirm_at"` // 充值确认时间
}
