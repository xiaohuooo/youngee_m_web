package common_model

type BreachHandledConditions struct {
	HandleResult    int32 `condition:"default_status"` // 处理结果
	TerminateReason int32 `condition:"break_type"`     // 解约原因
}
