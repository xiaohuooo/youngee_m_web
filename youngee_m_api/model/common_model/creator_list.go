package common_model

type CreatorListConditions struct {
	Id               string `condition:"id"`                 // 达人id
	TalentWxNickname string `condition:"talent_wx_nickname"` // 达人的微信昵称
	CreateDate       string `condition:"create_date"`        // 创建时间
	InBlacklist      int    `condition:"in_blacklist"`       // 是否拉黑
}
