package common_model

type TalentConditions struct {
	ProjectId        string `condition:"project_id"`        // 项目ID
	TaskStatus       int64  `condition:"task_status"`       // 任务状态
	LogisticsStatus  int64  `condition:"logistics_status"`  // 物流状态
	ScriptStatus     int64  `condition:"script_status"`     // 脚本状态
	SketchStatus     int64  `condition:"sketch_status"`     // 初稿状态
	LinkStatus       int64  `condition:"link_status"`       // 链接状态
	DataStatus       int64  `condition:"data_status"`       // 数据状态
	SettleStatus     int64  `condition:"settle_status"`     // 结算状态
	DefaultStatus    int64  `condition:"default_status"`    // 违约状态
	StrategyId       int64  `condition:"strategy_id"`       // 策略ID
	TaskId           string `condition:"task_id"`           // 任务ID
	PlatformNickname string `condition:"platform_nickname"` // 账号昵称
}
