package common_model

type TaskConditions struct {
	ProjectId        string `condition:"project_id"`        // 项目ID
	TaskStatus       int64  `condition:"task_status"`       // 任务状态
	StrategyId       int64  `condition:"strategy_id"`       // 策略ID
	TaskId           string `condition:"task_id"`           // 任务ID
	PlatformNickname string `condition:"platform_nickname"` // 账号昵称
}
