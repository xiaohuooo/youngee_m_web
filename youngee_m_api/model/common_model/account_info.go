package common_model

type AccountInfoConditions struct {
	PlatformID       int64  `condition:"platform_id"`       // 平台
	BindDate         string `condition:"bind_date"`         // 绑定时间
	FansLow          int64  `condition:"fans_low"`          // 最低粉丝数
	FansHigh         int64  `condition:"fans_high"`         // 最高粉丝数
	TalentId         string `condition:"talent_id"`         // 创作者ID
	PlatformNickname string `condition:"platform_nickname"` // 平台昵称
}
