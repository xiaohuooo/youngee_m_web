package common_model

type InvoiceRecordsCondition struct {
	SubmitAt  string `condition:"submit_at"`  // 申请提交时间
	BillingAt string `condition:"billing_at"` // 开票时间
}
