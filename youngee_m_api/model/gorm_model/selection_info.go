package gorm_model

import (
	"time"
)

type YounggeeSelectionInfo struct {
	SelectionID      string    `gorm:"column:selection_id;primary_key"` // 选品项目id
	SelectionName    string    `gorm:"column:selection_name"`           // 选品项目名称
	EnterpriseID     string    `gorm:"column:enterprise_id"`            // 所属企业id
	ProductID        int       `gorm:"column:product_id"`               // 关联商品id
	ContentType      int       `gorm:"column:content_type"`             // 内容形式，1代表图文，2代表视频
	SelectionStatus  int       `gorm:"column:selection_status"`         // 选品项目状态，1-8分别代表创建中、待审核、审核通过、待支付、已支付、执行中、失效、已结案
	TaskMode         int       `gorm:"column:task_mode"`                // 任务形式，1、2分别表示悬赏任务、纯佣带货
	Platform         int       `gorm:"column:platform"`                 // 项目平台，1-7分别代表小红书、抖音、微博、快手、b站、大众点评、知乎
	SampleMode       int       `gorm:"column:sample_mode"`              // 领样形式，1、2分别表示免费领样、垫付领样
	ProductUrl       string    `gorm:"column:product_url"`              // 带货链接
	SampleNum        int       `gorm:"column:sample_num"`               // 样品数量
	RemainNum        int       `gorm:"column:remain_num"`               // 剩余数量
	CommissionRate   int       `gorm:"column:commission_rate"`          // 佣金比例
	EstimatedCost    string    `gorm:"column:estimated_cost"`           // 预估成本
	TaskReward       string    `gorm:"column:task_reward"`              // 任务悬赏
	SampleCondition  string    `gorm:"column:sample_condition"`         // 领样条件
	RewardCondition  string    `gorm:"column:reward_condition"`         // 返现悬赏条件
	SettlementAmount string    `gorm:"column:settlement_amount"`        // 结算金额
	TaskDdl          time.Time `gorm:"column:task_ddl"`                 // 招募截止时间
	Detail           string    `gorm:"column:detail"`                   // 卖点总结
	ProductSnap      string    `gorm:"column:product_snap"`             // 商品信息快照
	ProductPhotoSnap string    `gorm:"column:product_photo_snap"`       // 商品图片快照
	CreatedAt        time.Time `gorm:"column:created_at"`               // 创建时间
	UpdatedAt        time.Time `gorm:"column:updated_at"`               // 修改时间
	SubmitAt         time.Time `gorm:"column:submit_at"`                // 提交审核时间
	PassAt           time.Time `gorm:"column:pass_at"`                  // 审核通过时间
	FailReason       int       `gorm:"column:fail_reason"`              // 失效原因，1、2分别表示逾期未支付、项目存在风险
	PayAt            time.Time `gorm:"column:pay_at"`                   // 支付时间
	FinishAt         time.Time `gorm:"column:finish_at"`                // 结案时间
	IsRead           int       `gorm:"column:is_read"`                  // 是否已读
}

func (m *YounggeeSelectionInfo) TableName() string {
	return "younggee_selection_info"
}
