// Code generated by sql2gorm. DO NOT EDIT.
package gorm_model

import (
	"time"
)

type YounggeeUser struct {
	ID            int64       `gorm:"column:id;primary_key;AUTO_INCREMENT"` // 用户表id
	User          string    `gorm:"column:user"`                          // 账号
	Username      string    `gorm:"column:username"`                      // 后台用户名
	Password      string    `gorm:"column:password"`                      // 用户密码
	RealName      string    `gorm:"column:real_name"`                     // 真实姓名
	Role          string    `gorm:"column:role"`                          // 角色 1，超级管理员； 2，管理员；3，企业用户
	Phone         string    `gorm:"column:phone"`                         // 绑定手机
	Email         string    `gorm:"column:email"`                         // 电子邮件
	LastLoginTime time.Time `gorm:"column:last_login_time"`               // 最后一次登录时间
	UserState     string    `gorm:"column:user_state;default:1;NOT NULL"` // 0，禁用，1，正常
	CreatedAt     time.Time `gorm:"column:created_at"`                    // 创建时间
	UpdatedAt     time.Time `gorm:"column:updated_at"`                    // 更新时间
}

func (m *YounggeeUser) TableName() string {
	return "younggee_user"
}

