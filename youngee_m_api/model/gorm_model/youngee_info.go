package gorm_model

import (
	"time"
)

type YounggeeTalentTeam struct {
	ID              int       `gorm:"column:id;primary_key;AUTO_INCREMENT"` // 递增id
	TeamID          string    `gorm:"column:team_id"`                       // young之团id，10位随机数
	TalentID        string    `gorm:"column:talent_id"`                     // 团长达人id，对应youngee_talent_info中id字段
	ProjectID       string    `gorm:"column:project_id"`                    // 项目id
	SelectionID     string    `gorm:"column:selection_id"`                  // 选品id
	ProjectType     int64     `gorm:"column:project_type"`                  // 项目类型，1为项目，2为选品
	ProjectName     string    `gorm:"column:project_name"`                  // 项目名称
	ProjectPhoto    string    `gorm:"column:project_photo"`                 // 项目主图
	Platform        int64     `gorm:"column:platform"`                      // 项目平台
	TeamStatus      int64     `gorm:"column:team_status"`                   // young之团状态，1表示创建中，2表示开团中，3表示已结束
	NumApply        int       `gorm:"column:num_apply"`                     // 报名人数
	NumApplySuccess int       `gorm:"column:num_apply_success"`             // 申请成功人数
	NumEnd          int       `gorm:"column:num_end"`                       // 结案人数
	PointIncome     int64     `gorm:"column:point_income"`                  // 积分收益
	MoneyIncome     string    `gorm:"column:money_income"`                  // 现金收益
	CreateAt        time.Time `gorm:"column:create_at"`                     // 成团时间
	EndAt           time.Time `gorm:"column:end_at"`                        // 结束时间
}

func (m *YounggeeTalentTeam) TableName() string {
	return "younggee_talent_team"
}
