package gorm_model

// Code generated by sql2gorm. DO NOT EDIT.

import (
	"time"
)

type YounggeeSecTaskInfo struct {
	ID                     int       `gorm:"column:id;primary_key"`                        // 递增id
	TaskID                 string    `gorm:"column:task_id"`                               // 选品任务id
	SelectionID            string    `gorm:"column:selection_id"`                          // 选品id
	TalentID               string    `gorm:"column:talent_id"`                             // 达人id
	AccountID              int       `gorm:"column:account_id"`                            // 账号id
	TalentPlatformInfoSnap string    `gorm:"column:talent_platform_info_snap"`             // 达人平台信息快照
	TalentPersonalInfoSnap string    `gorm:"column:talent_personal_info_snap"`             // 达人个人信息快照
	TalentPostAddrSnap     string    `gorm:"column:talent_post_addr_snap"`                 // 收货地址快照
	TaskReward             string    `gorm:"column:task_reward"`                           //  达人赏金
	TalentPayment          string    `gorm:"column:talent_payment"`                        // 达人垫付金额
	IsPayPayment           int       `gorm:"column:is_pay_payment"`                        // 企业是否返样品钱
	IsPayReward            int       `gorm:"column:is_pay_reward"`                         // 企业是否结算悬赏
	TaskMode               int       `gorm:"column:task_mode"`                             // 任务形式，1、2分别表示纯佣带货、悬赏任务
	SampleMode             int       `gorm:"column:sample_mode"`                           // 领样形式，1-3分别表示免费领样、垫付买样、不提供样品
	TaskStatus             int       `gorm:"column:task_status;default:1"`                 // 任务状态 1待选 2已选 3落选
	TaskStage              int       `gorm:"column:task_stage"`                            // 任务阶段，详情见info_sec_task_stage表
	CreateDate             time.Time `gorm:"column:create_date;default:CURRENT_TIMESTAMP"` // 创建时间
	SelectDate             time.Time `gorm:"column:select_date"`                           // 反选时间
	DeliveryDate           time.Time `gorm:"column:delivery_date"`                         // 发货时间
	CompleteDate           time.Time `gorm:"column:complete_date"`                         // 结束时间
	WithdrawDate           time.Time `gorm:"column:withdraw_date"`                         // 提现时间
	CompleteStatus         int       `gorm:"column:complete_status;default:1"`             // 结束方式 1未结束 2正常结束 3反选失败
	LogisticsStatus        int       `gorm:"column:logistics_status;default:1"`            // 发货状态 1 待发货 2已发货 3 已签收
	AssignmentStatus       uint      `gorm:"column:assignment_status;default:1"`           // 作业上传状态 1-5分别代表待添加、已添加、待修改、已修改、已通过
	UpdateAt               time.Time `gorm:"column:update_at"`                             // 更新时间
	WithdrawStatus         int       `gorm:"column:withdraw_status;default:1"`             // 提现状态，1-4分别代表不可提现、可提现、提现中、已提现
	LeadTeamID             string    `gorm:"column:lead_team_id"`                          // 作为团长的young之团id，对应younggee_talent_team中的team_id字段
	TeamID                 string    `gorm:"column:team_id"`                               // 作为团员的young之团id，对应younggee_talent_team中的team_id字段
	TeamIncome             int       `gorm:"column:team_income"`                           // young之团团长现金收益
	TeamPoint              int       `gorm:"column:team_point"`                            // young之团团长积分收益
}

func (m *YounggeeSecTaskInfo) TableName() string {
	return "younggee_sec_task_info"
}
