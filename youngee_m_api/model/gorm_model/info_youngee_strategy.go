package gorm_model

import (
	"time"
)

type InfoYoungeeStrategy struct {
	ID          int     `gorm:"column:id;primary_key;AUTO_INCREMENT"` //  id
	ProjectType int64   `gorm:"column:project_type;NOT NULL"`         // 项目类型，0表示不限，1为全流程项目，2为选品项目
	StrategyId  string  `gorm:"column:strategyId;NOT NULL"`           // 定价策略编号
	Platform    int64   `gorm:"column:platform;NOT NULL"`             // 项目平台，1-7分别代表小红书、抖音、微博、快手、b站、大众点评、知乎
	TaskType    int64   `gorm:"column:task_type;NOT NULL"`            // 任务形式，实体商品寄拍、虚拟产品测评、线下探店打卡、悬赏任务、纯佣带货
	ContentType int     `gorm:"column:content_type;NOT NULL"`         // 内容形式 图文、视频、直播
	Reason      int     `gorm:"column:reason"`                        // 奖励原因，成团-新用户、成团-老用户、成单-申请成功、成单-结案完毕；
	Points      float64 `gorm:"column:points;NOT NULL"`               // 积分奖励

	Cash float64 `gorm:"column:cash"` // 现金奖励

	Status   int       `gorm:"column:status"`             // '定价策略当前状态，0表示正常，1表示禁用',
	CreateAt time.Time `gorm:"column:create_at;NOT NULL"` // 创建时间
}

func (m *InfoYoungeeStrategy) TableName() string {
	return "info_youngee_strategy"
}
