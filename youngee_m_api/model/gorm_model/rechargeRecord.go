package gorm_model

// Code generated by sql2gorm. DO NOT EDIT.

import (
	"time"
)

type YounggeeRechargeRecord struct {
	RechargeID         string    `gorm:"column:recharge_id;primary_key"`       // 充值订单ID
	EnterpriseID       string    `gorm:"column:enterprise_id;NOT NULL"`        // 企业id
	RechargeAmount     float64   `gorm:"column:recharge_amount;NOT NULL"`      // 充值金额
	TransferVoucherUrl string    `gorm:"column:transfer_voucher_url;NOT NULL"` // 转账凭证图片链接
	Phone              string    `gorm:"column:phone;NOT NULL"`                // 联系方式
	RechargeMethod     int64     `gorm:"column:recharge_method;NOT NULL"`      // 充值方式：1为对公转账，2为支付宝在线支付，3为微信支付
	Status             int64     `gorm:"column:status;NOT NULL"`               // 充值状态：0为充值待确认，1为充值已确认
	InvoiceStatus      int64     `gorm:"column:invoice_status;NOT NULL"`       // 开票状态：1为可开票，2为待开票，3为已开票
	CommitAt           time.Time `gorm:"column:commit_at;NOT NULL"`            // 充值申请提交时间
	ConfirmAt          time.Time `gorm:"column:confirm_at"`                    // 充值确认时间
}

func (m *YounggeeRechargeRecord) TableName() string {
	return "younggee_recharge_record"
}
