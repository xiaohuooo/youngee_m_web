// Package gorm_model Code gorm_model by sql2gorm. DO NOT EDIT.
package gorm_model

import (
	"time"
)

type YoungeeTaskLogistics struct {
	LogisticsID           int64      `gorm:"column:logistics_id;primary_key;AUTO_INCREMENT"` // 货物-id
	CompanyName           string     `gorm:"column:company_name"`                            // 实物商品-物流公司名称
	LogisticsNumber       string     `gorm:"column:logistics_number"`                        // 实物商品-物流单号
	ExplorestoreStarttime time.Time  `gorm:"column:explorestore_starttime"`                  // 线下探店-探店开始时间
	ExplorestoreEndtime   time.Time  `gorm:"column:explorestore_endtime"`                    // 线下探店-探店结束时间
	ExplorestorePeriod    string     `gorm:"column:explorestore_period"`                     // 线下探店-探店持续时间
	CouponCodeInformation string     `gorm:"column:coupon_code_information"`                 // 虚拟产品-券码信息
	TaskID                string     `gorm:"column:task_id;NOT NULL"`                        // 任务id
	DeliveryTime          time.Time  `gorm:"column:delivery_time"`                           // 发货时间
	ThingsType            int64      `gorm:"column:things_type;NOT NULL"`                    // 任务类型：1 实物，2：线下探店，3：虚拟产品
	SignedTime            *time.Time `gorm:"column:signed_time"`                             // 实物商品-签收时间
	AutoSignAt            *time.Time `gorm:"column:auto_sign_at"`                            // 自动签收时间
	AutoScriptBreakAt     *time.Time `gorm:"column:auto_script_break_at"`                    // 脚本违约自动处理时间
	AutoSketchBreakAt     *time.Time `gorm:"column:auto_sketch_break_at"`                    // 初稿违约自动处理时间
	Status                int        `gorm:"column:status;default:0"`                        // 签收状态，0为未签收，1为已签收
}
