package gorm_model

// Code generated by sql2gorm. DO NOT EDIT.

import (
	"time"
)

type YounggeeWithdrawRecord struct {
	WithdrawID     string    `gorm:"column:withdraw_id;primary_key"`  // 提现订单ID
	TalentID       string    `gorm:"column:talent_id;NOT NULL"`       // 达人id
	WithdrawAmount float64   `gorm:"column:withdraw_amount;NOT NULL"` // 提现金额
	AmountPayable  float64   `gorm:"column:amount_payable;NOT NULL"`  // 应付金额
	TaskIDList     string    `gorm:"column:task_id_list;NOT NULL"`    // 该提现订单包含的task_id列表
	ReceiveInfo    string    `gorm:"column:receive_info;NOT NULL"`    // 收款信息
	Status         int       `gorm:"column:status;NOT NULL"`          // 提现状态：1为提现待确认，2为已提现
	BankType       int       `gorm:"column:bank_type;NOT NULL"`       // 到账方式，1为支付宝，2为银行卡
	SubmitAt       time.Time `gorm:"column:submit_at;NOT NULL"`       // 申请提交时间
	WithdrawAt     time.Time `gorm:"column:withdraw_at"`              // 提现时间
}

func (m *YounggeeWithdrawRecord) TableName() string {
	return "younggee_withdraw_record"
}
