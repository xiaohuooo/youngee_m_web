package redis_model

// Auth redis 存放的用户信息Session
type Auth struct {
	Phone        string `json:"phone"`
	ID           int64  `json:"id"`        // 用户表id
	User         string `json:"user"`      // 账号
	Username     string `json:"username"`  // 后台用户名
	RealName     string `json:"real_name"` // 真实姓名
	Role         string `json:"role"`      // 角色 1，超级管理员； 2，管理员；3，企业用户
	Email        string `json:"email"`     // 电子邮件
	Token        string `json:"token"`
}
