package system_model

type Config struct {
	Mysql  *Mysql  `yaml:"mysql"`
	Redis  *Redis  `yaml:"redis"`
	Server *Server `yaml:"server"`
}
type Mysql struct {
	Host     string `yaml:"host"`
	Port     int32  `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Database string `yaml:"database"`
}
type Redis struct {
	Host     string `yaml:"host"`
	Port     int32  `yaml:"port"`
	Auth     string `yaml:"auth"`
	Database int32  `yaml:"database"`
}
type Session struct {
	TTL int32 `yaml:"ttl"`
}
type Server struct {
	Host    string   `yaml:"host"`
	Port    int32    `yaml:"port"`
	Session *Session `yaml:"session"`
}
