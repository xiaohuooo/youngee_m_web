package http_model

type FindAllProductsRequest struct {
	EnterpriseID string `json:"enterprise_id"`
}

type ProductInfos struct {
	ProductID   int64  `json:"product_id"`
	BrandName   string `json:"brand_name"`
	ProductName string `json:"product_name"`
	ProductType string `json:"product_type"`
}

type FindAllProductsData struct {
	ProductInfos []ProductInfos `json:"product_infos"`
}

func NewFindAllProductsRequest() *FindAllProductsRequest {
	return new(FindAllProductsRequest)
}
func NewFindAllProductsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(FindAllProductsData)
	return resp
}
