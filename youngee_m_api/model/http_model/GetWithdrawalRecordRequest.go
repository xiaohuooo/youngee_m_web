package http_model

type GetWithdrawalRecordRequest struct {
	TalentId string `json:"talent_id"`
}

type WithdrawalRecordData struct {
	WithdrawAmount float32 `json:"withdraw_amount"`
	WithdrawAt     string  `json:"withdraw_at"`
}

type WithdrawalRecordPreview struct {
	WithdrawalRecordData []*WithdrawalRecordData `json:"withdrawal_record_data"`
}

func NewGetWithdrawalRecordRequest() *GetWithdrawalRecordRequest {
	return new(GetWithdrawalRecordRequest)
}

func NewGetWithdrawRecordRequest() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(WithdrawalRecordData)
	return resp
}
