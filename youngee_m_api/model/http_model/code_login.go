package http_model

type CodeLoginRequest struct {
	User     string `json:"user"`
	Password string `json:"password"`
}

type CodeLoginData struct {
	Token    string `json:"token"`
	Username string `json:"username"`
	Role     string `json:"role"`
}

func NewCodeLoginRequest() *CodeLoginRequest {
	return new(CodeLoginRequest)
}
func NewCodeLoginResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CodeLoginData)
	return resp
}
