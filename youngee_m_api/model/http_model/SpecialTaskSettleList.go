package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type SpecialTaskSettleListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	SettleStatus     string `json:"settle_status"`     // 结算状态，待结算、已结算
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type SpecialTaskSettlePreview struct {
	TaskID           string  `json:"task_id"`           // 任务ID
	PlatformNickname string  `json:"platform_nickname"` // 账号昵称
	FansCount        string  `json:"fans_count"`        // 粉丝数
	PlayNumber       int     `json:"play_number"`       // 播放量/阅读量
	LikeNumber       int     `json:"like_number"`       // 点赞数
	CommentNumber    int     `json:"comment_number"`    // 评论数
	CollectNumber    int     `json:"collect_number"`    // 收藏数
	LinkUrl          string  `json:"link_url"`          // 上传链接url
	PhotoUrl         string  `json:"photo_url"`         // 数据截图url
	AllPayment       float64 `json:"all_payment"`       // 企业支付
	RealPayment      float64 `json:"real_payment"`      // 企业实际支付（扣除违约扣款）
	Phone            string  `json:"phone"`             // 联系方式
	SubmitAt         string  `json:"submit_at"`         // 提交时间
	AgreeAt          string  `json:"agree_at"`          // 同意时间
	ReviseOpinion    string  `json:"revise_opinion"`    // 审稿意见
	UpdateAt         string  `json:"update_at"`         // 更新时间
}

type SpecialTaskSettleInfo struct {
	TaskID           string    `json:"task_id"`           // 任务ID
	PlatformNickname string    `json:"platform_nickname"` // 账号昵称
	FansCount        string    `json:"fans_count"`        // 粉丝数
	DataId           int       `json:"data_id"`           // 数据ID
	PlayNumber       int       `json:"play_number"`       // 播放量/阅读量
	LikeNumber       int       `json:"like_number"`       // 点赞数
	CommentNumber    int       `json:"comment_number"`    // 评论数
	CollectNumber    int       `json:"collect_number"`    // 收藏数
	LinkUrl          string    `json:"link_url"`          // 上传链接url
	PhotoUrl         string    `json:"photo_url"`         // 数据截图url
	AllPayment       float64   `json:"all_payment"`       // 企业支付
	RealPayment      float64   `json:"real_payment"`      // 企业实际支付（扣除违约扣款）
	ReviseOpinion    string    `json:"revise_opinion"`    // 审稿意见
	Phone            string    `json:"phone"`             // 联系方式
	CreateAt         time.Time `json:"create_at"`         // 创建时间
	SubmitAt         time.Time `json:"submit_at"`         // 提交时间
	AgreeAt          time.Time `json:"agree_at"`          // 同意时间
	UpdateAt         time.Time `json:"update_at"`         // 更新时间
	RejectAt         time.Time `json:"reject_at"`         // 拒绝时间
	IsReview         int       `json:"is_review"`         // 是否审核
}

type SpecialTaskSettle struct {
	Talent gorm_model.YoungeeTaskInfo
	Data   gorm_model.YounggeeDataInfo
	Link   gorm_model.YounggeeLinkInfo
}

type SpecialTaskSettleListData struct {
	SpecialTaskSettlePreview []*SpecialTaskSettlePreview `json:"project_task_pre_view"`
	Total                    string                      `json:"total"`
}

func NewSpecialTaskSettleListRequest() *SpecialTaskSettleListRequest {
	return new(SpecialTaskSettleListRequest)
}

func NewSpecialTaskSettleListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SpecialTaskSettleListData)
	return resp
}
