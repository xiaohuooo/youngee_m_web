package http_model

type ProjectPayRequest struct {
	ProjectID    string  `json:"project_id"`
	PaySum       float64 `json:"pay_sum"`
	EnterpriseId string  `json:"enterprise_id"`
}

type ProjectPayData struct {
	RecordId int64 `json:"record_id"`
}

func NewProjectPayRequest() *ProjectPayRequest {
	return new(ProjectPayRequest)
}
func NewProjectPayResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectPayData)
	return resp
}
