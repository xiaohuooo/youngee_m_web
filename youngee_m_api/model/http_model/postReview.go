package http_model

type PostReviewRequest struct {
	Time int32 `json:"time"`
}

func NewPostReviewRequest() *PostReviewRequest {
	return new(PostReviewRequest)
}

func NewPostReviewResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
