package http_model

type ProjectHandleRequest struct {
	ProjectID     string `json:"Project_id"`     // 项目id
	ProjectStatus string `json:"project_status"` // 项目状态
}

func NewProjectHandleRequest() *ProjectHandleRequest {
	return new(ProjectHandleRequest)
}

func NewProjectHandleResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
