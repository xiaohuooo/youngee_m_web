package http_model

type PassSecTaskCoopRequest struct {
	SelectionId string   `json:"selection_id"`
	TaskIds     []string `json:"task_ids"`
}

type PassSecTaskCoopData struct {
}

func NewPassSecTaskCoopRequest() *PassSecTaskCoopRequest {
	return new(PassSecTaskCoopRequest)
}

func NewPassSecTaskCoopResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(PassSecTaskCoopData)
	return resp
}
