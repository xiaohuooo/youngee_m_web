package http_model

type AcceptScriptRequest struct {
	TaskIds string `json:"task_id_list"` //任务id列表
}

type AcceptScriptData struct {
	TaskIds []string `json:"task_id_list"` //任务id列表
}

func NewAcceptScriptRequest() *AcceptScriptRequest {
	return new(AcceptScriptRequest)
}
func NewAcceptScriptResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(AcceptScriptData)
	return resp
}
