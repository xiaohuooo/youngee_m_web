package http_model

type GetInvoiceNumsRequest struct {
}

type InvoiceNums struct {
	InvoiceNums int `json:"invoice_nums"`
}

func NewGetInvoiceNumsRequest() *GetInvoiceNumsRequest {
	return new(GetInvoiceNumsRequest)
}

func NewGetInvoiceNumsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(InvoiceNums)
	return resp
}
