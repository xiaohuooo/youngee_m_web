package http_model

type SignInOfflineRequest struct {
	Time int32 `json:"time"`
}

func NewSignInOfflineRequest() *SignInOfflineRequest {
	return new(SignInOfflineRequest)
}

func NewSignInOfflineResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
