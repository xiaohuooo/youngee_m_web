package http_model

type GetSpecialFinishDataRequest struct {
	ProjectId string `json:"project_id"` // 项目id
}

type GetSpecialFinishDataData struct {
	FinishNumber  int64 `json:"finish_number"`  // 结案数量
	FanNumber     int   `json:"fan_number"`     // 粉丝数
	PlayNumber    int   `json:"play_number"`    // 播放量/阅读量
	LikeNumber    int   `json:"like_number"`    // 点赞数
	CommentNumber int   `json:"comment_number"` // 评论数
	CollectNumber int   `json:"collect_number"` // 收藏数
}

func NewGetSpecialFinishDataRequest() *GetSpecialFinishDataRequest {
	return new(GetSpecialFinishDataRequest)
}

func NewGetSpecialFinishDataResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSpecialFinishDataData)
	return resp
}
