package http_model

type TransferToPublicRequest struct {
	EnterpriseID       string  `json:"enterprise_id"`
	Amount             float64 `json:"amount"`
	TransferVoucherUrl string  `json:"transfer_voucher_url"`
}

func NewTransferToPublicRequest() *TransferToPublicRequest {
	return new(TransferToPublicRequest)
}

func NewTransferToPubliceResponse() *CommonResponse {
	return new(CommonResponse)
}
