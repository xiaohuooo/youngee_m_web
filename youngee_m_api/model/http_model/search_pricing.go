package http_model

type SearchPricingRequest struct {
	PageSize    int32  `json:"page_size"`
	PageNum     int32  `json:"page_num"`
	ProjectType string `json:"project_type"` // 项目类型
	FeeForm     string `json:"fee_form"`     // 稿费形式
	Platform    string `json:"platform"`     // 社媒平台
	UpdateAt    string `json:"update_at"`    // 创建时间
}

type SearchPricingPreview struct {
	ID             string `json:"id"`              // 编号
	StrategyId     string `json:"strategyId"`      // 定价策略编号
	ProjectType    string `json:"project_type"`    // 项目类型
	Platform       string `json:"platform"`        // 社媒平台
	ManuscriptForm string `json:"manuscript_form"` // 稿费形式
	Fans           string `json:"fans"`            // 对应创作者粉丝量
	ServiceCharge  string `json:"service_charge"`  // 基础报价
	ServiceRate    string `json:"service_rate"`    // 平台服务费
	UpdateTime     string `json:"update_time"`     // 更新时间
}

type SearchPricingData struct {
	SearchPricingPreview []*SearchPricingPreview `json:"search_pricing_preview"`
	Total                string                  `json:"total"`
}

func NewSearchPricingRequest() *SearchPricingRequest {
	return new(SearchPricingRequest)
}

func NewSearchPricingResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SearchPricingPreview)
	return resp
}
