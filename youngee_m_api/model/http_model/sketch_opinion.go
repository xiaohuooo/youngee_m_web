package http_model

type SketchOpinionRequest struct {
	StrategyID    int64  `json:"strategy_id"`    //招募策略id
	TaskID        string `json:"task_id"`        //任务-id
	SketchOpinion string `json:"sketch_opinion"` //初稿意见
}

type SketchOpinionData struct {
	TaskID string `json:"task_id"` // 脚本ID
}

func NewSketchOpinionRequest() *SketchOpinionRequest {
	return new(SketchOpinionRequest)
}
func NewSketchOpinionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SketchOpinionData)
	return resp
}
