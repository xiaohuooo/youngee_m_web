package http_model

type GetRechargeNumsRequest struct {
}

type RechargeNums struct {
	RechargeNums int64 `json:"recharge_nums"`
}

func NewGetRechargeNumsRequest() *GetRechargeNumsRequest {
	return new(GetRechargeNumsRequest)
}

func NewGetRechargeNumsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(RechargeNums)
	return resp
}
