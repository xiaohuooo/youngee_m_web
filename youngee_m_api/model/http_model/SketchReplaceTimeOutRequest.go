package http_model

type SketchReplaceTimeOutRequest struct {
	Rate int `json:"rate"`
}

func NewSketchReplaceTimeOutRequest() *SketchReplaceTimeOutRequest {
	return new(SketchReplaceTimeOutRequest)
}

func NewSketchReplaceTimeOutResponse() *CommonResponse {
	return new(CommonResponse)
}
