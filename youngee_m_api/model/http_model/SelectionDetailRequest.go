package http_model

import "youngee_m_api/model/gorm_model"

type SelectionDetailRequest struct {
	EnterpriseId string `json:"enterprise_id"`
	SelectionId  string `json:"selection_id"` // 选品id
}

type SelectionDetail struct {
	SelectionInfo    *gorm_model.YounggeeSelectionInfo  // 选品详情
	SelectionBrief   []*gorm_model.YounggeeSecBrief     // 选品brief列表
	SelectionExample []*gorm_model.YounggeeSecExample   // 选品示例列表
	ProductInfo      *gorm_model.YounggeeProduct        // 商品详情
	ProductPhotoInfo []*gorm_model.YounggeeProductPhoto // 商品图片列表
}

func NewSelectionDetailRequest() *SelectionDetailRequest {
	return new(SelectionDetailRequest)
}

func NewSelectionDetailResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SelectionDetail)
	return resp
}
