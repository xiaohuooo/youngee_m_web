package http_model

type SearchYoungeeRequest struct {
	PageSize    int32  `json:"page_size"`
	PageNum     int32  `json:"page_num"`
	ProjectType string `json:"project_type"` // 项目类型
	TaskType    string `json:"task_type"`    // 任务形式
	Platform    string `json:"platform"`     // 社媒平台
	ContentType string `json:"content_type"` // 内容形式
}

type SearchYoungeePreview struct {
	ID          string `json:"id"`           // 编号
	StrategyId  string `json:"strategyId"`   // 定价策略编号
	ProjectType string `json:"project_type"` // 项目类型
	Platform    string `json:"platform"`     // 社媒平台
	TaskType    string `json:"task_type"`    // 任务形式
	ContentType string `json:"content_type"` // 内容形式
	Reason      string `json:"reason"`       // 奖励原因
	Points      string `json:"points"`       // 积分奖励
	Cash        string `json:"cash"`         // 现金奖励
	Createat    string `json:"create_at"`    // 更新时间
}

type SearchYoungeeData struct {
	SearchYoungeePreview []*SearchYoungeePreview `json:"search_Youngee_preview"`
	Total                string                  `json:"total"`
}

func NewSearchYoungeeRequest() *SearchYoungeeRequest {
	return new(SearchYoungeeRequest)
}

func NewSearchYoungeeResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SearchYoungeePreview)
	return resp
}
