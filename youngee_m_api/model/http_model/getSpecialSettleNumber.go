package http_model

type GetSpecialSettleNumberRequest struct {
	ProjectId string `json:"project_id"` // 项目id
}

type GetSpecialSettleNumberData struct {
	SettleNumber   int64 `json:"settle_number"`   // 应结算数量
	UnsettleNumber int64 `json:"unsettle_number"` // 待结算数量
	SettledNumber  int64 `json:"settled_number"`  // 已结算数量
}

func NewGetSpecialSettleNumberRequest() *GetSpecialSettleNumberRequest {
	return new(GetSpecialSettleNumberRequest)
}

func NewGetSpecialSettleNumberResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSpecialSettleNumberData)
	return resp
}
