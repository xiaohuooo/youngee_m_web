package http_model

type AddPricingRequest struct {
	ProjectType    string  `json:"project_type"`    //项目类型
	ManuscriptForm string  `json:"manuscript_form"` //稿费形式
	Platform       string  `json:"platform"`        //社媒平台
	FansLow        int64   `json:"fans_low"`        //对应创作者 粉丝量
	FansHigh       int64   `json:"fans_high"`       //对应创作者 粉丝量
	ServiceCharge  float32 `json:"service_charge"`  //基础报价
	ServiceRate    int64   `json:"service_rate"`    //平台服务费
}

type AddPricingResponse struct {
}

func NewAddPricingRequest() *AddPricingRequest {
	return new(AddPricingRequest)
}

func NewAddPricingResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
