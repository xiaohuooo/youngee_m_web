package http_model

type GetSelectionInfoRequest struct {
	PageSize     int32  `json:"page_size"`
	PageNum      int32  `json:"page_num"`
	EnterpriseId int32  `json:"enterprise_id"`
	UpdateAt     string `json:"updated_at"`
}

type SelectionInfoPreview struct {
	SelectionId   string `json:"selection_id"`
	SelectionName string `json:"selection_name"`
	TaskModel     string `json:"task_model"`
	SampleModel   string `json:"sample_model"`
	UpdateAt      string `json:"update_at"`
}

type SelectionInfoData struct {
	SelectionInfoPreview []*SelectionInfoPreview `json:"selection_info_preview"`
	Total                string                  `json:"total"`
}

func NewGetSelectionInfoRequest() *GetSelectionInfoRequest {
	return new(GetSelectionInfoRequest)
}

func NewGetSelectionInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SelectionInfoData)
	return resp
}
