package http_model

type AcceptSketchRequest struct {
	TaskIds string `json:"task_id_list"` //任务id列表
}

type AcceptSketchData struct {
	TaskIds []string `json:"task_id_list"` //任务id列表
}

func NewAcceptSketchRequest() *AcceptSketchRequest {
	return new(AcceptSketchRequest)
}
func NewAcceptSketchResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(AcceptSketchData)
	return resp
}
