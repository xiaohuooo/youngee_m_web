package http_model

type SelectionReviewNumberRequest struct {
}

type ReviewNums struct {
	ReviewNums int64 `json:"review_nums"` // 选品待审核数量
}

func NewSelectionReviewNumberRequest() *SelectionReviewNumberRequest {
	return new(SelectionReviewNumberRequest)
}
func NewSelectionReviewNumberResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ReviewNums)
	return resp
}
