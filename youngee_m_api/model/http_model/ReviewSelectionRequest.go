package http_model

type ReviewSelectionRequest struct {
	EnterpriseId string `json:"enterprise_id"`
	SelectionId  string `json:"selection_id"`
	IsPass       int    `json:"is_pass"`
}

type ReviewSelectionData struct {
	SelectionId string `json:"selection_id"`
}

func NewReviewSelectionRequest() *ReviewSelectionRequest {
	return new(ReviewSelectionRequest)
}

func NewReviewSelectionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ReviewSelectionData)
	return resp
}
