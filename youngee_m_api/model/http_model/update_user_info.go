package http_model

type UpdateUserInfoRequest struct {
	User     string `json:"user"`
	Username string `json:"username"`
	Role     string `json:"role"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
}

func NewUpdateUserInfoRequest() *UpdateUserInfoRequest {
	return new(UpdateUserInfoRequest)
}

func NewUpdateUserInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
