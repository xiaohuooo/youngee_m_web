package http_model

type PaySumRequest struct {
	ProjectId string `json:"project_id"` // 项目ID
}
type PaySum struct {
	StrategyId int     `json:"strategy_id"` // 策略ID
	AllPayment float64 `json:"all_payment"` //任务的全部费用
}
type PaySumResponce struct {
	//ProjectId  string  `json:"project_id"`   // 项目ID
	PaySum []PaySum `json:"pay_sum"` //总数
	//FeeForm    int     `json:"fee_form""`    //稿费形式
}

func NewPaySumRequest() *PaySumRequest {
	return new(PaySumRequest)
}

func NewPaySumResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectTaskListData)
	return resp
}
