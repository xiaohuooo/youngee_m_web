package http_model

type GetBankInfoRequest struct {
	BankId          string `json:"bank_id"`
	BankOpenAddress string `json:"bank_open_address"`
}

type BankInfo struct {
	BankName        string `json:"bank_name"`
	BankOpenAddress string `json:"bank_open_address"`
}

func NewGetBankInfoRequest() *GetBankInfoRequest {
	return new(GetBankInfoRequest)
}

func NewGetBankInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(BankInfo)
	return resp
}
