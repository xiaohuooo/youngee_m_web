package http_model

type CreateUserRequest struct {
	Username string `json:"username"`
	Role     string `json:"role"`
	Password string `json:"password"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
}

func NewCreateUserRequest() *CreateUserRequest {
	return new(CreateUserRequest)
}

func NewCreateUserResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
