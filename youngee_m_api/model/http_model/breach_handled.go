package http_model

type BreachHandledRequest struct {
	PageSize        int32  `json:"page_size"`
	PageNum         int32  `json:"page_num"`
	HandleResult    string `json:"default_status"` // 处理结果
	TerminateReason string `json:"break_type"`     // 解约原因
	TaskId          string `json:"task_id"`        // 任务id
	ProjectName     string `json:"project_name"`   // 项目名称
}

type BreachHandledPreview struct {
	ContractId      int32  `json:"contract_id"`
	ProjectId       string `json:"project_id"`
	UserId          string `json:"user_id"`
	ProjectName     string `json:"project_name"`
	UserPhone       string `json:"user_phone"`
	TaskId          string `json:"task_id"`
	TalentId        string `json:"talent_id"`
	TalentPhone     string `json:"talent_phone"`
	TerminateReason string `json:"terminate_reason"` // 解约原因
	HandleResult    string `json:"handle_result"`    // 处理结果
	HandleAt        string `json:"handle_at"`        // 处理时间
}

type BreachHandledData struct {
	BreachHandledPreview []*BreachHandledPreview `json:"breach_handled_preview"`
	Total                int64                   `json:"total"`
}

func NewBreachHandledRequest() *BreachHandledRequest {
	return new(BreachHandledRequest)
}

func NewBreachHandledResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(BreachHandledData)
	return resp
}
