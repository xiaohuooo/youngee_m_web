package http_model

type DisabledUserRequest struct {
	User string `json:"user"`
}

func NewDisabledUserRequest() *DisabledUserRequest {
	return new(DisabledUserRequest)
}

func NewDisabledUserResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
