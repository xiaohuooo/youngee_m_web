package http_model

type AutoReviewUnlimitedRequest struct {
	Time int32 `json:"time"`
}

func NewAutoReviewUnlimitedRequest() *AutoReviewUnlimitedRequest {
	return new(AutoReviewUnlimitedRequest)
}

func NewAutoReviewUnlimitedResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
