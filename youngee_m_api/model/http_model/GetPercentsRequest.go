package http_model

type GetPercentsRequest struct {
}

type Percents struct {
	SketchOtherTimeOut     int `json:"sketch_other_time_out"`
	SketchOtherNotUpload   int `json:"sketch_other_not_upload"`
	SketchReplaceTimeOut   int `json:"sketch_replace_time_out"`
	SketchReplaceNotUpload int `json:"sketch_replace_not_upload"`
	ScriptOtherTimeOut     int `json:"script_other_time_out"`
	ScriptOtherNotUpload   int `json:"script_other_not_upload"`
	ScriptReplaceTimeOut   int `json:"script_replace_time_out"`
	ScriptReplaceNotUpload int `json:"script_replace_not_upload"`
	LinkOtherTimeOut       int `json:"link_other_time_out"`
	LinkOtherNotUpload     int `json:"link_other_not_upload"`
	LinkReplaceTimeOut     int `json:"link_replace_time_out"`
	LinkReplaceNotUpload   int `json:"link_replace_not_upload"`
	DataOtherNotUpload     int `json:"data_other_not_upload"`
	DataOtherTimeOut       int `json:"data_other_time_out"`
	DataReplaceTimeOut     int `json:"data_replace_time_out"`
	DataReplaceNotUpload   int `json:"data_replace_not_upload"`
}

func NewGetPercentsRequest() *GetPercentsRequest {
	return new(GetPercentsRequest)
}

func NewGetPercentsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(Percents)
	return resp
}
