package http_model

type GetEnterPriseBalanceRequest struct {
	EnterPriseId string `json:"enterprise_id"`
}

type BalanceData struct {
	Balance float64 `json:"balance"`
}

func NewEnterpriseBalanceRequest() *GetEnterPriseBalanceRequest {
	return new(GetEnterPriseBalanceRequest)
}

func NewGetEnterpriseBalanceResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(BalanceData)
	return resp
}
