package http_model

type LinkReplaceTimeOutRequest struct {
	Rate int `json:"rate"`
}

func NewLinkReplaceTimeOutRequest() *LinkReplaceTimeOutRequest {
	return new(LinkReplaceTimeOutRequest)
}

func NewLinkReplaceTimeOutResponse() *CommonResponse {
	return new(CommonResponse)
}
