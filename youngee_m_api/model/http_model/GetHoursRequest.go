package http_model

type GetHoursRequest struct {
}

type Hours struct {
	SignInOffline     int32 `json:"sign_in_offline"`
	SignInVirtual     int32 `json:"sign_in_virtual"`
	ReviewInMv        int32 `json:"review_in_mv"`
	ReviewUnlimited   int32 `json:"review_unlimited"`
	PostReview        int32 `json:"post_review"`
	CaseClose         int32 `json:"case_close"`
	Invalid           int32 `json:"invalid"`
	DraftDefaultInPic int32 `json:"draft_default_in_pic"`
	DraftDefaultInMv  int32 `json:"draft_default_in_mv"`
	ScriptDefault     int32 `json:"script_default"`
	LinkBreach        int32 `json:"link_breach"`
	CaseCloseDefault  int32 `json:"case_close_default"`
}

func NewGetHoursRequest() *GetHoursRequest {
	return new(GetHoursRequest)
}

func NewGetHoursResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(Hours)
	return resp
}
