package http_model

type ApproveProjectRequest struct {
	ProjectId string `json:"project_id"` // 项目ID
	IsApprove int64  `json:"is_approve"`
}

func NewApproveProjectRequest() *ApproveProjectRequest {
	return new(ApproveProjectRequest)
}

func NewApproveProjectResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
