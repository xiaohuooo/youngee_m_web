package http_model

type GetDefaultNumberInfoRequest struct {
	ProjectId   string `json:"project_id"`   // 项目id
	StrategyIds string `json:"strategy_ids"` // 招募策略id列表
}

type DefaultNumberInfo struct {
	StrategyId           int64 `json:"strategy_id"`            // 招募策略id
	ShouldFinishNumber   int64 `json:"should_finish_number"`   // 应结案数量
	UnuploadSketchNumber int64 `json:"unupload_sketch_number"` // 未传初稿违约数量
	UnuploadScriptNumber int64 `json:"unupload_script_number"` // 未传脚本违约数量
	UnuploadLinkNumber   int64 `json:"unupload_link_number"`   // 未传链接违约数量
	UnuploadDataNumber   int64 `json:"unupload_data_number"`   // 未传数据违约数量
	TerminateNumber      int64 `json:"terminate_number"`       // 解约数量
}

type GetDefaultNumberInfoData struct {
	DefaultNumberInfoList []*DefaultNumberInfo `json:"number_info_list"`
}

func NewGetDefaultNumberInfoRequest() *GetDefaultNumberInfoRequest {
	return new(GetDefaultNumberInfoRequest)
}
func NewGetDefaultNumberInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetDefaultNumberInfoData)
	return resp
}
