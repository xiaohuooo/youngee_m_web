package http_model

type GetSketchInfoByTaskIdRequest struct {
	TaskId string `json:"task_id"`
}

type SketchInfoResponse struct {
	Title    string `json:"title"`
	PhotoUrl string `json:"photo_url"`
	Type     int32  `json:"type"`
	Content  string `json:"content"`
}

func NewGetSketchInfoByTaskIdRequest() *GetSketchInfoByTaskIdRequest {
	return new(GetSketchInfoByTaskIdRequest)
}

func NewGetSketchInfoByTaskIdResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SketchInfoResponse)
	return resp
}
