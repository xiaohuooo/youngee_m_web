package http_model

type GetServiceChargeRequest struct {
	FollowersLow int64 `json:"followers_low"` // 达人粉丝数下限
	FollowersUp  int64 `json:"followers_up"`  // 达人粉丝数上限
	FeeForm      int64 `json:"fee_form"`      // 稿费形式
	Platform     int64 `json:"platform"`      // 项目平台，1-7分别代表小红书、抖音、微博、快手、b站、大众点评、知乎
}

type ServiceChargeData struct {
	ServiceCharge float64 `json:"service_charge"` // 服务费
}

func NewGetServiceChargeRequest() *GetServiceChargeRequest {
	return new(GetServiceChargeRequest)
}

func NewGetServiceChargeResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ServiceChargeData)
	return resp
}
