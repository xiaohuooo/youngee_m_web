package http_model

type RechargeBalanceRequest struct {
	Amount       float64 `json:"amount"`
	EnterpriseId string  `json:"enterprise_id"`
}

func NewRechargeBalanceResponse() *RechargeBalanceRequest {
	return new(RechargeBalanceRequest)
}

func NewRechargeBalanceRequest() *CommonResponse {
	return new(CommonResponse)
}
