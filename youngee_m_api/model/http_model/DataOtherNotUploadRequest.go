package http_model

type DataOtherNotUploadRequest struct {
	Rate int `json:"rate"`
}

func NewDataOtherNotUploadRequest() *DataOtherNotUploadRequest {
	return new(DataOtherNotUploadRequest)
}

func NewDataOtherNotUploadResponse() *CommonResponse {
	return new(CommonResponse)
}
