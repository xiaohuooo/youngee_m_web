package http_model

type SignForSpecialLogisticRequest struct {
	TaskId string `json:"task_id"`
}

func NewSignForSpecialLogisticRequest() *SignForSpecialLogisticRequest {
	return new(SignForSpecialLogisticRequest)
}

func NewSignForSpecialLogisticResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
