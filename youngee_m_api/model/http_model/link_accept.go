package http_model

type AcceptLinkRequest struct {
	TaskIds string `json:"task_id_list"` //任务id列表
}

type AcceptLinkData struct {
	TaskIds []string `json:"task_id_list"` //任务id列表
}

func NewAcceptLinkRequest() *AcceptLinkRequest {
	return new(AcceptLinkRequest)
}
func NewAcceptLinkResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(AcceptLinkData)
	return resp
}
