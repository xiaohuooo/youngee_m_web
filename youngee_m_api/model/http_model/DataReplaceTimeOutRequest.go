package http_model

type DataReplaceTimeOutRequest struct {
	Rate int `json:"rate"`
}

func NewDataReplaceTimeOutRequest() *DataReplaceTimeOutRequest {
	return new(DataReplaceTimeOutRequest)
}

func NewDataReplaceTimeOutResponse() *CommonResponse {
	return new(CommonResponse)
}
