package http_model

// CommonResponse 接口通用的返回结构体
type CommonResponse struct {
	Status  int32       `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
type CommonRequest interface {
}
