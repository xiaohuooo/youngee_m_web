package http_model

type GetTaskRecordRequest struct {
	TalentId string `json:"talent_id"`
}

type GetTaskRecordResponse struct {
	TaskRecordData []*TaskRecordData `json:"task_record_data"`
	Total          string            `json:"total"`
}

type TaskRecordData struct {
	ProjectId       string `json:"project_id"`
	ProjectName     string `json:"project_name"`
	ProjectType     string `json:"project_type"`
	ProjectPlatform string `json:"project_platform"`
	TaskId          string `json:"task_id"`
	TaskStage       string `json:"task_stage"`
	UpdatedAt       string `json:"updated_at"`
}

func NewGetTaskRecordRequest() *GetTaskRecordRequest {
	return new(GetTaskRecordRequest)
}

func NewGetTaskRecordResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetTaskRecordResponse)
	return resp
}
