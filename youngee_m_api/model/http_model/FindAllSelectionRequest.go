package http_model

import "youngee_m_api/model/gorm_model"

type FindAllSelectionRequest struct {
	EnterpriseId    string `json:"enterprise_id"`
	PageSize        int64  `json:"page_size"`
	PageNum         int64  `json:"page_num"`
	SelectionStatus int8   `json:"selection_status"` // 选品阶段
	Platform        int8   `json:"platform"`         // 社媒平台
	SampleMode      int8   `json:"sample_mode"`      // 领样形式
	ContentType     int8   `json:"content_type"`     // 内容形式
	TaskMode        int8   `json:"task_mode"`        // 任务形式
	SearchValue     string `json:"search_value"`     // 项目id或项目名称
	SubmitAt        string `json:"submit_at"`        // 提交审核时间
	TaskDdl         string `json:"task_ddl"`         // 提交审核时间
}

type SelectionData struct {
	SelectionInfo []*gorm_model.YounggeeSelectionInfo `json:"selection_info"`
	Total         string                              `json:"total"`
}

func NewFindAllSelectionRequest() *FindAllSelectionRequest {
	return new(FindAllSelectionRequest)
}

func NewFindAllSelectionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SelectionData)
	return resp
}
