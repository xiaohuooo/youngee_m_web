package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type SpecialTaskInviteListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	TaskStatus       string `json:"task_status"`       // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type SpecialTaskInvitePreview struct {
	TaskId             string `json:"task_id"`               // 任务ID
	PlatformNickname   string `json:"platform_nickname"`     // 账号昵称
	FansCount          string `json:"fans_count"`            // 粉丝数
	HomePageCaptureUrl string `json:"home_page_capture_url"` // 主页截图链接
	AllPayment         string `json:"all_payment"`           // 企业支付
	TaskReward         string `json:"task_reward"`           // 任务奖励金额
	HomePageUrl        string `json:"home_page_url"`         // 主页链接
	TaskStatus         string `json:"task_status"`           // 任务状态
	Phone              string `json:"phone"`                 // 联系方式
	CreateDate         string `json:"create_date"`           // 创建时间
}

type SpecialTaskInviteInfo struct {
	TaskID             string    `json:"task_id"`               // 任务id
	PlatformNickname   string    `json:"platform_nickname"`     // 在平台上的昵称
	FansCount          string    `json:"fans_count"`            // 粉丝数
	HomePageCaptureUrl string    `json:"home_page_capture_url"` // 主页截图链接
	AllPayment         float64   `json:"all_payment"`           // 企业支付
	TaskReward         float64   `json:"task_reward"`           // 任务奖励金额
	HomePageUrl        string    `json:"home_page_url"`         // 主页链接
	TaskStatus         string    `json:"task_status"`           // 任务状态 1待选 2已选 3落选
	Phone              string    `json:"phone"`                 // 联系方式
	CreateDate         time.Time `json:"create_date"`           // 创建时间
}

type SpecialTaskInvite struct {
	Talent gorm_model.YoungeeTaskInfo
}

type SpecialTaskInviteListData struct {
	SpecialTaskInvitePreview []*SpecialTaskInvitePreview `json:"project_task_pre_view"`
	Total                    string                      `json:"total"`
}

func NewSpecialTaskInviteListRequest() *SpecialTaskInviteListRequest {
	return new(SpecialTaskInviteListRequest)
}

func NewSpecialTaskInviteListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SpecialTaskInviteListData)
	return resp
}
