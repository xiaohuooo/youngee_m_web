package http_model

type GetDataNumberInfoRequest struct {
	ProjectId   string `json:"project_id"`   // 项目id
	StrategyIds string `json:"strategy_ids"` // 招募策略id列表
}

type DataNumberInfo struct {
	StrategyId         int64 `json:"strategy_id"`          // 招募策略id
	DataNumber         int64 `json:"data_number"`          // 应传数据数量
	DataUnreviewNumber int64 `json:"data_unreview_number"` // 数据待审数量
	DataPassedNumber   int64 `json:"data_passed_number"`   // 数据已通过数量
}

type GetDataNumberInfoData struct {
	DataNumberInfoList []*DataNumberInfo `json:"number_info_list"`
}

func NewGetDataNumberInfoRequest() *GetDataNumberInfoRequest {
	return new(GetDataNumberInfoRequest)
}
func NewGetDataNumberInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetDataNumberInfoData)
	return resp
}
