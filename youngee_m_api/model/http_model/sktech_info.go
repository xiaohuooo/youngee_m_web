package http_model

type GetSketchInfoRequest struct {
	TaskID string `json:"task_id"`
}

type SketchPhotoInfo struct {
	PhotoUrl string `json:"photo_url"` // 图片或视频url
	PhotoUid string `json:"photo_uid"`
	Symbol   int64  `json:"symbol"` // 标志位，1为详情图，2为视频
}

type GetSketchInfoData struct {
	SketchPhotos []SketchPhotoInfo `json:"sketch_photos"` //初稿图片以及视频
	Title        string            `json:"title"`
	Content      string            `json:"content"`
}

func NewGetSketchInfoRequest() *GetSketchInfoRequest {
	return new(GetSketchInfoRequest)
}
func NewGetSketchInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSketchInfoData)
	return resp
}
