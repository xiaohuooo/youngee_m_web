package http_model

type InvalidRequest struct {
	Time int32 `json:"time"`
}

func NewInvalidRequest() *InvalidRequest {
	return new(InvalidRequest)
}

func NewInvalidResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
