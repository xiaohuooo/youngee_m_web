package http_model

type SketchOtherNotUploadRequest struct {
	Rate int `json:"rate"`
}

func NewSketchOtherNotUploadRequest() *SketchOtherNotUploadRequest {
	return new(SketchOtherNotUploadRequest)
}

func NewSketchOtherNotUploadResponse() *CommonResponse {
	return new(CommonResponse)
}
