package http_model

type UpdateProjectPhoto struct {
	PhotoUrl string `json:"photo_url"` // 图片url
	PhotoUid string `json:"photo_uid"`
	FileName string `json:"name"` //文件名称
}

type UpdateRecruitStrategy struct {
	FeeForm       int64   `json:"fee_form"`       // 稿费形式，1-3分别代表自报价、固定稿费、产品置换
	StrategyID    int64   `json:"strategy_id"`    // 策略id
	FollowersLow  int64   `json:"followers_low"`  // 达人粉丝数下限
	FollowersUp   int64   `json:"followers_up"`   // 达人粉丝数上限
	RecruitNumber int64   `json:"recruit_number"` // 招募数量
	ServiceCharge float64 `json:"service_charge"` // 服务费
	Offer         float64 `json:"offer"`          // 报价
	TOffer        float64 `json:"t_offer"`        // 报价
}

type UpdateProjectRequest struct {
	ProjectForm      string                  `json:"project_form"`      // 项目形式，1-4分别代表实体商品寄拍、虚拟产品测评、线下探店打卡、素材微原创
	ProjectType      int64                   `json:"project_type"`      // 项目类型，1代表全流程项目，2代表专项项目
	TalentType       string                  `json:"talent_type"`       // 达人类型
	ProjectStatus    int64                   `json:"project_status"`    // 要保存的项目状态
	RecruitDdl       string                  `json:"recruit_ddl"`       // 招募截止时间
	ContentType      string                  `json:"content_type"`      // 内容形式，1代表图文，2代表视频
	ProjectDetail    string                  `json:"project_detail"`    // 项目详情
	RecruitStrategys []UpdateRecruitStrategy `json:"recruit_strategys"` // 定价策略
	ProjectPhotos    []UpdateProjectPhoto    `json:"project_photos"`    // 项目图片
	EnterpriseID     string                  `json:"enterprise_id"`     // 企业id
	ProjectID        string                  `json:"project_id"`        // 项目id
}

type UpdateProjectData struct {
	ProjectID string `json:"Project_id"` // 项目id
}

func NewUpdateProjectRequest() *UpdateProjectRequest {
	return new(UpdateProjectRequest)
}
func NewUpdateProjectResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(UpdateProjectData)
	return resp
}
