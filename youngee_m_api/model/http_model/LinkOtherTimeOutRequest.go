package http_model

type LinkOtherTimeOutRequest struct {
	Rate int `json:"rate"`
}

func NewLinkOtherTimeOutRequest() *LinkOtherTimeOutRequest {
	return new(LinkOtherTimeOutRequest)
}

func NewLinkOtherTimeOutResponse() *CommonResponse {
	return new(CommonResponse)
}
