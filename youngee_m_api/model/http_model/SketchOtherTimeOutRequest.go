package http_model

type SketchOtherTimeOutRequest struct {
	Rate int `json:"rate"`
}

func NewSketchOtherTimeOutRequest() *SketchOtherTimeOutRequest {
	return new(SketchOtherTimeOutRequest)
}

func NewSketchOtherTimeOutResponse() *CommonResponse {
	return new(CommonResponse)
}
