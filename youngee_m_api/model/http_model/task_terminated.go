package http_model

import (
	"youngee_m_api/model/gorm_model"
)

type TaskTerminatedListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	StrategyId       string `json:"strategy_id"`       // 策略ID
	DefaultStatus    string `json:"default_status"`    // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type TaskTerminatedPreview struct {
	TaskID            string  `json:"task_id"`             // 任务ID
	ProjectID         string  `json:"project_id"`          // 项目ID
	PlatformNickname  string  `json:"platform_nickname"`   // 账号昵称
	FansCount         string  `json:"fans_count"`          // 粉丝数
	RecruitStrategyID string  `json:"recruit_strategy_id"` // 招募策略ID
	StrategyID        string  `json:"strategy_id"`         // 报名选择的招募策略id
	AllPayment        float64 `json:"all_payment"`         // 企业支付
	RealPayment       float64 `json:"real_payment"`        // 企业实际支付（扣除违约扣款）
	HandleAt          string  `json:"handle_at"`           // 处理时间
	BreakType         string  `json:"break_type"`          // 违约类型（4类严重违约）：1（脚本）2（初稿）3（链接）4（数据）
}

type TaskTerminatedInfo struct {
	TaskID            string  `json:"task_id"`             // 任务ID
	ProjectID         string  `json:"project_id"`          // 项目ID
	PlatformNickname  string  `json:"platform_nickname"`   // 账号昵称
	FansCount         string  `json:"fans_count"`          // 粉丝数
	RecruitStrategyID int     `json:"recruit_strategy_id"` // 招募策略ID
	StrategyID        int     `json:"strategy_id"`         // 报名选择的招募策略id
	AllPayment        float64 `json:"all_payment"`         // 企业支付
	RealPayment       float64 `json:"real_payment"`        // 企业实际支付（扣除违约扣款）
	HandleAt          string  `json:"handle_at"`           // 处理时间
	BreakType         int     `json:"break_type"`          // 违约类型（4类严重违约）：1（脚本）2（初稿）3（链接）4（数据）
}

type TaskTerminated struct {
	Talent   gorm_model.YoungeeTaskInfo
	Default  gorm_model.YoungeeContractInfo
	LinkInfo gorm_model.YounggeeLinkInfo
}

type TaskTerminatedListData struct {
	TaskDefaultPreview []*TaskTerminatedPreview `json:"project_default_pre_view"`
	Total              string                   `json:"total"`
}

func NewTaskTerminatedListRequest() *TaskTerminatedListRequest {
	return new(TaskTerminatedListRequest)
}

func NewTaskTerminatedListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectTaskListData)
	return resp
}
