package http_model

type SpecialSettlePayRequest struct {
	EnterPriseId string  `json:"enterprise_id"`
	TaskId       string  `json:"task_id"`
	Amount       float64 `json:"amount"`
	ProjectID    string  `json:"project_id"`
}

func NewSpecialSettlePayRequest() *SpecialSettlePayRequest {
	return new(SpecialSettlePayRequest)
}

func NewSpecialSettlePayResponse() *CommonResponse {
	return new(CommonResponse)
}
