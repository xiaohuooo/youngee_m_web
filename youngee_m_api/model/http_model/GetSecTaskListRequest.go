package http_model

import (
	"time"
)

type GetSecTaskListRequest struct {
	PageSize      int64  `json:"page_size"`
	PageNum       int64  `json:"page_num"`
	SelectionId   string `json:"selection_id"`
	SecTaskStatus int    `json:"sec_task_status"`
	SearchValue   string `json:"search_value"`
}

type GetSecTaskListData struct {
	SecTaskList []*SecTaskInfo `json:"sec_task_list"`
	Total       string         `json:"total"`
}

type SecTaskInfo struct {
	SelectionId        string    `json:"selection_id"`
	PlatformNickname   string    `json:"platform_nickname"`                            // 帐号昵称
	FansCount          string    `json:"fans_count"`                                   // 粉丝数
	HomePageCaptureUrl string    `json:"home_page_capture_url"`                        // 主页截图链接
	HomePageUrl        string    `json:"home_page_url"`                                // 主页链接
	CreateDate         time.Time `json:"column:create_date;default:CURRENT_TIMESTAMP"` // 创建时间
	SelectDate         time.Time `json:"column:select_date"`                           // 反选时间
}

func NewGetSecTaskListRequest() *GetSecTaskListRequest {
	return new(GetSecTaskListRequest)
}

func NewGetSecTaskListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSecTaskListData)
	return resp
}
