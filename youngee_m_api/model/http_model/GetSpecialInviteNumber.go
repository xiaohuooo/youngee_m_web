package http_model

type GetSpecialInviteNumberRequest struct {
	ProjectId string `json:"project_id"` // 项目id
}

type GetSpecialInviteNumberData struct {
	UnpassNumber int64 `json:"unpass_number"` // 待确认数量
	PassNumber   int64 `json:"passed_number"` // 已确认数量
}

func NewGetSpecialInviteNumberRequest() *GetSpecialInviteNumberRequest {
	return new(GetSpecialInviteNumberRequest)
}

func NewGetSpecialInviteNumberResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSpecialInviteNumberData)
	return resp
}
