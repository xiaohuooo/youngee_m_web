package http_model

type GetAllProjectRequest struct {
	PageSize int32 `json:"page_size"`
	PageNum  int32 `json:"page_num"`
}

type GetAllProjectPreview struct {
	ProjectUpdated string `json:"project_updated"` // 最后操作时间
	EnterpriseID   string `json:"enterprise_id"`   // 所属企业id
	Username       string `json:"username"`        // 用户名称
	ProjectStatus  string `json:"project_status"`  // 项目状态
	Phone          string `json:"phone"`           // 联系方式
	ProjectId      string `json:"project_id"`      // 项目ID
}

type GetAllProjectData struct {
	AllProjectPreview []*GetAllProjectPreview `json:"all_project_preview"`
	Total             string                  `json:"total"`
}

func NewGetAllProjectRequest() *GetAllProjectRequest {
	return new(GetAllProjectRequest)
}
func NewGetAllProjectResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetAllProjectData)
	return resp
}
