package http_model

type GetFinishNumberInfoRequest struct {
	ProjectId   string `json:"project_id"`   // 项目id
	StrategyIds string `json:"strategy_ids"` // 招募策略id列表
}

type FinishNumberInfo struct {
	StrategyId         int64 `json:"strategy_id"`          // 招募策略id
	ShouldFinishNumber int64 `json:"should_finish_number"` // 应结案数量
	FinishedNumber     int64 `json:"finished_number"`      // 实际结案数量
}

type GetFinishNumberInfoData struct {
	FinishNumberInfoList []*FinishNumberInfo `json:"number_info_list"`
}

func NewGetFinishNumberInfoRequest() *GetFinishNumberInfoRequest {
	return new(GetFinishNumberInfoRequest)
}
func NewGetFinishNumberInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetFinishNumberInfoData)
	return resp
}
