package http_model

type RefuseSecTaskCoopRequest struct {
	TaskIds []string `json:"task_ids"`
}

type RefuseSecTaskCoopData struct {
}

func NewRefuseSecTaskCoopRequest() *RefuseSecTaskCoopRequest {
	return new(RefuseSecTaskCoopRequest)
}

func NewRefuseSecTaskCoopResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(RefuseSecTaskCoopData)
	return resp
}
