package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type SpecialTaskLinkListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	LinkStatus       string `json:"link_status"`       // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type SpecialTaskLinkPreview struct {
	TaskID           string `json:"task_id"`           // 任务ID
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
	FansCount        string `json:"fans_count"`        // 粉丝数
	LinkUrl          string `json:"link_url"`          // 上传链接url
	PhotoUrl         string `json:"photo_url"`         // 上传截图url
	Phone            string `json:"phone"`             // 联系方式
	Submit           string `json:"link_upload_time"`  // 创建时间
	AgreeAt          string `json:"agree_at"`          // 同意时间
	ReviseOpinion    string `json:"revise_opinion"`    // 审稿意见
}

type SpecialTaskLinkInfo struct {
	TaskID           string    `json:"task_id"`           // 任务ID
	PlatformNickname string    `json:"platform_nickname"` // 账号昵称
	FansCount        string    `json:"fans_count"`        // 粉丝数
	LinkId           int       `json:"link_id"`           // 链接ID
	LinkUrl          string    `json:"link_url"`          // 上传链接url
	PhotoUrl         string    `json:"photo_url"`         // 上传截图url
	ReviseOpinion    string    `json:"revise_opinion"`    // 审稿意见
	Phone            string    `json:"phone"`             // 联系方式
	CreateAt         time.Time `json:"create_at"`         // 创建时间
	SubmitAt         time.Time `json:"submit_at"`         // 提交时间
	AgreeAt          time.Time `json:"agree_at"`          // 同意时间
	RejectAt         time.Time `json:"reject_at"`         // 拒绝时间
	IsReview         int       `json:"is_review"`         // 是否审核
}

type SpecialTaskLink struct {
	Talent gorm_model.YoungeeTaskInfo
	Link   gorm_model.YounggeeLinkInfo
}

type SpecialTaskLinkListData struct {
	SpecialTaskLinkPreview []*SpecialTaskLinkPreview `json:"project_task_pre_view"`
	Total                  string                    `json:"total"`
}

func NewSpecialTaskLinkListRequest() *SpecialTaskLinkListRequest {
	return new(SpecialTaskLinkListRequest)
}

func NewSpecialTaskLinkListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SpecialTaskLinkListData)
	return resp
}
