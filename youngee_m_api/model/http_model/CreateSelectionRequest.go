package http_model

type CreateSelectionRequest struct {
	EnterpriseId string `json:"enterprise_id"`
	Platform     int    `json:"platform"`
	ProductId    string `json:"product_id"`
}

type CreateSelectionData struct {
	SelectionId string `json:"selection_id"`
}

func NewCreateSelectionRequest() *CreateSelectionRequest {
	return new(CreateSelectionRequest)
}

func NewCreateSelectionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CreateSelectionData)
	return resp
}
