package http_model

type GetReviewNumberInfoRequest struct {
	ProjectId   string `json:"project_id"`   // 项目id
	StrategyIds string `json:"strategy_ids"` // 招募策略id列表
}

type ReviewNumberInfo struct {
	StrategyId           int64 `json:"strategy_id"`            // 招募策略id
	ReviewNumber         int64 `json:"review_number"`          // 应审稿数量
	ScriptUnreviewNumber int64 `json:"script_unreview_number"` // 脚本待审数量
	ScriptPassedNumber   int64 `json:"script_passed_number"`   // 脚本已通过数量
	SketchUnreviewNumber int64 `json:"sketch_unreview_number"` // 初稿待审数量
	SketchPassedNumber   int64 `json:"sketch_passed_number"`   // 初稿已通过数量
}

type GetReviewNumberInfoData struct {
	ReviewNumberInfoList []*ReviewNumberInfo `json:"number_info_list"`
}

func NewGetReviewNumberInfoRequest() *GetReviewNumberInfoRequest {
	return new(GetReviewNumberInfoRequest)
}
func NewGetReviewNumberInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetReviewNumberInfoData)
	return resp
}
