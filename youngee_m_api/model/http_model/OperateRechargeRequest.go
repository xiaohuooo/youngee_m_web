package http_model

type OperateRechargeRequest struct {
	Method         int32   `json:"method"`
	RechargeId     string  `json:"recharge_id"`
	EnterpriseId   string  `json:"enterprise_id"`
	RechargeAmount float64 `json:"recharge_amount"`
}

func NewOperateRechargeRequest() *OperateRechargeRequest {
	return new(OperateRechargeRequest)
}

func NewOperateRechargeResponse() *CommonResponse {
	return new(CommonResponse)
}
