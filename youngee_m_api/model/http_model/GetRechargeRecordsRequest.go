package http_model

type GetRechargeRecordsRequest struct {
	PageSize       int32  `json:"page_size"`
	PageNum        int32  `json:"page_num"`
	Status         int32  `json:"status"`
	BusinessName   string `json:"business_name"`
	UserId         int64  `json:"user_id"`
	Username       string `json:"username"`
	RechargeMethod int32  `json:"recharge_method"`
	CommitAt       string `json:"commit_at"`
	ConfirmAt      string `json:"confirm_at"`
}

type RechargeRecordsPreview struct {
	RechargeId      string  `json:"recharge_id"`
	EnterpriseID    string  `json:"enterprise_id"`
	UserId          string  `json:"user_id"`
	Username        string  `json:"username"`
	BusinessName    string  `json:"business_name"`
	RechargeAmount  float64 `json:"recharge_amount"`
	RechargeMethod  string  `json:"recharge_method"`
	TransferVoucher string  `json:"transfer_voucher"` // 转账凭证
	Phone           string  `json:"phone"`
	CommitAt        string  `json:"commit_at"`
	ConfirmAt       string  `json:"confirm_at"`
}

type RechargeRecordsData struct {
	RechargeRecordsPreview []*RechargeRecordsPreview `json:"recharge_records_preview"`
	Total                  string                    `json:"total"`
}

func NewGetRechargeRecordsRequest() *GetRechargeRecordsRequest {
	return new(GetRechargeRecordsRequest)
}

func NewGetRechargeRecordsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(RechargeRecordsData)
	return resp
}
