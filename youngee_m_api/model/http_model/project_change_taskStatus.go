package http_model

type ProjectChangeTaskStatusRequest struct {
	TaskIds    []string `json:"taskIds"`
	TaskStatus string   `json:"task_status"`
	TaskStage  string   `json:"task_stage"`
	IsSpecial  string   `json:"is_special"`
	ProjectId  string   `json:"project_id"`
	ClickIndex string   `json:"clickIndex"`
}

func NewProjectChangeTaskStatusRequst() *ProjectChangeTaskStatusRequest {
	return new(ProjectChangeTaskStatusRequest)
}

func NewProjectChangeTaskStatusResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
