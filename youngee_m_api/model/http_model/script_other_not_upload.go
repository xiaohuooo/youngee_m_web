package http_model

type ScriptOtherNotUploadRequest struct {
	Rate int `json:"rate"`
}

func NewScriptOtherNotUploadRequest() *ScriptOtherNotUploadRequest {
	return new(ScriptOtherNotUploadRequest)
}

func NewScriptOtherNotUploadResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
