package http_model

type GetLoginUserRequest struct{}

type GetLoginUserList struct {
	User []string `json:"user"`
}

func NewGetLoginUserRequest() *GetLoginUserRequest {
	return new(GetLoginUserRequest)
}

func NewGetLoginUserResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetLoginUserList)
	return resp
}
