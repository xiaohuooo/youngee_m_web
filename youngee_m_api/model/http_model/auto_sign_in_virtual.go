package http_model

type SignInVirtualRequest struct {
	Time int32 `json:"time"`
}

func NewSignInVirtualRequest() *SignInVirtualRequest {
	return new(SignInVirtualRequest)
}

func NewSignInVirtualResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
