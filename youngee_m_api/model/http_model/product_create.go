package http_model

type CreateProductPhoto struct {
	PhotoUrl string `json:"photo_url"` // 图片或视频url
	PhotoUid string `json:"photo_uid"`
	Symbol   int64  `json:"symbol"` // 图片为主图或详情图标志位，1为主图，2为详情图，3为视频
}

type CreateProductRequest struct {
	ProductId     int64                `json:"product_id"`     // 产品id
	EnterpriseId  string               `json:"enterprise_id"`  // 要绑定的企业id
	ProductName   string               `json:"product_name"`   // 商品名称
	ProductType   int64                `json:"product_type"`   // 商品类型
	ShopAddress   string               `json:"shop_address"`   // 店铺地址，商品类型为线下品牌时需填写
	ProductDetail string               `json:"product_detail"` // 商品详细
	ProductPrice  float64              `json:"product_price"`  // 商品价值
	ProductPhotos []CreateProductPhoto `json:"product_photos"` // 商品图片列表
	ProductUrl    string               `json:"product_url"`    // 商品链接，可为电商网址、公司官网、大众点评的店铺地址等可以说明商品信息或者品牌信息的线上地址；
	BrandName     string               `json:"brand_name"`
}

type CreateProductData struct {
	ProductID int64 `json:"product_id"` // 商品id
}

func NewCreateProductRequest() *CreateProductRequest {
	return new(CreateProductRequest)
}
func NewCreateProductResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CreateProductData)
	return resp
}
