package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type SpecialTaskFinishDataListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type SpecialTaskFinishDataPreview struct {
	TaskID           string  `json:"task_id"`           // 任务ID
	PlatformNickname string  `json:"platform_nickname"` // 账号昵称
	FansCount        string  `json:"fans_count"`        // 粉丝数
	PlayNumber       int     `json:"play_number"`       // 播放量/阅读量
	LikeNumber       int     `json:"like_number"`       // 点赞数
	CommentNumber    int     `json:"comment_number"`    // 评论数
	CollectNumber    int     `json:"collect_number"`    // 收藏数
	PhotoUrl         string  `json:"photo_url"`         // 数据截图url
	RealPayment      float64 `json:"real_payment"`      // 企业实际支付（扣除违约扣款）
	Phone            string  `json:"phone"`             // 联系方式
	SubmitAt         string  `json:"submit_at"`         // 提交时间
	LinkUrl          string  `json:"link_url"`          // 上传链接url
}

type SpecialTaskFinishDataInfo struct {
	TaskID           string    `json:"task_id"`           // 任务ID
	PlatformNickname string    `json:"platform_nickname"` // 账号昵称
	FansCount        string    `json:"fans_count"`        // 粉丝数
	DataId           int       `json:"data_id"`           // 数据ID
	PlayNumber       int       `json:"play_number"`       // 播放量/阅读量
	LikeNumber       int       `json:"like_number"`       // 点赞数
	CommentNumber    int       `json:"comment_number"`    // 评论数
	CollectNumber    int       `json:"collect_number"`    // 收藏数
	PhotoUrl         string    `json:"photo_url"`         // 数据截图url
	RealPayment      float64   `json:"real_payment"`      // 企业实际支付（扣除违约扣款）
	LinkUrl          string    `json:"link_url"`          // 上传链接url
	Phone            string    `json:"phone"`             // 联系方式
	SubmitAt         time.Time `json:"submit_at"`         // 提交时间
}

type SpecialTaskFinishData struct {
	Talent gorm_model.YoungeeTaskInfo
	Data   gorm_model.YounggeeDataInfo
	Link   gorm_model.YounggeeLinkInfo
}

type SpecialTaskFinishDataListData struct {
	SpecialTaskFinishDataPreview []*SpecialTaskFinishDataPreview `json:"project_task_pre_view"`
	Total                        string                          `json:"total"`
}

func NewSpecialTaskFinishDataListRequest() *SpecialTaskFinishDataListRequest {
	return new(SpecialTaskFinishDataListRequest)
}

func NewSpecialTaskFinishDataListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SpecialTaskFinishDataListData)
	return resp
}
