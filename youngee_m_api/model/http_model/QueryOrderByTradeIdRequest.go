package http_model

type QueryOrderByTradeIdRequest struct {
	TradeId string `json:"trade_id"`
}

type OrderStatus struct {
	Status string `json:"status"`
}

func NewQueryOrderByTradeIdRequest() *QueryOrderByTradeIdRequest {
	return new(QueryOrderByTradeIdRequest)
}

func NewQueryOrderByTradeIdResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(OrderStatus)
	return resp
}
