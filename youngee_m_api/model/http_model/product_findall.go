package http_model

type FindAllProductRequest struct {
	UserID string `json:"userID"` // 用户ID
}

type ProductInfo struct {
	ProductID   int64  `json:"product_id"`
	BrandName   string `json:"brand_name"`
	ProductName string `json:"product_name"`
	ProductType string `json:"product_type"`
}

type FindAllProductData struct {
	ProductInfos []ProductInfo `json:"product_infos"`
}

func NewFindAllProductRequest() *FindAllProductRequest {
	return new(FindAllProductRequest)
}
func NewFindAllProductResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(FindAllProductData)
	return resp
}
