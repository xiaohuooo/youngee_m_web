package http_model

type DataOtherTimeOutRequest struct {
	Rate int `json:"rate"`
}

func NewDataOtherTimeOutRequest() *DataOtherTimeOutRequest {
	return new(DataOtherTimeOutRequest)
}

func NewDataOtherTimeOutResponse() *CommonResponse {
	return new(CommonResponse)
}
