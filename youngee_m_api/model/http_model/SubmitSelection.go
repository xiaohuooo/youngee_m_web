package http_model

type SubmitSelectionRequest struct {
	SelectionId     string `json:"selection_id"` // 选品id
	SelectionStatus int    `json:"selection_status"`
}

type SubmitSelectionData struct{}

func NewSubmitSelectionRequest() *SubmitSelectionRequest {
	return new(SubmitSelectionRequest)
}

func NewSubmitSelectionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SubmitSelectionData)
	return resp
}
