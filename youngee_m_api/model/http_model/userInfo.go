package http_model

type UserInfoRequest struct {
	Token string
}

type UserInfoResponse struct {
	User     string
	Username string
	Role     string
}

func NewUserInfoRequest() *UserInfoRequest {
	return new(UserInfoRequest)
}

func NewUserInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(UserInfoResponse)
	return resp
}
