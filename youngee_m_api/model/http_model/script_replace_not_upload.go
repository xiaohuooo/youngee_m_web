package http_model

type ScriptReplaceNotUploadRequest struct {
	Rate int `json:"rate"`
}

func NewScriptReplaceNotUploadRequest() *ScriptReplaceNotUploadRequest {
	return new(ScriptReplaceNotUploadRequest)
}

func NewScriptReplaceNotUploadResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
