package http_model

type TalentInfoRequest struct {
	TalentId string `json:"talent_id"`
}

type TalentInfoResponse struct {
	Income          float32 `json:"income"`            // 收益总数
	Withdrawing     float32 `json:"withdrawing"`       // 提现中金额
	Canwithdraw     float32 `json:"canwithdraw"`       // 可提现金额
	Withdrawed      float32 `json:"withdrawed"`        // 已提现金额
	DetailAddr      string  `json:"detail_addr"`       // 详细地址
	PhoneNumber     string  `json:"phone_number"`      // 联系电话
	ReceiverName    string  `json:"receiver_name"`     // 收货人名字
	Bank            string  `json:"bank"`              // 银行
	Name            string  `json:"name"`              // 户主姓名
	BankOpenAddress string  `json:"bank_open_address"` // 开户地址区域编码
	BankCardNumber  string  `json:"bank_card_number"`  // 银行卡号
	AliPayNumber    string  `json:"ali_pay_number"`    // 支付宝账号
	AliPayRealName  string  `json:"ali_pay_real_name"` // 支付宝真实姓名
}

func NewTalentInfoRequest() *TalentInfoRequest {
	return new(TalentInfoRequest)
}

func NewTalentInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(TalentInfoResponse)
	return resp
}
