package http_model

type TaskStrategyID struct {
	TaskId     string `json:"task_id"`
	StrategyId string `json:"strategy_id"`
}

type SignForReceiptRequest struct {
	TaskStrategyIds []TaskStrategyID `json:"task_strategy_ids"`
}

func NewSignForReceiptRequst() *SignForReceiptRequest {
	return new(SignForReceiptRequest)
}

func NewSignForReceiptResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
