package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type TaskDataListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	StrategyId       string `json:"strategy_id"`       // 策略ID
	DataStatus       string `json:"data_status"`       // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type TaskDataPreview struct {
	TaskID            string  `json:"task_id"`             // 任务ID
	PlatformNickname  string  `json:"platform_nickname"`   // 账号昵称
	FansCount         string  `json:"fans_count"`          // 粉丝数
	RecruitStrategyID string  `json:"recruit_strategy_id"` // 招募策略ID
	StrategyID        string  `json:"strategy_id"`         // 报名选择的招募策略id
	PlayNumber        int     `json:"play_number"`         // 播放量/阅读量
	LikeNumber        int     `json:"like_number"`         // 点赞数
	CommentNumber     int     `json:"comment_number"`      // 评论数
	CollectNumber     int     `json:"collect_number"`      // 收藏数
	PhotoUrl          string  `json:"photo_url"`           // 数据截图url
	AllPayment        float64 `json:"all_payment"`         // 企业支付
	RealPayment       float64 `json:"real_payment"`        // 企业实际支付（扣除违约扣款）
	Submit            string  `json:"submit_at"`           // 提交时间
	AgreeAt           string  `json:"agree_at"`            // 同意时间
	ReviseOpinion     string  `json:"revise_opinion"`      // 审稿意见
}

type TaskDataInfo struct {
	TaskID            string    `json:"task_id"`             // 任务ID
	PlatformNickname  string    `json:"platform_nickname"`   // 账号昵称
	FansCount         string    `json:"fans_count"`          // 粉丝数
	RecruitStrategyID int       `json:"recruit_strategy_id"` // 招募策略ID
	StrategyID        int       `json:"strategy_id"`         // 报名选择的招募策略id
	DataId            int       `json:"data_id"`             // 数据ID
	PlayNumber        int       `json:"play_number"`         // 播放量/阅读量
	LikeNumber        int       `json:"like_number"`         // 点赞数
	CommentNumber     int       `json:"comment_number"`      // 评论数
	CollectNumber     int       `json:"collect_number"`      // 收藏数
	PhotoUrl          string    `json:"photo_url"`           // 数据截图url
	AllPayment        float64   `json:"all_payment"`         // 企业支付
	RealPayment       float64   `json:"real_payment"`        // 企业实际支付（扣除违约扣款）
	ReviseOpinion     string    `json:"revise_opinion"`      // 审稿意见
	CreateAt          time.Time `json:"create_at"`           // 创建时间
	SubmitAt          time.Time `json:"submit_at"`           // 提交时间
	AgreeAt           time.Time `json:"agree_at"`            // 同意时间
	RejectAt          time.Time `json:"reject_at"`           // 拒绝时间
	IsReview          int       `json:"is_review"`           // 是否审核
}

type TaskData struct {
	Link   gorm_model.YounggeeLinkInfo
	Talent gorm_model.YoungeeTaskInfo
	Data   gorm_model.YounggeeDataInfo
}

type TaskDataListData struct {
	TaskDataPreview []*TaskDataPreview `json:"project_data_pre_view"`
	Total           string             `json:"total"`
}

func NewTaskDataListRequest() *TaskDataListRequest {
	return new(TaskDataListRequest)
}

func NewTaskDataListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectTaskListData)
	return resp
}
