package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type TaskSketchListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	StrategyId       string `json:"strategy_id"`       // 策略ID
	SketchStatus     string `json:"sketch_status"`     // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type TaskSketchPreview struct {
	TaskID            string `json:"task_id"`             // 任务ID
	SketchID          string `json:"sketch_id"`           //初稿ID
	PlatformNickname  string `json:"platform_nickname"`   // 账号昵称
	FansCount         string `json:"fans_count"`          // 粉丝数
	RecruitStrategyID string `json:"recruit_strategy_id"` //招募策略ID
	StrategyID        string `json:"strategy_id"`         // 报名选择的招募策略id
	Submit            string `json:"sketch_upload_time"`  //创建时间
	AgreeAt           string `json:"argee_at"`            //同意时间
	Title             string `json:"title"`               //初稿标题
	Content           string `json:"content"`             //初稿内容
	ReviseOpinion     string `json:"revise_opinion"`      //审稿意见
	//SketchPhotos      []SketchPhoto `json:"sketch_photos"`       //初稿图片以及视频
}

type TaskSketchInfo struct {
	TaskID            string    `json:"task_id"`           // 任务ID
	PlatformNickname  string    `json:"platform_nickname"` // 账号昵称
	FansCount         string    `json:"fans_count"`        // 粉丝数
	RecruitStrategyID int       `json:"recruit_strategy_id"`
	StrategyID        int       `json:"strategy_id"`    // 报名选择的招募策略id
	SketchId          int       `json:"sketch_id"`      //初稿ID
	Title             string    `json:"title"`          //初稿标题
	Content           string    `json:"content"`        //初稿内容
	ReviseOpinion     string    `json:"revise_opinion"` //审稿意见
	CreateAt          time.Time `json:"create_at"`      //创建时间
	SubmitAt          time.Time `json:"submit_at"`      // 提交时间
	AgreeAt           time.Time `json:"agree_at"`       //同意时间
	RejectAt          time.Time `json:"reject_at"`      //拒绝时间
	IsReview          int       `json:"is_review"`      //是否审核
	//SketchPhotos      []SketchPhoto `json:"sketch_photos"`  //初稿图片以及视频
}

type TaskSketch struct {
	Talent gorm_model.YoungeeTaskInfo
	Sketch gorm_model.YounggeeSketchInfo
	//Account   gorm_model.YoungeePlatformAccountInfo
}

type TaskSketchListData struct {
	TaskSketchPreview []*TaskSketchPreview `json:"project_sketch_pre_view"`
	Total             string               `json:"total"`
}

func NewTaskSketchListRequest() *TaskSketchListRequest {
	return new(TaskSketchListRequest)
}

func NewTaskSketchListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectTaskListData)
	return resp
}
