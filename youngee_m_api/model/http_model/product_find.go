package http_model

type FindProductRequest struct {
	ProductID int64 `json:"product_id"`
}

type ProductPhoto struct {
	PhotoUrl string `json:"photo_url"` // 图片或视频url
	PhotoUid string `json:"photo_uid"` // 图片uuid
	Symbol   int64  `json:"symbol"`    // 图片为主图或详情图标志位，1为主图，2为详情图，3为视频
}

type FindProductData struct {
	ProductID     int64          `json:"product_id"`
	ProductName   string         `json:"product_name"`   // 商品名称
	ProductType   int64          `json:"product_type"`   // 商品类型
	ShopAddress   string         `json:"shop_address"`   // 店铺地址，商品类型为线下品牌时需填写
	ProductPrice  float64        `json:"product_price"`  // 商品价值
	ProductDetail string         `json:"product_detail"` // 商品描述
	ProductPhotos []ProductPhoto `json:"product_photos"` // 商品图片列表
	ProductUrl    string         `json:"product_url"`    // 商品链接，可为电商网址、公司官网、大众点评的店铺地址等可以说明商品信息或者品牌信息的线上地址；
	EnterpriseID  string         `json:"enterprise_id"`  // 所属企业id
	BrandName     string         `json:"brand_name"`     // 品牌名称
}

func NewFindProductRequest() *FindProductRequest {
	return new(FindProductRequest)
}
func NewFindProductResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(FindProductData)
	return resp
}
