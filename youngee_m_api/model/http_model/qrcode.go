package http_model

type GetWxQRCodeRequest struct {
	Scene string `json:"scene"` // 需要携带的参数，目前为任务id
	Page  string `json:"page"`  // 页面path
}

type WxAccessTokenResponse struct {
	AccessToken string `json:"access_token"` // 获取到的凭证
	ExpiresIn   int    `json:"expires_in"`   // 凭证有效时间，单位：秒。目前是7200秒之内的值。
	Errcode     int    `json:"errcode"`      // 错误码
	Errmsg      string `json:"errmsg"`       // 错误信息
}

type WxQrCodeRequest struct {
	Scene      string `json:"scene"`
	Page       string `json:"page"`
	Width      int    `json:"width,omitempty"`
	CheckPath  bool   `json:"check_path,omitempty"`
	EnvVersion string `json:"env_version,omitempty"`
}

type WxQrCodeResponse struct {
	Errcode     int    `json:"errcode"`
	Errmsg      string `json:"errmsg"`
	ContentType string `json:"contentType"`
	Buffer      []byte `json:"buffer"`
}
type GetWxQRCodeData struct {
	QrCodeBytes []byte `json:"qr_code_bytes"`
}

func NewGetWxQRCodeRequest() *GetWxQRCodeRequest {
	return new(GetWxQRCodeRequest)
}

func NewGetWxQRCodeResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetWxQRCodeData)
	return resp
}
