package http_model

type AccountInfoRequest struct {
	PageSize         int32  `json:"page_size"`
	PageNum          int32  `json:"page_num"`
	PlatformID       string `json:"platform_id"`
	BindDate         string `json:"bind_date"`
	FansLow          int64  `json:"fans_low"`
	FansHigh         int64  `json:"fans_high"`
	TalentId         string `json:"talent_id"`
	PlatformNickname string `json:"platform_nickname"`
}

type AccountInfoData struct {
	TalentId           string `json:"talent_id"`             //创作者ID
	PlatformNickname   string `json:"platform_nickname"`     //账号昵称
	Platform           string `json:"platform"`              //社媒平台
	Fans               string `json:"fans"`                  //粉丝量
	Phone              string `json:"phone"`                 //联系方式
	BindDate           string `json:"bind_date"`             // 绑定时间
	HomePageCaptureUrl string `json:"home_page_capture_url"` //主页截图链接
	HomePageUrl        string `json:"home_page_url"`         //主页链接
	PlatformType       string `json:"platform_type"`         //标签及类型
}

type AccountInfoPreView struct {
	AccountInfoData []*AccountInfoData `json:"account_info_data"`
	Total           string             `json:"total"`
}

func NewAccountInfoRequset() *AccountInfoRequest {
	return new(AccountInfoRequest)
}

func NewAccountInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(AccountInfoPreView)
	return resp
}
