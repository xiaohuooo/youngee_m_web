package http_model

type DraftDefaultInPicRequest struct {
	Time int32 `json:"time"`
}

func NewDraftDefaultInPicRequest() *DraftDefaultInPicRequest {
	return new(DraftDefaultInPicRequest)
}

func NewDraftDefaultInPicResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
