package http_model

type UserListRequest struct {
	PageSize int32 `json:"page_size"`
	PageNum  int32 `json:"page_num"`
}
type UserListPreview struct {
	User      string `json:"user"`       // 账号
	Username  string `json:"username"`   // 后台用户名
	Password  string `json:"password"`   // 用户密码
	Role      string `json:"role"`       // 角色 1，超级管理员； 2，管理员；3，企业用户
	Phone     string `json:"phone"`      // 绑定手机
	Email     string `json:"email"`      // 电子邮件
	UserState string `json:"user_state"` // 0，禁用，1，正常
	CreatedAt string `json:"created_at"` // 创建时间
}
type UserListData struct {
	UserListPreview []*UserListPreview `json:"user_list_preview"`
	Total           string             `json:"total"`
}

func NewGetUserListRequest() *UserListRequest {
	return new(UserListRequest)
}
func NewGetUserListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(UserListData)
	return resp
}
