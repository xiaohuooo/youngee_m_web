package http_model

type DataReplaceNotUploadRequest struct {
	Rate int `json:"rate"`
}

func NewDataReplaceNotUploadRequest() *DataReplaceNotUploadRequest {
	return new(DataReplaceNotUploadRequest)
}

func NewDataReplaceNotUploadResponse() *CommonResponse {
	return new(CommonResponse)
}
