package http_model

type GetLinkNumberInfoRequest struct {
	ProjectId   string `json:"project_id"`   // 项目id
	StrategyIds string `json:"strategy_ids"` // 招募策略id列表
}

type LinkNumberInfo struct {
	StrategyId         int64 `json:"strategy_id"`          // 招募策略id
	LinkNumber         int64 `json:"link_number"`          // 应发布数量
	LinkUnreviewNumber int64 `json:"link_unreview_number"` // 链接未审数量
	LinkPassedNumber   int64 `json:"link_passed_number"`   // 链接已通过数量
}

type GetLinkNumberInfoData struct {
	LinkNumberInfoList []*LinkNumberInfo `json:"number_info_list"`
}

func NewGetLinkNumberInfoRequest() *GetLinkNumberInfoRequest {
	return new(GetLinkNumberInfoRequest)
}
func NewGetLinkNumberInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetLinkNumberInfoData)
	return resp
}
