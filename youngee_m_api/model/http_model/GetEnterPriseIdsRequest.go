package http_model

type GetEnterPriseIdsRequest struct {
}

type EnterPriseIds struct {
	EnterPriseIds []string `json:"enter_prise_ids"`
}

func NewGetEnterpriseIdsRequest() *GetEnterPriseIdsRequest {
	return new(GetEnterPriseIdsRequest)
}

func NewGetEnterpriseIdsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(EnterPriseIds)
	return resp
}
