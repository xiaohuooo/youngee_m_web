package http_model

type PaySelectionRequest struct {
	EnterpriseId string  `json:"enterprise_id"`
	PayMoney     float64 `json:"pay_money"`
	SelectionId  string  `json:"selection_id"`
}

type PaySelectionData struct {
	SelectionId string `json:"selection_id"`
}

func NewPaySelectionRequest() *PaySelectionRequest {
	return new(PaySelectionRequest)
}

func NewPaySelectionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(PaySelectionData)
	return resp
}
