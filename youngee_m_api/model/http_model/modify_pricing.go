package http_model

type ModifyPricingRequest struct {
	ID            string `json:"id"`
	StrategyId    string `json:"strategy_id"`
	ProjectType   string `json:"project_type"`   //项目类型
	FeeForm       string `json:"fee_form"`       //稿费形式
	Platform      string `json:"platform"`       //社媒平台
	FansLow       int64  `json:"fans_low"`       //对应创作者 粉丝量
	FansHigh      int64  `json:"fans_high"`      //对应创作者 粉丝量
	ServiceCharge int64  `json:"service_charge"` //平台服务费
	ServiceRate   int64  `json:"service_rate"`   //平台服务费率
}

type ModifyPricingResponse struct {
}

func NewModifyPricingRequest() *ModifyPricingRequest {
	return new(ModifyPricingRequest)
}

func NewModifyPricingResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
