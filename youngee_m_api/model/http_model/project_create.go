package http_model

type CreateProjectPhoto struct {
	PhotoUrl string `json:"photo_url"` // 图片url
	PhotoUid string `json:"photo_uid"`
}

type CreateRecruitStrategy struct {
	FeeForm       int64   `json:"fee_form"`       // 稿费形式，1-3分别代表自报价、固定稿费、产品置换
	StrategyID    int64   `json:"strategy_id"`    // 策略id
	FollowersLow  int64   `json:"followers_low"`  // 达人粉丝数下限
	FollowersUp   int64   `json:"followers_up"`   // 达人粉丝数上限
	RecruitNumber int64   `json:"recruit_number"` // 招募数量
	ServiceCharge float64 `json:"service_charge"` // 服务费
	Offer         float64 `json:"offer"`          // 报价
	TOffer        float64 `json:"t_offer"`        // 达人端报价
}

type CreateProjectRequest struct {
	ProjectName      string                  `json:"project_name"`      // 项目名称
	ProjectStatus    int64                   `json:"project_status"`    // 项目状态，1-7分别代表创建中、待审核、招募中、待支付、失效、执行中、已结案
	ProjectType      int64                   `json:"project_type"`      // 项目类型，1代表全流程项目，2代表专项项目
	ProjectPlatform  int64                   `json:"project_platform"`  // 项目平台，1-7分别代表红book、抖音、微博、快手、b站、大众点评、知乎
	ProjectForm      int64                   `json:"project_form"`      // 项目形式，1-4分别代表实体商品寄拍、虚拟产品测评、线下探店打卡、素材微原创
	TalentType       string                  `json:"talent_type"`       // 达人类型
	RecruitDdl       string                  `json:"recruit_ddl"`       // 招募截止时间
	ContentType      int64                   `json:"content_type"`      // 内容形式，1代表图文，2代表视频
	ProjectDetail    string                  `json:"project_detail"`    // 项目详情
	RecruitStrategys []CreateRecruitStrategy `json:"recruit_strategys"` // 定价策略
	ProjectPhotos    []CreateProjectPhoto    `json:"project_photos"`    // 项目图片
	ProductID        int64                   `json:"product_id"`        // 关联商品id
	EnterpriseID     string                  `json:"enterprise_id"`     // 企业id
}

type CreateProjectData struct {
	ProjectID string `json:"Project_id"` // 项目id
}

func NewCreateProjectRequest() *CreateProjectRequest {
	return new(CreateProjectRequest)
}
func NewCreateProjectResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CreateProjectData)
	return resp
}
