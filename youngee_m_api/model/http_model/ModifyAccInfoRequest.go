package http_model

type ModifyAccInfoRequest struct {
	User               string `json:"user"`                  //管理员id
	AccountId          int    `json:"account_id"`            //账号所属id
	PlatformNickname   string `json:"platform_nickname"`     //账号昵称
	Fans               int64  `json:"fans"`                  //粉丝量
	HomePageCaptureUrl string `json:"home_page_capture_url"` //主页截图链接
	HomePageUrl        string `json:"home_page_url"`         //主页链接
}

func NewModifyAccInfoRequest() *ModifyAccInfoRequest {
	return new(ModifyAccInfoRequest)
}

func NewModifyAccInfoResponse() *CommonResponse {
	return new(CommonResponse)
}
