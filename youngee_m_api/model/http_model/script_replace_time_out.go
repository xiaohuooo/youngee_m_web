package http_model

type ScriptReplaceTimeOutRequest struct {
	Rate int `json:"rate"`
}

func NewScriptReplaceTimeOutRequest() *ScriptReplaceTimeOutRequest {
	return new(ScriptReplaceTimeOutRequest)
}

func NewScriptReplaceTimeOutResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
