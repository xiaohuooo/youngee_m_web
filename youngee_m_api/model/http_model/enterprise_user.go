package http_model

import "youngee_m_api/model/gorm_model"

type EnterpriseUserRequest struct {
	PageSize  int32  `json:"page_size"`
	PageNum   int32  `json:"page_num"`
	User      string `json:"user"`       // 企业用户ID
	Username  string `json:"username"`   // 企业名称
	CreatedAt string `json:"created_at"` // 创建时间
}

type EnterpriseUserPreview struct {
	User             string  `json:"user"`              // 企业用户ID
	UserID           string  `json:"userID"`            // 用户ID
	EnterpriseID     string  `json:"enterprise_id"`     // 企业ID
	Username         string  `json:"username"`          // 企业名称
	Balance          float64 `json:"balance"`           // 账户余额
	FrozenBalance    float64 `json:"frozen_balance"`    // 冻结余额
	AvailableBalance float64 `json:"available_balance"` // 可用余额
	Phone            string  `json:"phone"`             // 联系方式
	CreatedAt        string  `json:"created_at"`        // 创建时间
}

type EnterpriseUserData struct {
	EnterpriseUserPreview []*EnterpriseUserPreview `json:"enterprise_user_preview"`
	Total                 string                   `json:"total"`
}

type EnterpriseUser struct {
	Enterprise  gorm_model.Enterprise
	YoungeeUser gorm_model.YounggeeUser
}

func NewEnterpriseUserRequest() *EnterpriseUserRequest {
	return new(EnterpriseUserRequest)
}

func NewEnterpriseUserResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(EnterpriseUserData)
	return resp
}
