package http_model

type CaseCloseRequest struct {
	Time int32 `json:"time"`
}

func NewCaseCloseRequest() *CaseCloseRequest {
	return new(CaseCloseRequest)
}

func NewCaseCloseResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
