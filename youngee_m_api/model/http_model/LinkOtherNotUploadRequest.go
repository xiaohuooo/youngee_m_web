package http_model

type LinkOtherNotUploadRequest struct {
	Rate int `json:"rate"`
}

func NewLinkOtherNotUploadRequest() *LinkOtherNotUploadRequest {
	return new(LinkOtherNotUploadRequest)
}

func NewLinkOtherNotUploadResponse() *CommonResponse {
	return new(CommonResponse)
}
