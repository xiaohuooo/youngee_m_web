package http_model

type AutoReviewRequest struct {
	Time        int32 `json:"time"`
	ContentType int32 `json:"content_type"`
}

func NewAutoReviewRequest() *AutoReviewRequest {
	return new(AutoReviewRequest)
}

func NewAutoReviewResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
