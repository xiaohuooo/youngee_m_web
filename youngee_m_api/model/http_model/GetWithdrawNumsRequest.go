package http_model

type GetWithdrawNumsRequest struct {
}

type WithdrawNums struct {
	WithdrawNums int `json:"withdraw_nums"`
}

func NewGetWithdrawNumsRequest() *GetWithdrawNumsRequest {
	return new(GetWithdrawNumsRequest)
}

func NewGetWithdrawNumsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(WithdrawNums)
	return resp
}
