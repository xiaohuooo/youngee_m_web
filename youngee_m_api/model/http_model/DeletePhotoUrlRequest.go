package http_model

type DeletePhotoUrlRequest struct {
	PhotoUrl string `json:"photo_url"`
}

func NewDeletePhotoUrlRequest() *DeletePhotoUrlRequest {
	return new(DeletePhotoUrlRequest)
}

func NewDeletePhotoUrlResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
