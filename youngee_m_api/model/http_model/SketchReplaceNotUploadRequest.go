package http_model

type SketchReplaceNotUploadRequest struct {
	Rate int `json:"rate"`
}

func NewSketchReplaceNotUploadRequest() *SketchReplaceNotUploadRequest {
	return new(SketchReplaceNotUploadRequest)
}

func NewSketchReplaceNotUploadResponse() *CommonResponse {
	return new(CommonResponse)
}
