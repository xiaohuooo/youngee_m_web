package http_model

type GetSpecialLogisticNumberRequest struct {
	ProjectId string `json:"project_id"` // 项目id
}

type GetSpecialLogisticNumberData struct {
	DeliverNumber     int64 `json:"deliver_number"`     // 应发货数量
	UndeliveredNumber int64 `json:"undelivered_number"` // 未发货数量
	DeliveredNumber   int64 `json:"delivered_number"`   // 已发货数量
	SignedNumber      int64 `json:"signed_number"`      // 已签收数量
}

func NewGetSpecialLogisticNumberRequest() *GetSpecialLogisticNumberRequest {
	return new(GetSpecialLogisticNumberRequest)
}

func NewGetSpecialLogisticNumberResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSpecialLogisticNumberData)
	return resp
}
