package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type TaskFinishListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	StrategyId       string `json:"strategy_id"`       // 策略ID
	DataStatus       string `json:"data_status"`       // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type TaskFinishPreview struct {
	TaskID            string  `json:"task_id"`             // 任务ID
	PlatformNickname  string  `json:"platform_nickname"`   // 账号昵称
	FansCount         string  `json:"fans_count"`          // 粉丝数
	RecruitStrategyID string  `json:"recruit_strategy_id"` // 招募策略ID
	StrategyID        string  `json:"strategy_id"`         // 报名选择的招募策略id
	PlayNumber        int     `json:"play_number"`         // 播放量/阅读量
	LikeNumber        int     `json:"like_number"`         // 点赞数
	CommentNumber     int     `json:"comment_number"`      // 评论数
	CollectNumber     int     `json:"collect_number"`      // 收藏数
	PhotoUrl          string  `json:"photo_url"`           // 数据截图url
	RealPayment       float64 `json:"real_payment"`        // 企业实际支付（扣除违约扣款）
	SubmitAt          string  `json:"submit_at"`           // 提交时间
	LinkUrl           string  `json:"link_url"`            // 上传链接url
}

type TaskFinishInfo struct {
	TaskID            string    `json:"task_id"`             // 任务ID
	PlatformNickname  string    `json:"platform_nickname"`   // 账号昵称
	FansCount         string    `json:"fans_count"`          // 粉丝数
	RecruitStrategyID int       `json:"recruit_strategy_id"` // 招募策略ID
	StrategyID        int       `json:"strategy_id"`         // 报名选择的招募策略id
	DataId            int       `json:"data_id"`             // 数据ID
	PlayNumber        int       `json:"play_number"`         // 播放量/阅读量
	LikeNumber        int       `json:"like_number"`         // 点赞数
	CommentNumber     int       `json:"comment_number"`      // 评论数
	CollectNumber     int       `json:"collect_number"`      // 收藏数
	PhotoUrl          string    `json:"photo_url"`           // 数据截图url
	RealPayment       float64   `json:"real_payment"`        // 企业实际支付（扣除违约扣款）
	LinkUrl           string    `json:"link_url"`            // 上传链接url
	SubmitAt          time.Time `json:"submit_at"`           // 提交时间
}

type TaskFinish struct {
	Talent gorm_model.YoungeeTaskInfo
	Data   gorm_model.YounggeeDataInfo
	Link   gorm_model.YounggeeLinkInfo
}

type TaskFinishListData struct {
	TaskFinishPreview []*TaskFinishPreview `json:"project_finish_pre_view"`
	Total             string               `json:"total"`
}

func NewTaskFinishListRequest() *TaskFinishListRequest {
	return new(TaskFinishListRequest)
}

func NewTaskFinishListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectTaskListData)
	return resp
}
