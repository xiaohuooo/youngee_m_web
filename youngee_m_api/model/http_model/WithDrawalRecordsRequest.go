package http_model

type WithdrawalRecordsRequest struct {
	PageSize   int32  `json:"page_size"`
	PageNum    int32  `json:"page_num"`
	Status     int32  `json:"status"`
	SubmitAt   string `json:"submit_at"`
	WithdrawAt string `json:"withdraw_at"`
	TalentId   string `json:"talent_id"`
	TalentName string `json:"talent_name"`
}

type WithdrawalRecordsData struct {
	WithdrawId     string  `json:"withdraw_id"`
	TalentId       string  `json:"talent_id"`
	TalentName     string  `json:"talent_name"`
	WithdrawAmount float32 `json:"withdraw_amount"`
	AmountPayable  float32 `json:"amount_payable"`
	ReceiveInfo    string  `json:"receive_info"`
	BankType       int     `json:"bank_type"`
	Phone          string  `json:"phone"`
	SubmitAt       string  `json:"submit_at"`
	WithdrawAt     string  `json:"withdraw_at"`
}

type WithdrawalRecordsPreview struct {
	WithdrawalRecordsData []*WithdrawalRecordsData `json:"withdrawal_records_data"`
	Total                 int64                    `json:"total"`
}

func NewWithdrawalRecordsRequest() *WithdrawalRecordsRequest {
	return new(WithdrawalRecordsRequest)
}

func NewWithdrawalRecordsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(WithdrawalRecordsPreview)
	return resp
}
