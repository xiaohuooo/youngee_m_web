package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type TaskLogisticsListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	StrategyId       string `json:"strategy_id"`       // 策略ID
	LogisticsStatus  string `json:"logistics_status"`  // 任务状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type TaskLogisticsPreview struct {
	TaskID                string    `json:"task_id"`           // 任务ID
	PlatformNickname      string    `json:"platform_nickname"` // 账号昵称
	FansCount             string    `json:"fans_count"`        // 粉丝数
	RecruitStrategyID     string    `json:"recruit_strategy_id"`
	StrategyID            string    `json:"strategy_id"`            // 报名选择的招募策略id
	DetailAddr            string    `json:"detail_addr"`            // 物流信息
	CompanyName           string    `json:"company_name"`           // 物流公司
	LogisticsNumber       string    `json:"logistics_number"`       // 物流单号
	DeliveryTime          string    `json:"delivery_time"`          // 发货时间
	ExplorestoreStarttime time.Time `json:"explorestore_starttime"` // 线下探店-探店开始时间
	ExplorestoreEndtime   time.Time `json:"explorestore_endtime"`   // 线下探店-探店结束时间
	ExplorestorePeriod    string    `json:"explorestore_period"`    // 线下探店-探店持续时间
	SignedTime            string    `json:"signed_time"`            // 签收时间
	CouponCode            string    `json:"coupon_code"`            // 券码信息
}

type TaskLogisticsInfo struct {
	TaskID                string    `json:"task_id"`           // 任务ID
	PlatformNickname      string    `json:"platform_nickname"` // 账号昵称
	FansCount             string    `json:"fans_count"`        // 粉丝数
	RecruitStrategyID     int       `json:"recruit_strategy_id"`
	StrategyID            int       `json:"strategy_id"`            // 报名选择的招募策略id
	DetailAddr            string    `json:"detail_addr"`            // 物流信息
	CompanyName           string    `json:"company_name"`           // 物流公司
	LogisticsNumber       string    `json:"logistics_number"`       // 物流单号
	DeliveryTime          string    `json:"delivery_time"`          // 发货时间
	SignedTime            string    `json:"signed_time"`            // 签收时间
	ExplorestoreStarttime time.Time `json:"explorestore_starttime"` // 线下探店-探店开始时间
	ExplorestoreEndtime   time.Time `json:"explorestore_endtime"`   // 线下探店-探店结束时间
	ExplorestorePeriod    string    `json:"explorestore_period"`    // 线下探店-探店持续时间
	CouponCode            string    `json:"coupon_code"`            // 券码信息
}

type TaskLogistics struct {
	Talent    gorm_model.YoungeeTaskInfo
	Logistics gorm_model.YoungeeTaskLogistics
	Region    string
	//Account   gorm_model.YoungeePlatformAccountInfo
}

type TaskLogisticsListData struct {
	TaskLogisticsPreview []*TaskLogisticsPreview `json:"project_talent_pre_view"`
	Total                string                  `json:"total"`
}

func NewTaskLogisticsListRequest() *TaskLogisticsListRequest {
	return new(TaskLogisticsListRequest)
}

func NewTaskLogisticsListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectTaskListData)
	return resp
}
