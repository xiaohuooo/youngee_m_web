package http_model

type BreachPendingRequest struct {
	PageSize    int32  `json:"page_size"`
	PageNum     int32  `json:"page_num"`
	DefaultType int32  `json:"break_type"`
	TaskId      string `json:"task_id"`
	ProjectName string `json:"project_name"`
}

type BreachPendingPreview struct {
	ContractId       int32   `json:"contract_id"`
	ProjectId        string  `json:"project_id"`
	UserId           string  `json:"user_id"`
	ProjectName      string  `json:"project_name"`
	UserPhone        string  `json:"user_phone"`
	TaskId           string  `json:"task_id"`
	TalentId         string  `json:"talent_id"`
	TalentPhone      string  `json:"talent_phone"`
	SketchInfo       string  `json:"sketch_info"`
	LinkInfo         string  `json:"link_info"`
	Price            float64 `json:"price"`
	SettlementAmount float64 `json:"settlement_amount"`
	DefaultAt        string  `json:"default_at"`
	TerminateAt      string  `json:"terminate_at"`
}

type BreachPendingData struct {
	BreachPendingPreview []*BreachPendingPreview `json:"breach_pending_preview"`
	Total                int64                   `json:"total"`
}

func NewBreachPendingRequest() *BreachPendingRequest {
	return new(BreachPendingRequest)
}

func NewBreachPendingResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(BreachPendingData)
	return resp
}
