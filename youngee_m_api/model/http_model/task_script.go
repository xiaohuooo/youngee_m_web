package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type TaskScriptListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	StrategyId       string `json:"strategy_id"`       // 策略ID
	ScriptStatus     string `json:"script_status"`     // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type TaskScriptPreview struct {
	TaskID            string `json:"task_id"`             // 任务ID
	PlatformNickname  string `json:"platform_nickname"`   // 账号昵称
	FansCount         string `json:"fans_count"`          // 粉丝数
	RecruitStrategyID string `json:"recruit_strategy_id"` //招募策略ID
	StrategyID        string `json:"strategy_id"`         // 报名选择的招募策略id
	Submit            string `json:"script_upload_time"`  //创建时间
	Title             string `json:"title"`               //脚本标题
	AgreeAt           string `json:"agree_at"`            //同意时间
	Content           string `json:"content"`             //脚本内容
	ReviseOpinion     string `json:"revise_opinion"`      //审稿意见
}

type TaskScriptInfo struct {
	TaskID            string    `json:"task_id"`           // 任务ID
	PlatformNickname  string    `json:"platform_nickname"` // 账号昵称
	FansCount         string    `json:"fans_count"`        // 粉丝数
	RecruitStrategyID int       `json:"recruit_strategy_id"`
	StrategyID        int       `json:"strategy_id"`    // 报名选择的招募策略id
	ScriptId          int       `json:"script_id"`      //脚本ID
	Title             string    `json:"title"`          //脚本标题
	Content           string    `json:"content"`        //脚本内容
	ReviseOpinion     string    `json:"revise_opinion"` //审稿意见
	CreateAt          time.Time `json:"create_at"`      //创建时间
	SubmitAt          time.Time `json:"submit_at"`      // 提交时间
	AgreeAt           time.Time `json:"agree_at"`       //同意时间
	RejectAt          time.Time `json:"reject_at"`      //拒绝时间
	IsReview          int       `json:"is_review"`      //是否审核
}

type TaskScript struct {
	Talent gorm_model.YoungeeTaskInfo
	Script gorm_model.YounggeeScriptInfo
	//Account   gorm_model.YoungeePlatformAccountInfo
}

type TaskScriptListData struct {
	TaskScriptPreview []*TaskScriptPreview `json:"project_script_pre_view"`
	Total             string               `json:"total"`
}

func NewTaskScriptListRequest() *TaskScriptListRequest {
	return new(TaskScriptListRequest)
}

func NewTaskScriptListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectTaskListData)
	return resp
}
