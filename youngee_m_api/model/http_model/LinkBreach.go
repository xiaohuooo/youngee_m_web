package http_model

type LinkBreachRequest struct {
	Time int32 `json:"time"`
}

func NewLinkBreachRequest() *LinkBreachRequest {
	return new(LinkBreachRequest)
}

func NewLinkBreachResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
