package http_model

type CreateLogisticsRequest struct {
	StrategyID            int64  `json:"strategy_id"`             //招募策略id
	LogisticsID           int64  `json:"logistics_id"`            // 货物-id
	CompanyName           string `json:"company_name"`            // 实物商品-物流公司名称
	LogisticsNumber       string `json:"logistics_number"`        // 实物商品-物流单号
	ExplorestoreStarttime string `json:"explorestore_starttime"`  // 线下探店-探店开始时间
	ExplorestoreEndtime   string `json:"explorestore_endtime"`    // 线下探店-探店结束时间
	ExplorestorePeriod    string `json:"explorestore_period"`     // 线下探店-探店持续时间
	CouponCodeInformation string `json:"coupon_code_information"` // 虚拟产品-券码信息
	TaskID                string `json:"task_id"`                 // 任务id
	DeliveryTime          string `json:"delivery_time"`           // 发货时间
	ThingsType            int    `json:"things_type"`             //产品类型 1：实物, 2：虚拟产品，3：线下探店
	IsUpdate              int    `json:"is_update"`               //更新标志位 0：不更新 1：更新
}

type CreateLogisticsData struct {
	LogisticsID int64 `json:"logisitcs_id"` // 商品id
}

func NewCreateLogisticsRequest() *CreateLogisticsRequest {
	return new(CreateLogisticsRequest)
}
func NewCreateLogisticsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CreateLogisticsData)
	return resp
}
