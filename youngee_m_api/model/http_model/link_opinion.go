package http_model

type LinkOpinionRequest struct {
	StrategyID  int64  `json:"strategy_id"`  //招募策略id
	TaskID      string `json:"task_id"`      //任务-id
	LinkOpinion string `json:"Link_opinion"` //链接审核意见
}

type LinkOpinionData struct {
	TaskID string `json:"task_id"` // 任务ID
}

func NewLinkOpinionRequest() *LinkOpinionRequest {
	return new(LinkOpinionRequest)
}
func NewLinkOpinionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(LinkOpinionData)
	return resp
}
