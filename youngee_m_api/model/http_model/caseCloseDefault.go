package http_model

type CaseCloseDefaultRequest struct {
	Time int32 `json:"time"`
}

func NewCaseCloseDefaultRequest() *CaseCloseDefaultRequest {
	return new(CaseCloseDefaultRequest)
}

func NewCaseCloseDefaultResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
