package http_model

type ContractBreachRequest struct {
	ContractIds   []int32 `json:"contract_ids"`
	DefaultStatus int32   `json:"default_status"`
}

func NewContractBreachRequest() *ContractBreachRequest {
	return new(ContractBreachRequest)
}

func NewContractBreachResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
