package http_model

type ScriptOtherTimeOutRequest struct {
	Rate int `json:"rate"`
}

func NewScriptOtherTimeOutRequest() *ScriptOtherTimeOutRequest {
	return new(ScriptOtherTimeOutRequest)
}

func NewScriptOtherTimeOutResponse() *CommonResponse {
	return new(CommonResponse)
}
