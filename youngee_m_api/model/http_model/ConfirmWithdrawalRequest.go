package http_model

type ConfirmWithdrawalRequest struct {
	WithdrawId string `json:"withdraw_id"`
}

func NewConfirmWithdrawalRequest() *ConfirmWithdrawalRequest {
	return new(ConfirmWithdrawalRequest)
}

func NewConfirmWithdrawalResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
