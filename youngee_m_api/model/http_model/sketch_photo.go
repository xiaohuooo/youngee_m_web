package http_model

type FindSketchPhotoRequest struct {
	SketchID int64 `json:"sketch_id"`
}

type SketchPhoto struct {
	PhotoUrl string `json:"photo_url"` // 图片或视频url
	PhotoUid string `json:"photo_uid"`
	Symbol   int64  `json:"symbol"` // 标志位，1为详情图，2为视频
}

type FindSketchPhotoData struct {
	SketchPhotos []SketchPhoto `json:"sketch_photos"` //初稿图片以及视频
}

func NewFindSketchPhotoRequest() *FindSketchPhotoRequest {
	return new(FindSketchPhotoRequest)
}
func NewFindSketchPhotoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(FindSketchPhotoData)
	return resp
}
