package http_model

type GetLogisticsNumberInfoRequest struct {
	ProjectId   string `json:"project_id"`   // 项目id
	StrategyIds string `json:"strategy_ids"` // 招募策略id列表
}

type LogisticsNumberInfo struct {
	StrategyId        int64 `json:"strategy_id"`        // 招募策略id
	DeliverNumber     int64 `json:"deliver_number"`     // 应发货数量
	UndeliveredNumber int64 `json:"undelivered_number"` // 未发货数量
	DeliveredNumber   int64 `json:"delivered_number"`   // 已发货数量
	SignedNumber      int64 `json:"signed_number"`      // 已签收数量
}

type GetLogisticsNumberInfoData struct {
	LogisticsNumberInfoList []*LogisticsNumberInfo `json:"number_info_list"`
}

func NewGetLogisticsNumberInfoRequest() *GetLogisticsNumberInfoRequest {
	return new(GetLogisticsNumberInfoRequest)
}

func NewGetLogisticsNumberInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetLogisticsNumberInfoData)
	return resp
}
