package http_model

type SettleSecTaskRequest struct {
	EnterpriseId  string `json:"enterprise_id"`
	SelectionID   string `json:"selection_id"`    // 选品项目id
	TaskID        string `json:"task_id"`         // 任务id
	IsReturnMoney int    `json:"is_return_money"` //是否返回样品钱
	IsPayReward   int    `json:"is_pay_reward"`   //是否给悬赏金
	TotalPayMoney string `json:"total_pay_money"` //合计结算金额
}

type SettleSecTaskData struct {
}

func NewSettleSecTaskRequest() *SettleSecTaskRequest {
	return new(SettleSecTaskRequest)
}

func NewSettleSecTaskResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SettleSecTaskData)
	return resp
}
