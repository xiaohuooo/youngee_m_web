package http_model

type AcceptDataRequest struct {
	Payment   float64 `json:"payment"`      //招募策略id
	TaskIds   string  `json:"task_id_list"` //任务id列表
	ProjectId string  `json:"project_id"`   //项目id
	IsSpecial int     `json:"is_special"`
}

type AcceptDataData struct {
	TaskIds []string `json:"taskIds"` //任务id列表
	IsEnd   int      `json:"is_end"`  // 项目是否结案
}

func NewAcceptDataRequest() *AcceptDataRequest {
	return new(AcceptDataRequest)
}
func NewAcceptDataResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(AcceptDataData)
	return resp
}
