package http_model

type DraftDefaultInMvRequest struct {
	Time int32 `json:"time"`
}

func NewDraftDefaultInMvRequest() *DraftDefaultInMvRequest {
	return new(DraftDefaultInMvRequest)
}

func NewDraftDefaultInMvResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
