package http_model

type DeleteSelectionRequest struct {
	SelectionId string `json:"selection_id"`
}

func NewDeleteSelectionRequest() *DeleteSelectionRequest {
	return new(DeleteSelectionRequest)
}

func NewDeleteSelectionResponse() *CommonResponse {
	return new(CommonResponse)
}
