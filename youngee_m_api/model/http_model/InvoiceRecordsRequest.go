package http_model

type InvoiceRecordsRequest struct {
	PageSize      int32  `json:"page_size"`
	PageNum       int32  `json:"page_num"`
	InvoiceStatus int32  `json:"invoice_status"`
	SubmitAt      string `json:"submit_at"`
	BusinessName  string `json:"business_name"`
	Username      string `json:"username"`
	UserId        int64  `json:"user_id"`
	BillingAt     string `json:"billing_at"`
}

type InvoiceRecordsPreviews struct {
	BillingId      string  `json:"billing_id"`
	UserId         string  `json:"user_id"`
	Username       string  `json:"username"`
	BusinessName   string  `json:"business_name"`
	Amount         float64 `json:"amount"`
	InvoiceType    string  `json:"invoice_type"`
	InvoiceInfo    string  `json:"invoice_info"`
	InvoiceAddress string  `json:"invoice_address"`
	AddressInfo    string  `json:"address_info"`
	ShipmentNumber string  `json:"shipment_number"` // 物流单号
	Phone          string  `json:"phone"`
	SubmitAt       string  `json:"submit_at"`
	BillingAt      string  `json:"billing_at"`
}

type InvoiceRecordsData struct {
	InvoiceRecordsPreviews []*InvoiceRecordsPreviews `json:"invoice_records_previews"`
	Total                  string                    `json:"total"`
}

func NewInvoiceRecordsRequest() *InvoiceRecordsRequest {
	return new(InvoiceRecordsRequest)
}

func NewInvoiceRecordsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(InvoiceRecordsData)
	return resp
}
