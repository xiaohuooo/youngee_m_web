package http_model

import "time"

type ShowProjectPhoto struct {
	PhotoUrl string `json:"photo_url"` // 图片url
	PhotoUid string `json:"photo_uid"`
	FileName string `json:"name"` //文件名
}

type ShowRecruitStrategy struct {
	RecruitStrategyID string `json:"recruit_strategy_id"`
	FeeForm           string `json:"fee_form"`       // 稿费形式，1-3分别代表自报价、固定稿费、产品置换
	StrategyID        string `json:"strategy_id"`    // 策略id
	FollowersLow      string `json:"followers_low"`  // 达人粉丝数下限
	FollowersUp       string `json:"followers_up"`   // 达人粉丝数上限
	RecruitNumber     string `json:"recruit_number"` // 招募数量
	Offer             string `json:"offer"`          // 报价
	ServiceCharge     string `json:"service_charge"`
	SelectedNumber    int64  `json:"selected_number"`
	WaitingNumber     int64  `json:"waiting_number"`   // 待发货
	DeliveredNumber   int64  `json:"delivered_number"` // 已发货
	SignedNumber      int64  `json:"signed_number"`    // 已签收
}

type ShowProjectData struct {
	ProjectName      string                `json:"project_name"`       // 项目名称
	ProjectStatus    string                `json:"project_status"`     // 项目状态，1-7分别代表创建中、待审核、招募中、待支付、失效、执行中、已结案
	ProjectType      string                `json:"project_type"`       // 项目类型，1代表全流程项目，2代表专项项目
	ProjectPlatform  string                `json:"project_platform"`   // 项目平台，1-7分别代表红book、抖音、微博、快手、b站、大众点评、知乎
	ProjectForm      string                `json:"project_form"`       // 项目形式，1-4分别代表实体商品寄拍、虚拟产品测评、线下探店打卡、素材微原创
	TalentType       string                `json:"talent_type"`        // 达人类型
	RecruitDdl       time.Time             `json:"recruit_ddl"`        // 招募截止时间
	ContentType      string                `json:"content_type"`       // 内容形式，1代表图文，2代表视频
	ProjectDetail    string                `json:"project_detail"`     // 项目详情
	RecruitStrategys []ShowRecruitStrategy `json:"recruit_strategys"`  // 定价策略
	ProjectPhotos    []ShowProjectPhoto    `json:"project_photos"`     // 项目图片
	ProductID        string                `json:"product_id"`         // 关联商品id
	ProductInfo      string                `json:"product_info"`       // 商品信息
	ProductPhotoInfo string                `json:"product_photo_info"` // 商品图片
	EstimatedCost    string                `json:"estimated_cost"`     // 预估成本
	EnterpriseID     string                `json:"enterprise_id"`      // 企业id
	Balance          string                `json:"balance"`            // 企业余额
	AvailableBalance string                `json:"available_balance"`  // 可用余额
	ProjectID        string                `json:"project_id"`         // 项目id
	FailReason       string                `json:"fail_reason"`        // 失效原因
	Phone            string                `json:"phone"`              // 联系方式
	CreateAt         time.Time             `json:"create_at"`          // 创建时间
	UpdateAt         time.Time             `json:"update_at"`          // 更新时间
	PassAt           time.Time             `json:"pass_at"`            // 审核通过时间
	FinishAt         time.Time             `json:"finish_at"`          // 结案时间
	PayAt            time.Time             `json:"pay_at"`             // 支付时间
	AutoFailAt       time.Time             `json:"auto_fail_at"`       // 支付截止时间
	SubmitAt         time.Time             `json:"submit_at"`          // 提交审核时间
}

type ShowProjectRequest struct {
	ProjectID string `json:"Project_id"` // 项目id
}

func NewShowProjectRequest() *ShowProjectRequest {
	return new(ShowProjectRequest)
}
func NewShowProjectResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ShowProjectData)
	return resp
}
