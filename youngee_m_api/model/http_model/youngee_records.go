package http_model

type GetYoungeeRecordsRequest struct {
	TalentId string `json:"talent_id"`
}

type YoungeeRecordsPreview struct {
	ProjectId       string `json:"project_id"`
	ProjectName     string `json:"project_name"`
	ProjectType     string `json:"project_type"`
	ProjectPlatform string `json:"platform"`
	TeamId          string `json:"team_id"`
	TeamStatus      string `json:"team_status"`
	PintIncome      string `json:"point_income"`
	MoneyIncome     string `json:"money_income"`
}

type YoungeeRecordsData struct {
	YoungeeRecordsPreview []*YoungeeRecordsPreview `json:"Youngee_records_preview"`
	Total                 string                   `json:"total"`
}

func NewGetYoungeeRecordsRequest() *GetYoungeeRecordsRequest {
	return new(GetYoungeeRecordsRequest)
}

func NewGetYoungeeRecordsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(YoungeeRecordsData)
	return resp
}
