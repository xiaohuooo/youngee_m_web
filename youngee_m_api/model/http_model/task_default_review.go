package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type TaskDefaultReviewListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	StrategyId       string `json:"strategy_id"`       // 策略ID
	DefaultStatus    string `json:"default_status"`    // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type TaskDefaultReviewPreview struct {
	TaskID            string  `json:"task_id"`             // 任务ID
	ProjectID         string  `json:"project_id"`          // 项目ID
	PlatformNickname  string  `json:"platform_nickname"`   // 账号昵称
	FansCount         string  `json:"fans_count"`          // 粉丝数
	RecruitStrategyID string  `json:"recruit_strategy_id"` // 招募策略ID
	StrategyID        string  `json:"strategy_id"`         // 报名选择的招募策略id
	AllPayment        float64 `json:"all_payment"`         // 企业支付
	RealPayment       float64 `json:"real_payment"`        // 企业实际支付（扣除违约扣款）
	BreakAt           string  `json:"break_at"`            // 违约时间
}

type TaskDefaultReviewInfo struct {
	TaskID            string    `json:"task_id"`             // 任务ID
	ProjectID         string    `json:"project_id"`          // 项目ID
	PlatformNickname  string    `json:"platform_nickname"`   // 账号昵称
	FansCount         string    `json:"fans_count"`          // 粉丝数
	RecruitStrategyID int       `json:"recruit_strategy_id"` // 招募策略ID
	StrategyID        int       `json:"strategy_id"`         // 报名选择的招募策略id
	AllPayment        float64   `json:"all_payment"`         // 企业支付
	RealPayment       float64   `json:"real_payment"`        // 企业实际支付（扣除违约扣款）
	BreakAt           time.Time `json:"break_at"`            // 违约时间
}

type TaskDefaultReview struct {
	Talent  gorm_model.YoungeeTaskInfo
	Default gorm_model.YoungeeContractInfo
}

type TaskDefaultReviewListData struct {
	TaskDefaultPreview []*TaskDefaultReviewPreview `json:"project_default_pre_view"`
	Total              string                      `json:"total"`
}

func NewTaskDefaultReviewListRequest() *TaskDefaultReviewListRequest {
	return new(TaskDefaultReviewListRequest)
}

func NewTaskDefaultReviewListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectTaskListData)
	return resp
}
