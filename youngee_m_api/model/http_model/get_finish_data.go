package http_model

type GetFinishDataRequest struct {
	ProjectId string `json:"project_id"` // 项目id
}

type GetFinishDataInfo struct {
	FeeForm         string  `json:"fee_form"`         // 稿费形式，1-3分别代表产品置换、固定稿费、自报价
	StrategyID      string  `json:"strategy_id"`      // 策略id
	FollowersLow    string  `json:"followers_low"`    // 达人粉丝数下限
	FollowersUp     string  `json:"followers_up"`     // 达人粉丝数上限
	RecruitNumber   string  `json:"recruit_number"`   // 招募数量
	Offer           string  `json:"offer"`            // 报价
	ProjectID       string  `json:"project_id"`       // 所属项目id
	ServiceCharge   string  `json:"service_charge"`   // 平台服务费，稿费形式为产品置换时必填
	SelectedNumber  string  `json:"selected_number"`  // 已选数量，被企业选择的达人数量
	WaitingNumber   string  `json:"waiting_number"`   // 待发货
	DeliveredNumber string  `json:"delivered_number"` // 已发货
	SignedNumber    string  `json:"signed_number"`    // 已签收
	MaxOffer        string  `json:"max_offer"`        // 报价上限
	MinOffer        string  `json:"min_offer"`        // 报价下限
	FanNumber       string  `json:"fan_number"`       // 总粉丝量
	PlayNumber      string  `json:"play_number"`      // 总播放量
	LikeNumber      string  `json:"like_number"`      // 总点赞数
	CollectNumber   string  `json:"collect_number"`   // 总收藏量
	CommentNumber   string  `json:"comment_number"`   // 总评论数
	TerminateNumber string  `json:"terminate_number"` // 结案数量
	FinishNumber    string  `json:"finish_number"`    // 结案数量
	DefaultNumber   string  `json:"default_number"`   // 违约数量
	TotalOffer      float64 `json:"total_offer"`      // 支付合计
}

type GetFinishData struct {
	FinishRecruitStrategy []*GetFinishDataInfo `json:"finish_recruit_strategys"`
}

func NewGetFinishDataRequest() *GetFinishDataRequest {
	return new(GetFinishDataRequest)
}
func NewGetFinishDataResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetFinishData)
	return resp
}
