package http_model

type GetSpecialLinkNumberRequest struct {
	ProjectId string `json:"project_id"` // 项目id
}

type GetSpecialLinkNumberData struct {
	LinkNumber         int64 `json:"link_number"`          // 应传链接数量
	LinkUnreviewNumber int64 `json:"link_unreview_number"` // 链接待审数量
	LinkPassedNumber   int64 `json:"link_passed_number"`   // 链接已通过数量
}

func NewGetSpecialLinkNumberRequest() *GetSpecialLinkNumberRequest {
	return new(GetSpecialLinkNumberRequest)
}

func NewGetSpecialLinkNumberResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSpecialLinkNumberData)
	return resp
}
