package http_model

type FullProjectListRequest struct {
	PageSize           int32  `json:"page_size"`
	PageNum            int32  `json:"page_num"`
	ProjectId          string `json:"project_id"`           // 项目ID
	ProjectName        string `json:"project_name"`         // 项目名
	ProjectStatus      string `json:"project_status"`       // 项目状态
	ProjectType        string `json:"project_type"`         // 项目类型
	ProjectPlatform    string `json:"project_platform"`     // 项目平台
	ProjectForm        string `json:"project_form"`         // 项目形式
	ProjectContentType string `json:"project_content_type"` // 内容形式
	ProjectUpdated     string `json:"project_updated"`      // 最后操作时间
}

type FullProjectPreview struct {
	ProjectId          string `json:"project_id"`           // 项目ID
	ProjectName        string `json:"project_name"`         // 项目名
	EnterpriseID       string `json:"enterprise_id"`        // 所属企业id
	ProjectStatus      string `json:"project_status"`       // 项目状态
	ProjectPlatform    string `json:"project_platform"`     // 项目平台
	ProjectForm        string `json:"project_form"`         // 项目形式
	ProjectContentType string `json:"project_content_type"` // 内容形式
	ProjectUpdated     string `json:"project_updated"`      // 最后操作时间
}

type FullProjectListData struct {
	FullProjectPreview []*FullProjectPreview `json:"full_project_pre_view"`
	Total              string                `json:"total"`
}

func NewFullProjectListRequest() *FullProjectListRequest {
	return new(FullProjectListRequest)
}
func NewFullProjectListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(FullProjectListData)
	return resp
}
