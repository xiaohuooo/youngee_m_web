package http_model

type RechargeRecordRequest struct {
	ConfirmAt string `json:"confirm_at"`
	Method    int    `json:"method"`
}

type RechargeRecordData struct {
	RechargeId         string `json:"recharge_id"`
	RechargeAmount     string `json:"recharge_amount"`
	RechargeAmountReal string `json:"recharge_amount_real"`
	PayMethod          string `json:"pay_method"`
	TransferVoucher    string `json:"transfer_voucher"`
	ConfirmAt          string `json:"confirm_at"`
}

type RechargeRecordPreview struct {
	RechargeRecordData []*RechargeRecordData `json:"recharge_record_data"`
}

func NewRechargeRecordRequest() *RechargeRecordRequest {
	return new(RechargeRecordRequest)
}

func NewRechargeRecordResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(RechargeRecordPreview)
	return resp
}
