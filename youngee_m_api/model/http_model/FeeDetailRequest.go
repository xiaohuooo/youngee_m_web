package http_model

type FeeDetailRequest struct {
	EndTime string `json:"end_time"`
}

type FeeDetailData struct {
	ProjectID   int64  `json:"project_id"`
	ProjectName string `json:"project_name"`
	ProjectType string `json:"project_type"`
	Payment     string `json:"payment"`
	UpdatedAt   string `json:"updated_at"`
}

type FeeDetailPreview struct {
	FeeDetailData []*FeeDetailData `json:"fee_detail_data"`
}

func NewFeeDetailRequest() *FeeDetailRequest {
	return new(FeeDetailRequest)
}

func NewFeeDetailResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(FeeDetailPreview)
	return resp
}
