package http_model

type CreateSecTaskLogisticsRequest struct {
	TaskID                string `json:"task_id"`                // 任务id
	CompanyName           string `json:"company_name"`           // 实物商品-物流公司名称
	LogisticsNumber       string `json:"logistics_number"`       // 实物商品-物流单号
	ExplorestoreStarttime string `json:"explorestore_starttime"` // 线下探店-探店开始时间
	ExplorestoreEndtime   string `json:"explorestore_endtime"`   // 线下探店-探店结束时间
	ExplorestorePeriod    string `json:"explorestore_period"`    // 线下探店-探店持续时间
	ThingsType            int    `json:"things_type"`            // 产品类型 1：实物, 3：线下探店
}

type CreateSecTaskLogisticsData struct {
}

func NewCreateSecTaskLogisticsRequest() *CreateSecTaskLogisticsRequest {
	return new(CreateSecTaskLogisticsRequest)
}

func NewCreateSecTaskLogisticsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CreateSecTaskLogisticsData)
	return resp
}
