package http_model

type TaskTerminateRequest struct {
	TaskIds    string   `json:"task_id_list"` //任务id列表
	ProjectIds []string `json:"project_ids"`  // 项目id列表
}

type TaskTerminateData struct {
	TaskIds []string `json:"task_id_list"` //任务id列表
}

func NewTaskTerminateRequest() *TaskTerminateRequest {
	return new(TaskTerminateRequest)
}
func NewTaskTerminateResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(TaskTerminateData)
	return resp
}
