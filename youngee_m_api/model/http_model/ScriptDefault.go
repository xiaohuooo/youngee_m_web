package http_model

type ScriptDefaultRequest struct {
	Time int32 `json:"time"`
}

func NewScriptDefaultRequest() *ScriptDefaultRequest {
	return new(ScriptDefaultRequest)
}
func NewScriptDefaultResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
