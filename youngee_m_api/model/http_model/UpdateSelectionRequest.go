package http_model

type UpdateSelectionRequest struct {
	EnterpriseId     string            `json:"enterprise_id"`
	SelectionID      string            `json:"selection_id"` // 选品项目id
	SelectionStatus  int               `json:"selection_status"`
	Platform         int               `json:"platform"`
	ProductId        string            `json:"product_id"`
	ContentType      int               `json:"content_type"`
	TaskMode         int               `json:"task_mode"`
	SampleNum        int               `json:"sample_num"`  // 样品数量
	RemainNum        int               `json:"remain_num"`  // 剩余数量
	TaskReward       float64           `json:"task_reward"` // 任务悬赏
	TaskDdl          string            `json:"task_ddl"`
	SampleMode       int               `json:"sample_mode"`
	CommissionRate   int               `json:"commission_rate"`
	ProductUrl       string            `json:"product_url"`
	SampleCondition  string            `json:"sample_condition"`
	RewardCondition  string            `json:"reward_condition"` // 返现悬赏条件
	SecBrief         []*SecBriefInfo   `json:"sec_brief"`
	SecExample       []*SecExampleInfo `json:"sec_example"`
	Detail           string            `json:"detail"`
	SettlementAmount float64           `json:"settlement_amount"`
}

type SecBriefInfo struct {
	PhotoUrl string `json:"photo_url"`
	PhotoUid string `json:"photo_uid"`
	Name     string `json:"name"`
}

type SecExampleInfo struct {
	PhotoUrl string `json:"photo_url"`
	PhotoUid string `json:"photo_uid"`
	Name     string `json:"name"`
}

type UpdateSelectionData struct {
	SelectionId string `json:"selection_id"`
}

func NewUpdateSelectionRequest() *UpdateSelectionRequest {
	return new(UpdateSelectionRequest)
}

func NewUpdateSelectionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(UpdateSelectionData)
	return resp
}
