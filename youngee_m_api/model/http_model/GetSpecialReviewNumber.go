package http_model

type GetSpecialReviewNumberRequest struct {
	ProjectId string `json:"project_id"` // 项目id
}

type GetSpecialReviewNumberData struct {
	ReviewNumber         int64 `json:"review_number"`          // 应审稿数量
	ScriptUnreviewNumber int64 `json:"script_unreview_number"` // 脚本待审数量
	ScriptPassedNumber   int64 `json:"script_passed_number"`   // 脚本已通过数量
	SketchUnreviewNumber int64 `json:"sketch_unreview_number"` // 初稿待审数量
	SketchPassedNumber   int64 `json:"sketch_passed_number"`   // 初稿已通过数量
}

func NewGetSpecialReviewNumberRequest() *GetSpecialReviewNumberRequest {
	return new(GetSpecialReviewNumberRequest)
}

func NewGetSpecialReviewNumberResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSpecialReviewNumberData)
	return resp
}
