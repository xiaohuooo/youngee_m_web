package http_model

type ConfirmInvoiceRequest struct {
	BillingId      string `json:"billing_id"`
	ShipmentNumber string `json:"shipment_number"`
}

func NewConfirmInvoiceRequest() *ConfirmInvoiceRequest {
	return new(ConfirmInvoiceRequest)
}

func NewConfirmInvoiceResponse() *CommonResponse {
	return new(CommonResponse)
}
