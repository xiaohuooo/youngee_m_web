package http_model

type LinkReplaceNotUploadRequest struct {
	Rate int `json:"rate"`
}

func NewLinkReplaceNotUploadRequest() *LinkReplaceNotUploadRequest {
	return new(LinkReplaceNotUploadRequest)
}

func NewLinkReplaceNotUploadResponse() *CommonResponse {
	return new(CommonResponse)
}
