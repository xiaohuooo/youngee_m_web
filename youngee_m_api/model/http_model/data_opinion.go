package http_model

type DataOpinionRequest struct {
	TaskID      string `json:"task_id"`      //任务-id
	DataOpinion string `json:"Data_opinion"` //数据审核意见
}

type DataOpinionData struct {
	TaskID string `json:"task_id"` // 任务ID
}

func NewDataOpinionRequest() *DataOpinionRequest {
	return new(DataOpinionRequest)
}
func NewDataOpinionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(DataOpinionData)
	return resp
}
