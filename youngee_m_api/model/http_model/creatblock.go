package http_model

type BlockRequest struct {
	ID          string `json:"id"`           // 达人id
	InBlacklist uint   `json:"in_blacklist"` // 项目状态
}

func NewBlockRequest() *BlockRequest {
	return new(BlockRequest)
}

func NewBlockResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
