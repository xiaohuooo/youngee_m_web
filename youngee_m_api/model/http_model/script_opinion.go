package http_model

type ScriptOpinionRequest struct {
	StrategyID    int64  `json:"strategy_id"`    //招募策略id
	TaskID        string `json:"task_id"`        //任务-id
	ScriptOpinion string `json:"script_opinion"` //初稿意见
}

type ScriptOpinionData struct {
	TaskID string `json:"task_id"` // 脚本ID
}

func NewScriptOpinionRequest() *ScriptOpinionRequest {
	return new(ScriptOpinionRequest)
}
func NewScriptOpinionResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ScriptOpinionData)
	return resp
}
