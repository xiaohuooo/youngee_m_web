package http_model

type GetProjectRecordsRequest struct {
	PageSize int32 `json:"page_size"`
	PageNum  int32 `json:"page_num"`
	UserID   int64 `json:"user_id"`
}

type ProjectRecordsPreview struct {
	ProjectID     string `json:"project_id"`
	EnterpriseID  string `json:"enterprise_id"`
	ProjectName   string `json:"project_name"`
	ProjectType   string `json:"project_type"`
	ProjectStatus string `json:"project_status"`
	UpdatedAt     string `json:"updated_at"`
}

type ProjectRecordsData struct {
	ProjectRecordsPreview []*ProjectRecordsPreview `json:"project_records_preview"`
	Total                 string                   `json:"total"`
}

func NewGetProjectRecordsRequest() *GetProjectRecordsRequest {
	return new(GetProjectRecordsRequest)
}

func NewGetProjectRecordsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(ProjectRecordsData)
	return resp
}
