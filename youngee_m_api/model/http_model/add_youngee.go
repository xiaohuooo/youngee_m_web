package http_model

type AddYoungeeRequest struct {
	ProjectType string `json:"project_type"` //项目类型
	TaskType    string `json:"task_type"`    //任务形式
	Platform    string `json:"platform"`     //社媒平台
	ContentType string `json:"content_type"` //内容形式
	Reason      string `json:"reason"`       //奖励原因
	//Points      float32 `json:"points"`       //积分奖励
	Cash int64 `json:"cash"` //现金奖励
}

type AddYoungeeResponse struct {
}

func NewAddYoungeeRequest() *AddYoungeeRequest {
	return new(AddYoungeeRequest)
}

func NewAddYoungeeResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
