package http_model

type DeleteAccountRequest struct {
	TalentId         string `json:"talent_id"`
	PlatformID       string `json:"platform_id"`
	PlatformNickname string `json:"platform_nickname"`
}

func NewDeleteAccountRequest() *DeleteAccountRequest {
	return new(DeleteAccountRequest)
}

func NewDeleteAccountResponse() *CommonResponse {
	resp := new(CommonResponse)
	return resp
}
