package http_model

import (
	"time"
	"youngee_m_api/model/gorm_model"
)

type SpecialTaskSketchListRequest struct {
	PageSize         int64  `json:"page_size"`
	PageNum          int64  `json:"page_num"`
	ProjectId        string `json:"project_id"`        // 项目ID
	TaskId           string `json:"task_id"`           // 任务ID
	SketchStatus     string `json:"sketch_status"`     // 稿件状态
	PlatformNickname string `json:"platform_nickname"` // 账号昵称
}

type SpecialTaskSketchPreview struct {
	TaskID           string `json:"task_id"`            // 任务ID
	SketchID         string `json:"sketch_id"`          // 初稿ID
	PlatformNickname string `json:"platform_nickname"`  // 账号昵称
	FansCount        string `json:"fans_count"`         // 粉丝数
	Phone            string `json:"phone"`              // 联系方式
	Submit           string `json:"sketch_upload_time"` // 创建时间
	AgreeAt          string `json:"argee_at"`           // 同意时间
	Title            string `json:"title"`              // 初稿标题
	Content          string `json:"content"`            // 初稿内容
	ReviseOpinion    string `json:"revise_opinion"`     // 审稿意见
}

type SpecialTaskSketchInfo struct {
	TaskID           string    `json:"task_id"`           // 任务ID
	PlatformNickname string    `json:"platform_nickname"` // 账号昵称
	FansCount        string    `json:"fans_count"`        // 粉丝数
	SketchId         int       `json:"sketch_id"`         //初稿ID
	Title            string    `json:"title"`             //初稿标题
	Content          string    `json:"content"`           //初稿内容
	ReviseOpinion    string    `json:"revise_opinion"`    //审稿意见
	Phone            string    `json:"phone"`             //联系方式
	CreateAt         time.Time `json:"create_at"`         //创建时间
	SubmitAt         time.Time `json:"submit_at"`         //提交时间
	AgreeAt          time.Time `json:"agree_at"`          //同意时间
	RejectAt         time.Time `json:"reject_at"`         //拒绝时间
	IsReview         int       `json:"is_review"`         //是否审核
}

type SpecialTaskSketch struct {
	Talent gorm_model.YoungeeTaskInfo
	Sketch gorm_model.YounggeeSketchInfo
}

type SpecialTaskSketchListData struct {
	SpecialTaskSketchPreview []*SpecialTaskSketchPreview `json:"project_task_pre_view"`
	Total                    string                      `json:"total"`
}

func NewSpecialTaskSketchListRequest() *SpecialTaskSketchListRequest {
	return new(SpecialTaskSketchListRequest)
}

func NewSpecialTaskSketchListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(SpecialTaskSketchListData)
	return resp
}
