package http_model

type CountNumOfDefaultsRequest struct {
}

type CountNumOfDefaultsResponse struct {
	DraftDefaultNum  int32 `json:"draft_default_num"`
	ScriptDefaultNum int32 `json:"script_default_num"`
	LinkDefaultNum   int32 `json:"link_default_num"`
	DataDefaultNum   int32 `json:"data_default_num"`
}

func NewCountNumOfDefaultsRequest() *CountNumOfDefaultsRequest {
	return new(CountNumOfDefaultsRequest)
}
func NewCountNumOfDefaultsResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CountNumOfDefaultsResponse)
	return resp
}
