package http_model

type PlatformAccInfoRequest struct {
	TalentId string `json:"talent_id"`
}

type PlatformAccInfoData struct {
	AccountId          int    `json:"account_id"`            //账号所属id
	Platform           string `json:"platform"`              //社媒平台
	PlatformNickname   string `json:"platform_nickname"`     //账号昵称
	Fans               string `json:"fans"`                  //粉丝量
	HomePageCaptureUrl string `json:"home_page_capture_url"` //主页截图链接
	HomePageUrl        string `json:"home_page_url"`         //主页链接
}

type PlatformAccInfoPreView struct {
	PlatformAccInfoData []*PlatformAccInfoData `json:"platform_acc_info_data"`
	Total               string                 `json:"total"`
}

func NewPlatformAccInfoRequest() *PlatformAccInfoRequest {
	return new(PlatformAccInfoRequest)
}

func NewPlatformAccInfoResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(PlatformAccInfoPreView)
	return resp
}
