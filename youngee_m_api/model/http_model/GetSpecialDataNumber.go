package http_model

type GetSpecialDataNumberRequest struct {
	ProjectId string `json:"project_id"` // 项目id
}

type GetSpecialDataNumberData struct {
	DataNumber         int64 `json:"data_number"`          // 应传数据数量
	DataUnreviewNumber int64 `json:"data_unreview_number"` // 数据待审数量
	DataPassedNumber   int64 `json:"data_passed_number"`   // 数据已通过数量
}

func NewGetSpecialDataNumberRequest() *GetSpecialDataNumberRequest {
	return new(GetSpecialDataNumberRequest)
}

func NewGetSpecialDataNumberResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(GetSpecialDataNumberData)
	return resp
}
