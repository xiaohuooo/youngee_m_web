package http_model

type CreatorListRequest struct {
	PageSize         int32  `json:"page_size"`
	PageNum          int32  `json:"page_num"`
	Id               string `json:"id"`                 // 达人id
	TalentWxNickname string `json:"talent_wx_nickname"` // 达人的微信昵称
	CreateDate       string `json:"create_date"`        // 创建时间
	InBlacklist      int    `json:"in_blacklist"`       // 是否拉黑

}

type CreatorListPreview struct {
	Id               string  `json:"id"`                  // 达人id
	TalentWxNickname string  `json:"talent_wx_nickname"`  // 达人的微信昵称
	IsBindAccount    string  `json:"is_bind_account"`     // 是否绑定账号
	CanWithDraw      float64 `json:"canwithdraw"`         // 可用余额
	TalentPhone      string  `json:"talent_phone_number"` // 达人的联系方式
	CreateDate       string  `json:"create_date"`         // 创建时间
}

type CreatorListData struct {
	CreatorListPreview []*CreatorListPreview `json:"creator_list_preview"`
	Total              string                `json:"total"`
}

func NewCreatorListRequest() *CreatorListRequest {
	return new(CreatorListRequest)
}

func NewCreatorListResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CreatorListData)
	return resp
}
