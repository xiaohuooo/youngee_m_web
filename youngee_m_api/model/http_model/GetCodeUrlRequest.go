package http_model

type GetCodeUrlRequest struct {
	Amount int64 `json:"amount"`
	Type   int64 `json:"type"`
}

type CodeUrl struct {
	CodeUrl string `json:"code_url"`
	TradeId string `json:"trade_id"`
}

func NewGetCodeUrlRequest() *GetCodeUrlRequest {
	return new(GetCodeUrlRequest)
}

func NewGetCodeUrlResponse() *CommonResponse {
	resp := new(CommonResponse)
	resp.Data = new(CodeUrl)
	return resp
}
