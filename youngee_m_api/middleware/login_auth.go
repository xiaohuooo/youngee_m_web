package middleware

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/redis_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	log "github.com/sirupsen/logrus"

	"github.com/gin-gonic/gin"
)

func LoginAuthMiddleware(c *gin.Context) {
	token := c.Request.Header.Get("Authorization")
	if token != "" {
		//fmt.Println(token)
		if auth, err := service.LoginAuth.AuthToken(c, token); err == nil {
			c.Set(consts.SessionAuthSchema, auth)
			c.Next()
			return
		} else {
			log.Infof("[LoginAuthMiddleware] auth fail,err:%+v", err)
		}
	}
	util.PackErrorResp(c, consts.ErrorNotLogin)
	c.Abort()
}

func GetSessionAuth(c *gin.Context) *redis_model.Auth {
	auth, contain := c.Get(consts.SessionAuthSchema)
	if contain {
		return auth.(*redis_model.Auth)
	}
	return new(redis_model.Auth)
}
