package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"youngee_m_api/db"
	"youngee_m_api/model/system_model"
	"youngee_m_api/redis"
	"youngee_m_api/service"

	"gopkg.in/yaml.v2"
)

func Init() *system_model.Server {
	config := new(system_model.Config)
	env := getEnv()
	configPath := fmt.Sprintf("./config/%s.yaml", env)

	file, err := ioutil.ReadFile(configPath)
	if err != nil {
		panic(err)
	}
	//yaml文件内容影射到结构体中
	err = yaml.Unmarshal(file, config)
	if err != nil {
		panic(err)
	}
	loadExternelConfig(config)
	return config.Server
}

func loadExternelConfig(config *system_model.Config) {
	db.Init(config.Mysql)
	redis.Init(config.Redis)
	service.LoginAuthInit(config.Server.Session)
	//service.SendCodeInit(config.Server.Session)
	service.QrCodeInit(config.Server.Session)
}

func getEnv() string {
	env := os.Getenv("youngee_env")
	if env == "" {
		env = "dev"
	}
	return env
}
