module youngee_m_api

go 1.16

require (
	github.com/caixw/lib.go v0.0.0-20141220110639-1781da9139e0
	github.com/cstockton/go-conv v1.0.0
	github.com/gin-gonic/gin v1.8.1
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.3.4
	gorm.io/gorm v1.23.6
)

require (
	github.com/GUAIK-ORG/go-snowflake v0.0.0-20200116064823-220c4260e85f
	github.com/go-redis/redis/v8 v8.11.5
	github.com/google/uuid v1.3.0
	github.com/issue9/conv v1.3.4
	github.com/robfig/cron/v3 v3.0.0
	github.com/tidwall/gjson v1.14.3
	github.com/wechatpay-apiv3/wechatpay-go v0.2.15
)
