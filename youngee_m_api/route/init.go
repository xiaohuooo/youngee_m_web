package route

import (
	"youngee_m_api/handler"
	"youngee_m_api/handler/operate"
	"youngee_m_api/middleware"
	"youngee_m_api/model/http_model"

	"github.com/gin-gonic/gin"
)

func InitRoute(r *gin.Engine) {

	r.POST("/login", handler.WrapCodeLoginHandler)
	r.GET("/getLoginUser", handler.WrapGetLoginUserHandler)
	r.POST("/userInfo", handler.WrapGetUserInfoHandler)
	r.GET("/test/ping", func(c *gin.Context) {
		resp := http_model.CommonResponse{
			Status:  0,
			Message: "",
			Data:    "ping",
		}
		c.JSON(200, resp)
	})
	m := r.Group("/youngee/m")
	{
		//m.Use(middleware.LoginAuthMiddleware)
		m.POST("/test", func(c *gin.Context) {
			resp := http_model.CommonResponse{
				Status:  0,
				Message: "",
				Data:    "ping",
			}
			c.JSON(200, resp)
		})
		m.GET("/product/getEnterpriseIds", handler.WrapGetEnterpriseIdsHandler)                    // 获取所有企业用户id
		m.POST("/product/list", handler.WrapFullProjectListHandler)                                // 查询项目（全流程）列表
		m.POST("/project/show", handler.WrapShowProjectHandler)                                    // 项目展示查询
		m.POST("/project/handle", handler.WrapProjectHandleHandler)                                // 项目线索处理
		m.POST("/product/findall", handler.WrapFindEnterpriseAllProductHandler)                    // 企业用户详情所有产品
		m.POST("/product/findAllProduct", handler.WrapFindAllProductHandler)                       // 查找企业绑定的所有产品
		m.POST("/project/create", handler.WrapCreateProjectHandler)                                // 创建项目
		m.POST("/product/create", handler.WrapCreateProductHandler)                                // 创建产品
		m.POST("/product/find", handler.WrapFindProductHandler)                                    // 查询产品信息
		m.POST("/product/deletePhotoUrl", handler.WrapDeletePhotoUrlHandler)                       // 在数据库中删除图片url
		m.POST("/pay/paysum", handler.WrapPaySumHandler)                                           // 支付
		m.POST("/pay/projectpay", handler.WrapProjectPayHandler)                                   // 支付
		m.POST("/pay/getCodeUrl", handler.WrapGetCodeUrlHandler)                                   // 获取微信支付codeURL
		m.POST("/pay/queryOrderByTradeId", handler.WrapQueryOrderByTradeIdHandler)                 // 根据交易id查询微信是否扫码付款
		m.POST("/pay/rechargeBalance", handler.WrapRechargeBalanceHandler)                         // 支付成功后修改企业余额
		m.POST("/project/transferToPublic", handler.WrapTransferToPublicHandler)                   // 对公转账
		m.POST("/project/update", handler.WrapUpdateProjectHandler)                                // 更新项目信息
		m.POST("/project/approve", handler.WrapApproveProjectHandler)                              // 审核项目
		m.POST("/project/all", handler.WrapGetAllProjectHandler)                                   // 查找所有项目
		m.POST("/project/taskList", handler.WrapProjectTaskListHandler)                            // 招募管理中的任务列表
		m.POST("/project/changeTaskStatus", handler.WrapProjectChangeTaskStatusHandler)            // 更改任务状态
		m.POST("/project/getlinknumberinfo", handler.WrapGetLinkNumberInfoHandler)                 // 获取链接管理相关数据
		m.POST("/project/getdatanumberinfo", handler.WrapGetDataNumberInfoHandler)                 // 获取数据管理相关数据
		m.POST("/project/getreviewnumberinfo", handler.WrapGetReviewNumberInfoHandler)             // 获取审稿管理相关数据
		m.POST("/project/getdefaultnumberinfo", handler.WrapGetDefaultNumberInfoHandler)           // 获取违约管理相关数据
		m.POST("/project/tasklogisticslist", handler.WrapTaskLogisticsListHandler)                 // 物流信息查询
		m.POST("/project/createlogistics", handler.WrapCreateLogisticsHandler)                     // 创建物流信息
		m.POST("/project/signforreceipt", handler.WrapSignForReceiptHandler)                       // 签收订单
		m.POST("/project/taskscriptlist", handler.WrapTaskScriptListHandler)                       // 查询脚本列表
		m.POST("/project/scriptopinion", handler.WrapScriptOpinionHandler)                         // 脚本审核意见提交
		m.POST("/project/acceptscript", handler.WrapAcceptScriptHandler)                           // 同意脚本
		m.POST("/project/tasksketchlist", handler.WrapTaskSketchListHandler)                       // 查询初稿列表
		m.POST("/project/findsketchphoto", handler.WrapFindSketchPhotoHandler)                     // 查询脚本配图和视频demo
		m.POST("/project/sketchopinion", handler.WrapSketchOpinionHandler)                         // 初稿审核意见提交
		m.POST("/project/acceptsketch", handler.WrapAcceptSketchHandler)                           // 同意脚本
		m.POST("/project/tasklinklist", handler.WrapTaskLinkListHandler)                           // 查询链接列表
		m.POST("/project/linkopinion", handler.WrapLinkOpinionHandler)                             // 链接审核意见提交
		m.POST("/project/acceptlink", handler.WrapAcceptLinkHandler)                               // 同意链接
		m.POST("/project/taskdatalist", handler.WrapTaskDataListHandler)                           // 查询数据列表
		m.POST("/project/dataopinion", handler.WrapDataOpinionHandler)                             // 数据审核意见提交
		m.POST("/project/acceptdata", handler.WrapAcceptDataHandler)                               // 同意数据
		m.POST("/project/taskdefaultreviewlist", handler.WrapTaskDefaultReviewListHandler)         // 查询违约列表-脚本、初稿、链接上传违约
		m.POST("/project/taskdefaultdatalist", handler.WrapTaskDefaultDataListHandler)             // 查询违约列表-数据违约
		m.POST("/project/taskteminatinglist", handler.WrapTaskTerminatingListHandler)              // 查询违约列表-解约待处理
		m.POST("/project/taskteminatedlist", handler.WrapTaskTerminatedListHandler)                // 查询违约列表-解约
		m.POST("/project/taskteminate", handler.WrapTaskTerminateHandler)                          // 解约
		m.POST("/project/getsketchinfo", handler.WrapGetSketchInfoHandler)                         // 获取初稿
		m.POST("/project/taskfinishlist", handler.WrapTaskFinishListHandler)                       // 查询违约列表-数据违约
		m.POST("/project/getfinishnumberinfo", handler.WrapGetFinishNumberInfoHandler)             // 获取结案数量
		m.POST("/project/getProduceRecords", handler.WrapGetProjectRecordsHandler)                 // 获取项目记录
		m.POST("/project/recruit/getservicecharge", handler.WrapGetServiceChargeHandler)           // 获取产品置换服务费
		m.POST("/project/getlogisticsnumberinfo", handler.WrapGetLogisticsNumberInfoHandler)       // 获取物流数量
		m.POST("/project/getfinishdata", handler.WrapGetFinishDataHandler)                         // 获取结案信息
		m.POST("/project/getSpecialInviteNumber", handler.WrapGetSpecialInviteNumberHandler)       // 查询专项任务邀请管理任务数量
		m.POST("/project/getSpecialLogisticNumber", handler.WrapGetSpecialLogisticNumberHandler)   // 查询专项任务发货管理任务数量
		m.POST("/project/getSpecialLogisticList", handler.WrapGetSpecialLogisticListHandler)       // 查询专项任务发货管理任务列表
		m.POST("/project/createSpecialLogistics", handler.WrapCreateSpecialLogisticsHandler)       // 创建专项创建物流信息
		m.POST("/project/signForSpecialLogistic", handler.WrapSignForSpecialLogisticHandler)       // 签收专项创建物流订单
		m.POST("/project/getSpecialReviewNumber", handler.WrapGetSpecialReviewNumberHandler)       // 查询专项任务审稿管理任务数量
		m.POST("/project/getSpecialLinkNumber", handler.WrapGetSpecialLinkNumberHandler)           // 查询专项任务发布管理任务数量
		m.POST("/project/getSpecialDataNumber", handler.WrapGetSpecialDataNumberHandler)           // 查询专项任务数据管理任务数量
		m.POST("/project/specialTaskInviteList", handler.WrapSpecialTaskInviteListHandler)         // 查询专项任务邀请管理任务列表
		m.POST("/project/specialTaskSketchList", handler.WrapSpecialTaskSketchListHandler)         // 查询专项任务审稿管理任务列表
		m.POST("/project/specialTaskScriptList", handler.WrapSpecialTaskScriptListHandler)         // 查询专项任务审稿管理任务列表
		m.POST("/project/specialTaskLinkList", handler.WrapSpecialTaskLinkListHandler)             // 查询专项任务发布管理任务列表
		m.POST("/project/specialTaskDataList", handler.WrapSpecialTaskDataListHandler)             // 查询专项任务数据管理任务列表
		m.POST("/project/specialTaskFinishDataList", handler.WrapSpecialTaskFinishDataListHandler) // 查询专项任务结案数据任务列表
		m.POST("/project/getspecialfinishdata", handler.WrapGetSpecialFinishDataHandler)           // 查询专项任务结案单结案数据
		m.POST("/project/getSpecialSettleNumber", handler.WrapGetSpecialSettleNumberHandler)       // 查询专项任务结算管理任务数量
		m.POST("/project/specialTaskSettleList", handler.WrapSpecialTaskSettleListHandler)         // 查询专项任务结算管理任务列表
		m.POST("/project/getEnterpriseBalance", handler.WrapGetEnterpriseBalanceHandler)           // 查询当前账户所剩余额
		m.POST("/project/specialSettlePay", handler.WrapSpecialSettlePayHandler)                   // 结算
		m.POST("/qrcode/getwxqrcode", handler.WrapGetWxQRCodeHandler)                              // 获取微信二维码
	}
	u := r.Group("/youngee/m/user")
	{
		//u.Use(middleware.LoginAuthMiddleware)
		u.POST("/getUserList", handler.WrapGetUserListHandler)            // 查找员工账号信息
		u.POST("/updateUserInfo", handler.WrapUpdateUserInfoHandler)      // 修改员工信息
		u.POST("/createUser", handler.WrapCreateUserHandler)              // 创建员工账号
		u.POST("/disabledUser", handler.WrapDisabledUserHandler)          // 禁用员工账号
		u.POST("/enterpriseUser", handler.WrapEnterpriseUserHandler)      // 查找企业用户信息
		u.POST("/creatorList", handler.WrapCreatorListHandler)            // 查找创作者信息
		u.POST("/platformAccInfo", handler.WrapPlatformAccInfoHandler)    // 平台信息
		u.POST("/talentInfo", handler.WrapTalentInfoHandler)              // 达人信息
		u.POST("/accountInfo", handler.WrapAccountInfoHandler)            // 账号仓库-达人端账号信息
		u.POST("/deleteAccount", handler.WrapDeleteAccountHandler)        // 解绑达人端账号
		u.POST("/getTaskRecord", handler.WrapGetTaskRecordHandler)        // 创作者详细-任务记录
		u.POST("/getYoungeeRecord", handler.WrapGetYoungeeRecordsHandler) // 创作者详细-youngee记录
		u.POST("/modifyAccInfo", handler.WrapModifyAccInfoHandler)        // 更新用户账号信息
		u.POST("/block", handler.WrapBlockHandler)                        // 创作者用户拉黑与还原
		u.POST("/getSelectionInfo", handler.WrapGetSelectionInfoHandler)  // 商家用户管理-选品记录
	}
	o := r.Group("/youngee/m/operate")
	{
		o.Use(middleware.LoginAuthMiddleware)
		o.POST("/addPricing", operate.WrapAddPricingHandler)       //生成定价策略
		o.POST("/addYoungee", operate.WrapAddYoungeeHandler)       //生成youngee策略
		o.POST("/searchPricing", operate.WrapSearchPricingHandler) //查询定价管理列表
		o.POST("/searchYoungee", operate.WrapSearchYoungeeHandler) //查询youngee管理列表
		o.POST("/modifyPricing", operate.WrapModifyPricingHandler) //修改定价策略
		//o.POST("/modifyYoungee", operate.WrapModifyYoungeeHandler) //修改样之团策略
		o.POST("/signInOffline", operate.WrapSignInOfflineHandler)
		o.POST("/signInVirtual", operate.WrapSignInVirtualHandler)
		o.POST("/autoReview", operate.WrapAutoReviewHandler)
		o.POST("/autoReviewUnlimited", operate.WrapAutoReviewUnlimitedHandler)
		o.POST("/postReview", operate.WrapPostReviewHandler)
		o.POST("/caseClose", operate.WrapCaseCloseHandler)
		o.POST("/invalid", operate.WrapInvalidHandler)
		o.POST("/draftDefaultInPic", operate.WrapDraftDefaultInPicHandler)
		o.POST("/draftDefaultInMv", operate.WrapDraftDefaultInMvHandler)
		o.POST("/scriptDefault", operate.WrapScriptDefaultHandler)
		o.POST("/linkBreach", operate.WrapLinkBreachHandler)
		o.POST("/caseCloseDefault", operate.WrapCaseCloseDefaultHandler)
		o.GET("/countNumOfDefaults", operate.WrapCountNumOfDefaultsHandler)
		o.POST("/breachPending", operate.WrapBreachPendingHandler)
		o.POST("/contractBreach", operate.WrapContractBreachHandler) // 解约
		o.POST("/getSketchInfoByTaskId", operate.WrapGetSketchInfoByTaskIdHandler)
		o.POST("/breachHandled", operate.WrapBreachHandledHandler)
		o.POST("/sketchReplaceNotUpload", operate.WrapSketchReplaceNotUploadHandler)
		o.POST("/sketchReplaceTimeOut", operate.WrapSketchReplaceTimeOutHandler)
		o.POST("/sketchOtherNotUpload", operate.WrapSketchOtherNotUploadHandler)
		o.POST("/sketchOtherTimeOut", operate.WrapSketchOtherTimeOutHandler)
		o.POST("/scriptReplaceNotUpload", operate.WrapScriptReplaceNotUploadHandler)
		o.POST("/scriptReplaceTimeOut", operate.WrapScriptReplaceTimeOutHandler)
		o.POST("/scriptOtherNotUpload", operate.WrapScriptOtherNotUploadHandler)
		o.POST("/scriptOtherTimeOut", operate.WrapScriptOtherTimeOutHandler)
		o.POST("/linkReplaceNotUpload", operate.WrapLinkReplaceNotUploadHandler)
		o.POST("/linkReplaceTimeOut", operate.WrapLinkReplaceTimeOutHandler)
		o.POST("/linkOtherNotUpload", operate.WrapLinkOtherNotUploadHandler)
		o.POST("/linkOtherTimeOut", operate.WrapLinkOtherTimeOutHandler)
		o.POST("/dataReplaceNotUpload", operate.WrapDataReplaceNotUploadHandler)
		o.POST("/dataReplaceTimeOut", operate.WrapDataReplaceTimeOutHandler)
		o.POST("/dataOtherNotUpload", operate.WrapDataOtherNotUploadHandler)
		o.POST("/dataOtherTimeOut", operate.WrapDataOtherTimeOutHandler)
		o.GET("/getHours", operate.WrapGetHoursHandler)
		o.GET("/getPercents", operate.WrapGetPercentsHandler)
	}
	f := r.Group("/youngee/m/finance")
	{
		f.Use(middleware.LoginAuthMiddleware)
		f.POST("/withdrawalRecords", handler.WrapWithdrawalRecordsHandler)     // 搜索提现记录
		f.POST("/getWithdrawalRecord", handler.WrapGetWithdrawalRecordHandler) // 查看提现记录
		f.GET("/getWithdrawNums", handler.WrapGetWithdrawNumsHandler)          // 获取提现待确认的数量
		f.GET("/getInvoiceNums", handler.WrapGetInvoiceNumsHandler)            // 获取待开票的数量
		f.GET("/getRechargeNums", handler.WrapGetRechargeNumsHandler)          // 获取充值待确认的数量
		f.POST("/ConfirmWithdrawal", handler.WrapConfirmWithdrawalHandler)     // 确认提现
		f.POST("/getBankInfo", handler.WrapGetBankInfoHandler)                 // 获取银行开户地信息
		f.POST("/getCodeUrl", handler.WrapGetCodeUrlHandler)                   // 获取微信支付链接
		f.POST("/invoiceRecords", handler.WrapInvoiceRecordsHandler)           // 搜索开票记录
		f.POST("/confirmInvoice", handler.WrapConfirmInvoiceHandler)           // 确认开票
		f.POST("/rechargeRecords", handler.WrapRechargeRecordsHandler)         // 搜索充值记录
		f.POST("/operateRecharge", handler.WrapOperateRechargeHandler)         // 充值记录的修改和确认操作
	}
	// 选品广场相关接口
	s := r.Group("/youngee/m/selection")
	{
		//s.Use(middleware.LoginAuthMiddleware)
		//s.Use(middleware.LoginAuthMiddleware)
		s.GET("/reviewnumber", handler.WrapSelectionReviewNumberHandler)            //查询选品待审核的数量
		s.POST("/delete", handler.WrapDeleteSelectionHandler)                       //删除选品
		s.POST("/findAll", handler.WrapFindAllSelectionHandler)                     //选品列表
		s.POST("/detail", handler.WrapSelectionDetailHandler)                       //选品详情
		s.POST("/create", handler.WrapCreateSelectionHandler)                       // 创建选品
		s.POST("/update", handler.WrapUpdateSelectionHandler)                       // 更新选品(提交项目审核)
		s.POST("/submit", handler.WrapSubmitSelectionHandler)                       // 提交项目审核
		s.POST("/review", handler.WrapReviewSelectionHandler)                       // 选品审核通过
		s.POST("/pay", handler.WrapPaySelectionHandler)                             // 支付选品项目
		s.POST("/task/list", handler.WrapGetSecTaskListHandler)                     // 查询选品的任务列表
		s.POST("/task/coop/pass", handler.WrapPassSecTaskCoopHandler)               // 同意任务合作
		s.POST("/task/coop/refuse", handler.WrapRefuseSecTaskCoopHandler)           // 拒绝任务合作
		s.POST("/task/logistics/create", handler.WrapCreateSecTaskLogisticsHandler) // 上传物流信息
		s.POST("/task/logistics/update", handler.WrapUpdateSecTaskLogisticsHandler) // 修改物流信息
		s.POST("/task/settle", handler.WrapSettleSecTaskHandler)                    // 结算
	}
}
