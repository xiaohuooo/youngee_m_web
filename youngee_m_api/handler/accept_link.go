package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapAcceptLinkHandler(ctx *gin.Context) {
	handler := newAcceptLinkHandler(ctx)
	BaseRun(handler)
}

func newAcceptLinkHandler(ctx *gin.Context) *AcceptLinkHandler {
	return &AcceptLinkHandler{
		req:  http_model.NewAcceptLinkRequest(),
		resp: http_model.NewAcceptLinkResponse(),
		ctx:  ctx,
	}
}

type AcceptLinkHandler struct {
	req  *http_model.AcceptLinkRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *AcceptLinkHandler) getRequest() interface{} {
	return h.req
}
func (h *AcceptLinkHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *AcceptLinkHandler) getResponse() interface{} {
	return h.resp
}

func (h *AcceptLinkHandler) run() {
	data := http_model.AcceptLinkRequest{}
	data = *h.req
	res, err := service.Link.AcceptLink(h.ctx, data)
	if err != nil {
		logrus.Errorf("[ReviseOpinionHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "成功通过链接"
	h.resp.Data = res
	h.resp.Data = data
}

func (h *AcceptLinkHandler) checkParam() error {
	return nil
}
