package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapQueryOrderByTradeIdHandler(ctx *gin.Context) {
	handler := newQueryOrderByTradeIdHandler(ctx)
	BaseRun(handler)
}

type QueryOrderByTradeIdHandler struct {
	ctx  *gin.Context
	req  *http_model.QueryOrderByTradeIdRequest
	resp *http_model.CommonResponse
}

func (q QueryOrderByTradeIdHandler) getContext() *gin.Context {
	return q.ctx
}

func (q QueryOrderByTradeIdHandler) getResponse() interface{} {
	return q.resp
}

func (q QueryOrderByTradeIdHandler) getRequest() interface{} {
	return q.req
}

func (q QueryOrderByTradeIdHandler) run() {
	data, err := service.QueryOrderByOutTradeNo(q.req.TradeId)
	if err != nil {
		logrus.WithContext(q.ctx).Errorf("[QueryOrderByTradeIdHandler] error QueryOrderByOutTradeNo, err:%+v", err)
		util.HandlerPackErrorResp(q.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	q.resp.Data = data
}

func (q QueryOrderByTradeIdHandler) checkParam() error {
	return nil
}

func newQueryOrderByTradeIdHandler(ctx *gin.Context) *QueryOrderByTradeIdHandler {
	return &QueryOrderByTradeIdHandler{
		ctx:  ctx,
		req:  http_model.NewQueryOrderByTradeIdRequest(),
		resp: http_model.NewQueryOrderByTradeIdResponse(),
	}
}
