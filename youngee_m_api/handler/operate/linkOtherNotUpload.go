package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapLinkOtherNotUploadHandler(ctx *gin.Context) {
	handler := newLinkOtherNotUploadHandler(ctx)
	BaseRun(handler)
}

type LinkOtherNotUploadHandler struct {
	ctx  *gin.Context
	req  *http_model.LinkOtherNotUploadRequest
	resp *http_model.CommonResponse
}

func (l LinkOtherNotUploadHandler) getContext() *gin.Context {
	return l.ctx
}

func (l LinkOtherNotUploadHandler) getResponse() interface{} {
	return l.resp
}

func (l LinkOtherNotUploadHandler) getRequest() interface{} {
	return l.req
}

func (l LinkOtherNotUploadHandler) run() {
	rate := l.req.Rate
	err := db.UpdateAutoDefaultRate(l.ctx, rate, 11)
	if err != nil {
		logrus.WithContext(l.ctx).Errorf("[LinkOtherNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(l.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	l.resp.Message = "已更新自报价、固定稿费类型的链接违约未上传的扣款比率"
}

func (l LinkOtherNotUploadHandler) checkParam() error {
	return nil
}

func newLinkOtherNotUploadHandler(ctx *gin.Context) *LinkOtherNotUploadHandler {
	return &LinkOtherNotUploadHandler{
		ctx:  ctx,
		req:  http_model.NewLinkOtherNotUploadRequest(),
		resp: http_model.NewLinkOtherNotUploadResponse(),
	}
}
