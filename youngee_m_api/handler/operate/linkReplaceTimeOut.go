package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapLinkReplaceTimeOutHandler(ctx *gin.Context) {
	handler := newLinkReplaceTimeOutHandler(ctx)
	BaseRun(handler)
}

type LinkReplaceTimeOutHandler struct {
	ctx  *gin.Context
	req  *http_model.LinkReplaceTimeOutRequest
	resp *http_model.CommonResponse
}

func (l LinkReplaceTimeOutHandler) getContext() *gin.Context {
	return l.ctx
}

func (l LinkReplaceTimeOutHandler) getResponse() interface{} {
	return l.resp
}

func (l LinkReplaceTimeOutHandler) getRequest() interface{} {
	return l.req
}

func (l LinkReplaceTimeOutHandler) run() {
	rate := l.req.Rate
	err := db.UpdateAutoDefaultRate(l.ctx, rate, 10)
	if err != nil {
		logrus.WithContext(l.ctx).Errorf("[LinkReplaceTimeOutHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(l.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	l.resp.Message = "已更新产品置换类型的链接违约超时未上传的扣款比率"
}

func (l LinkReplaceTimeOutHandler) checkParam() error {
	return nil
}

func newLinkReplaceTimeOutHandler(ctx *gin.Context) *LinkReplaceTimeOutHandler {
	return &LinkReplaceTimeOutHandler{
		ctx:  ctx,
		req:  http_model.NewLinkReplaceTimeOutRequest(),
		resp: http_model.NewLinkReplaceTimeOutResponse(),
	}
}
