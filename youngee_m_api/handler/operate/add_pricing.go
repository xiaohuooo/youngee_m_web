package operate

import (
	"fmt"
	"github.com/caixw/lib.go/conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapAddPricingHandler(ctx *gin.Context) {
	handler := newPricingHandler(ctx)
	BaseRun(handler)
}

type PricingHandler struct {
	req  *http_model.AddPricingRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func newPricingHandler(ctx *gin.Context) *PricingHandler {
	return &PricingHandler{
		req:  http_model.NewAddPricingRequest(),
		resp: http_model.NewAddPricingResponse(),
		ctx:  ctx,
	}
}

func (p PricingHandler) getContext() *gin.Context {
	return p.ctx
}

func (p PricingHandler) getResponse() interface{} {
	return p.resp
}

func (p PricingHandler) getRequest() interface{} {
	return p.req
}

func (p PricingHandler) run() {
	toast, strategyId, err := db.CreatePricingStrategy(p.ctx, p.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[PricingHandler] call AddPricing err:%+v\n", err)
		util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
		logrus.Info("PricingHandler fail,req:%+v", p.req)
		return
	}
	if toast == "创建成功" {
		p.resp.Message = "编号为" + strategyId + "的策略添加成功"
	} else if toast == "该策略与已经运行的策略互斥，请修改相关字段后重新创建" {
		p.resp.Status = 1
	} else {
		p.resp.Status = 2
	}
}

func (p PricingHandler) checkParam() error {
	var errs []error
	p.req.ProjectType = util.IsNull(p.req.ProjectType)
	if _, err := conv.Int64(p.req.ProjectType); err != nil {
		errs = append(errs, err)
	}
	p.req.Platform = util.IsNull(p.req.Platform)
	if _, err := conv.Int64(p.req.Platform); err != nil {
		errs = append(errs, err)
	}
	p.req.ManuscriptForm = util.IsNull(p.req.ManuscriptForm)
	if _, err := conv.Int64(p.req.ManuscriptForm); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
