package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapSketchOtherNotUploadHandler(ctx *gin.Context) {
	handler := newSketchOtherNotUploadHandler(ctx)
	BaseRun(handler)
}

type SketchOtherNotUploadHandler struct {
	ctx  *gin.Context
	req  *http_model.SketchOtherNotUploadRequest
	resp *http_model.CommonResponse
}

func (s SketchOtherNotUploadHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SketchOtherNotUploadHandler) getResponse() interface{} {
	return s.resp
}

func (s SketchOtherNotUploadHandler) getRequest() interface{} {
	return s.req
}

func (s SketchOtherNotUploadHandler) run() {
	rate := s.req.Rate
	err := db.UpdateAutoDefaultRate(s.ctx, rate, 3)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[ScriptOtherNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已更新自报价、固定稿费类型的初稿违约未上传的扣款比率"
}

func (s SketchOtherNotUploadHandler) checkParam() error {
	return nil
}

func newSketchOtherNotUploadHandler(ctx *gin.Context) *SketchOtherNotUploadHandler {
	return &SketchOtherNotUploadHandler{
		ctx:  ctx,
		req:  http_model.NewSketchOtherNotUploadRequest(),
		resp: http_model.NewSketchOtherNotUploadResponse(),
	}
}
