package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetHoursHandler(ctx *gin.Context) {
	handler := newGetHoursHandler(ctx)
	BaseRun(handler)
}

type GetHoursHandler struct {
	ctx  *gin.Context
	req  *http_model.GetHoursRequest
	resp *http_model.CommonResponse
}

func (g GetHoursHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetHoursHandler) getResponse() interface{} {
	return g.resp
}

func (g GetHoursHandler) getRequest() interface{} {
	return g.req
}

func (g GetHoursHandler) run() {
	data, err := db.GetHours(g.ctx)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetHoursHandler] error GetHours, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetHoursHandler) checkParam() error {
	return nil
}

func newGetHoursHandler(ctx *gin.Context) *GetHoursHandler {
	return &GetHoursHandler{
		ctx:  ctx,
		req:  http_model.NewGetHoursRequest(),
		resp: http_model.NewGetHoursResponse(),
	}
}
