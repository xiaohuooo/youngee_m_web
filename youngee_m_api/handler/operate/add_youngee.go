package operate

import (
	"fmt"
	"github.com/caixw/lib.go/conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapAddYoungeeHandler(ctx *gin.Context) {
	handler := newYoungeeHandler(ctx)
	BaseRun(handler)
}

type YoungeeHandler struct {
	req  *http_model.AddYoungeeRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func newYoungeeHandler(ctx *gin.Context) *YoungeeHandler {
	return &YoungeeHandler{
		req:  http_model.NewAddYoungeeRequest(),
		resp: http_model.NewAddYoungeeResponse(),
		ctx:  ctx,
	}
}

func (p YoungeeHandler) getContext() *gin.Context {
	return p.ctx
}

func (p YoungeeHandler) getResponse() interface{} {
	return p.resp
}

func (p YoungeeHandler) getRequest() interface{} {
	return p.req
}

func (p YoungeeHandler) run() {
	toast, strategyId, err := db.CreateYoungeeStrategy(p.ctx, p.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[YoungeeHandler] call AddYoungee err:%+v\n", err)
		util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
		logrus.Info("YoungeeHandler fail,req:%+v", p.req)
		return
	}
	if toast == "创建成功" {
		p.resp.Message = "编号为" + strategyId + "的策略添加成功"
	} else if toast == "该策略与已经运行的策略互斥，请修改相关字段后重新创建" {
		p.resp.Status = 1
	} else {
		p.resp.Status = 2
	}
}

func (p YoungeeHandler) checkParam() error {
	var errs []error
	p.req.ProjectType = util.IsNull(p.req.ProjectType)
	if _, err := conv.Int64(p.req.ProjectType); err != nil {
		errs = append(errs, err)
	}
	p.req.Platform = util.IsNull(p.req.Platform)
	if _, err := conv.Int64(p.req.Platform); err != nil {
		errs = append(errs, err)
	}
	p.req.TaskType = util.IsNull(p.req.TaskType)
	if _, err := conv.Int64(p.req.TaskType); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
