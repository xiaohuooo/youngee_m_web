package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapCaseCloseDefaultHandler(ctx *gin.Context) {
	handler := newCaseCloseDefaultHandler(ctx)
	BaseRun(handler)
}

type caseCloseDefaultHandler struct {
	req  *http_model.CaseCloseDefaultRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (c caseCloseDefaultHandler) getContext() *gin.Context {
	return c.ctx
}

func (c caseCloseDefaultHandler) getResponse() interface{} {
	return c.resp
}

func (c caseCloseDefaultHandler) getRequest() interface{} {
	return c.req
}

func (c caseCloseDefaultHandler) run() {
	time := c.req.Time
	err := db.UpdateAutoTaskTime(c.ctx, time, 12)
	if err != nil {
		logrus.WithContext(c.ctx).Errorf("[caseCloseDefaultHandler] error AutoCaseCloseDefault, err:%+v", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	c.resp.Message = "已启动结案违约自动处理服务"
}

func (c caseCloseDefaultHandler) checkParam() error {
	return nil
}

func newCaseCloseDefaultHandler(ctx *gin.Context) *caseCloseDefaultHandler {
	return &caseCloseDefaultHandler{
		ctx:  ctx,
		req:  http_model.NewCaseCloseDefaultRequest(),
		resp: http_model.NewCaseCloseDefaultResponse(),
	}
}
