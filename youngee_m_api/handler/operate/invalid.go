package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapInvalidHandler(ctx *gin.Context) {
	handler := newInvalidHandler(ctx)
	BaseRun(handler)
}

type invalidHandler struct {
	req  *http_model.InvalidRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (i invalidHandler) getContext() *gin.Context {
	return i.ctx
}

func (i invalidHandler) getResponse() interface{} {
	return i.resp
}

func (i invalidHandler) getRequest() interface{} {
	return i.req
}

func (i invalidHandler) run() {
	time := i.req.Time
	err := db.UpdateAutoTaskTime(i.ctx, time, 7)
	if err != nil {
		logrus.WithContext(i.ctx).Errorf("[invalidHandler] error invalid, err:%+v", err)
		util.HandlerPackErrorResp(i.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	i.resp.Message = "已启动失效自动处理服务"
}

func (i invalidHandler) checkParam() error {
	return nil
}

func newInvalidHandler(ctx *gin.Context) *invalidHandler {
	return &invalidHandler{
		ctx:  ctx,
		req:  http_model.NewInvalidRequest(),
		resp: http_model.NewInvalidResponse(),
	}
}
