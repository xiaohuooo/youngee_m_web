package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapScriptOtherNotUploadHandler(ctx *gin.Context) {
	handler := newScriptOtherNotUploadHandler(ctx)
	BaseRun(handler)
}

type ScriptOtherNotUploadHandler struct {
	ctx  *gin.Context
	req  *http_model.ScriptOtherNotUploadRequest
	resp *http_model.CommonResponse
}

func (s ScriptOtherNotUploadHandler) getContext() *gin.Context {
	return s.ctx
}

func (s ScriptOtherNotUploadHandler) getResponse() interface{} {
	return s.resp
}

func (s ScriptOtherNotUploadHandler) getRequest() interface{} {
	return s.req
}

func (s ScriptOtherNotUploadHandler) run() {
	rate := s.req.Rate
	err := db.UpdateAutoDefaultRate(s.ctx, rate, 7)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[ScriptOtherNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已更新自报价、固定稿费类型的脚本违约未上传的扣款比率"
}

func (s ScriptOtherNotUploadHandler) checkParam() error {
	return nil
}

func newScriptOtherNotUploadHandler(ctx *gin.Context) *ScriptOtherNotUploadHandler {
	return &ScriptOtherNotUploadHandler{
		ctx:  ctx,
		req:  http_model.NewScriptOtherNotUploadRequest(),
		resp: http_model.NewScriptOtherNotUploadResponse(),
	}
}
