package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapAutoReviewHandler(ctx *gin.Context) {
	handler := newAutoReviewHandler(ctx)
	BaseRun(handler)
}

type autoReviewHandler struct {
	req  *http_model.AutoReviewRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func newAutoReviewHandler(ctx *gin.Context) *autoReviewHandler {
	return &autoReviewHandler{
		req:  http_model.NewAutoReviewRequest(),
		resp: http_model.NewAutoReviewResponse(),
		ctx:  ctx,
	}
}

func (a autoReviewHandler) getContext() *gin.Context {
	return a.ctx
}

func (a autoReviewHandler) getResponse() interface{} {
	return a.resp
}

func (a autoReviewHandler) getRequest() interface{} {
	return a.req
}

func (a autoReviewHandler) run() {
	time := a.req.Time
	err := db.UpdateAutoTaskTime(a.ctx, time, 3)
	if err != nil {
		logrus.WithContext(a.ctx).Errorf("[autoReviewHandler] error autoReview, err:%+v", err)
		util.HandlerPackErrorResp(a.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	a.resp.Message = "已启动视频形式的审稿自动处理服务"
}

func (a autoReviewHandler) checkParam() error {
	return nil
}
