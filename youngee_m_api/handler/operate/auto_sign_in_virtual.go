package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapSignInVirtualHandler(ctx *gin.Context) {
	handler := newSignInVirtualHandler(ctx)
	BaseRun(handler)
}

type SignInVirtualHandler struct {
	req  *http_model.SignInVirtualRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func newSignInVirtualHandler(ctx *gin.Context) *SignInVirtualHandler {
	return &SignInVirtualHandler{
		req:  http_model.NewSignInVirtualRequest(),
		resp: http_model.NewSignInVirtualResponse(),
		ctx:  ctx,
	}
}

func (s SignInVirtualHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SignInVirtualHandler) getResponse() interface{} {
	return s.resp
}

func (s SignInVirtualHandler) getRequest() interface{} {
	return s.req
}

func (s SignInVirtualHandler) run() {
	time := s.req.Time
	err := db.UpdateAutoTaskTime(s.ctx, time, 2)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[SignInVirtualHandler] error AutoSignInVirtual, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已启动虚拟产品测评的自动签收服务"
}

func (s SignInVirtualHandler) checkParam() error {
	return nil
}
