package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapSignInOfflineHandler(ctx *gin.Context) {
	handler := newSignInOfflineHandler(ctx)
	BaseRun(handler)
}

func newSignInOfflineHandler(ctx *gin.Context) *SignInOfflineHandler {
	return &SignInOfflineHandler{
		req:  http_model.NewSignInOfflineRequest(),
		resp: http_model.NewSignInOfflineResponse(),
		ctx:  ctx,
	}
}

type SignInOfflineHandler struct {
	req  *http_model.SignInOfflineRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (s SignInOfflineHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SignInOfflineHandler) getResponse() interface{} {
	return s.resp
}

func (s SignInOfflineHandler) getRequest() interface{} {
	return s.req
}

func (s SignInOfflineHandler) run() {
	time := s.req.Time
	err := db.UpdateAutoTaskTime(s.ctx, time, 1)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[SignInOfflineHandler] error AutoSignInOffline, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已启动线下探店打卡的自动签收服务"
}

func (s SignInOfflineHandler) checkParam() error {
	return nil
}
