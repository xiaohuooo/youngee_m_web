package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapSketchReplaceNotUploadHandler(ctx *gin.Context) {
	handler := newSketchReplaceNotUploadHandler(ctx)
	BaseRun(handler)
}

type SketchReplaceNotUploadHandler struct {
	ctx  *gin.Context
	req  *http_model.SketchReplaceNotUploadRequest
	resp *http_model.CommonResponse
}

func (s SketchReplaceNotUploadHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SketchReplaceNotUploadHandler) getResponse() interface{} {
	return s.resp
}

func (s SketchReplaceNotUploadHandler) getRequest() interface{} {
	return s.req
}

func (s SketchReplaceNotUploadHandler) run() {
	rate := s.req.Rate
	err := db.UpdateAutoDefaultRate(s.ctx, rate, 1)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[SketchReplaceNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已更新产品置换类型的初稿违约未上传的扣款比率"
}

func (s SketchReplaceNotUploadHandler) checkParam() error {
	return nil
}

func newSketchReplaceNotUploadHandler(ctx *gin.Context) *SketchReplaceNotUploadHandler {
	return &SketchReplaceNotUploadHandler{
		ctx:  ctx,
		req:  http_model.NewSketchReplaceNotUploadRequest(),
		resp: http_model.NewSketchReplaceNotUploadResponse(),
	}
}
