package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDataReplaceNotUploadHandler(ctx *gin.Context) {
	handler := newDataReplaceNotUploadHandler(ctx)
	BaseRun(handler)
}

type DataReplaceNotUploadHandler struct {
	ctx  *gin.Context
	req  *http_model.DataReplaceNotUploadRequest
	resp *http_model.CommonResponse
}

func (d DataReplaceNotUploadHandler) getContext() *gin.Context {
	return d.ctx
}

func (d DataReplaceNotUploadHandler) getResponse() interface{} {
	return d.resp
}

func (d DataReplaceNotUploadHandler) getRequest() interface{} {
	return d.req
}

func (d DataReplaceNotUploadHandler) run() {
	rate := d.req.Rate
	err := db.UpdateAutoDefaultRate(d.ctx, rate, 13)
	if err != nil {
		logrus.WithContext(d.ctx).Errorf("[DataReplaceNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	d.resp.Message = "已更新产品置换类型的数据违约未上传的扣款比率"
}

func (d DataReplaceNotUploadHandler) checkParam() error {
	return nil
}

func newDataReplaceNotUploadHandler(ctx *gin.Context) *DataReplaceNotUploadHandler {
	return &DataReplaceNotUploadHandler{
		ctx:  ctx,
		req:  http_model.NewDataReplaceNotUploadRequest(),
		resp: http_model.NewDataReplaceNotUploadResponse(),
	}
}
