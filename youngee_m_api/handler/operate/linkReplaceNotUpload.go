package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapLinkReplaceNotUploadHandler(ctx *gin.Context) {
	handler := newLinkReplaceNotUploadHandler(ctx)
	BaseRun(handler)
}

type LinkReplaceNotUploadHandler struct {
	ctx  *gin.Context
	req  *http_model.LinkReplaceNotUploadRequest
	resp *http_model.CommonResponse
}

func (l LinkReplaceNotUploadHandler) getContext() *gin.Context {
	return l.ctx
}

func (l LinkReplaceNotUploadHandler) getResponse() interface{} {
	return l.resp
}

func (l LinkReplaceNotUploadHandler) getRequest() interface{} {
	return l.req
}

func (l LinkReplaceNotUploadHandler) run() {
	rate := l.req.Rate
	err := db.UpdateAutoDefaultRate(l.ctx, rate, 9)
	if err != nil {
		logrus.WithContext(l.ctx).Errorf("[LinkReplaceNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(l.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	l.resp.Message = "已更新产品置换类型的链接违约未上传的扣款比率"
}

func (l LinkReplaceNotUploadHandler) checkParam() error {
	return nil
}

func newLinkReplaceNotUploadHandler(ctx *gin.Context) *LinkReplaceNotUploadHandler {
	return &LinkReplaceNotUploadHandler{
		ctx:  ctx,
		req:  http_model.NewLinkReplaceNotUploadRequest(),
		resp: http_model.NewLinkReplaceNotUploadResponse(),
	}
}
