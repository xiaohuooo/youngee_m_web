package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapCountNumOfDefaultsHandler(ctx *gin.Context) {
	handler := newCountNumOfDefaults(ctx)
	BaseRun(handler)
}

type CountNumOfDefaults struct {
	ctx  *gin.Context
	req  *http_model.CountNumOfDefaultsRequest
	resp *http_model.CommonResponse
}

func newCountNumOfDefaults(ctx *gin.Context) *CountNumOfDefaults {
	return &CountNumOfDefaults{
		ctx:  ctx,
		req:  http_model.NewCountNumOfDefaultsRequest(),
		resp: http_model.NewCountNumOfDefaultsResponse(),
	}
}

func (c CountNumOfDefaults) getContext() *gin.Context {
	return c.ctx
}

func (c CountNumOfDefaults) getResponse() interface{} {
	return c.resp
}

func (c CountNumOfDefaults) getRequest() interface{} {
	return c.req
}

func (c CountNumOfDefaults) run() {
	err, data := db.CountDefaultNum(c.ctx)
	if err != nil {
		logrus.WithContext(c.ctx).Errorf("[CountNumOfDefaults] error CountDefaultNum, err:%+v", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	c.resp.Data = data
}

func (c CountNumOfDefaults) checkParam() error {
	return nil
}
