package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapScriptOtherTimeOutHandler(ctx *gin.Context) {
	handler := newScriptOtherTimeOutHandler(ctx)
	BaseRun(handler)
}

type ScriptOtherTimeOutHandler struct {
	ctx  *gin.Context
	req  *http_model.ScriptOtherTimeOutRequest
	resp *http_model.CommonResponse
}

func (s ScriptOtherTimeOutHandler) getContext() *gin.Context {
	return s.ctx
}

func (s ScriptOtherTimeOutHandler) getResponse() interface{} {
	return s.resp
}

func (s ScriptOtherTimeOutHandler) getRequest() interface{} {
	return s.req
}

func (s ScriptOtherTimeOutHandler) run() {
	rate := s.req.Rate
	err := db.UpdateAutoDefaultRate(s.ctx, rate, 8)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[ScriptOtherTimeOutHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已更新自报价、固定稿费类型的脚本违约未上传的扣款比率"
}

func (s ScriptOtherTimeOutHandler) checkParam() error {
	return nil
}

func newScriptOtherTimeOutHandler(ctx *gin.Context) *ScriptOtherTimeOutHandler {
	return &ScriptOtherTimeOutHandler{
		ctx:  ctx,
		req:  http_model.NewScriptOtherTimeOutRequest(),
		resp: http_model.NewScriptOtherTimeOutResponse(),
	}
}
