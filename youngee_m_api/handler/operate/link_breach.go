package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapLinkBreachHandler(ctx *gin.Context) {
	handler := newLinkBreachHandler(ctx)
	BaseRun(handler)
}

type linkBreachHandler struct {
	req  *http_model.LinkBreachRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (l linkBreachHandler) getContext() *gin.Context {
	return l.ctx
}

func (l linkBreachHandler) getResponse() interface{} {
	return l.resp
}

func (l linkBreachHandler) getRequest() interface{} {
	return l.req
}

func (l linkBreachHandler) run() {
	time := l.req.Time
	err := db.UpdateAutoTaskTime(l.ctx, time, 11)
	if err != nil {
		logrus.WithContext(l.ctx).Errorf("[linkBreachHandler] error AutoLinkBreach, err:%+v", err)
		util.HandlerPackErrorResp(l.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	l.resp.Message = "已启动链接违约自动处理服务"
}

func (l linkBreachHandler) checkParam() error {
	return nil
}

func newLinkBreachHandler(ctx *gin.Context) *linkBreachHandler {
	return &linkBreachHandler{
		req:  http_model.NewLinkBreachRequest(),
		resp: http_model.NewLinkBreachResponse(),
		ctx:  ctx,
	}
}
