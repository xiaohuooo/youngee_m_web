package operate

import (
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapModifyPricingHandler(ctx *gin.Context) {
	handler := newModifyPricingHandler(ctx)
	BaseRun(handler)
}

type ModifyPricingHandler struct {
	req  *http_model.ModifyPricingRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func newModifyPricingHandler(ctx *gin.Context) *ModifyPricingHandler {
	return &ModifyPricingHandler{
		req:  http_model.NewModifyPricingRequest(),
		resp: http_model.NewModifyPricingResponse(),
		ctx:  ctx,
	}
}

func (m ModifyPricingHandler) getContext() *gin.Context {
	return m.ctx
}

func (m ModifyPricingHandler) getResponse() interface{} {
	return m.resp
}

func (m ModifyPricingHandler) getRequest() interface{} {
	return m.req
}

func (m ModifyPricingHandler) run() {
	toast, err := db.ModifyPricing(m.ctx, m.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[ModifyPricingHandler] call ModifyPricing err:%+v\n", err)
		util.HandlerPackErrorResp(m.resp, consts.ErrorInternal, "")
		logrus.Info("PricingHandler fail,req:%+v", m.req)
		return
	}
	if toast == 0 {
		m.resp.Message = "编号为" + m.req.StrategyId + "的策略修改成功"
	} else if toast == 1 {
		m.resp.Message = "该策略与已经运行的策略互斥，请修改相关字段后重新创建"
		m.resp.Status = 1
	} else {
		m.resp.Message = "网络错误，修改失败"
		m.resp.Status = 2
	}
}

func (m ModifyPricingHandler) checkParam() error {
	return nil
}
