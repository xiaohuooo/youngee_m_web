package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDraftDefaultInPicHandler(ctx *gin.Context) {
	handler := newDraftDefaultInPicHandler(ctx)
	BaseRun(handler)
}

type draftDefaultInPic struct {
	req  *http_model.DraftDefaultInPicRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (d draftDefaultInPic) getContext() *gin.Context {
	return d.ctx
}

func (d draftDefaultInPic) getResponse() interface{} {
	return d.resp
}

func (d draftDefaultInPic) getRequest() interface{} {
	return d.req
}

func (d draftDefaultInPic) run() {
	time := d.req.Time
	err := db.UpdateAutoTaskTime(d.ctx, time, 8)
	if err != nil {
		logrus.WithContext(d.ctx).Errorf("[draftDefaultInPic] error draftDefaultInPic, err:%+v", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	d.resp.Message = "已启动图片初稿违约自动处理服务"
}

func (d draftDefaultInPic) checkParam() error {
	return nil
}

func newDraftDefaultInPicHandler(ctx *gin.Context) *draftDefaultInPic {
	return &draftDefaultInPic{
		ctx:  ctx,
		req:  http_model.NewDraftDefaultInPicRequest(),
		resp: http_model.NewDraftDefaultInPicResponse(),
	}

}
