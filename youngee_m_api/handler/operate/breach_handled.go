package operate

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/util"
)

func WrapBreachHandledHandler(ctx *gin.Context) {
	handler := newBreachHandledHandler(ctx)
	BaseRun(handler)
}

type BreachHandledHandler struct {
	ctx  *gin.Context
	req  *http_model.BreachHandledRequest
	resp *http_model.CommonResponse
}

func (b BreachHandledHandler) getContext() *gin.Context {
	return b.ctx
}

func (b BreachHandledHandler) getResponse() interface{} {
	return b.resp
}

func (b BreachHandledHandler) getRequest() interface{} {
	return b.req
}

func (b BreachHandledHandler) run() {
	conditions := pack.HttpBreachHandledRequestToConditions(b.req)
	data, err := db.BreachHandled(b.ctx, b.req.PageSize, b.req.PageNum, b.req, conditions)
	if err != nil {
		logrus.WithContext(b.ctx).Errorf("[BreachHandledHandler] error BreachHandled, err:%+v", err)
		util.HandlerPackErrorResp(b.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	b.resp.Data = data
}

func (b BreachHandledHandler) checkParam() error {
	var errs []error
	if b.req.PageNum < 0 || b.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	b.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}

func newBreachHandledHandler(ctx *gin.Context) *BreachHandledHandler {
	return &BreachHandledHandler{
		ctx:  ctx,
		req:  http_model.NewBreachHandledRequest(),
		resp: http_model.NewBreachHandledResponse(),
	}
}
