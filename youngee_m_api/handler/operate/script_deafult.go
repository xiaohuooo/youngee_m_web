package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapScriptDefaultHandler(ctx *gin.Context) {
	handler := newScriptDefaultHandler(ctx)
	BaseRun(handler)
}

type scriptDefaultHandler struct {
	req  *http_model.ScriptDefaultRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (s scriptDefaultHandler) getContext() *gin.Context {
	return s.ctx
}

func (s scriptDefaultHandler) getResponse() interface{} {
	return s.resp
}

func (s scriptDefaultHandler) getRequest() interface{} {
	return s.req
}

func (s scriptDefaultHandler) run() {
	time := s.req.Time
	err := db.UpdateAutoTaskTime(s.ctx, time, 10)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[scriptDefaultHandler] error AutoScriptDefault, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已启动脚本违约自动处理服务"
}

func (s scriptDefaultHandler) checkParam() error {
	return nil
}

func newScriptDefaultHandler(ctx *gin.Context) *scriptDefaultHandler {
	return &scriptDefaultHandler{
		ctx:  ctx,
		req:  http_model.NewScriptDefaultRequest(),
		resp: http_model.NewScriptDefaultResponse(),
	}
}
