package operate

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapBreachPendingHandler(ctx *gin.Context) {
	handler := newBreachPendingHandler(ctx)
	BaseRun(handler)
}

type BreachPendingHandler struct {
	ctx  *gin.Context
	req  *http_model.BreachPendingRequest
	resp *http_model.CommonResponse
}

func newBreachPendingHandler(ctx *gin.Context) *BreachPendingHandler {
	return &BreachPendingHandler{
		ctx:  ctx,
		req:  http_model.NewBreachPendingRequest(),
		resp: http_model.NewBreachPendingResponse(),
	}
}

func (b BreachPendingHandler) getContext() *gin.Context {
	return b.ctx
}

func (b BreachPendingHandler) getResponse() interface{} {
	return b.resp
}

func (b BreachPendingHandler) getRequest() interface{} {
	return b.req
}

func (b BreachPendingHandler) run() {
	data, err := db.BreachPending(b.ctx, b.req.PageSize, b.req.PageNum, b.req)
	if err != nil {
		logrus.WithContext(b.ctx).Errorf("[BreachPendingHandler] error BreachPending, err:%+v", err)
		util.HandlerPackErrorResp(b.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	b.resp.Data = data
}

func (b BreachPendingHandler) checkParam() error {
	var errs []error
	if b.req.PageNum < 0 || b.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	b.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
