package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDataReplaceTimeOutHandler(ctx *gin.Context) {
	handler := newDataReplaceTimeOutHandler(ctx)
	BaseRun(handler)
}

type DataReplaceTimeOutHandler struct {
	ctx  *gin.Context
	req  *http_model.DataReplaceTimeOutRequest
	resp *http_model.CommonResponse
}

func (d DataReplaceTimeOutHandler) getContext() *gin.Context {
	return d.ctx
}

func (d DataReplaceTimeOutHandler) getResponse() interface{} {
	return d.resp
}

func (d DataReplaceTimeOutHandler) getRequest() interface{} {
	return d.req
}

func (d DataReplaceTimeOutHandler) run() {
	rate := d.req.Rate
	err := db.UpdateAutoDefaultRate(d.ctx, rate, 14)
	if err != nil {
		logrus.WithContext(d.ctx).Errorf("[DataReplaceTimeOutHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	d.resp.Message = "已更新产品置换类型的数据违约超时未上传的扣款比率"
}

func (d DataReplaceTimeOutHandler) checkParam() error {
	return nil
}

func newDataReplaceTimeOutHandler(ctx *gin.Context) *DataReplaceTimeOutHandler {
	return &DataReplaceTimeOutHandler{
		ctx:  ctx,
		req:  http_model.NewDataReplaceTimeOutRequest(),
		resp: http_model.NewDataReplaceTimeOutResponse(),
	}
}
