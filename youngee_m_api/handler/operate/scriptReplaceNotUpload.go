package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapScriptReplaceNotUploadHandler(ctx *gin.Context) {
	handler := newScriptReplaceNotUploadHandler(ctx)
	BaseRun(handler)
}

type ScriptReplaceNotUploadHandler struct {
	ctx  *gin.Context
	req  *http_model.ScriptReplaceNotUploadRequest
	resp *http_model.CommonResponse
}

func (s ScriptReplaceNotUploadHandler) getContext() *gin.Context {
	return s.ctx
}

func (s ScriptReplaceNotUploadHandler) getResponse() interface{} {
	return s.resp
}

func (s ScriptReplaceNotUploadHandler) getRequest() interface{} {
	return s.req
}

func (s ScriptReplaceNotUploadHandler) run() {
	rate := s.req.Rate
	err := db.UpdateAutoDefaultRate(s.ctx, rate, 5)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[ScriptReplaceNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已更新产品置换类型的脚本违约未上传的扣款比率"
}

func (s ScriptReplaceNotUploadHandler) checkParam() error {
	return nil
}

func newScriptReplaceNotUploadHandler(ctx *gin.Context) *ScriptReplaceNotUploadHandler {
	return &ScriptReplaceNotUploadHandler{
		ctx:  ctx,
		req:  http_model.NewScriptReplaceNotUploadRequest(),
		resp: http_model.NewScriptReplaceNotUploadResponse(),
	}
}
