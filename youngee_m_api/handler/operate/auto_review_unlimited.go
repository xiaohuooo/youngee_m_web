package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapAutoReviewUnlimitedHandler(ctx *gin.Context) {
	handler := newAutoReviewUnlimited(ctx)
	BaseRun(handler)
}

func newAutoReviewUnlimited(ctx *gin.Context) *autoReviewUnlimitedHandler {
	return &autoReviewUnlimitedHandler{
		ctx:  ctx,
		req:  http_model.NewAutoReviewUnlimitedRequest(),
		resp: http_model.NewAutoReviewUnlimitedResponse(),
	}
}

type autoReviewUnlimitedHandler struct {
	req  *http_model.AutoReviewUnlimitedRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (a autoReviewUnlimitedHandler) getContext() *gin.Context {
	return a.ctx
}

func (a autoReviewUnlimitedHandler) getResponse() interface{} {
	return a.resp
}

func (a autoReviewUnlimitedHandler) getRequest() interface{} {
	return a.req
}

func (a autoReviewUnlimitedHandler) run() {
	time := a.req.Time
	err := db.UpdateAutoTaskTime(a.ctx, time, 4)
	if err != nil {
		logrus.WithContext(a.ctx).Errorf("[autoReviewUnlimitedHandler] error autoReviewUnlimited, err:%+v", err)
		util.HandlerPackErrorResp(a.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	a.resp.Message = "已启动不限形式的审稿自动处理服务"
}

func (a autoReviewUnlimitedHandler) checkParam() error {
	return nil
}
