package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDataOtherTimeOutHandler(ctx *gin.Context) {
	handler := newDataOtherTimeOutHandler(ctx)
	BaseRun(handler)
}

type DataOtherTimeOutHandler struct {
	ctx  *gin.Context
	req  *http_model.DataOtherTimeOutRequest
	resp *http_model.CommonResponse
}

func (d DataOtherTimeOutHandler) getContext() *gin.Context {
	return d.ctx
}

func (d DataOtherTimeOutHandler) getResponse() interface{} {
	return d.resp
}

func (d DataOtherTimeOutHandler) getRequest() interface{} {
	return d.req
}

func (d DataOtherTimeOutHandler) run() {
	rate := d.req.Rate
	err := db.UpdateAutoDefaultRate(d.ctx, rate, 16)
	if err != nil {
		logrus.WithContext(d.ctx).Errorf("[DataOtherNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	d.resp.Message = "已更新自报价、固定稿费类型的数据违约超时未上传的扣款比率"
}

func (d DataOtherTimeOutHandler) checkParam() error {
	return nil
}

func newDataOtherTimeOutHandler(ctx *gin.Context) *DataOtherTimeOutHandler {
	return &DataOtherTimeOutHandler{
		ctx:  ctx,
		req:  http_model.NewDataOtherTimeOutRequest(),
		resp: http_model.NewDataOtherTimeOutResponse(),
	}
}
