package operate

import (
	"errors"
	"fmt"
	"github.com/caixw/lib.go/conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapSearchPricingHandler(ctx *gin.Context) {
	handler := newSearchPricing(ctx)
	BaseRun(handler)
}

type SearchPricingHandler struct {
	req  *http_model.SearchPricingRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func newSearchPricing(ctx *gin.Context) *SearchPricingHandler {
	return &SearchPricingHandler{
		req:  http_model.NewSearchPricingRequest(),
		resp: http_model.NewSearchPricingResponse(),
		ctx:  ctx,
	}
}

func (s SearchPricingHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SearchPricingHandler) getResponse() interface{} {
	return s.resp
}

func (s SearchPricingHandler) getRequest() interface{} {
	return s.req
}

func (s SearchPricingHandler) run() {
	conditions := pack.HttpPricingRequestToConditions(s.req)
	data, err := service.Operate.SearchPricing(s.ctx, s.req.PageSize, s.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[SearchPricingHandler] error SearchPricing , err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Data = data
}

func (s SearchPricingHandler) checkParam() error {
	var errs []error
	if s.req.PageNum < 0 || s.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	s.req.PageNum--
	s.req.Platform = util.IsNull(s.req.Platform)
	if _, err := conv.Int64(s.req.Platform); err != nil {
		errs = append(errs, err)
	}
	s.req.FeeForm = util.IsNull(s.req.FeeForm)
	if _, err := conv.Int64(s.req.FeeForm); err != nil {
		errs = append(errs, err)
	}
	s.req.ProjectType = util.IsNull(s.req.ProjectType)
	if _, err := conv.Int64(s.req.ProjectType); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
