package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapSketchOtherTimeOutHandler(ctx *gin.Context) {
	handler := newSketchOtherTimeOutHandler(ctx)
	BaseRun(handler)
}

type SketchOtherTimeOutHandler struct {
	ctx  *gin.Context
	req  *http_model.SketchOtherTimeOutRequest
	resp *http_model.CommonResponse
}

func (s SketchOtherTimeOutHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SketchOtherTimeOutHandler) getResponse() interface{} {
	return s.resp
}

func (s SketchOtherTimeOutHandler) getRequest() interface{} {
	return s.req
}

func (s SketchOtherTimeOutHandler) run() {
	rate := s.req.Rate
	err := db.UpdateAutoDefaultRate(s.ctx, rate, 4)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[SketchOtherTimeOutHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已更新自报价、固定稿费类型的初稿违约未上传的扣款比率"
}

func (s SketchOtherTimeOutHandler) checkParam() error {
	return nil
}

func newSketchOtherTimeOutHandler(ctx *gin.Context) *SketchOtherTimeOutHandler {
	return &SketchOtherTimeOutHandler{
		ctx:  ctx,
		req:  http_model.NewSketchOtherTimeOutRequest(),
		resp: http_model.NewSketchOtherTimeOutResponse(),
	}
}
