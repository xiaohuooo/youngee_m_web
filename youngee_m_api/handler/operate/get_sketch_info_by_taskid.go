package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetSketchInfoByTaskIdHandler(ctx *gin.Context) {
	handler := newGetSketchInfoByTaskId(ctx)
	BaseRun(handler)
}

type GetSketchInfoByTaskId struct {
	ctx  *gin.Context
	req  *http_model.GetSketchInfoByTaskIdRequest
	resp *http_model.CommonResponse
}

func (g GetSketchInfoByTaskId) getContext() *gin.Context {
	return g.ctx
}

func (g GetSketchInfoByTaskId) getResponse() interface{} {
	return g.resp
}

func (g GetSketchInfoByTaskId) getRequest() interface{} {
	return g.req
}

func (g GetSketchInfoByTaskId) run() {
	data, err := db.GetSketchInfoByTaskId(g.ctx, g.req)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[caseCloseDefaultHandler] error AutoCaseCloseDefault, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetSketchInfoByTaskId) checkParam() error {
	return nil
}

func newGetSketchInfoByTaskId(ctx *gin.Context) *GetSketchInfoByTaskId {
	return &GetSketchInfoByTaskId{
		ctx:  ctx,
		req:  http_model.NewGetSketchInfoByTaskIdRequest(),
		resp: http_model.NewGetSketchInfoByTaskIdResponse(),
	}
}
