package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapSketchReplaceTimeOutHandler(ctx *gin.Context) {
	handler := newSketchReplaceTimeOutHandler(ctx)
	BaseRun(handler)
}

type SketchReplaceTimeOutHandler struct {
	ctx  *gin.Context
	req  *http_model.SketchReplaceTimeOutRequest
	resp *http_model.CommonResponse
}

func (s SketchReplaceTimeOutHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SketchReplaceTimeOutHandler) getResponse() interface{} {
	return s.resp
}

func (s SketchReplaceTimeOutHandler) getRequest() interface{} {
	return s.req
}

func (s SketchReplaceTimeOutHandler) run() {
	rate := s.req.Rate
	err := db.UpdateAutoDefaultRate(s.ctx, rate, 2)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[SketchReplaceNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已更新产品置换类型的初稿超时未上传的扣款比率"
}

func (s SketchReplaceTimeOutHandler) checkParam() error {
	return nil
}

func newSketchReplaceTimeOutHandler(ctx *gin.Context) *SketchReplaceTimeOutHandler {
	return &SketchReplaceTimeOutHandler{
		ctx:  ctx,
		req:  http_model.NewSketchReplaceTimeOutRequest(),
		resp: http_model.NewSketchReplaceTimeOutResponse(),
	}
}
