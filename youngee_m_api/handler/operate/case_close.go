package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapCaseCloseHandler(ctx *gin.Context) {
	handler := newCaseCloseHandler(ctx)
	BaseRun(handler)
}

type caseCloseHandler struct {
	req  *http_model.CaseCloseRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (c caseCloseHandler) getContext() *gin.Context {
	return c.ctx
}

func (c caseCloseHandler) getResponse() interface{} {
	return c.resp
}

func (c caseCloseHandler) getRequest() interface{} {
	return c.req
}

func (c caseCloseHandler) run() {
	time := c.req.Time
	err := db.UpdateAutoTaskTime(c.ctx, time, 6)
	if err != nil {
		logrus.WithContext(c.ctx).Errorf("[caseCloseHandler] error caseClose, err:%+v", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	c.resp.Message = "已启动结案自动处理服务"
}

func (c caseCloseHandler) checkParam() error {
	return nil
}

func newCaseCloseHandler(ctx *gin.Context) *caseCloseHandler {
	return &caseCloseHandler{
		ctx:  ctx,
		req:  http_model.NewCaseCloseRequest(),
		resp: http_model.NewCaseCloseResponse(),
	}
}
