package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDataOtherNotUploadHandler(ctx *gin.Context) {
	handler := newDataOtherNotUploadHandler(ctx)
	BaseRun(handler)
}

type DataOtherNotUploadHandler struct {
	ctx  *gin.Context
	req  *http_model.DataOtherNotUploadRequest
	resp *http_model.CommonResponse
}

func (d DataOtherNotUploadHandler) getContext() *gin.Context {
	return d.ctx
}

func (d DataOtherNotUploadHandler) getResponse() interface{} {
	return d.resp
}

func (d DataOtherNotUploadHandler) getRequest() interface{} {
	return d.req
}

func (d DataOtherNotUploadHandler) run() {
	rate := d.req.Rate
	err := db.UpdateAutoDefaultRate(d.ctx, rate, 15)
	if err != nil {
		logrus.WithContext(d.ctx).Errorf("[DataOtherNotUploadHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	d.resp.Message = "已更新自报价、固定稿费类型的数据违约未上传的扣款比率"
}

func (d DataOtherNotUploadHandler) checkParam() error {
	return nil
}

func newDataOtherNotUploadHandler(ctx *gin.Context) *DataOtherNotUploadHandler {
	return &DataOtherNotUploadHandler{
		ctx:  ctx,
		req:  http_model.NewDataOtherNotUploadRequest(),
		resp: http_model.NewDataOtherNotUploadResponse(),
	}
}
