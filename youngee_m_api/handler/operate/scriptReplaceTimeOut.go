package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapScriptReplaceTimeOutHandler(ctx *gin.Context) {
	handler := newScriptReplaceTimeOutHandler(ctx)
	BaseRun(handler)
}

type ScriptReplaceTimeOutHandler struct {
	ctx  *gin.Context
	req  *http_model.ScriptReplaceTimeOutRequest
	resp *http_model.CommonResponse
}

func (s ScriptReplaceTimeOutHandler) getContext() *gin.Context {
	return s.ctx
}

func (s ScriptReplaceTimeOutHandler) getResponse() interface{} {
	return s.resp
}

func (s ScriptReplaceTimeOutHandler) getRequest() interface{} {
	return s.req
}

func (s ScriptReplaceTimeOutHandler) run() {
	rate := s.req.Rate
	err := db.UpdateAutoDefaultRate(s.ctx, rate, 6)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[ScriptReplaceTimeOutHandler] error UpdateAutoDefaultRate, err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Message = "已更新产品置换类型的脚本违约超时未上传的扣款比率"
}

func (s ScriptReplaceTimeOutHandler) checkParam() error {
	return nil
}

func newScriptReplaceTimeOutHandler(ctx *gin.Context) *ScriptReplaceTimeOutHandler {
	return &ScriptReplaceTimeOutHandler{
		ctx:  ctx,
		req:  http_model.NewScriptReplaceTimeOutRequest(),
		resp: http_model.NewScriptReplaceTimeOutResponse(),
	}
}
