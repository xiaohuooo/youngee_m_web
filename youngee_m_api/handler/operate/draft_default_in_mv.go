package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDraftDefaultInMvHandler(ctx *gin.Context) {
	handler := newDraftDefaultInMvHandler(ctx)
	BaseRun(handler)
}

type draftDefaultInMv struct {
	req  *http_model.DraftDefaultInMvRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (d draftDefaultInMv) getContext() *gin.Context {
	return d.ctx
}

func (d draftDefaultInMv) getResponse() interface{} {
	return d.resp
}

func (d draftDefaultInMv) getRequest() interface{} {
	return d.req
}

func (d draftDefaultInMv) run() {
	time := d.req.Time
	err := db.UpdateAutoTaskTime(d.ctx, time, 9)
	if err != nil {
		logrus.WithContext(d.ctx).Errorf("[draftDefaultInMv] error draftDefaultInMv, err:%+v", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	d.resp.Message = "已启动视频初稿违约自动处理服务"
}

func (d draftDefaultInMv) checkParam() error {
	return nil
}

func newDraftDefaultInMvHandler(ctx *gin.Context) *draftDefaultInMv {
	return &draftDefaultInMv{
		ctx:  ctx,
		req:  http_model.NewDraftDefaultInMvRequest(),
		resp: http_model.NewDraftDefaultInMvResponse(),
	}
}
