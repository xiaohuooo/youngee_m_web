package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapPostReviewHandler(ctx *gin.Context) {
	handler := newPostReviewHandler(ctx)
	BaseRun(handler)
}

type postReviewHandler struct {
	req  *http_model.PostReviewRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (p postReviewHandler) getContext() *gin.Context {
	return p.ctx
}

func (p postReviewHandler) getResponse() interface{} {
	return p.resp
}

func (p postReviewHandler) getRequest() interface{} {
	return p.req
}

func (p postReviewHandler) run() {
	time := p.req.Time
	err := db.UpdateAutoTaskTime(p.ctx, time, 5)
	if err != nil {
		logrus.WithContext(p.ctx).Errorf("[postReviewHandler] error postReview, err:%+v", err)
		util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	p.resp.Message = "已启动发布审核自动处理服务"
}

func (p postReviewHandler) checkParam() error {
	return nil
}

func newPostReviewHandler(ctx *gin.Context) *postReviewHandler {
	return &postReviewHandler{
		ctx:  ctx,
		req:  http_model.NewPostReviewRequest(),
		resp: http_model.NewPostReviewResponse(),
	}
}
