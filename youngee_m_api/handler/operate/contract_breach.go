package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapContractBreachHandler(ctx *gin.Context) {
	handler := newContractBreachHandler(ctx)
	BaseRun(handler)
}

func newContractBreachHandler(ctx *gin.Context) *ContractBreachHandler {
	return &ContractBreachHandler{
		ctx:  ctx,
		req:  http_model.NewContractBreachRequest(),
		resp: http_model.NewContractBreachResponse(),
	}
}

type ContractBreachHandler struct {
	ctx  *gin.Context
	req  *http_model.ContractBreachRequest
	resp *http_model.CommonResponse
}

func (c ContractBreachHandler) getContext() *gin.Context {
	return c.ctx
}

func (c ContractBreachHandler) getResponse() interface{} {
	return c.resp
}

func (c ContractBreachHandler) getRequest() interface{} {
	return c.req
}

func (c ContractBreachHandler) run() {
	err := db.ContractBreach(c.ctx, c.req)
	if err != nil {
		logrus.WithContext(c.ctx).Errorf("[ContractBreachHandler] error ContractBreach, err:%+v", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	c.resp.Message = "违约记录处理成功"
}

func (c ContractBreachHandler) checkParam() error {
	return nil
}
