package operate

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetPercentsHandler(ctx *gin.Context) {
	handler := newGetPercentsHandler(ctx)
	BaseRun(handler)
}

type GetPercentHandler struct {
	ctx  *gin.Context
	req  *http_model.GetPercentsRequest
	resp *http_model.CommonResponse
}

func (g GetPercentHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetPercentHandler) getResponse() interface{} {
	return g.resp
}

func (g GetPercentHandler) getRequest() interface{} {
	return g.req
}

func (g GetPercentHandler) run() {
	data, err := db.GetPercents(g.ctx)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetPercentHandler] error GetPercents, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetPercentHandler) checkParam() error {
	return nil
}

func newGetPercentsHandler(ctx *gin.Context) *GetPercentHandler {
	return &GetPercentHandler{
		ctx:  ctx,
		req:  http_model.NewGetPercentsRequest(),
		resp: http_model.NewGetPercentsResponse(),
	}
}
