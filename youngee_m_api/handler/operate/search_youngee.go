package operate

import (
	"errors"
	"fmt"
	"github.com/caixw/lib.go/conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapSearchYoungeeHandler(ctx *gin.Context) {
	handler := newSearchYoungee(ctx)
	BaseRun(handler)
}

type SearchYoungeeHandler struct {
	req  *http_model.SearchYoungeeRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func newSearchYoungee(ctx *gin.Context) *SearchYoungeeHandler {
	return &SearchYoungeeHandler{
		req:  http_model.NewSearchYoungeeRequest(),
		resp: http_model.NewSearchYoungeeResponse(),
		ctx:  ctx,
	}
}

func (s SearchYoungeeHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SearchYoungeeHandler) getResponse() interface{} {
	return s.resp
}

func (s SearchYoungeeHandler) getRequest() interface{} {
	return s.req
}

func (s SearchYoungeeHandler) run() {
	conditions := pack.HttpYoungeeRequestToConditions(s.req)
	fmt.Println(conditions)
	data, err := service.Operate.SearchYoungee(s.ctx, s.req.PageSize, s.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(s.ctx).Errorf("[SearchYoungeeHandler] error SearchYoungee , err:%+v", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	s.resp.Data = data
}

func (s SearchYoungeeHandler) checkParam() error {
	var errs []error
	if s.req.PageNum < 0 || s.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	s.req.PageNum--
	s.req.Platform = util.IsNull(s.req.Platform)
	if _, err := conv.Int64(s.req.Platform); err != nil {
		errs = append(errs, err)
	}
	s.req.TaskType = util.IsNull(s.req.TaskType)
	if _, err := conv.Int64(s.req.TaskType); err != nil {
		errs = append(errs, err)
	}
	s.req.ProjectType = util.IsNull(s.req.ProjectType)
	if _, err := conv.Int64(s.req.ProjectType); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
