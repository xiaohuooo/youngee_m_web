package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapLinkOpinionHandler(ctx *gin.Context) {
	handler := newLinkOpinionHandler(ctx)
	BaseRun(handler)
}

func newLinkOpinionHandler(ctx *gin.Context) *LinkOpinionHandler {
	return &LinkOpinionHandler{
		req:  http_model.NewLinkOpinionRequest(),
		resp: http_model.NewLinkOpinionResponse(),
		ctx:  ctx,
	}
}

type LinkOpinionHandler struct {
	req  *http_model.LinkOpinionRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *LinkOpinionHandler) getRequest() interface{} {
	return h.req
}
func (h *LinkOpinionHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *LinkOpinionHandler) getResponse() interface{} {
	return h.resp
}

func (h *LinkOpinionHandler) run() {
	data := http_model.LinkOpinionRequest{}
	data = *h.req
	res, err := service.Link.LinkOpinion(h.ctx, data)
	if err != nil {
		logrus.Errorf("[LinkOpinionHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "提交脚本修改意见成功"
	h.resp.Data = res
	h.resp.Data = data
}

/***
func (h *LinkOpinionHandler) run() {
	data := http_model.LinkOpinionRequest{}
	data = *h.req
	isRefuse := data.IsRefuse
	if isRefuse== 0 {
		fmt.Println("Create in")
		res, err := service.Project.Create(h.ctx, data)
		if err != nil {
			logrus.Errorf("[LinkOpinionHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功添加修改意见"
		h.resp.Data = res
	} else {
		res, err := service.Logistics.Update(h.ctx, data)
		if err != nil {
			logrus.Errorf("[LinkOpinionHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功修改物流信息"
		h.resp.Data = res
	}

}
***/
func (h *LinkOpinionHandler) checkParam() error {
	return nil
}
