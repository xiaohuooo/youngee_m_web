package handler

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapEnterpriseUserHandler(ctx *gin.Context) {
	handler := newEnterpriseUser(ctx)
	BaseRun(handler)
}

func newEnterpriseUser(ctx *gin.Context) *EnterpriseUser {
	return &EnterpriseUser{
		req:  http_model.NewEnterpriseUserRequest(),
		resp: http_model.NewEnterpriseUserResponse(),
		ctx:  ctx,
	}
}

type EnterpriseUser struct {
	req  *http_model.EnterpriseUserRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (e EnterpriseUser) getContext() *gin.Context {
	return e.ctx
}

func (e EnterpriseUser) getResponse() interface{} {
	return e.resp
}

func (e EnterpriseUser) getRequest() interface{} {
	return e.req
}

func (e EnterpriseUser) run() {
	condition := pack.HttpEnterpriseUserRequestToCondition(e.req)
	data, err := service.User.EnterpriseUserList(e.ctx, e.req.PageSize, e.req.PageNum, condition)
	if err != nil {
		logrus.WithContext(e.ctx).Errorf("[EnterpriseUserHandler] error EnterpriseUserList, err:%+v", err)
		util.HandlerPackErrorResp(e.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	e.resp.Data = data
}

func (e EnterpriseUser) checkParam() error {
	var errs []error
	if e.req.PageNum < 0 || e.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	e.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("EnterpriseUser check param errs:%+v", errs)
	}
	return nil
}
