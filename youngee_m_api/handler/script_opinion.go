package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapScriptOpinionHandler(ctx *gin.Context) {
	handler := newScriptOpinionHandler(ctx)
	BaseRun(handler)
}

func newScriptOpinionHandler(ctx *gin.Context) *ScriptOpinionHandler {
	return &ScriptOpinionHandler{
		req:  http_model.NewScriptOpinionRequest(),
		resp: http_model.NewScriptOpinionResponse(),
		ctx:  ctx,
	}
}

type ScriptOpinionHandler struct {
	req  *http_model.ScriptOpinionRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *ScriptOpinionHandler) getRequest() interface{} {
	return h.req
}
func (h *ScriptOpinionHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *ScriptOpinionHandler) getResponse() interface{} {
	return h.resp
}

func (h *ScriptOpinionHandler) run() {
	data := http_model.ScriptOpinionRequest{}
	data = *h.req
	res, err := service.Script.ScriptOpinion(h.ctx, data)
	if err != nil {
		logrus.Errorf("[ScriptOpinionHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "提交脚本修改意见成功"
	h.resp.Data = res
	h.resp.Data = data
}

/***
func (h *ScriptOpinionHandler) run() {
	data := http_model.ScriptOpinionRequest{}
	data = *h.req
	isRefuse := data.IsRefuse
	if isRefuse== 0 {
		fmt.Println("Create in")
		res, err := service.Project.Create(h.ctx, data)
		if err != nil {
			logrus.Errorf("[ScriptOpinionHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功添加修改意见"
		h.resp.Data = res
	} else {
		res, err := service.Logistics.Update(h.ctx, data)
		if err != nil {
			logrus.Errorf("[ScriptOpinionHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功修改物流信息"
		h.resp.Data = res
	}

}
***/
func (h *ScriptOpinionHandler) checkParam() error {
	return nil
}
