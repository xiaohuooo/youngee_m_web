package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetSpecialFinishDataHandler(ctx *gin.Context) {
	handler := newGetSpecialFinishDataHandler(ctx)
	BaseRun(handler)
}

func newGetSpecialFinishDataHandler(ctx *gin.Context) *GetSpecialFinishDataHandler {
	return &GetSpecialFinishDataHandler{
		req:  http_model.NewGetSpecialFinishDataRequest(),
		resp: http_model.NewGetSpecialFinishDataResponse(),
		ctx:  ctx,
	}
}

type GetSpecialFinishDataHandler struct {
	req  *http_model.GetSpecialFinishDataRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetSpecialFinishDataHandler) getRequest() interface{} {
	return h.req
}
func (h *GetSpecialFinishDataHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetSpecialFinishDataHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetSpecialFinishDataHandler) run() {
	data := http_model.GetSpecialFinishDataRequest{}
	data = *h.req
	res, err := service.Number.GetSpecialFinishData(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetSpecialFinishDataHandler] call GetSpecialFinishData err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetSpecialFinishData fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetSpecialFinishDataHandler) checkParam() error {
	return nil
}
