package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
)

func WrapCreateLogisticsHandler(ctx *gin.Context) {
	handler := newCreateLogisticsHandler(ctx)
	BaseRun(handler)
}

func newCreateLogisticsHandler(ctx *gin.Context) *CreateLogisticsHandler {
	return &CreateLogisticsHandler{
		req:  http_model.NewCreateLogisticsRequest(),
		resp: http_model.NewCreateLogisticsResponse(),
		ctx:  ctx,
	}
}

type CreateLogisticsHandler struct {
	req  *http_model.CreateLogisticsRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *CreateLogisticsHandler) getRequest() interface{} {
	return h.req
}

func (h *CreateLogisticsHandler) getContext() *gin.Context {
	return h.ctx
}

func (h *CreateLogisticsHandler) getResponse() interface{} {
	return h.resp
}

func (h *CreateLogisticsHandler) run() {
	data := http_model.CreateLogisticsRequest{}
	data = *h.req
	isUpdate := data.IsUpdate
	if isUpdate == 0 {
		res, err := service.Logistics.Create(h.ctx, data)
		if err != nil {
			logrus.Errorf("[CreateLogisticsHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功添加物流信息"
		h.resp.Data = res
	} else {
		res, err := service.Logistics.Update(h.ctx, data)
		if err != nil {
			logrus.Errorf("[CreateLogisticsHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功修改物流信息"
		h.resp.Data = res
	}
}

func (h *CreateLogisticsHandler) checkParam() error {
	return nil
}
