package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapCreateSpecialLogisticsHandler(ctx *gin.Context) {
	handler := newCreateSpecialLogisticsHandler(ctx)
	BaseRun(handler)
}

type CreateSpecialLogistics struct {
	ctx  *gin.Context
	req  *http_model.CreateSpecialLogisticsRequest
	resp *http_model.CommonResponse
}

func (c CreateSpecialLogistics) getContext() *gin.Context {
	return c.ctx
}

func (c CreateSpecialLogistics) getResponse() interface{} {
	return c.resp
}

func (c CreateSpecialLogistics) getRequest() interface{} {
	return c.req
}

func (c CreateSpecialLogistics) run() {
	data := http_model.CreateSpecialLogisticsRequest{}
	data = *c.req
	isUpdate := data.IsUpdate
	if isUpdate == 0 {
		res, err := service.Logistics.CreateSpecialLogistics(c.ctx, data)
		if err != nil {
			logrus.Errorf("[CreateSpecialLogistics] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
			logrus.Info("CreateSpecialLogistics fail,req:%+v", c.req)
			return
		}
		c.resp.Message = "成功添加物流信息"
		c.resp.Data = res
	} else {
		res, err := service.Logistics.UpdateSpecialLogistics(c.ctx, data)
		if err != nil {
			logrus.Errorf("[CreateSpecialLogistics] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
			logrus.Info("CreateSpecialLogistics fail,req:%+v", c.req)
			return
		}
		c.resp.Message = "成功修改物流信息"
		c.resp.Data = res
	}
}

func (c CreateSpecialLogistics) checkParam() error {
	return nil
}

func newCreateSpecialLogisticsHandler(ctx *gin.Context) *CreateSpecialLogistics {
	return &CreateSpecialLogistics{
		ctx:  ctx,
		req:  http_model.NewCreateSpecialLogisticsRequest(),
		resp: http_model.NewCreateSpecialLogisticsResponse(),
	}
}
