package handler

import (
	"errors"
	"fmt"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapSpecialTaskFinishDataListHandler(ctx *gin.Context) {
	handler := newSpecialTaskFinishDataListHandler(ctx)
	BaseRun(handler)
}

func newSpecialTaskFinishDataListHandler(ctx *gin.Context) *SpecialTaskFinishDataListHandler {
	return &SpecialTaskFinishDataListHandler{
		req:  http_model.NewSpecialTaskFinishDataListRequest(),
		resp: http_model.NewSpecialTaskFinishDataListResponse(),
		ctx:  ctx,
	}
}

type SpecialTaskFinishDataListHandler struct {
	req  *http_model.SpecialTaskFinishDataListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *SpecialTaskFinishDataListHandler) getRequest() interface{} {
	return h.req
}
func (h *SpecialTaskFinishDataListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *SpecialTaskFinishDataListHandler) getResponse() interface{} {
	return h.resp
}
func (h *SpecialTaskFinishDataListHandler) run() {
	conditions := pack.HttpSpecialTaskFinishDataListRequestToCondition(h.req)
	data, err := service.SpecialTask.GetSpecialTaskFinishDataList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *SpecialTaskFinishDataListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
