package handler

import (
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
)

func WrapSignForReceiptHandler(ctx *gin.Context) {
	handler := newSignForReceiptHandler(ctx)
	BaseRun(handler)
}

func newSignForReceiptHandler(ctx *gin.Context) *SignForReceiptHandler {
	return &SignForReceiptHandler{
		req:  http_model.NewSignForReceiptRequst(),
		resp: http_model.NewSignForReceiptResponse(),
		ctx:  ctx,
	}
}

type SignForReceiptHandler struct {
	req  *http_model.SignForReceiptRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (p *SignForReceiptHandler) getContext() *gin.Context {
	return p.ctx
}

func (p *SignForReceiptHandler) getResponse() interface{} {
	return p.resp
}

func (p *SignForReceiptHandler) getRequest() interface{} {
	return p.req
}

func (p *SignForReceiptHandler) run() {
	data := http_model.SignForReceiptRequest{}
	data = *p.req
	err := service.Logistics.SignForReceipt(p.ctx, data)
	if err != nil {
		logrus.Errorf("[ChangeLogisticsStatusHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
		logrus.Info("ChangeLogisticsStatus fail,req:%+v", p.req)
		return
	}
	p.resp.Message = "物流状态更换成功"
}

func (p *SignForReceiptHandler) checkParam() error {
	return nil
}
