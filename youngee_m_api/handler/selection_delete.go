package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDeleteSelectionHandler(ctx *gin.Context) {
	handler := NewDeleteSelection(ctx)
	BaseRun(handler)
}

type DeleteSelectionHandler struct {
	ctx  *gin.Context
	req  *http_model.DeleteSelectionRequest
	resp *http_model.CommonResponse
}

func (d DeleteSelectionHandler) getContext() *gin.Context {
	return d.ctx
}

func (d DeleteSelectionHandler) getResponse() interface{} {
	return d.resp
}

func (d DeleteSelectionHandler) getRequest() interface{} {
	return d.req
}

func (d DeleteSelectionHandler) run() {
	err := db.DeleteSelection(d.ctx, d.req.SelectionId)
	if err != nil {
		logrus.WithContext(d.ctx).Errorf("[DeleteSelectionHandler] error DeleteSelection, err:%+v", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	d.resp.Message = "选品删除成功"
}

func (d DeleteSelectionHandler) checkParam() error {
	return nil
}

func NewDeleteSelection(ctx *gin.Context) *DeleteSelectionHandler {
	return &DeleteSelectionHandler{
		ctx:  ctx,
		req:  http_model.NewDeleteSelectionRequest(),
		resp: http_model.NewDeleteSelectionResponse(),
	}
}
