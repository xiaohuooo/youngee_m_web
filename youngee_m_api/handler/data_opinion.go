package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapDataOpinionHandler(ctx *gin.Context) {
	handler := newDataOpinionHandler(ctx)
	BaseRun(handler)
}

func newDataOpinionHandler(ctx *gin.Context) *DataOpinionHandler {
	return &DataOpinionHandler{
		req:  http_model.NewDataOpinionRequest(),
		resp: http_model.NewDataOpinionResponse(),
		ctx:  ctx,
	}
}

type DataOpinionHandler struct {
	req  *http_model.DataOpinionRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *DataOpinionHandler) getRequest() interface{} {
	return h.req
}
func (h *DataOpinionHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *DataOpinionHandler) getResponse() interface{} {
	return h.resp
}

func (h *DataOpinionHandler) run() {
	data := http_model.DataOpinionRequest{}
	data = *h.req
	res, err := service.Data.DataOpinion(h.ctx, data)
	if err != nil {
		logrus.Errorf("[DataOpinionHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "提交数据审核意见成功"
	h.resp.Data = res
	h.resp.Data = data
}

/***
func (h *DataOpinionHandler) run() {
	data := http_model.DataOpinionRequest{}
	data = *h.req
	isRefuse := data.IsRefuse
	if isRefuse== 0 {
		fmt.Println("Create in")
		res, err := service.Project.Create(h.ctx, data)
		if err != nil {
			logrus.Errorf("[DataOpinionHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功添加修改意见"
		h.resp.Data = res
	} else {
		res, err := service.Logistics.Update(h.ctx, data)
		if err != nil {
			logrus.Errorf("[DataOpinionHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功修改物流信息"
		h.resp.Data = res
	}

}
***/
func (h *DataOpinionHandler) checkParam() error {
	return nil
}
