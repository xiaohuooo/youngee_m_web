package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapUpdateUserInfoHandler(ctx *gin.Context) {
	handler := newUpdateUserInfoHandler(ctx)
	BaseRun(handler)
}

func newUpdateUserInfoHandler(ctx *gin.Context) *UpdateUserInfoHandler {
	return &UpdateUserInfoHandler{
		req:  http_model.NewUpdateUserInfoRequest(),
		resp: http_model.NewUpdateUserInfoResponse(),
		ctx:  ctx,
	}
}

type UpdateUserInfoHandler struct {
	req  *http_model.UpdateUserInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (u UpdateUserInfoHandler) getContext() *gin.Context {
	return u.ctx
}

func (u UpdateUserInfoHandler) getResponse() interface{} {
	return u.resp
}

func (u UpdateUserInfoHandler) getRequest() interface{} {
	return u.req
}

func (u UpdateUserInfoHandler) run() {
	toast, err := db.UpdateUserInfo(u.ctx, u.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[UpdateUserInfoHandler] call UpdateUserInfo err:%+v\n", err)
		util.HandlerPackErrorResp(u.resp, consts.ErrorInternal, "")
		logrus.Info("UpdateUserInfoHandler fail,req:%+v", u.req)
		return
	}
	if toast == "更新成功" {
		u.resp.Message = "更新成功"
	} else {
		u.resp.Message = "更新失败"
	}
}

func (u UpdateUserInfoHandler) checkParam() error {
	return nil
}
