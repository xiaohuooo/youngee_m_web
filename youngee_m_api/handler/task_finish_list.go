package handler

import (
	"errors"
	"fmt"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapTaskFinishListHandler(ctx *gin.Context) {
	handler := newTaskFinishListHandler(ctx)
	BaseRun(handler)
}

func newTaskFinishListHandler(ctx *gin.Context) *TaskFinishListHandler {
	return &TaskFinishListHandler{
		req:  http_model.NewTaskFinishListRequest(),
		resp: http_model.NewTaskFinishListResponse(),
		ctx:  ctx,
	}
}

type TaskFinishListHandler struct {
	req  *http_model.TaskFinishListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *TaskFinishListHandler) getRequest() interface{} {
	return h.req
}
func (h *TaskFinishListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *TaskFinishListHandler) getResponse() interface{} {
	return h.resp
}
func (h *TaskFinishListHandler) run() {
	conditions := pack.HttpTaskFinishListRequestToCondition(h.req)
	data, err := service.Project.GetTaskFinishList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *TaskFinishListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
