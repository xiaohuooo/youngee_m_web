package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetWithdrawalRecordHandler(ctx *gin.Context) {
	handler := newGetWithdrawalRecordHandler(ctx)
	BaseRun(handler)
}

type GetWithdrawalRecordHandler struct {
	ctx  *gin.Context
	req  *http_model.GetWithdrawalRecordRequest
	resp *http_model.CommonResponse
}

func (g GetWithdrawalRecordHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetWithdrawalRecordHandler) getResponse() interface{} {
	return g.resp
}

func (g GetWithdrawalRecordHandler) getRequest() interface{} {
	return g.req
}

func (g GetWithdrawalRecordHandler) run() {
	//fmt.Println("talentId1:", g.req.TalentId)
	data, err := db.GetWithdrawRecord(g.ctx, g.req)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetWithdrawalRecordHandler] error GetWithdrawRecord, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetWithdrawalRecordHandler) checkParam() error {
	return nil
}

func newGetWithdrawalRecordHandler(ctx *gin.Context) *GetWithdrawalRecordHandler {
	return &GetWithdrawalRecordHandler{
		ctx:  ctx,
		req:  http_model.NewGetWithdrawalRecordRequest(),
		resp: http_model.NewGetWithdrawRecordRequest(),
	}
}
