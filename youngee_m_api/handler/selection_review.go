package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapReviewSelectionHandler(ctx *gin.Context) {
	handler := newReviewSelectionHandler(ctx)
	BaseRun(handler)
}

type ReviewSelection struct {
	ctx  *gin.Context
	req  *http_model.ReviewSelectionRequest
	resp *http_model.CommonResponse
}

func (c ReviewSelection) getContext() *gin.Context {
	return c.ctx
}

func (c ReviewSelection) getResponse() interface{} {
	return c.resp
}

func (c ReviewSelection) getRequest() interface{} {
	return c.req
}

func (c ReviewSelection) run() {
	data := http_model.ReviewSelectionRequest{}
	data = *c.req
	res, err := service.Selection.Review(c.ctx, data)
	if err != nil {
		logrus.Errorf("[ReviewSelection] call ReviewSelection err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("ReviewSelection fail,req:%+v", c.req)
		return
	}
	if data.IsPass == 1 {
		c.resp.Message = "审核成功"
	}
	c.resp.Data = res
}

func (c ReviewSelection) checkParam() error {
	return nil
}

func newReviewSelectionHandler(ctx *gin.Context) *ReviewSelection {
	return &ReviewSelection{
		ctx:  ctx,
		req:  http_model.NewReviewSelectionRequest(),
		resp: http_model.NewReviewSelectionResponse(),
	}
}
