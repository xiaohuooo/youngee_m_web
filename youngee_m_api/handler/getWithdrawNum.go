package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetWithdrawNumsHandler(ctx *gin.Context) {
	handler := newGetWithdrawNumsHandler(ctx)
	BaseRun(handler)
}

type GetWithdrawNumsHandler struct {
	ctx  *gin.Context
	req  *http_model.GetWithdrawNumsRequest
	resp *http_model.CommonResponse
}

func (g GetWithdrawNumsHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetWithdrawNumsHandler) getResponse() interface{} {
	return g.resp
}

func (g GetWithdrawNumsHandler) getRequest() interface{} {
	return g.req
}

func (g GetWithdrawNumsHandler) run() {
	data, err := db.GetWithdrawNums(g.ctx)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetWithdrawNumsHandler] error GetWithdrawNums, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetWithdrawNumsHandler) checkParam() error {
	return nil
}

func newGetWithdrawNumsHandler(ctx *gin.Context) *GetWithdrawNumsHandler {
	return &GetWithdrawNumsHandler{
		ctx:  ctx,
		req:  http_model.NewGetWithdrawNumsRequest(),
		resp: http_model.NewGetWithdrawNumsResponse(),
	}
}
