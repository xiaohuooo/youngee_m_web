package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapTalentInfoHandler(ctx *gin.Context) {
	handler := newTalentInfo(ctx)
	BaseRun(handler)
}

func newTalentInfo(ctx *gin.Context) *TalentInfo {
	return &TalentInfo{
		req:  http_model.NewTalentInfoRequest(),
		resp: http_model.NewTalentInfoResponse(),
		ctx:  ctx,
	}
}

type TalentInfo struct {
	req  *http_model.TalentInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (a TalentInfo) getContext() *gin.Context {
	return a.ctx
}

func (a TalentInfo) getResponse() interface{} {
	return a.resp
}

func (a TalentInfo) getRequest() interface{} {
	return a.req
}

func (a TalentInfo) run() {
	data, err := db.AccountIncome(a.ctx, a.req.TalentId)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[AccountIncomeHandler] call AccountIncome err:%+v\n", err)
		util.HandlerPackErrorResp(a.resp, consts.ErrorInternal, "")
		logrus.Info("AccountIncomeHandler fail,req:%+v", a.req)
		return
	}
	a.resp.Data = data
}

func (a TalentInfo) checkParam() error {
	return nil
}
