package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

// WrapGetSketchInfoHandler

func WrapGetSketchInfoHandler(ctx *gin.Context) {
	handler := newGetSketchInfoHandler(ctx)
	BaseRun(handler)
}

func newGetSketchInfoHandler(ctx *gin.Context) *GetSketchInfoHandler {
	return &GetSketchInfoHandler{
		req:  http_model.NewGetSketchInfoRequest(),
		resp: http_model.NewGetSketchInfoResponse(),
		ctx:  ctx,
	}
}

type GetSketchInfoHandler struct {
	req  *http_model.GetSketchInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetSketchInfoHandler) getRequest() interface{} {
	return h.req
}
func (h *GetSketchInfoHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetSketchInfoHandler) getResponse() interface{} {
	return h.resp
}
func (h *GetSketchInfoHandler) run() {
	data := *&http_model.GetSketchInfoRequest{}
	data = *h.req
	res, err := service.Sketch.GetSketchInfo(h.ctx, data)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[GetSketchInfoHandler] call GetByID err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetProduct fail,req:%+v", h.req)
		return
	}
	h.resp.Data = res
}
func (h *GetSketchInfoHandler) checkParam() error {
	return nil
}
