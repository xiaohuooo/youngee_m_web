package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetInvoiceNumsHandler(ctx *gin.Context) {
	handler := newGetInvoiceNumsHandler(ctx)
	BaseRun(handler)
}

type GetInvoiceNumsHandler struct {
	ctx  *gin.Context
	req  *http_model.GetInvoiceNumsRequest
	resp *http_model.CommonResponse
}

func (g GetInvoiceNumsHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetInvoiceNumsHandler) getResponse() interface{} {
	return g.resp
}

func (g GetInvoiceNumsHandler) getRequest() interface{} {
	return g.req
}

func (g GetInvoiceNumsHandler) run() {
	data, err := db.GetInvoiceNums(g.ctx)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetInvoiceNumsHandler] error GetInvoiceNums, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetInvoiceNumsHandler) checkParam() error {
	return nil
}

func newGetInvoiceNumsHandler(ctx *gin.Context) *GetInvoiceNumsHandler {
	return &GetInvoiceNumsHandler{
		ctx:  ctx,
		req:  http_model.NewGetInvoiceNumsRequest(),
		resp: http_model.NewGetInvoiceNumsResponse(),
	}
}
