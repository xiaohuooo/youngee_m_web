package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
)

func WrapGetLoginUserHandler(ctx *gin.Context) {
	handler := newGetLoginUserHandler(ctx)
	BaseRun(handler)
}

func newGetLoginUserHandler(ctx *gin.Context) *GetLoginUserHandler {
	return &GetLoginUserHandler{
		req:  http_model.NewGetLoginUserRequest(),
		resp: http_model.NewGetLoginUserResponse(),
		ctx:  ctx,
	}
}

type GetLoginUserHandler struct {
	req  *http_model.GetLoginUserRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetLoginUserHandler) getRequest() interface{} {
	return h.req
}
func (h *GetLoginUserHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetLoginUserHandler) getResponse() interface{} {
	return h.resp
}
func (h *GetLoginUserHandler) run() {
	user, err := db.GetAllUser(h.ctx)
	if err != nil {
		logrus.Errorf("[GetLoginUserHandler] get LoginUser err:%+v\n", err)
		logrus.Info("GetLoginUser fail,req:%+v", h.req)
		return
	}
	data := http_model.GetLoginUserList{}
	data.User = user
	h.resp.Data = data
}
func (h *GetLoginUserHandler) checkParam() error {
	return nil
}
