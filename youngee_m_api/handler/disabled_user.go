package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDisabledUserHandler(ctx *gin.Context) {
	handler := newDisabledUserHandler(ctx)
	BaseRun(handler)
}

func newDisabledUserHandler(ctx *gin.Context) *DisabledUserHandler {
	return &DisabledUserHandler{
		req:  http_model.NewDisabledUserRequest(),
		resp: http_model.NewDisabledUserResponse(),
		ctx:  ctx,
	}
}

type DisabledUserHandler struct {
	req  *http_model.DisabledUserRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (d DisabledUserHandler) getContext() *gin.Context {
	return d.ctx
}

func (d DisabledUserHandler) getResponse() interface{} {
	return d.resp
}

func (d DisabledUserHandler) getRequest() interface{} {
	return d.req
}

func (d DisabledUserHandler) run() {
	toast, err := db.DisabledUser(d.ctx, d.req.User)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[DisabledUserHandler] call DisabledUser err:%+v\n", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, "")
		logrus.Info("DisabledUserHandler fail,req:%+v", d.req)
		return
	}
	d.resp.Message = toast
}

func (d DisabledUserHandler) checkParam() error {
	return nil
}
