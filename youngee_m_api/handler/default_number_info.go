package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetDefaultNumberInfoHandler(ctx *gin.Context) {
	handler := newGetDefaultNumberInfoHandler(ctx)
	BaseRun(handler)
}

func newGetDefaultNumberInfoHandler(ctx *gin.Context) *GetDefaultNumberInfoHandler {
	return &GetDefaultNumberInfoHandler{
		req:  http_model.NewGetDefaultNumberInfoRequest(),
		resp: http_model.NewGetDefaultNumberInfoResponse(),
		ctx:  ctx,
	}
}

type GetDefaultNumberInfoHandler struct {
	req  *http_model.GetDefaultNumberInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetDefaultNumberInfoHandler) getRequest() interface{} {
	return h.req
}
func (h *GetDefaultNumberInfoHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetDefaultNumberInfoHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetDefaultNumberInfoHandler) run() {
	data := http_model.GetDefaultNumberInfoRequest{}
	data = *h.req
	res, err := service.Number.GetDefaultNumberInfo(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetDefaultNumberInfoHandler] call GetDefaultNumberInfo err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetDefaultNumberInfo fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetDefaultNumberInfoHandler) checkParam() error {
	return nil
}
