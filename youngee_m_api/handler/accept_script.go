package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapAcceptScriptHandler(ctx *gin.Context) {
	handler := newAcceptScriptHandler(ctx)
	BaseRun(handler)
}

func newAcceptScriptHandler(ctx *gin.Context) *AcceptScriptHandler {
	return &AcceptScriptHandler{
		req:  http_model.NewAcceptScriptRequest(),
		resp: http_model.NewAcceptScriptResponse(),
		ctx:  ctx,
	}
}

type AcceptScriptHandler struct {
	req  *http_model.AcceptScriptRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *AcceptScriptHandler) getRequest() interface{} {
	return h.req
}
func (h *AcceptScriptHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *AcceptScriptHandler) getResponse() interface{} {
	return h.resp
}

func (h *AcceptScriptHandler) run() {
	data := http_model.AcceptScriptRequest{}
	data = *h.req
	res, err := service.Script.AcceptScript(h.ctx, data)
	if err != nil {
		logrus.Errorf("[ReviseOpinionHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "成功通过脚本"
	h.resp.Data = res
	h.resp.Data = data
}

/***
func (h *AcceptScriptHandler) run() {
	data := http_model.AcceptScriptRequest{}
	data = *h.req
	isRefuse := data.IsRefuse
	if isRefuse== 0 {
		fmt.Println("Create in")
		res, err := service.Project.Create(h.ctx, data)
		if err != nil {
			logrus.Errorf("[AcceptScriptHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功添加修改意见"
		h.resp.Data = res
	} else {
		res, err := service.Logistics.Update(h.ctx, data)
		if err != nil {
			logrus.Errorf("[AcceptScriptHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功修改物流信息"
		h.resp.Data = res
	}

}
***/
func (h *AcceptScriptHandler) checkParam() error {
	return nil
}
