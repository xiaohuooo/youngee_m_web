package handler

import (
	"errors"
	"fmt"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapTaskTerminatingListHandler(ctx *gin.Context) {
	handler := newTaskTerminatingListHandler(ctx)
	BaseRun(handler)
}

func newTaskTerminatingListHandler(ctx *gin.Context) *TaskTerminatingListHandler {
	return &TaskTerminatingListHandler{
		req:  http_model.NewTaskTerminatingListRequest(),
		resp: http_model.NewTaskTerminatingListResponse(),
		ctx:  ctx,
	}
}

type TaskTerminatingListHandler struct {
	req  *http_model.TaskTerminatingListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *TaskTerminatingListHandler) getRequest() interface{} {
	return h.req
}
func (h *TaskTerminatingListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *TaskTerminatingListHandler) getResponse() interface{} {
	return h.resp
}
func (h *TaskTerminatingListHandler) run() {
	conditions := pack.HttpTaskTerminatingListRequestToCondition(h.req)
	data, err := service.Default.GetTaskTerminatingList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *TaskTerminatingListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	h.req.StrategyId = util.IsNull(h.req.StrategyId)
	if _, err := conv.Int64(h.req.StrategyId); err != nil {
		errs = append(errs, err)
	}
	h.req.DefaultStatus = util.IsNull(h.req.DefaultStatus)
	if _, err := conv.Int64(h.req.DefaultStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
