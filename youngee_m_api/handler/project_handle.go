package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapProjectHandleHandler(ctx *gin.Context) {
	handler := NewProjectHandleHandler(ctx)
	BaseRun(handler)
}

func NewProjectHandleHandler(ctx *gin.Context) *ProjectHandleHandler {
	return &ProjectHandleHandler{
		ctx:  ctx,
		req:  http_model.NewProjectHandleRequest(),
		resp: http_model.NewProjectHandleResponse(),
	}
}

type ProjectHandleHandler struct {
	ctx  *gin.Context
	req  *http_model.ProjectHandleRequest
	resp *http_model.CommonResponse
}

func (p ProjectHandleHandler) getContext() *gin.Context {
	return p.ctx
}

func (p ProjectHandleHandler) getResponse() interface{} {
	return p.resp
}

func (p ProjectHandleHandler) getRequest() interface{} {
	return p.req
}

func (p ProjectHandleHandler) run() {
	data := http_model.ProjectHandleRequest{}
	data = *p.req
	err := db.ProjectHandle(p.ctx, data)
	if err != nil {
		logrus.Errorf("[ProjectHandleHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
		logrus.Info("ProjectHandle fail,req:%+v", p.req)
		return
	}
	p.resp.Message = "处理成功"
}

func (p ProjectHandleHandler) checkParam() error {
	return nil
}
