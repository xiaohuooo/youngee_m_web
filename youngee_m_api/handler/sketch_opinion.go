package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapSketchOpinionHandler(ctx *gin.Context) {
	handler := newSketchOpinionHandler(ctx)
	BaseRun(handler)
}

func newSketchOpinionHandler(ctx *gin.Context) *SketchOpinionHandler {
	return &SketchOpinionHandler{
		req:  http_model.NewSketchOpinionRequest(),
		resp: http_model.NewSketchOpinionResponse(),
		ctx:  ctx,
	}
}

type SketchOpinionHandler struct {
	req  *http_model.SketchOpinionRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *SketchOpinionHandler) getRequest() interface{} {
	return h.req
}
func (h *SketchOpinionHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *SketchOpinionHandler) getResponse() interface{} {
	return h.resp
}

func (h *SketchOpinionHandler) run() {
	data := http_model.SketchOpinionRequest{}
	data = *h.req
	res, err := service.Sketch.SketchOption(h.ctx, data)
	if err != nil {
		logrus.Errorf("[SketchOpinionHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "提交初稿修改意见成功"
	h.resp.Data = res
	h.resp.Data = data
}

/***
func (h *SketchOpinionHandler) run() {
	data := http_model.SketchOpinionRequest{}
	data = *h.req
	isRefuse := data.IsRefuse
	if isRefuse== 0 {
		fmt.Println("Create in")
		res, err := service.Project.Create(h.ctx, data)
		if err != nil {
			logrus.Errorf("[SketchOpinionHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功添加修改意见"
		h.resp.Data = res
	} else {
		res, err := service.Logistics.Update(h.ctx, data)
		if err != nil {
			logrus.Errorf("[SketchOpinionHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功修改物流信息"
		h.resp.Data = res
	}

}
***/
func (h *SketchOpinionHandler) checkParam() error {
	return nil
}
