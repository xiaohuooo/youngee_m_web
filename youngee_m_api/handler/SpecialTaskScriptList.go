package handler

import (
	"errors"
	"fmt"
	"github.com/cstockton/go-conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapSpecialTaskScriptListHandler(ctx *gin.Context) {
	handler := newSpecialTaskScriptListHandler(ctx)
	BaseRun(handler)
}

func newSpecialTaskScriptListHandler(ctx *gin.Context) *SpecialTaskScriptListHandler {
	return &SpecialTaskScriptListHandler{
		req:  http_model.NewSpecialTaskScriptListRequest(),
		resp: http_model.NewSpecialTaskScriptListResponse(),
		ctx:  ctx,
	}
}

type SpecialTaskScriptListHandler struct {
	req  *http_model.SpecialTaskScriptListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *SpecialTaskScriptListHandler) getRequest() interface{} {
	return h.req
}
func (h *SpecialTaskScriptListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *SpecialTaskScriptListHandler) getResponse() interface{} {
	return h.resp
}
func (h *SpecialTaskScriptListHandler) run() {
	conditions := pack.HttpSpecialTaskScriptListRequestToCondition(h.req)
	data, err := service.SpecialTask.GetSpecialTaskScriptList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *SpecialTaskScriptListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	h.req.ScriptStatus = util.IsNull(h.req.ScriptStatus)
	if _, err := conv.Int64(h.req.ScriptStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
