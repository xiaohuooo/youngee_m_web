package handler

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapAccountInfoHandler(ctx *gin.Context) {
	handler := newAccountInfoHandler(ctx)
	BaseRun(handler)
}

type accountHandler struct {
	ctx  *gin.Context
	req  *http_model.AccountInfoRequest
	resp *http_model.CommonResponse
}

func newAccountInfoHandler(ctx *gin.Context) *accountHandler {
	return &accountHandler{
		ctx:  ctx,
		req:  http_model.NewAccountInfoRequset(),
		resp: http_model.NewAccountInfoResponse(),
	}
}
func (a accountHandler) getContext() *gin.Context {
	return a.ctx
}

func (a accountHandler) getResponse() interface{} {
	return a.resp
}

func (a accountHandler) getRequest() interface{} {
	return a.req
}

func (a accountHandler) run() {
	conditions := pack.HttpAccountInfoRequestToCondition(a.req)
	data, err := service.User.AccountInfo(a.ctx, a.req.PageSize, a.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(a.ctx).Errorf("[WrapAccountInfoHandler] error AccountInfo, err:%+v", err)
		util.HandlerPackErrorResp(a.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	a.resp.Data = data
}

func (a accountHandler) checkParam() error {
	var errs []error
	if a.req.PageNum < 0 || a.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	a.req.PageNum--
	return nil
}
