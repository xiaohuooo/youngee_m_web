package handler

import (
	"errors"
	"fmt"
	"github.com/cstockton/go-conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetSpecialLogisticListHandler(ctx *gin.Context) {
	handler := GetSpecialLogisticListHandler(ctx)
	BaseRun(handler)
}

type GetSpecialLogisticList struct {
	ctx  *gin.Context
	req  *http_model.GetSpecialLogisticListRequest
	resp *http_model.CommonResponse
}

func (g GetSpecialLogisticList) getContext() *gin.Context {
	return g.ctx
}

func (g GetSpecialLogisticList) getResponse() interface{} {
	return g.resp
}

func (g GetSpecialLogisticList) getRequest() interface{} {
	return g.req
}

func (g GetSpecialLogisticList) run() {
	conditions := pack.HttpSpecialProjectTaskRequestToCondition(g.req)
	data, err := service.Project.GetSpecialProjectTaskList(g.ctx, g.req.ProjectId, g.req.PageSize, g.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetSpecialLogisticList] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetSpecialLogisticList) checkParam() error {
	var errs []error
	if g.req.PageNum < 0 || g.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	g.req.PageNum--
	g.req.ProjectId = util.IsNull(g.req.ProjectId)
	if _, err := conv.Int64(g.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}

func GetSpecialLogisticListHandler(ctx *gin.Context) *GetSpecialLogisticList {
	return &GetSpecialLogisticList{
		ctx:  ctx,
		req:  http_model.NewGetSpecialLogisticListRequest(),
		resp: http_model.NewGetSpecialLogisticListResponse(),
	}
}
