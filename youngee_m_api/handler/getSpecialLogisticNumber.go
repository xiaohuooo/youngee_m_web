package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetSpecialLogisticNumberHandler(ctx *gin.Context) {
	handler := GetSpecialLogisticNumberHandler(ctx)
	BaseRun(handler)
}

type GetSpecialLogisticNumber struct {
	ctx  *gin.Context
	req  *http_model.GetSpecialLogisticNumberRequest
	resp *http_model.CommonResponse
}

func (g GetSpecialLogisticNumber) getContext() *gin.Context {
	return g.ctx
}

func (g GetSpecialLogisticNumber) getResponse() interface{} {
	return g.resp
}

func (g GetSpecialLogisticNumber) getRequest() interface{} {
	return g.req
}

func (g GetSpecialLogisticNumber) run() {
	data := http_model.GetSpecialLogisticNumberRequest{}
	data = *g.req
	res, err := service.Number.GetSpecialLogisticNumber(g.ctx, data)
	if err != nil {
		logrus.Errorf("[GetSpecialInviteNumberHandler] call GetSpecialInviteNumber err:%+v\n", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, "")
		logrus.Info("GetSpecialInviteNumber fail,req:%+v", g.req)
		return
	}
	g.resp.Message = ""
	g.resp.Data = res
}

func (g GetSpecialLogisticNumber) checkParam() error {
	return nil
}

func GetSpecialLogisticNumberHandler(ctx *gin.Context) *GetSpecialLogisticNumber {
	return &GetSpecialLogisticNumber{
		ctx:  ctx,
		req:  http_model.NewGetSpecialLogisticNumberRequest(),
		resp: http_model.NewGetSpecialLogisticNumberResponse(),
	}
}
