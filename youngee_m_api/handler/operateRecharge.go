package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapOperateRechargeHandler(ctx *gin.Context) {
	handler := newOperateRechargeHandler(ctx)
	BaseRun(handler)
}

type OperateRechargeHandler struct {
	ctx  *gin.Context
	req  *http_model.OperateRechargeRequest
	resp *http_model.CommonResponse
}

func (o OperateRechargeHandler) getContext() *gin.Context {
	return o.ctx
}

func (o OperateRechargeHandler) getResponse() interface{} {
	return o.resp
}

func (o OperateRechargeHandler) getRequest() interface{} {
	return o.req
}

func (o OperateRechargeHandler) run() {
	err := db.OperateRecharge(o.ctx, o.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[OperateRechargeHandler] call OperateRecharge err:%+v\n", err)
		util.HandlerPackErrorResp(o.resp, consts.ErrorInternal, "")
		logrus.Info("OperateRecharge fail,req:%+v", o.req)
		return
	}
	if o.req.Method == 1 {
		o.resp.Message = "修改充值金额成功"
	} else {
		o.resp.Message = "确认充值"
	}
}

func (o OperateRechargeHandler) checkParam() error {
	return nil
}

func newOperateRechargeHandler(ctx *gin.Context) *OperateRechargeHandler {
	return &OperateRechargeHandler{
		ctx:  ctx,
		req:  http_model.NewOperateRechargeRequest(),
		resp: http_model.NewOperateRechargeResponse(),
	}
}
