package handler

import (
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
)

// WrapProjectChangeTaskStatusHandler
// @BasePath /youngee/m/
// SendCode godoc
// @Summary ProjectChangeTaskStatus 更改项目任务状态
// @Schemes
// @Description  更改项目任务的状态
// @Accept json
// @Produce json
// @Param req body http_model.ProjectChangeTaskStatusRequest true "更改项目任务状态的请求结构体"
// @Success 200 {object} http_model.CommonResponse{} "更改项目任务状态相应结构体"
// @Router /product/changeTaskStatus [post]
func WrapProjectChangeTaskStatusHandler(ctx *gin.Context) {
	handler := newProjectChangeTaskStatusHandler(ctx)
	BaseRun(handler)
}

func newProjectChangeTaskStatusHandler(ctx *gin.Context) *ProjectChangeTaskStatusHandler {
	return &ProjectChangeTaskStatusHandler{
		req:  http_model.NewProjectChangeTaskStatusRequst(),
		resp: http_model.NewProjectChangeTaskStatusResponse(),
		ctx:  ctx,
	}
}

type ProjectChangeTaskStatusHandler struct {
	req  *http_model.ProjectChangeTaskStatusRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (p *ProjectChangeTaskStatusHandler) getContext() *gin.Context {
	return p.ctx
}

func (p *ProjectChangeTaskStatusHandler) getResponse() interface{} {
	return p.resp
}

func (p *ProjectChangeTaskStatusHandler) getRequest() interface{} {
	return p.req
}

func (p *ProjectChangeTaskStatusHandler) run() {
	data := http_model.ProjectChangeTaskStatusRequest{}
	data = *p.req
	if data.IsSpecial == "1" {
		err := service.Project.ChangeSpecialTaskStatus(p.ctx, data)
		if err != nil {
			logrus.Errorf("[ChangeTaskStatusHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
			logrus.Info("ChangeTaskStatus fail,req:%+v", p.req)
			return
		}
	} else {
		err := service.Project.ChangeTaskStatus(p.ctx, data)
		if err != nil {
			logrus.Errorf("[ChangeTaskStatusHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
			logrus.Info("ChangeTaskStatus fail,req:%+v", p.req)
			return
		}
	}
	// err := service.Project.ChangeTaskStatus(p.ctx, data)
	// if err != nil {
	// 	logrus.Errorf("[ChangeTaskStatusHandler] call Create err:%+v\n", err)
	// 	util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
	// 	logrus.Info("ChangeTaskStatus fail,req:%+v", p.req)
	// 	return
	// }
	p.resp.Message = "任务状态更换成功"
}

func (p *ProjectChangeTaskStatusHandler) checkParam() error {
	return nil
}
