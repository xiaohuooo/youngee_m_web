package handler

import (
	"errors"
	"fmt"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/cstockton/go-conv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapSpecialTaskDataListHandler(ctx *gin.Context) {
	handler := newSpecialTaskDataListHandler(ctx)
	BaseRun(handler)
}

func newSpecialTaskDataListHandler(ctx *gin.Context) *SpecialTaskDataListHandler {
	return &SpecialTaskDataListHandler{
		req:  http_model.NewSpecialTaskDataListRequest(),
		resp: http_model.NewSpecialTaskDataListResponse(),
		ctx:  ctx,
	}
}

type SpecialTaskDataListHandler struct {
	req  *http_model.SpecialTaskDataListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *SpecialTaskDataListHandler) getRequest() interface{} {
	return h.req
}

func (h *SpecialTaskDataListHandler) getContext() *gin.Context {
	return h.ctx
}

func (h *SpecialTaskDataListHandler) getResponse() interface{} {
	return h.resp
}

func (h *SpecialTaskDataListHandler) run() {
	conditions := pack.HttpSpecialTaskDataListRequestToCondition(h.req)
	data, err := service.SpecialTask.GetSpecialTaskDataList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}

func (h *SpecialTaskDataListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.DataStatus = util.IsNull(h.req.DataStatus)
	if _, err := conv.Int64(h.req.DataStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
