package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapSpecialSettlePayHandler(ctx *gin.Context) {
	handler := newSpecialTaskSettlePayHandler(ctx)
	BaseRun(handler)
}

type SpecialTaskSettlePayHandler struct {
	ctx  *gin.Context
	req  *http_model.SpecialSettlePayRequest
	resp *http_model.CommonResponse
}

func (s SpecialTaskSettlePayHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SpecialTaskSettlePayHandler) getResponse() interface{} {
	return s.resp
}

func (s SpecialTaskSettlePayHandler) getRequest() interface{} {
	return s.req
}

func (s SpecialTaskSettlePayHandler) run() {
	err := service.ProjectPay.SpecialSettlePay(s.ctx, s.req)
	if err != nil {
		logrus.Errorf("[SpecialTaskSettlePayHandler] call Show err:%+v\n", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, "")
		logrus.Info("SpecialTaskSettlePayHandler fail,req:%+v", s.req)
		return
	}
	s.resp.Message = "支付成功"
}

func (s SpecialTaskSettlePayHandler) checkParam() error {
	return nil
}

func newSpecialTaskSettlePayHandler(ctx *gin.Context) *SpecialTaskSettlePayHandler {
	return &SpecialTaskSettlePayHandler{
		ctx:  ctx,
		req:  http_model.NewSpecialSettlePayRequest(),
		resp: http_model.NewSpecialSettlePayResponse(),
	}
}
