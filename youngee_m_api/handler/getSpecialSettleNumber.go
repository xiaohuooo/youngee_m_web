package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
)

func WrapGetSpecialSettleNumberHandler(ctx *gin.Context) {
	handler := newGetSpecialSettleNumberHandler(ctx)
	BaseRun(handler)
}

func newGetSpecialSettleNumberHandler(ctx *gin.Context) *GetSpecialSettleNumberHandler {
	return &GetSpecialSettleNumberHandler{
		req:  http_model.NewGetSpecialSettleNumberRequest(),
		resp: http_model.NewGetSpecialSettleNumberResponse(),
		ctx:  ctx,
	}
}

type GetSpecialSettleNumberHandler struct {
	req  *http_model.GetSpecialSettleNumberRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetSpecialSettleNumberHandler) getRequest() interface{} {
	return h.req
}
func (h *GetSpecialSettleNumberHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetSpecialSettleNumberHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetSpecialSettleNumberHandler) run() {
	data := http_model.GetSpecialSettleNumberRequest{}
	data = *h.req
	res, err := service.Number.GetSpecialSettleNumber(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetSpecialSettleNumberHandler] call GetSpecialSettleNumber err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetSpecialSettleNumber fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetSpecialSettleNumberHandler) checkParam() error {
	return nil
}
