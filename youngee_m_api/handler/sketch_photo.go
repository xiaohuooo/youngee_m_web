package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

// WrapFindSketchPhotoHandler

func WrapFindSketchPhotoHandler(ctx *gin.Context) {
	handler := newFindSketchPhotoHandler(ctx)
	BaseRun(handler)
}

func newFindSketchPhotoHandler(ctx *gin.Context) *FindSketchPhotoHandler {
	return &FindSketchPhotoHandler{
		req:  http_model.NewFindSketchPhotoRequest(),
		resp: http_model.NewFindSketchPhotoResponse(),
		ctx:  ctx,
	}
}

type FindSketchPhotoHandler struct {
	req  *http_model.FindSketchPhotoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *FindSketchPhotoHandler) getRequest() interface{} {
	return h.req
}
func (h *FindSketchPhotoHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *FindSketchPhotoHandler) getResponse() interface{} {
	return h.resp
}
func (h *FindSketchPhotoHandler) run() {
	data := *&http_model.FindSketchPhotoRequest{}
	data = *h.req
	res, err := service.Sketch.FindPhoto(h.ctx, data)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[FindSketchPhotoHandler] call FindByID err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("FindProduct fail,req:%+v", h.req)
		return
	}
	h.resp.Data = res
}
func (h *FindSketchPhotoHandler) checkParam() error {
	return nil
}
