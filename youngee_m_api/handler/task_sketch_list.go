package handler

import (
	"errors"
	"fmt"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapTaskSketchListHandler(ctx *gin.Context) {
	handler := newTaskSketchListHandler(ctx)
	BaseRun(handler)
}

func newTaskSketchListHandler(ctx *gin.Context) *TaskSketchListHandler {
	return &TaskSketchListHandler{
		req:  http_model.NewTaskSketchListRequest(),
		resp: http_model.NewTaskSketchListResponse(),
		ctx:  ctx,
	}
}

type TaskSketchListHandler struct {
	req  *http_model.TaskSketchListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *TaskSketchListHandler) getRequest() interface{} {
	return h.req
}
func (h *TaskSketchListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *TaskSketchListHandler) getResponse() interface{} {
	return h.resp
}
func (h *TaskSketchListHandler) run() {
	conditions := pack.HttpTaskSketchListRequestToCondition(h.req)
	data, err := service.Project.GetTaskSketchList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *TaskSketchListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	h.req.StrategyId = util.IsNull(h.req.StrategyId)
	if _, err := conv.Int64(h.req.StrategyId); err != nil {
		errs = append(errs, err)
	}
	h.req.SketchStatus = util.IsNull(h.req.SketchStatus)
	if _, err := conv.Int64(h.req.SketchStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
