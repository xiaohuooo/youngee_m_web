package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapPaySelectionHandler(ctx *gin.Context) {
	handler := newPaySelectionHandler(ctx)
	BaseRun(handler)
}

type PaySelection struct {
	ctx  *gin.Context
	req  *http_model.PaySelectionRequest
	resp *http_model.CommonResponse
}

func (c PaySelection) getContext() *gin.Context {
	return c.ctx
}

func (c PaySelection) getResponse() interface{} {
	return c.resp
}

func (c PaySelection) getRequest() interface{} {
	return c.req
}

func (c PaySelection) run() {
	data := http_model.PaySelectionRequest{}
	data = *c.req
	//auth := middleware.GetSessionAuth(c.ctx)
	//enterpriseID := auth.EnterpriseID
	res, err := service.Selection.Pay(c.ctx, data, data.EnterpriseId)
	if err != nil {
		logrus.Errorf("[PaySelection] call PaySelection err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("PaySelection fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "支付成功"
	c.resp.Data = res
}

func (c PaySelection) checkParam() error {
	return nil
}

func newPaySelectionHandler(ctx *gin.Context) *PaySelection {
	return &PaySelection{
		ctx:  ctx,
		req:  http_model.NewPaySelectionRequest(),
		resp: http_model.NewPaySelectionResponse(),
	}
}
