package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapBlockHandler(ctx *gin.Context) {
	handler := NewBlockHandler(ctx)
	BaseRun(handler)
}

func NewBlockHandler(ctx *gin.Context) *BlockHandler {
	return &BlockHandler{
		ctx:  ctx,
		req:  http_model.NewBlockRequest(),
		resp: http_model.NewBlockResponse(),
	}
}

type BlockHandler struct {
	ctx  *gin.Context
	req  *http_model.BlockRequest
	resp *http_model.CommonResponse
}

func (p BlockHandler) getContext() *gin.Context {
	return p.ctx
}

func (p BlockHandler) getResponse() interface{} {
	return p.resp
}

func (p BlockHandler) getRequest() interface{} {
	return p.req
}

func (p BlockHandler) run() {
	data := http_model.BlockRequest{}
	data = *p.req
	err := db.Block(p.ctx, data)
	if err != nil {
		logrus.Errorf("[BlockHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
		logrus.Info("Block fail,req:%+v", p.req)
		return
	}
	p.resp.Message = "处理成功"
}

func (p BlockHandler) checkParam() error {
	return nil
}
