package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapAcceptSketchHandler(ctx *gin.Context) {
	handler := newAcceptSketchHandler(ctx)
	BaseRun(handler)
}

func newAcceptSketchHandler(ctx *gin.Context) *AcceptSketchHandler {
	return &AcceptSketchHandler{
		req:  http_model.NewAcceptSketchRequest(),
		resp: http_model.NewAcceptSketchResponse(),
		ctx:  ctx,
	}
}

type AcceptSketchHandler struct {
	req  *http_model.AcceptSketchRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *AcceptSketchHandler) getRequest() interface{} {
	return h.req
}
func (h *AcceptSketchHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *AcceptSketchHandler) getResponse() interface{} {
	return h.resp
}

func (h *AcceptSketchHandler) run() {
	data := http_model.AcceptSketchRequest{}
	data = *h.req
	res, err := service.Sketch.AcceptSketch(h.ctx, data)
	if err != nil {
		logrus.Errorf("[ReviseOpinionHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "成功通过初稿"
	h.resp.Data = res
	h.resp.Data = data
}

/***
func (h *AcceptSketchHandler) run() {
	data := http_model.AcceptSketchRequest{}
	data = *h.req
	isRefuse := data.IsRefuse
	if isRefuse== 0 {
		fmt.Println("Create in")
		res, err := service.Project.Create(h.ctx, data)
		if err != nil {
			logrus.Errorf("[AcceptSketchHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功添加修改意见"
		h.resp.Data = res
	} else {
		res, err := service.Logistics.Update(h.ctx, data)
		if err != nil {
			logrus.Errorf("[AcceptSketchHandler] call Create err:%+v\n", err)
			util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
			log.Info("CreateProject fail,req:%+v", h.req)
			return
		}
		h.resp.Message = "成功修改物流信息"
		h.resp.Data = res
	}

}
***/
func (h *AcceptSketchHandler) checkParam() error {
	return nil
}
