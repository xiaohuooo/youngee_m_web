package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetEnterpriseIdsHandler(ctx *gin.Context) {
	handler := newGetEnterpriseIdsHandler(ctx)
	BaseRun(handler)
}

type GetEnterpriseIdsHandler struct {
	ctx  *gin.Context
	req  *http_model.GetEnterPriseIdsRequest
	resp *http_model.CommonResponse
}

func (g GetEnterpriseIdsHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetEnterpriseIdsHandler) getResponse() interface{} {
	return g.resp
}

func (g GetEnterpriseIdsHandler) getRequest() interface{} {
	return g.req
}

func (g GetEnterpriseIdsHandler) run() {
	data, err := db.GetEnterpriseIds(g.ctx)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetEnterpriseIdsHandler] error GetEnterpriseIds, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetEnterpriseIdsHandler) checkParam() error {
	return nil
}

func newGetEnterpriseIdsHandler(ctx *gin.Context) *GetEnterpriseIdsHandler {
	return &GetEnterpriseIdsHandler{
		ctx:  ctx,
		req:  http_model.NewGetEnterpriseIdsRequest(),
		resp: http_model.NewGetEnterpriseIdsResponse(),
	}
}
