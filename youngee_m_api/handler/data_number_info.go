package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetDataNumberInfoHandler(ctx *gin.Context) {
	handler := newGetDataNumberInfoHandler(ctx)
	BaseRun(handler)
}

func newGetDataNumberInfoHandler(ctx *gin.Context) *GetDataNumberInfoHandler {
	return &GetDataNumberInfoHandler{
		req:  http_model.NewGetDataNumberInfoRequest(),
		resp: http_model.NewGetDataNumberInfoResponse(),
		ctx:  ctx,
	}
}

type GetDataNumberInfoHandler struct {
	req  *http_model.GetDataNumberInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetDataNumberInfoHandler) getRequest() interface{} {
	return h.req
}
func (h *GetDataNumberInfoHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetDataNumberInfoHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetDataNumberInfoHandler) run() {
	data := http_model.GetDataNumberInfoRequest{}
	data = *h.req
	res, err := service.Number.GetDataNumberInfo(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetDataNumberInfoHandler] call GetDataNumberInfo err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetDataNumberInfo fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetDataNumberInfoHandler) checkParam() error {
	return nil
}
