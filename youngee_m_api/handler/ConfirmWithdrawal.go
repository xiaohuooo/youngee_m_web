package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapConfirmWithdrawalHandler(ctx *gin.Context) {
	handler := newConfirmWithdrawalHandler(ctx)
	BaseRun(handler)
}

type ConfirmWithdrawalHandler struct {
	ctx  *gin.Context
	req  *http_model.ConfirmWithdrawalRequest
	resp *http_model.CommonResponse
}

func (c ConfirmWithdrawalHandler) getContext() *gin.Context {
	return c.ctx
}

func (c ConfirmWithdrawalHandler) getResponse() interface{} {
	return c.resp
}

func (c ConfirmWithdrawalHandler) getRequest() interface{} {
	return c.req
}

func (c ConfirmWithdrawalHandler) run() {
	err := db.ConfirmWithdrawal(c.ctx, c.req.WithdrawId)
	if err != nil {
		logrus.WithContext(c.ctx).Errorf("[ConfirmWithdrawalHandler] error ConfirmWithdrawal, err:%+v", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	c.resp.Message = "确认提现成功"
}

func (c ConfirmWithdrawalHandler) checkParam() error {
	return nil
}

func newConfirmWithdrawalHandler(ctx *gin.Context) *ConfirmWithdrawalHandler {
	return &ConfirmWithdrawalHandler{
		ctx:  ctx,
		req:  http_model.NewConfirmWithdrawalRequest(),
		resp: http_model.NewConfirmWithdrawalResponse(),
	}
}
