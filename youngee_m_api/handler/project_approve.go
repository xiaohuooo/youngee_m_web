package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapApproveProjectHandler(ctx *gin.Context) {
	handler := newApproveProjectHandler(ctx)
	BaseRun(handler)
}

func newApproveProjectHandler(ctx *gin.Context) *ApproveProjectHandler {
	return &ApproveProjectHandler{
		req:  http_model.NewApproveProjectRequest(),
		resp: http_model.NewApproveProjectResponse(),
		ctx:  ctx,
	}
}

type ApproveProjectHandler struct {
	req  *http_model.ApproveProjectRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *ApproveProjectHandler) getRequest() interface{} {
	return h.req
}
func (h *ApproveProjectHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *ApproveProjectHandler) getResponse() interface{} {
	return h.resp
}
func (h *ApproveProjectHandler) run() {
	data := http_model.ApproveProjectRequest{}
	data = *h.req
	fmt.Println(data.IsApprove, data.ProjectId)
	err, message := service.Project.ApproveProject(h.ctx, data)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[FindAllProductHandler] call FindAll err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		logrus.Info("FindAllProduct fail,req:%+v", h.req)
		return
	}
	if message == "操作失败" {
		h.resp.Status = 1
	}
	h.resp.Message = message
}
func (h *ApproveProjectHandler) checkParam() error {
	return nil
}
