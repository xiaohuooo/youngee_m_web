package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapShowProjectHandler(ctx *gin.Context) {
	handler := newShowProjectHandler(ctx)
	BaseRun(handler)
}

func newShowProjectHandler(ctx *gin.Context) *ShowProjectHandler {
	return &ShowProjectHandler{
		req:  http_model.NewShowProjectRequest(),
		resp: http_model.NewShowProjectResponse(),
		ctx:  ctx,
	}
}

type ShowProjectHandler struct {
	req  *http_model.ShowProjectRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *ShowProjectHandler) getRequest() interface{} {
	return h.req
}
func (h *ShowProjectHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *ShowProjectHandler) getResponse() interface{} {
	return h.resp
}

func (h *ShowProjectHandler) run() {
	data := http_model.ShowProjectRequest{}
	data = *h.req
	res, err := service.Project.GetProjectDetail(h.ctx, data.ProjectID)
	if err != nil {
		logrus.Errorf("[ShowProjectHandler] call Show err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		logrus.Info("ShowProject fail,req:%+v", h.req)
		return
	}
	//h.resp.Message = "成功查询项目"
	h.resp.Data = res
}

func (h *ShowProjectHandler) checkParam() error {
	return nil
}
