package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapTaskTerminateHandler(ctx *gin.Context) {
	handler := newTaskTerminateHandler(ctx)
	BaseRun(handler)
}

func newTaskTerminateHandler(ctx *gin.Context) *TaskTerminateHandler {
	return &TaskTerminateHandler{
		req:  http_model.NewTaskTerminateRequest(),
		resp: http_model.NewTaskTerminateResponse(),
		ctx:  ctx,
	}
}

type TaskTerminateHandler struct {
	req  *http_model.TaskTerminateRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *TaskTerminateHandler) getRequest() interface{} {
	return h.req
}
func (h *TaskTerminateHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *TaskTerminateHandler) getResponse() interface{} {
	return h.resp
}
func (h *TaskTerminateHandler) run() {
	data := http_model.TaskTerminateRequest{}
	data = *h.req
	res, err := service.Terminate.TaskTerminate(h.ctx, data)
	if err != nil {
		logrus.Errorf("[TaskTerminateHandler] call TaskTerminate err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}
func (h *TaskTerminateHandler) checkParam() error {
	return nil
}
