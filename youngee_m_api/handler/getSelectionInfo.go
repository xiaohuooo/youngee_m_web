package handler

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetSelectionInfoHandler(ctx *gin.Context) {
	handler := newGetSelectionInfoHandler(ctx)
	BaseRun(handler)
}

type GetSelectionInfoHandler struct {
	ctx  *gin.Context
	req  *http_model.GetSelectionInfoRequest
	resp *http_model.CommonResponse
}

func (g GetSelectionInfoHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetSelectionInfoHandler) getResponse() interface{} {
	return g.resp
}

func (g GetSelectionInfoHandler) getRequest() interface{} {
	return g.req
}

func (g GetSelectionInfoHandler) run() {
	//condition := pack.HttpRechargeRecordsRequestToCondition(g.req)
	data, err := db.GetSelectionInfo(g.ctx, g.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[GetSelectionInfoHandler] call GetSelectionInfo err:%+v\n", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, "")
		logrus.Info("GetSelectionInfo fail,req:%+v", g.req)
		return
	}
	g.resp.Data = data
}

func (g GetSelectionInfoHandler) checkParam() error {
	var errs []error
	if g.req.PageNum < 0 || g.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	g.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}

func newGetSelectionInfoHandler(ctx *gin.Context) *GetSelectionInfoHandler {
	return &GetSelectionInfoHandler{
		ctx:  ctx,
		req:  http_model.NewGetSelectionInfoRequest(),
		resp: http_model.NewGetSelectionInfoResponse(),
	}
}
