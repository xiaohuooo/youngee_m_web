package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetFinishDataHandler(ctx *gin.Context) {
	handler := newGetFinishDataHandler(ctx)
	BaseRun(handler)
}

func newGetFinishDataHandler(ctx *gin.Context) *GetFinishDataHandler {
	return &GetFinishDataHandler{
		req:  http_model.NewGetFinishDataRequest(),
		resp: http_model.NewGetFinishDataResponse(),
		ctx:  ctx,
	}
}

type GetFinishDataHandler struct {
	req  *http_model.GetFinishDataRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetFinishDataHandler) getRequest() interface{} {
	return h.req
}
func (h *GetFinishDataHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetFinishDataHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetFinishDataHandler) run() {
	data := http_model.GetFinishDataRequest{}
	data = *h.req
	res, err := service.Finish.GetFinishData(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetFinishDataHandler] call GetFinishData err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetFinishData fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetFinishDataHandler) checkParam() error {
	return nil
}
