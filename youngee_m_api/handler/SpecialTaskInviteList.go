package handler

import (
	"errors"
	"fmt"
	"github.com/cstockton/go-conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapSpecialTaskInviteListHandler(ctx *gin.Context) {
	handler := newSpecialTaskInviteListHandler(ctx)
	BaseRun(handler)
}

func newSpecialTaskInviteListHandler(ctx *gin.Context) *SpecialTaskInviteListHandler {
	return &SpecialTaskInviteListHandler{
		req:  http_model.NewSpecialTaskInviteListRequest(),
		resp: http_model.NewSpecialTaskInviteListResponse(),
		ctx:  ctx,
	}
}

type SpecialTaskInviteListHandler struct {
	req  *http_model.SpecialTaskInviteListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *SpecialTaskInviteListHandler) getRequest() interface{} {
	return h.req
}

func (h *SpecialTaskInviteListHandler) getContext() *gin.Context {
	return h.ctx
}

func (h *SpecialTaskInviteListHandler) getResponse() interface{} {
	return h.resp
}

func (h *SpecialTaskInviteListHandler) run() {
	conditions := pack.HttpSpecialTaskInviteListRequestToCondition(h.req)
	data, err := service.SpecialTask.GetSpecialTaskInviteList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}

func (h *SpecialTaskInviteListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	h.req.TaskStatus = util.IsNull(h.req.TaskStatus)
	if _, err := conv.Int64(h.req.TaskStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
