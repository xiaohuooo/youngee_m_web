package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapPassSecTaskCoopHandler(ctx *gin.Context) {
	handler := newPassSecTaskCoopHandler(ctx)
	BaseRun(handler)
}

type PassSecTaskCoop struct {
	ctx  *gin.Context
	req  *http_model.PassSecTaskCoopRequest
	resp *http_model.CommonResponse
}

func (c PassSecTaskCoop) getContext() *gin.Context {
	return c.ctx
}

func (c PassSecTaskCoop) getResponse() interface{} {
	return c.resp
}

func (c PassSecTaskCoop) getRequest() interface{} {
	return c.req
}

func (c PassSecTaskCoop) run() {
	data := http_model.PassSecTaskCoopRequest{}
	data = *c.req
	//auth := middleware.GetSessionAuth(c.ctx)
	//enterpriseID := auth.EnterpriseID
	res, err := service.SelectionTask.PassCoop(c.ctx, data)
	if err != nil {
		logrus.Errorf("[PassSecTaskCoop] call PassSecTaskCoop err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("PassSecTaskCoop fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "成功合作选品任务"
	c.resp.Data = res
}

func (c PassSecTaskCoop) checkParam() error {
	return nil
}

func newPassSecTaskCoopHandler(ctx *gin.Context) *PassSecTaskCoop {
	return &PassSecTaskCoop{
		ctx:  ctx,
		req:  http_model.NewPassSecTaskCoopRequest(),
		resp: http_model.NewPassSecTaskCoopResponse(),
	}
}
