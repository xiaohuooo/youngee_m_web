package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapCodeLoginHandler(ctx *gin.Context) {
	handler := newCodeLoginHandler(ctx)
	BaseRun(handler)
}

func newCodeLoginHandler(ctx *gin.Context) *CodeLoginHandler {
	return &CodeLoginHandler{
		req:  http_model.NewCodeLoginRequest(),
		resp: http_model.NewCodeLoginResponse(),
		ctx:  ctx,
	}
}

type CodeLoginHandler struct {
	req  *http_model.CodeLoginRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *CodeLoginHandler) getRequest() interface{} {
	return h.req
}
func (h *CodeLoginHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *CodeLoginHandler) getResponse() interface{} {
	return h.resp
}
func (h *CodeLoginHandler) run() {
	token, username, role, err := service.LoginAuth.AuthCode(h.ctx, h.req.User, h.req.Password)
	if err != nil {
		logrus.Errorf("[CodeLoginHandler] call AuthCode err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, token)
		logrus.Info("login fail,req:%+v", h.req)
		return
	}
	data := http_model.CodeLoginData{}
	data.Token = token
	data.Username = username
	data.Role = role
	h.resp.Data = data
}
func (h *CodeLoginHandler) checkParam() error {
	return nil
}
