package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetSpecialLinkNumberHandler(ctx *gin.Context) {
	handler := newGetSpecialLinkNumberHandler(ctx)
	BaseRun(handler)
}

func newGetSpecialLinkNumberHandler(ctx *gin.Context) *GetSpecialLinkNumberHandler {
	return &GetSpecialLinkNumberHandler{
		req:  http_model.NewGetSpecialLinkNumberRequest(),
		resp: http_model.NewGetSpecialLinkNumberResponse(),
		ctx:  ctx,
	}
}

type GetSpecialLinkNumberHandler struct {
	req  *http_model.GetSpecialLinkNumberRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetSpecialLinkNumberHandler) getRequest() interface{} {
	return h.req
}
func (h *GetSpecialLinkNumberHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetSpecialLinkNumberHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetSpecialLinkNumberHandler) run() {
	data := http_model.GetSpecialLinkNumberRequest{}
	data = *h.req
	res, err := service.Number.GetSpecialLinkNumber(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetSpecialLinkNumberHandler] call GetSpecialLinkNumber err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetSpecialLinkNumber fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetSpecialLinkNumberHandler) checkParam() error {
	return nil
}
