package handler

import (
	"errors"
	"fmt"
	"github.com/cstockton/go-conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapSpecialTaskLinkListHandler(ctx *gin.Context) {
	handler := newSpecialTaskLinkListHandler(ctx)
	BaseRun(handler)
}

func newSpecialTaskLinkListHandler(ctx *gin.Context) *SpecialTaskLinkListHandler {
	return &SpecialTaskLinkListHandler{
		req:  http_model.NewSpecialTaskLinkListRequest(),
		resp: http_model.NewSpecialTaskLinkListResponse(),
		ctx:  ctx,
	}
}

type SpecialTaskLinkListHandler struct {
	req  *http_model.SpecialTaskLinkListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *SpecialTaskLinkListHandler) getRequest() interface{} {
	return h.req
}
func (h *SpecialTaskLinkListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *SpecialTaskLinkListHandler) getResponse() interface{} {
	return h.resp
}
func (h *SpecialTaskLinkListHandler) run() {
	conditions := pack.HttpSpecialTaskLinkListRequestToCondition(h.req)
	data, err := service.SpecialTask.GetSpecialTaskLinkList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *SpecialTaskLinkListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.LinkStatus = util.IsNull(h.req.LinkStatus)
	if _, err := conv.Int64(h.req.LinkStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
