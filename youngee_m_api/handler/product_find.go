package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapFindProductHandler(ctx *gin.Context) {
	handler := newFindProductHandler(ctx)
	BaseRun(handler)
}

func newFindProductHandler(ctx *gin.Context) *FindProductHandler {
	return &FindProductHandler{
		req:  http_model.NewFindProductRequest(),
		resp: http_model.NewFindProductResponse(),
		ctx:  ctx,
	}
}

type FindProductHandler struct {
	req  *http_model.FindProductRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *FindProductHandler) getRequest() interface{} {
	return h.req
}
func (h *FindProductHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *FindProductHandler) getResponse() interface{} {
	return h.resp
}
func (h *FindProductHandler) run() {
	data := *&http_model.FindProductRequest{}
	data = *h.req
	res, err := service.Product.FindByID(h.ctx, data.ProductID)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[FindProductHandler] call FindByID err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		logrus.Info("FindProduct fail,req:%+v", h.req)
		return
	}
	h.resp.Data = res
}
func (h *FindProductHandler) checkParam() error {
	return nil
}
