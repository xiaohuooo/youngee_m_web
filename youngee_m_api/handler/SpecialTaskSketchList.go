package handler

import (
	"errors"
	"fmt"
	"github.com/cstockton/go-conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapSpecialTaskSketchListHandler(ctx *gin.Context) {
	handler := newSpecialTaskSketchListHandler(ctx)
	BaseRun(handler)
}

func newSpecialTaskSketchListHandler(ctx *gin.Context) *SpecialTaskSketchListHandler {
	return &SpecialTaskSketchListHandler{
		req:  http_model.NewSpecialTaskSketchListRequest(),
		resp: http_model.NewSpecialTaskSketchListResponse(),
		ctx:  ctx,
	}
}

type SpecialTaskSketchListHandler struct {
	req  *http_model.SpecialTaskSketchListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *SpecialTaskSketchListHandler) getRequest() interface{} {
	return h.req
}
func (h *SpecialTaskSketchListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *SpecialTaskSketchListHandler) getResponse() interface{} {
	return h.resp
}
func (h *SpecialTaskSketchListHandler) run() {
	conditions := pack.HttpSpecialTaskSketchListRequestToCondition(h.req)
	data, err := service.SpecialTask.GetSpecialTaskSketchList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *SpecialTaskSketchListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.SketchStatus = util.IsNull(h.req.SketchStatus)
	if _, err := conv.Int64(h.req.SketchStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
