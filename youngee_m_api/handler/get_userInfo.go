package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetUserInfoHandler(ctx *gin.Context) {
	handler := newGetUserInfo(ctx)
	BaseRun(handler)
}

func newGetUserInfo(ctx *gin.Context) *UserInfo {
	return &UserInfo{
		req:  http_model.NewUserInfoRequest(),
		resp: http_model.NewUserInfoResponse(),
		ctx:  ctx,
	}
}

type UserInfo struct {
	req  *http_model.UserInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (u UserInfo) getContext() *gin.Context {
	return u.ctx
}

func (u UserInfo) getResponse() interface{} {
	return u.resp
}

func (u UserInfo) getRequest() interface{} {
	return u.req
}

func (u UserInfo) run() {
	userInfo, err := service.LoginAuth.AuthToken(u.ctx, u.req.Token)
	if err != nil {
		logrus.Errorf("[CodeLoginHandler] call AuthCode err:%+v\n", err)
		util.HandlerPackErrorResp(u.resp, consts.ErrorInternal, u.req.Token)
		logrus.Info("login fail,req:%+v", u.req)
		return
	}
	data := http_model.UserInfoResponse{}
	data.User = userInfo.User
	data.Username = userInfo.Username
	data.Role = userInfo.Role
	u.resp.Data = data
}

func (u UserInfo) checkParam() error {
	return nil
}
