package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/middleware"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapRechargeBalanceHandler(ctx *gin.Context) {
	handler := newRechargeBalanceHandler(ctx)
	BaseRun(handler)
}

type RechargeBalanceHandler struct {
	ctx  *gin.Context
	req  *http_model.RechargeBalanceRequest
	resp *http_model.CommonResponse
}

func (r RechargeBalanceHandler) getContext() *gin.Context {
	return r.ctx
}

func (r RechargeBalanceHandler) getResponse() interface{} {
	return r.resp
}

func (r RechargeBalanceHandler) getRequest() interface{} {
	return r.req
}

func (r RechargeBalanceHandler) run() {
	enterpriseID := r.req.EnterpriseId
	phone := middleware.GetSessionAuth(r.ctx).Phone
	err := db.RechargeAmount(r.ctx, enterpriseID, r.req.Amount, phone)
	if err != nil {
		logrus.WithContext(r.ctx).Errorf("[QueryOrderByTradeIdHandler] error QueryOrderByOutTradeNo, err:%+v", err)
		util.HandlerPackErrorResp(r.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	r.resp.Message = "充值成功"
}

func (r RechargeBalanceHandler) checkParam() error {
	return nil
}

func newRechargeBalanceHandler(ctx *gin.Context) *RechargeBalanceHandler {
	return &RechargeBalanceHandler{
		ctx:  ctx,
		req:  http_model.NewRechargeBalanceResponse(),
		resp: http_model.NewRechargeBalanceRequest(),
	}
}
