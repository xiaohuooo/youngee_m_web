package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetYoungeeRecordsHandler(ctx *gin.Context) {
	handler := newGetYoungeeRecordsHandler(ctx)
	BaseRun(handler)
}

type GetYoungeeRecordsHandler struct {
	ctx  *gin.Context
	req  *http_model.GetYoungeeRecordsRequest
	resp *http_model.CommonResponse
}

func (g GetYoungeeRecordsHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetYoungeeRecordsHandler) getResponse() interface{} {
	return g.resp
}

func (g GetYoungeeRecordsHandler) getRequest() interface{} {
	return g.req
}

func newGetYoungeeRecordsHandler(ctx *gin.Context) *GetYoungeeRecordsHandler {
	return &GetYoungeeRecordsHandler{
		ctx:  ctx,
		req:  http_model.NewGetYoungeeRecordsRequest(),
		resp: http_model.NewGetYoungeeRecordsResponse(),
	}
}

func (g GetYoungeeRecordsHandler) run() {
	data, err := db.GetYoungeeRecords(g.ctx, g.req.TalentId)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetYoungeeRecordsHandler] error GetYoungeeRecords, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetYoungeeRecordsHandler) checkParam() error {
	return nil
}
