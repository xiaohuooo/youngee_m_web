package handler

import (
	"errors"
	"fmt"
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// WrapProjectTaskListHandler
// @BasePath /youngee/m/
// SendCode godoc
// @Summary ProjectTaskList 项目任务列表
// @Schemes
// @Description 展示某个项目的任务列表
// @Accept json
// @Produce json
// @Param req body http_model.ProjectTaskListRequest true "查询项目的任务列表的请求结构体"
// @Success 200 {object} http_model.CommonResponse{data=http_model.ProjectTaskListData} "查询项目的任务列表相应结构体"
// @Router /product/taskList [post]
func WrapProjectTaskListHandler(ctx *gin.Context) {
	handler := newProjectTaskListHandler(ctx)
	BaseRun(handler)
}

func newProjectTaskListHandler(ctx *gin.Context) *ProjectTaskListHandler {
	return &ProjectTaskListHandler{
		req:  http_model.NewProjectTaskListRequest(),
		resp: http_model.NewProjectTaskListResponse(),
		ctx:  ctx,
	}
}

type ProjectTaskListHandler struct {
	req  *http_model.ProjectTaskListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *ProjectTaskListHandler) getRequest() interface{} {
	return h.req
}
func (h *ProjectTaskListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *ProjectTaskListHandler) getResponse() interface{} {
	return h.resp
}
func (h *ProjectTaskListHandler) run() {
	conditions := pack.HttpProjectTaskRequestToCondition(h.req)
	data, err := service.Project.GetProjectTaskList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[ProjectTaskListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *ProjectTaskListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	h.req.StrategyId = util.IsNull(h.req.StrategyId)
	if _, err := conv.Int64(h.req.StrategyId); err != nil {
		errs = append(errs, err)
	}
	h.req.TaskStatus = util.IsNull(h.req.TaskStatus)
	if _, err := conv.Int64(h.req.TaskStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
