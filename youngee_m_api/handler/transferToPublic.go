package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapTransferToPublicHandler(ctx *gin.Context) {
	handler := newTransferToPublicHandler(ctx)
	BaseRun(handler)
}

type TransferToPublicHandler struct {
	ctx  *gin.Context
	req  *http_model.TransferToPublicRequest
	resp *http_model.CommonResponse
}

func (t TransferToPublicHandler) getContext() *gin.Context {
	return t.ctx
}

func (t TransferToPublicHandler) getResponse() interface{} {
	return t.resp
}

func (t TransferToPublicHandler) getRequest() interface{} {
	return t.req
}

func (t TransferToPublicHandler) run() {
	enterpriseID := t.req.EnterpriseID
	err := db.TransferToPublic(t.ctx, t.req.Amount, enterpriseID, t.req.TransferVoucherUrl)
	if err != nil {
		logrus.Errorf("[TransferToPublicHandler] call TransferToPublic err:%+v\n", err)
		util.HandlerPackErrorResp(t.resp, consts.ErrorInternal, "")
		logrus.Info("TransferToPublic fail,req:%+v", t.req)
		return
	}
	t.resp.Message = "转账成功，请等待管理员确认"
}

func (t TransferToPublicHandler) checkParam() error {
	return nil
}

func newTransferToPublicHandler(ctx *gin.Context) *TransferToPublicHandler {
	return &TransferToPublicHandler{
		ctx:  ctx,
		req:  http_model.NewTransferToPublicRequest(),
		resp: http_model.NewTransferToPubliceResponse(),
	}
}
