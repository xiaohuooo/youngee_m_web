package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDeletePhotoUrlHandler(ctx *gin.Context) {
	handler := newDeletePhotoUrl(ctx)
	BaseRun(handler)
}

type DeletePhotoUrl struct {
	req  *http_model.DeletePhotoUrlRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (d DeletePhotoUrl) getContext() *gin.Context {
	return d.ctx
}

func (d DeletePhotoUrl) getResponse() interface{} {
	return d.resp
}

func (d DeletePhotoUrl) getRequest() interface{} {
	return d.req
}

func (d DeletePhotoUrl) run() {
	err := db.DeletePhotoUrl(d.ctx, d.req.PhotoUrl)
	if err != nil {
		logrus.WithContext(d.ctx).Errorf("[DeletePhotoUrl] error DeletePhotoUrl, err:%+v", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	d.resp.Message = "图片删除成功"
}

func (d DeletePhotoUrl) checkParam() error {
	return nil
}

func newDeletePhotoUrl(ctx *gin.Context) *DeletePhotoUrl {
	return &DeletePhotoUrl{
		req:  http_model.NewDeletePhotoUrlRequest(),
		resp: http_model.NewDeletePhotoUrlResponse(),
		ctx:  ctx,
	}
}
