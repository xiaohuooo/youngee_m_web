package handler

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/util"
)

func WrapInvoiceRecordsHandler(ctx *gin.Context) {
	handler := newInvoiceRecordsHandler(ctx)
	BaseRun(handler)
}

type InvoiceRecordsHandler struct {
	ctx  *gin.Context
	req  *http_model.InvoiceRecordsRequest
	resp *http_model.CommonResponse
}

func (i InvoiceRecordsHandler) getContext() *gin.Context {
	return i.ctx
}

func (i InvoiceRecordsHandler) getResponse() interface{} {
	return i.resp
}

func (i InvoiceRecordsHandler) getRequest() interface{} {
	return i.req
}

func (i InvoiceRecordsHandler) run() {
	condition := pack.HttpInvoiceRecordsRequestToCondition(i.req)
	data, err := db.GetInvoiceRecords(i.ctx, i.req, condition)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[InvoiceRecordsHandler] call GetInvoiceRecords err:%+v\n", err)
		util.HandlerPackErrorResp(i.resp, consts.ErrorInternal, "")
		logrus.Info("GetInvoiceRecords fail,req:%+v", i.req)
		return
	}
	i.resp.Data = data
}

func (i InvoiceRecordsHandler) checkParam() error {
	var errs []error
	if i.req.PageNum < 0 || i.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	i.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}

func newInvoiceRecordsHandler(ctx *gin.Context) *InvoiceRecordsHandler {
	return &InvoiceRecordsHandler{
		ctx:  ctx,
		req:  http_model.NewInvoiceRecordsRequest(),
		resp: http_model.NewInvoiceRecordsResponse(),
	}
}
