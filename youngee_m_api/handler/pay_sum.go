package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapPaySumHandler(ctx *gin.Context) {
	handler := newPaySumHandler(ctx)
	BaseRun(handler)
}

func newPaySumHandler(ctx *gin.Context) *PaySumHandler {
	return &PaySumHandler{
		req:  http_model.NewPaySumRequest(),
		resp: http_model.NewPaySumResponse(),
		ctx:  ctx,
	}
}

type PaySumHandler struct {
	req  *http_model.PaySumRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *PaySumHandler) getRequest() interface{} {
	return h.req
}
func (h *PaySumHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *PaySumHandler) getResponse() interface{} {
	return h.resp
}
func (h *PaySumHandler) run() {
	data := http_model.PaySumRequest{}
	data = *h.req
	res, err := service.Pay.GetPaysum(h.ctx, data)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[FindAllProductHandler] call FindAll err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		logrus.Info("FindAllProduct fail,req:%+v", h.req)
		return
	}
	// h.resp.Message = "查询成功"
	h.resp.Data = res
}
func (h *PaySumHandler) checkParam() error {
	return nil
}
