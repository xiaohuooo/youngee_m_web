package handler

import (
	"errors"
	"fmt"
	"github.com/caixw/lib.go/conv"
	"time"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapFullProjectListHandler(ctx *gin.Context) {
	handler := newFullProjectListHandler(ctx)
	BaseRun(handler)
}

func newFullProjectListHandler(ctx *gin.Context) *FullProjectListHandler {
	return &FullProjectListHandler{
		req:  http_model.NewFullProjectListRequest(),
		resp: http_model.NewFullProjectListResponse(),
		ctx:  ctx,
	}
}

type FullProjectListHandler struct {
	req  *http_model.FullProjectListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *FullProjectListHandler) getRequest() interface{} {
	return h.req
}
func (h *FullProjectListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *FullProjectListHandler) getResponse() interface{} {
	return h.resp
}
func (h *FullProjectListHandler) run() {
	condition := pack.HttpFullProjectRequestToCondition(h.req)
	data, err := service.Project.GetFullProjectList(h.ctx, h.req.PageSize, h.req.PageNum, condition, h.req.ProjectType)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[FullProjectListHandler] error GetFullProjectList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}

func (h *FullProjectListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectForm = util.IsNull(h.req.ProjectForm)
	if _, err := conv.Int64(h.req.ProjectForm); err != nil {
		errs = append(errs, err)
	}
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	//h.req.ProjectName = util.IsNull(h.req.ProjectName)

	h.req.ProjectStatus = util.IsNull(h.req.ProjectStatus)
	if _, err := conv.Int64(h.req.ProjectStatus); err != nil {
		errs = append(errs, err)
	}
	formatTime, _ := time.Parse("2006-01-02 15:04:05", h.req.ProjectUpdated)
	//fmt.Println(h.req.ProjectUpdated)
	h.req.ProjectUpdated = util.IsNull(h.req.ProjectUpdated)
	if _, err := conv.Int64(formatTime.Unix()); err != nil {
		errs = append(errs, err)
	}
	h.req.ProjectContentType = util.IsNull(h.req.ProjectContentType)
	if _, err := conv.Int64(h.req.ProjectContentType); err != nil {
		errs = append(errs, err)
	}
	h.req.ProjectPlatform = util.IsNull(h.req.ProjectPlatform)
	if _, err := conv.Int64(h.req.ProjectPlatform); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
