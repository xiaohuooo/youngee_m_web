package handler

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapCreatorListHandler(ctx *gin.Context) {
	handler := newCreatorListHandler(ctx)
	BaseRun(handler)
}

func newCreatorListHandler(ctx *gin.Context) *CreatorList {
	return &CreatorList{
		req:  http_model.NewCreatorListRequest(),
		resp: http_model.NewCreatorListResponse(),
		ctx:  ctx,
	}
}

type CreatorList struct {
	req  *http_model.CreatorListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (c CreatorList) getContext() *gin.Context {
	return c.ctx
}

func (c CreatorList) getResponse() interface{} {
	return c.resp
}

func (c CreatorList) getRequest() interface{} {
	return c.req
}

func (c CreatorList) run() {
	conditions := pack.HttpCreatorListRequestToCondition(c.req)
	data, err := service.User.CreatorList(c.ctx, c.req.PageSize, c.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(c.ctx).Errorf("[EnterpriseUserHandler] error EnterpriseUserList, err:%+v", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	c.resp.Data = data
}

func (c CreatorList) checkParam() error {
	var errs []error
	if c.req.PageNum < 0 || c.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	c.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("CreatorList check param errs:%+v", errs)
	}
	return nil
}
