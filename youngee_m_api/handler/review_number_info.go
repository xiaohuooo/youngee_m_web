package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetReviewNumberInfoHandler(ctx *gin.Context) {
	handler := newGetReviewNumberInfoHandler(ctx)
	BaseRun(handler)
}

func newGetReviewNumberInfoHandler(ctx *gin.Context) *GetReviewNumberInfoHandler {
	return &GetReviewNumberInfoHandler{
		req:  http_model.NewGetReviewNumberInfoRequest(),
		resp: http_model.NewGetReviewNumberInfoResponse(),
		ctx:  ctx,
	}
}

type GetReviewNumberInfoHandler struct {
	req  *http_model.GetReviewNumberInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetReviewNumberInfoHandler) getRequest() interface{} {
	return h.req
}
func (h *GetReviewNumberInfoHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetReviewNumberInfoHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetReviewNumberInfoHandler) run() {
	data := http_model.GetReviewNumberInfoRequest{}
	data = *h.req
	res, err := service.Number.GetReviewNumberInfo(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetReviewNumberInfoHandler] call GetReviewNumberInfo err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetReviewNumberInfo fail,req:%+v", h.req)
		return
	}
	h.resp.Data = res
}

func (h *GetReviewNumberInfoHandler) checkParam() error {
	return nil
}
