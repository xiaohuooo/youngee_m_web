package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetLogisticsNumberInfoHandler(ctx *gin.Context) {
	handler := newGetLogisticsNumberInfoHandler(ctx)
	BaseRun(handler)
}

func newGetLogisticsNumberInfoHandler(ctx *gin.Context) *GetLogisticsNumberInfoHandler {
	return &GetLogisticsNumberInfoHandler{
		req:  http_model.NewGetLogisticsNumberInfoRequest(),
		resp: http_model.NewGetLogisticsNumberInfoResponse(),
		ctx:  ctx,
	}
}

type GetLogisticsNumberInfoHandler struct {
	req  *http_model.GetLogisticsNumberInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetLogisticsNumberInfoHandler) getRequest() interface{} {
	return h.req
}
func (h *GetLogisticsNumberInfoHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetLogisticsNumberInfoHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetLogisticsNumberInfoHandler) run() {
	data := http_model.GetLogisticsNumberInfoRequest{}
	data = *h.req
	res, err := service.Number.GetLogisticsNumberInfo(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetLogisticsNumberInfoHandler] call GetLogisticsNumberInfo err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetLogisticsNumberInfo fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetLogisticsNumberInfoHandler) checkParam() error {
	return nil
}
