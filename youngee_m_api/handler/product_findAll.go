package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapFindEnterpriseAllProductHandler(ctx *gin.Context) {
	handler := newFindEnterpriseAllProductHandler(ctx)
	BaseRun(handler)
}

func newFindEnterpriseAllProductHandler(ctx *gin.Context) *FindAllProductHandler {
	return &FindAllProductHandler{
		req:  http_model.NewFindAllProductRequest(),
		resp: http_model.NewFindAllProductResponse(),
		ctx:  ctx,
	}
}

type FindAllProductHandler struct {
	req  *http_model.FindAllProductRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *FindAllProductHandler) getRequest() interface{} {
	return h.req
}
func (h *FindAllProductHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *FindAllProductHandler) getResponse() interface{} {
	return h.resp
}
func (h *FindAllProductHandler) run() {
	enterpriseID := db.GetEnterpriseIDByUserID(h.ctx, h.req.UserID)
	if enterpriseID == "" {
		// 数据库查询失败，返回5001
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("FindAllProduct fail,req:%+v", h.req)
		return
	}
	res, err := service.Product.FindAll(h.ctx, enterpriseID)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[FindAllProductHandler] call FindAll err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("FindAllProduct fail,req:%+v", h.req)
		return
	}
	h.resp.Data = res
}
func (h *FindAllProductHandler) checkParam() error {
	return nil
}
