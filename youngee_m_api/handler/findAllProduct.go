package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapFindAllProductHandler(ctx *gin.Context) {
	handler := newFindAllProductHandler(ctx)
	BaseRun(handler)
}

type AllProductHandler struct {
	ctx  *gin.Context
	req  *http_model.FindAllProductsRequest
	resp *http_model.CommonResponse
}

func (a AllProductHandler) getContext() *gin.Context {
	return a.ctx
}

func (a AllProductHandler) getResponse() interface{} {
	return a.resp
}

func (a AllProductHandler) getRequest() interface{} {
	return a.req
}

func (a AllProductHandler) run() {
	res, err := service.Product.FindAll(a.ctx, a.req.EnterpriseID)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[FindAllProductHandler] call FindAll err:%+v\n", err)
		util.HandlerPackErrorResp(a.resp, consts.ErrorInternal, "")
		logrus.Info("FindAllProduct fail,req:%+v", a.req)
		return
	}
	// h.resp.Message = "查询成功"
	a.resp.Data = res
}

func (a AllProductHandler) checkParam() error {
	return nil
}

func newFindAllProductHandler(ctx *gin.Context) *AllProductHandler {
	return &AllProductHandler{
		ctx:  ctx,
		req:  http_model.NewFindAllProductsRequest(),
		resp: http_model.NewFindAllProductsResponse(),
	}
}
