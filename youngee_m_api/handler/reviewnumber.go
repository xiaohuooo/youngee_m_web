package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapSelectionReviewNumberHandler(ctx *gin.Context) {
	handler := newSelectionReviewNumberHandler(ctx)
	BaseRun(handler)
}

type SelectionReviewNumberHandler struct {
	ctx  *gin.Context
	req  *http_model.SelectionReviewNumberRequest
	resp *http_model.CommonResponse
}

func (g SelectionReviewNumberHandler) getContext() *gin.Context {
	return g.ctx
}

func (g SelectionReviewNumberHandler) getResponse() interface{} {
	return g.resp
}

func (g SelectionReviewNumberHandler) getRequest() interface{} {
	return g.req
}

func (g SelectionReviewNumberHandler) run() {
	data, err := db.SelectionReviewNumber(g.ctx)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[SelectionReviewNumberHandler] error SelectionReviewNumber, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g SelectionReviewNumberHandler) checkParam() error {
	return nil
}

func newSelectionReviewNumberHandler(ctx *gin.Context) *SelectionReviewNumberHandler {
	return &SelectionReviewNumberHandler{
		ctx:  ctx,
		req:  http_model.NewSelectionReviewNumberRequest(),
		resp: http_model.NewSelectionReviewNumberResponse(),
	}
}
