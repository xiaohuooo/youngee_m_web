package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapSubmitSelectionHandler(ctx *gin.Context) {
	handler := newSubmitSelectionHandler(ctx)
	BaseRun(handler)
}

type SubmitSelection struct {
	ctx  *gin.Context
	req  *http_model.SubmitSelectionRequest
	resp *http_model.CommonResponse
}

func (c SubmitSelection) getContext() *gin.Context {
	return c.ctx
}

func (c SubmitSelection) getResponse() interface{} {
	return c.resp
}

func (c SubmitSelection) getRequest() interface{} {
	return c.req
}

func (c SubmitSelection) run() {
	data := http_model.SubmitSelectionRequest{}
	data = *c.req
	//auth := middleware.GetSessionAuth(c.ctx)
	//enterpriseID := auth.EnterpriseID
	res, err := service.Selection.Submit(c.ctx, data)
	if err != nil {
		logrus.Errorf("[SubmitSelection] call SubmitSelection err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("SubmitSelection fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "项目已提交审核"
	c.resp.Data = res
}

func (c SubmitSelection) checkParam() error {
	return nil
}

func newSubmitSelectionHandler(ctx *gin.Context) *SubmitSelection {
	return &SubmitSelection{
		ctx:  ctx,
		req:  http_model.NewSubmitSelectionRequest(),
		resp: http_model.NewSubmitSelectionResponse(),
	}
}
