package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapCreateSelectionHandler(ctx *gin.Context) {
	handler := newCreateSelectionHandler(ctx)
	BaseRun(handler)
}

type CreateSelection struct {
	ctx  *gin.Context
	req  *http_model.CreateSelectionRequest
	resp *http_model.CommonResponse
}

func (c CreateSelection) getContext() *gin.Context {
	return c.ctx
}

func (c CreateSelection) getResponse() interface{} {
	return c.resp
}

func (c CreateSelection) getRequest() interface{} {
	return c.req
}

func (c CreateSelection) run() {
	data := http_model.CreateSelectionRequest{}
	data = *c.req
	//auth := middleware.GetSessionAuth(c.ctx)
	//enterpriseID := auth.EnterpriseID
	res, err := service.Selection.Create(c.ctx, data)
	if err != nil {
		logrus.Errorf("[CreateSelection] call CreateSelection err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("CreateSelection fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "成功创建选品"
	c.resp.Data = res
}

func (c CreateSelection) checkParam() error {
	return nil
}

func newCreateSelectionHandler(ctx *gin.Context) *CreateSelection {
	return &CreateSelection{
		ctx:  ctx,
		req:  http_model.NewCreateSelectionRequest(),
		resp: http_model.NewCreateSelectionResponse(),
	}
}
