package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapCreateProjectHandler(ctx *gin.Context) {
	handler := newCreateProjectHandler(ctx)
	BaseRun(handler)
}

func newCreateProjectHandler(ctx *gin.Context) *CreateProjectHandler {
	return &CreateProjectHandler{
		req:  http_model.NewCreateProjectRequest(),
		resp: http_model.NewCreateProjectResponse(),
		ctx:  ctx,
	}
}

type CreateProjectHandler struct {
	req  *http_model.CreateProjectRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *CreateProjectHandler) getRequest() interface{} {
	return h.req
}
func (h *CreateProjectHandler) getContext() *gin.Context {
	return h.ctx
}

func (h *CreateProjectHandler) getResponse() interface{} {
	return h.resp
}

func (h *CreateProjectHandler) run() {
	data := http_model.CreateProjectRequest{}
	data = *h.req
	fmt.Println("data:", data)
	fmt.Println("EnterpriseID:", h.req.EnterpriseID)
	res, err := service.Project.Create(h.ctx, data, h.req.EnterpriseID)
	if err != nil {
		logrus.Errorf("[CreateProjectHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "成功创建项目"
	h.resp.Data = res
}

func (h *CreateProjectHandler) checkParam() error {
	return nil
}
