package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapUpdateSelectionHandler(ctx *gin.Context) {
	handler := newUpdateSelectionHandler(ctx)
	BaseRun(handler)
}

type UpdateSelection struct {
	ctx  *gin.Context
	req  *http_model.UpdateSelectionRequest
	resp *http_model.CommonResponse
}

func (c UpdateSelection) getContext() *gin.Context {
	return c.ctx
}

func (c UpdateSelection) getResponse() interface{} {
	return c.resp
}

func (c UpdateSelection) getRequest() interface{} {
	return c.req
}

func (c UpdateSelection) run() {
	data := http_model.UpdateSelectionRequest{}
	data = *c.req
	res, err := service.Selection.Update(c.ctx, data, data.EnterpriseId)
	if err != nil {
		logrus.Errorf("[UpdateSelection] call UpdateSelection err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("UpdateSelection fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "更新选品成功"
	c.resp.Data = res
}

func (c UpdateSelection) checkParam() error {
	return nil
}

func newUpdateSelectionHandler(ctx *gin.Context) *UpdateSelection {
	return &UpdateSelection{
		ctx:  ctx,
		req:  http_model.NewUpdateSelectionRequest(),
		resp: http_model.NewUpdateSelectionResponse(),
	}
}
