package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapUpdateSecTaskLogisticsHandler(ctx *gin.Context) {
	handler := newUpdateSecTaskLogisticsHandler(ctx)
	BaseRun(handler)
}

type UpdateSecTaskLogistics struct {
	ctx  *gin.Context
	req  *http_model.UpdateSecTaskLogisticsRequest
	resp *http_model.CommonResponse
}

func (c UpdateSecTaskLogistics) getContext() *gin.Context {
	return c.ctx
}

func (c UpdateSecTaskLogistics) getResponse() interface{} {
	return c.resp
}

func (c UpdateSecTaskLogistics) getRequest() interface{} {
	return c.req
}

func (c UpdateSecTaskLogistics) run() {
	data := http_model.UpdateSecTaskLogisticsRequest{}
	data = *c.req
	res, err := service.SecLogistics.Update(c.ctx, data)
	if err != nil {
		logrus.Errorf("[UpdateSecTaskLogistics] call UpdateSecTaskLogistics err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("UpdateSecTaskLogistics fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "成功添加发货信息"
	c.resp.Data = res
}

func (c UpdateSecTaskLogistics) checkParam() error {
	return nil
}

func newUpdateSecTaskLogisticsHandler(ctx *gin.Context) *UpdateSecTaskLogistics {
	return &UpdateSecTaskLogistics{
		ctx:  ctx,
		req:  http_model.NewUpdateSecTaskLogisticsRequest(),
		resp: http_model.NewUpdateSecTaskLogisticsResponse(),
	}
}
