package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapProjectPayHandler(ctx *gin.Context) {
	handler := newProjectPayHandler(ctx)
	BaseRun(handler)
}

func newProjectPayHandler(ctx *gin.Context) *ProjectPayHandler {
	return &ProjectPayHandler{
		req:  http_model.NewProjectPayRequest(),
		resp: http_model.NewProjectPayResponse(),
		ctx:  ctx,
	}
}

type ProjectPayHandler struct {
	req  *http_model.ProjectPayRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *ProjectPayHandler) getRequest() interface{} {
	return h.req
}
func (h *ProjectPayHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *ProjectPayHandler) getResponse() interface{} {
	return h.resp
}
func (h *ProjectPayHandler) run() {
	enterpriseID := h.req.EnterpriseId
	data := http_model.ProjectPayRequest{}
	data = *h.req
	record, err := service.ProjectPay.Pay(h.ctx, data, enterpriseID)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[ProjectPayHandler] error Pay, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = record
}
func (h *ProjectPayHandler) checkParam() error {
	return nil
}
