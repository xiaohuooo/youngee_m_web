package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapDeleteAccountHandler(ctx *gin.Context) {
	handler := newDeleteAccountHandler(ctx)
	BaseRun(handler)
}

type DeleteAccount struct {
	ctx  *gin.Context
	req  *http_model.DeleteAccountRequest
	resp *http_model.CommonResponse
}

func (d DeleteAccount) getContext() *gin.Context {
	return d.ctx
}

func (d DeleteAccount) getResponse() interface{} {
	return d.resp
}

func (d DeleteAccount) getRequest() interface{} {
	return d.req
}

func (d DeleteAccount) run() {
	err := db.DeleteAccount(d.ctx, d.req.PlatformID, d.req.PlatformNickname, d.req.TalentId)
	if err != nil {
		logrus.Errorf("[DeleteAccountHandler] call Delete err:%+v\n", err)
		util.HandlerPackErrorResp(d.resp, consts.ErrorInternal, "")
		logrus.Info("DeleteAccount fail,req:%+v", d.req)
		return
	}
	d.resp.Message = "解绑成功"
}

func (d DeleteAccount) checkParam() error {
	return nil
}

func newDeleteAccountHandler(ctx *gin.Context) *DeleteAccount {
	return &DeleteAccount{
		ctx:  ctx,
		req:  http_model.NewDeleteAccountRequest(),
		resp: http_model.NewDeleteAccountResponse(),
	}
}
