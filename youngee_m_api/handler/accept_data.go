package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapAcceptDataHandler(ctx *gin.Context) {
	handler := newAcceptDataHandler(ctx)
	BaseRun(handler)
}

func newAcceptDataHandler(ctx *gin.Context) *AcceptDataHandler {
	return &AcceptDataHandler{
		req:  http_model.NewAcceptDataRequest(),
		resp: http_model.NewAcceptDataResponse(),
		ctx:  ctx,
	}
}

type AcceptDataHandler struct {
	req  *http_model.AcceptDataRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *AcceptDataHandler) getRequest() interface{} {
	return h.req
}
func (h *AcceptDataHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *AcceptDataHandler) getResponse() interface{} {
	return h.resp
}

func (h *AcceptDataHandler) run() {
	data := http_model.AcceptDataRequest{}
	data = *h.req
	res, err := service.Data.AcceptData(h.ctx, data)
	if err != nil {
		logrus.Errorf("[ReviseOpinionHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "成功通过脚本"
	h.resp.Data = res
}

func (h *AcceptDataHandler) checkParam() error {
	return nil
}
