package handler

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetAllProjectHandler(ctx *gin.Context) {
	handler := newGetAllProjectHandler(ctx)
	BaseRun(handler)
}

func newGetAllProjectHandler(ctx *gin.Context) *GetAllProjectHandler {
	return &GetAllProjectHandler{
		req:  http_model.NewGetAllProjectRequest(),
		resp: http_model.NewGetAllProjectResponse(),
		ctx:  ctx,
	}
}

type GetAllProjectHandler struct {
	req  *http_model.GetAllProjectRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetAllProjectHandler) getRequest() interface{} {
	return h.req
}
func (h *GetAllProjectHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetAllProjectHandler) getResponse() interface{} {
	return h.resp
}
func (h *GetAllProjectHandler) run() {
	data, err := service.Project.GetAllProject(h.ctx, h.req.PageSize, h.req.PageNum)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[GetAllProjectHandler] error GetAllProjectHandler, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}

func (h *GetAllProjectHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	return nil
}
