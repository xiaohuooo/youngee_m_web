package handler

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/util"
)

func WrapWithdrawalRecordsHandler(ctx *gin.Context) {
	handler := newWithdrawalRecordsHandler(ctx)
	BaseRun(handler)
}

type WithdrawalRecordsHandler struct {
	ctx  *gin.Context
	req  *http_model.WithdrawalRecordsRequest
	resp *http_model.CommonResponse
}

func (w WithdrawalRecordsHandler) getContext() *gin.Context {
	return w.ctx
}

func (w WithdrawalRecordsHandler) getResponse() interface{} {
	return w.resp
}

func (w WithdrawalRecordsHandler) getRequest() interface{} {
	return w.req
}

func (w WithdrawalRecordsHandler) run() {
	condition := pack.HttpWithdrawRecordsRequestToCondition(w.req)
	data, err := db.GetWithdrawRecords(w.ctx, w.req.PageSize, w.req.PageNum, w.req, condition)
	if err != nil {
		logrus.WithContext(w.ctx).Errorf("[WithdrawalRecordsHandler] error GetWithdrawRecords, err:%+v", err)
		util.HandlerPackErrorResp(w.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	w.resp.Data = data
}

func (w WithdrawalRecordsHandler) checkParam() error {
	var errs []error
	if w.req.PageNum < 0 || w.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	w.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}

func newWithdrawalRecordsHandler(ctx *gin.Context) *WithdrawalRecordsHandler {
	return &WithdrawalRecordsHandler{
		ctx:  ctx,
		req:  http_model.NewWithdrawalRecordsRequest(),
		resp: http_model.NewWithdrawalRecordsResponse(),
	}
}
