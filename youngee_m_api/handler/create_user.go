package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapCreateUserHandler(ctx *gin.Context) {
	handler := newCreateUserHandler(ctx)
	BaseRun(handler)
}

func newCreateUserHandler(ctx *gin.Context) *CreateUserHandler {
	return &CreateUserHandler{
		req:  http_model.NewCreateUserRequest(),
		resp: http_model.NewCreateUserResponse(),
		ctx:  ctx,
	}
}

type CreateUserHandler struct {
	req  *http_model.CreateUserRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (c CreateUserHandler) getContext() *gin.Context {
	return c.ctx
}

func (c CreateUserHandler) getResponse() interface{} {
	return c.resp
}

func (c CreateUserHandler) getRequest() interface{} {
	return c.req
}

func (c CreateUserHandler) run() {
	toast, err := db.CreateUser(c.ctx, c.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[CreateUserHandler] call CreateUser err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("CreateUserHandler fail,req:%+v", c.req)
		return
	}
	if toast == "创建成功" {
		c.resp.Message = "创建成功"
	} else if toast == "手机号已存在，不能再次创建" {
		c.resp.Status = 1
	} else {
		c.resp.Status = 2
	}
}

func (c CreateUserHandler) checkParam() error {
	return nil
}
