package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetFinishNumberInfoHandler(ctx *gin.Context) {
	handler := newGetFinishNumberInfoHandler(ctx)
	BaseRun(handler)
}

func newGetFinishNumberInfoHandler(ctx *gin.Context) *GetFinishNumberInfoHandler {
	return &GetFinishNumberInfoHandler{
		req:  http_model.NewGetFinishNumberInfoRequest(),
		resp: http_model.NewGetFinishNumberInfoResponse(),
		ctx:  ctx,
	}
}

type GetFinishNumberInfoHandler struct {
	req  *http_model.GetFinishNumberInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetFinishNumberInfoHandler) getRequest() interface{} {
	return h.req
}
func (h *GetFinishNumberInfoHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetFinishNumberInfoHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetFinishNumberInfoHandler) run() {
	data := http_model.GetFinishNumberInfoRequest{}
	data = *h.req
	res, err := service.Number.GetFinishNumberInfo(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetFinishNumberInfoHandler] call GetFinishNumberInfo err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetFinishNumberInfo fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetFinishNumberInfoHandler) checkParam() error {
	return nil
}
