package handler

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetProjectRecordsHandler(ctx *gin.Context) {
	handler := newGetProductRecordsHandler(ctx)
	BaseRun(handler)
}

type GetProductRecordsHandler struct {
	ctx  *gin.Context
	req  *http_model.GetProjectRecordsRequest
	resp *http_model.CommonResponse
}

func (g GetProductRecordsHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetProductRecordsHandler) getResponse() interface{} {
	return g.resp
}

func (g GetProductRecordsHandler) getRequest() interface{} {
	return g.req
}

func (g GetProductRecordsHandler) run() {
	data, err := db.GetProjectRecords(g.ctx, g.req.UserID, g.req.PageSize, g.req.PageNum)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetProductRecordsHandler] error GetProjectRecords, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetProductRecordsHandler) checkParam() error {
	var errs []error
	if g.req.PageNum < 0 || g.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	g.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}

func newGetProductRecordsHandler(ctx *gin.Context) *GetProductRecordsHandler {
	return &GetProductRecordsHandler{
		ctx:  ctx,
		req:  http_model.NewGetProjectRecordsRequest(),
		resp: http_model.NewGetProjectRecordsResponse(),
	}
}
