package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapConfirmInvoiceHandler(ctx *gin.Context) {
	handler := newConfirmInvoiceHandler(ctx)
	BaseRun(handler)
}

type ConfirmInvoiceHandler struct {
	ctx  *gin.Context
	req  *http_model.ConfirmInvoiceRequest
	resp *http_model.CommonResponse
}

func (c ConfirmInvoiceHandler) getContext() *gin.Context {
	return c.ctx
}

func (c ConfirmInvoiceHandler) getResponse() interface{} {
	return c.resp
}

func (c ConfirmInvoiceHandler) getRequest() interface{} {
	return c.req
}

func (c ConfirmInvoiceHandler) run() {
	err := db.ConfirmInvoice(c.ctx, c.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[ConfirmInvoiceHandler] call ConfirmInvoice err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("ConfirmInvoice fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "确认开票成功"
}

func (c ConfirmInvoiceHandler) checkParam() error {
	return nil
}

func newConfirmInvoiceHandler(ctx *gin.Context) *ConfirmInvoiceHandler {
	return &ConfirmInvoiceHandler{
		ctx:  ctx,
		req:  http_model.NewConfirmInvoiceRequest(),
		resp: http_model.NewConfirmInvoiceResponse(),
	}
}
