package handler

import (
	"errors"
	"fmt"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/cstockton/go-conv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapSpecialTaskSettleListHandler(ctx *gin.Context) {
	handler := newSpecialTaskSettleListHandler(ctx)
	BaseRun(handler)
}

func newSpecialTaskSettleListHandler(ctx *gin.Context) *SpecialTaskSettleListHandler {
	return &SpecialTaskSettleListHandler{
		req:  http_model.NewSpecialTaskSettleListRequest(),
		resp: http_model.NewSpecialTaskSettleListResponse(),
		ctx:  ctx,
	}
}

type SpecialTaskSettleListHandler struct {
	req  *http_model.SpecialTaskSettleListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *SpecialTaskSettleListHandler) getRequest() interface{} {
	return h.req
}

func (h *SpecialTaskSettleListHandler) getContext() *gin.Context {
	return h.ctx
}

func (h *SpecialTaskSettleListHandler) getResponse() interface{} {
	return h.resp
}

func (h *SpecialTaskSettleListHandler) run() {
	conditions := pack.HttpSpecialTaskSettleListRequestToCondition(h.req)
	data, err := service.SpecialTask.GetSpecialTaskSettleList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}

func (h *SpecialTaskSettleListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.SettleStatus = util.IsNull(h.req.SettleStatus)
	if _, err := conv.Int64(h.req.SettleStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
