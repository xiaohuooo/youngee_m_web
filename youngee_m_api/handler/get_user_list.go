package handler

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
)

func WrapGetUserListHandler(ctx *gin.Context) {
	handler := newGetUserListHandler(ctx)
	BaseRun(handler)
}

func newGetUserListHandler(ctx *gin.Context) *GetUserListHandler {
	return &GetUserListHandler{
		req:  http_model.NewGetUserListRequest(),
		resp: http_model.NewGetUserListResponse(),
		ctx:  ctx,
	}
}

type GetUserListHandler struct {
	req  *http_model.UserListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetUserListHandler) getRequest() interface{} {
	return h.req
}
func (h *GetUserListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetUserListHandler) getResponse() interface{} {
	return h.resp
}
func (h *GetUserListHandler) run() {
	data, err := db.GetUserList(h.ctx, h.req.PageNum, h.req.PageSize)
	if err != nil {
		logrus.Errorf("[GetUserListHandler] GetUserList err:%+v\n", err)
		logrus.Info("GetUserList fail,req:%+v", h.req)
		return
	}
	h.resp.Data = data
}
func (h *GetUserListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	return nil
}
