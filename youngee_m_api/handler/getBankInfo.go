package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetBankInfoHandler(ctx *gin.Context) {
	handler := newGetBankInfoHandler(ctx)
	BaseRun(handler)
}

type GetBankInfo struct {
	ctx  *gin.Context
	req  *http_model.GetBankInfoRequest
	resp *http_model.CommonResponse
}

func (g GetBankInfo) getContext() *gin.Context {
	return g.ctx
}

func (g GetBankInfo) getResponse() interface{} {
	return g.resp
}

func (g GetBankInfo) getRequest() interface{} {
	return g.req
}

func (g GetBankInfo) run() {
	data, err := db.GetBankInfo(g.ctx, g.req)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetBankInfoHandler] error GetBankInfo, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data

}

func (g GetBankInfo) checkParam() error {
	return nil
}

func newGetBankInfoHandler(ctx *gin.Context) *GetBankInfo {
	return &GetBankInfo{
		ctx:  ctx,
		req:  http_model.NewGetBankInfoRequest(),
		resp: http_model.NewGetBankInfoResponse(),
	}
}
