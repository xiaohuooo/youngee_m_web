package handler

import (
	"errors"
	"fmt"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapTaskScriptListHandler(ctx *gin.Context) {
	handler := newTaskScriptListHandler(ctx)
	BaseRun(handler)
}

func newTaskScriptListHandler(ctx *gin.Context) *TaskScriptListHandler {
	return &TaskScriptListHandler{
		req:  http_model.NewTaskScriptListRequest(),
		resp: http_model.NewTaskScriptListResponse(),
		ctx:  ctx,
	}
}

type TaskScriptListHandler struct {
	req  *http_model.TaskScriptListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *TaskScriptListHandler) getRequest() interface{} {
	return h.req
}
func (h *TaskScriptListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *TaskScriptListHandler) getResponse() interface{} {
	return h.resp
}
func (h *TaskScriptListHandler) run() {
	conditions := pack.HttpTaskScriptListRequestToCondition(h.req)
	data, err := service.Project.GetTaskScriptList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *TaskScriptListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	h.req.StrategyId = util.IsNull(h.req.StrategyId)
	if _, err := conv.Int64(h.req.StrategyId); err != nil {
		errs = append(errs, err)
	}
	h.req.ScriptStatus = util.IsNull(h.req.ScriptStatus)
	if _, err := conv.Int64(h.req.ScriptStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
