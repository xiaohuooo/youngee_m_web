package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapCreateProductHandler(ctx *gin.Context) {
	handler := newCreateProductHandler(ctx)
	BaseRun(handler)
}

func newCreateProductHandler(ctx *gin.Context) *CreateProductHandler {
	return &CreateProductHandler{
		req:  http_model.NewCreateProductRequest(),
		resp: http_model.NewCreateProductResponse(),
		ctx:  ctx,
	}
}

type CreateProductHandler struct {
	req  *http_model.CreateProductRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *CreateProductHandler) getRequest() interface{} {
	return h.req
}
func (h *CreateProductHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *CreateProductHandler) getResponse() interface{} {
	return h.resp
}
func (h *CreateProductHandler) run() {
	data := http_model.CreateProductRequest{}
	data = *h.req
	fmt.Println("ProductId:", h.req.ProductId)
	enterpriseID := db.GetEnterpriseIDByProductID(h.ctx, h.req.ProductId)
	fmt.Println("enterpriseID:", enterpriseID)
	// 根据品牌名和商品名查询商品是否存在，若存在则更新，否则新增
	product, err := service.Product.FindByID(h.ctx, data.ProductId)
	if err != nil {
		logrus.Errorf("[CreateProductHandler] call FindByID err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("CreateProduct fail,req:%+v", h.req)
		return
	} else {
		if product != nil {
			// 该商品存在，更新
			fmt.Println("该商品存在，更新")
			fmt.Println("商品价格：", data.ProductPrice)
			data.ProductId = product.ProductID
			res, err := service.Product.Update(h.ctx, data, enterpriseID)
			if err != nil {
				logrus.Errorf("[CreateProductHandler] call Update err:%+v\n", err)
				util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
				log.Info("CreateProduct fail,req:%+v", h.req)
				return
			}
			h.resp.Message = "成功更新商品信息"
			h.resp.Data = res
		} else {
			// 商品不存在，新增
			fmt.Println("商品不存在，新增")
			fmt.Println("data.EnterpriseId:", data.EnterpriseId)
			res, err := service.Product.Create(h.ctx, data, data.EnterpriseId)
			if err != nil {
				logrus.Errorf("[CreateProductHandler] call Create err:%+v\n", err)
				util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
				log.Info("CreateProduct fail,req:%+v", h.req)
				return
			}
			h.resp.Message = "成功新增商品信息"
			h.resp.Data = res
		}
	}
}
func (h *CreateProductHandler) checkParam() error {
	return nil
}
