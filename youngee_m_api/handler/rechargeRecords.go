package handler

import (
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/util"
)

func WrapRechargeRecordsHandler(ctx *gin.Context) {
	handler := newRechargeRecordsHandler(ctx)
	BaseRun(handler)
}

type RechargeRecordsHandler struct {
	ctx  *gin.Context
	req  *http_model.GetRechargeRecordsRequest
	resp *http_model.CommonResponse
}

func (r RechargeRecordsHandler) getContext() *gin.Context {
	return r.ctx
}

func (r RechargeRecordsHandler) getResponse() interface{} {
	return r.resp
}

func (r RechargeRecordsHandler) getRequest() interface{} {
	return r.req
}

func (r RechargeRecordsHandler) run() {
	condition := pack.HttpRechargeRecordsRequestToCondition(r.req)
	data, err := db.GetRechargeRecords(r.ctx, r.req, condition)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[RechargeRecordsHandler] call GetRechargeRecords err:%+v\n", err)
		util.HandlerPackErrorResp(r.resp, consts.ErrorInternal, "")
		logrus.Info("GetRechargeRecords fail,req:%+v", r.req)
		return
	}
	r.resp.Data = data
}

func (r RechargeRecordsHandler) checkParam() error {
	var errs []error
	if r.req.PageNum < 0 || r.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	r.req.PageNum--
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}

func newRechargeRecordsHandler(ctx *gin.Context) *RechargeRecordsHandler {
	return &RechargeRecordsHandler{
		ctx:  ctx,
		req:  http_model.NewGetRechargeRecordsRequest(),
		resp: http_model.NewGetRechargeRecordsResponse(),
	}
}
