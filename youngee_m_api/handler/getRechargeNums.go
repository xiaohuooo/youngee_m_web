package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetRechargeNumsHandler(ctx *gin.Context) {
	handler := newGetRechargeNumsHandler(ctx)
	BaseRun(handler)
}

type GetRechargeNumsHandler struct {
	ctx  *gin.Context
	req  *http_model.GetRechargeNumsRequest
	resp *http_model.CommonResponse
}

func (g GetRechargeNumsHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetRechargeNumsHandler) getResponse() interface{} {
	return g.resp
}

func (g GetRechargeNumsHandler) getRequest() interface{} {
	return g.req
}

func (g GetRechargeNumsHandler) run() {
	data, err := db.GetRechargeNums(g.ctx)
	if err != nil {
		logrus.WithContext(g.ctx).Errorf("[GetRechargeNumsHandler] error GetRechargeNums, err:%+v", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	g.resp.Data = data
}

func (g GetRechargeNumsHandler) checkParam() error {
	return nil
}

func newGetRechargeNumsHandler(ctx *gin.Context) *GetRechargeNumsHandler {
	return &GetRechargeNumsHandler{
		ctx:  ctx,
		req:  http_model.NewGetRechargeNumsRequest(),
		resp: http_model.NewGetRechargeNumsResponse(),
	}
}
