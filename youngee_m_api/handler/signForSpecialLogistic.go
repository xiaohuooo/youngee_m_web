package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapSignForSpecialLogisticHandler(ctx *gin.Context) {
	handler := newSignForSpecialLogisticHandler(ctx)
	BaseRun(handler)
}

type SignForSpecialLogisticHandler struct {
	ctx  *gin.Context
	req  *http_model.SignForSpecialLogisticRequest
	resp *http_model.CommonResponse
}

func (s SignForSpecialLogisticHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SignForSpecialLogisticHandler) getResponse() interface{} {
	return s.resp
}

func (s SignForSpecialLogisticHandler) getRequest() interface{} {
	return s.req
}

func (s SignForSpecialLogisticHandler) run() {
	data := http_model.SignForSpecialLogisticRequest{}
	data = *s.req
	err := service.Logistics.SignForSpecialLogistic(s.ctx, data)
	if err != nil {
		logrus.Errorf("[SignForSpecialLogisticHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, "")
		logrus.Info("SignForSpecialLogisticHandler fail,req:%+v", s.req)
		return
	}
	s.resp.Message = "物流状态更换成功"
}

func (s SignForSpecialLogisticHandler) checkParam() error {
	return nil
}

func newSignForSpecialLogisticHandler(ctx *gin.Context) *SignForSpecialLogisticHandler {
	return &SignForSpecialLogisticHandler{
		ctx:  ctx,
		req:  http_model.NewSignForSpecialLogisticRequest(),
		resp: http_model.NewSignForSpecialLogisticResponse(),
	}
}
