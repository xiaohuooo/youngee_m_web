package handler

import (
	"errors"
	"fmt"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/pack"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/caixw/lib.go/conv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapTaskDefaultDataListHandler(ctx *gin.Context) {
	handler := newTaskDefaultDataListHandler(ctx)
	BaseRun(handler)
}

func newTaskDefaultDataListHandler(ctx *gin.Context) *TaskDefaultDataListHandler {
	return &TaskDefaultDataListHandler{
		req:  http_model.NewTaskDefaultDataListRequest(),
		resp: http_model.NewTaskDefaultDataListResponse(),
		ctx:  ctx,
	}
}

type TaskDefaultDataListHandler struct {
	req  *http_model.TaskDefaultDataListRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *TaskDefaultDataListHandler) getRequest() interface{} {
	return h.req
}
func (h *TaskDefaultDataListHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *TaskDefaultDataListHandler) getResponse() interface{} {
	return h.resp
}
func (h *TaskDefaultDataListHandler) run() {
	conditions := pack.HttpTaskDefaultDataListRequestToCondition(h.req)
	data, err := service.Default.GetTaskDefaultDataList(h.ctx, h.req.ProjectId, h.req.PageSize, h.req.PageNum, conditions)
	if err != nil {
		logrus.WithContext(h.ctx).Errorf("[TaskLogisticsListHandler] error GetProjectTaskList, err:%+v", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	h.resp.Data = data
}
func (h *TaskDefaultDataListHandler) checkParam() error {
	var errs []error
	if h.req.PageNum < 0 || h.req.PageSize <= 0 {
		errs = append(errs, errors.New("page param error"))
	}
	h.req.PageNum--
	h.req.ProjectId = util.IsNull(h.req.ProjectId)
	if _, err := conv.Int64(h.req.ProjectId); err != nil {
		errs = append(errs, err)
	}
	h.req.StrategyId = util.IsNull(h.req.StrategyId)
	if _, err := conv.Int64(h.req.StrategyId); err != nil {
		errs = append(errs, err)
	}
	h.req.DefaultStatus = util.IsNull(h.req.DefaultStatus)
	if _, err := conv.Int64(h.req.DefaultStatus); err != nil {
		errs = append(errs, err)
	}
	if len(errs) != 0 {
		return fmt.Errorf("check param errs:%+v", errs)
	}
	return nil
}
