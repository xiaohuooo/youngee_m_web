package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetCodeUrlHandler(ctx *gin.Context) {
	handler := newGetCodeUrlHandler(ctx)
	BaseRun(handler)
}

type getCodeUrlHandler struct {
	ctx  *gin.Context
	req  *http_model.GetCodeUrlRequest
	resp *http_model.CommonResponse
}

func (g getCodeUrlHandler) getContext() *gin.Context {
	return g.ctx
}

func (g getCodeUrlHandler) getResponse() interface{} {
	return g.resp
}

func (g getCodeUrlHandler) getRequest() interface{} {
	return g.req
}

func (g getCodeUrlHandler) run() {
	tradeId := util.GetRandomString(32)
	if g.req.Type == 1 {
		data, err := service.NativeApiServicePrepay(tradeId, g.req.Amount)
		if err != nil {
			logrus.WithContext(g.ctx).Errorf("[ConfirmWithdrawalHandler] error ConfirmWithdrawal, err:%+v", err)
			util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, consts.DefaultToast)
			return
		}
		codeUrlData := new(http_model.CodeUrl)
		codeUrlData.CodeUrl = data
		codeUrlData.TradeId = tradeId
		g.resp.Data = codeUrlData
	}
}

func (g getCodeUrlHandler) checkParam() error {
	return nil
}

func newGetCodeUrlHandler(ctx *gin.Context) *getCodeUrlHandler {
	return &getCodeUrlHandler{
		ctx:  ctx,
		req:  http_model.NewGetCodeUrlRequest(),
		resp: http_model.NewGetCodeUrlResponse(),
	}
}
