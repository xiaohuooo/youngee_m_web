package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetSpecialReviewNumberHandler(ctx *gin.Context) {
	handler := newGetSpecialReviewNumberHandler(ctx)
	BaseRun(handler)
}

func newGetSpecialReviewNumberHandler(ctx *gin.Context) *GetSpecialReviewNumberHandler {
	return &GetSpecialReviewNumberHandler{
		req:  http_model.NewGetSpecialReviewNumberRequest(),
		resp: http_model.NewGetSpecialReviewNumberResponse(),
		ctx:  ctx,
	}
}

type GetSpecialReviewNumberHandler struct {
	req  *http_model.GetSpecialReviewNumberRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetSpecialReviewNumberHandler) getRequest() interface{} {
	return h.req
}
func (h *GetSpecialReviewNumberHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetSpecialReviewNumberHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetSpecialReviewNumberHandler) run() {
	data := http_model.GetSpecialReviewNumberRequest{}
	data = *h.req
	res, err := service.Number.GetSpecialReviewNumber(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetSpecialReviewNumberHandler] call GetSpecialReviewNumber err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetSpecialReviewNumber fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetSpecialReviewNumberHandler) checkParam() error {
	return nil
}
