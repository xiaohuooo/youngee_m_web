package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapSettleSecTaskHandler(ctx *gin.Context) {
	handler := newSettleSecTaskHandler(ctx)
	BaseRun(handler)
}

type SettleSecTask struct {
	ctx  *gin.Context
	req  *http_model.SettleSecTaskRequest
	resp *http_model.CommonResponse
}

func (c SettleSecTask) getContext() *gin.Context {
	return c.ctx
}

func (c SettleSecTask) getResponse() interface{} {
	return c.resp
}

func (c SettleSecTask) getRequest() interface{} {
	return c.req
}

func (c SettleSecTask) run() {
	//enterpriseID := middleware.GetSessionAuth(c.ctx).EnterpriseID
	data := http_model.SettleSecTaskRequest{}
	data = *c.req
	res, err := service.SelectionTask.Settle(c.ctx, data.EnterpriseId, data)
	if err != nil {
		logrus.Errorf("[SettleSecTask] call SettleSecTask err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("SettleSecTask fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "成功添加发货信息"
	c.resp.Data = res
}

func (c SettleSecTask) checkParam() error {
	return nil
}

func newSettleSecTaskHandler(ctx *gin.Context) *SettleSecTask {
	return &SettleSecTask{
		ctx:  ctx,
		req:  http_model.NewSettleSecTaskRequest(),
		resp: http_model.NewSettleSecTaskResponse(),
	}
}
