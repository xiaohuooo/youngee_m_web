package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetServiceChargeHandler(ctx *gin.Context) {
	handler := newGetServiceChargeHandler(ctx)
	BaseRun(handler)
}

func newGetServiceChargeHandler(ctx *gin.Context) *GetServiceChargeHandler {
	return &GetServiceChargeHandler{
		req:  http_model.NewGetServiceChargeRequest(),
		resp: http_model.NewGetServiceChargeResponse(),
		ctx:  ctx,
	}
}

type GetServiceChargeHandler struct {
	req  *http_model.GetServiceChargeRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetServiceChargeHandler) getRequest() interface{} {
	return h.req
}
func (h *GetServiceChargeHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetServiceChargeHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetServiceChargeHandler) run() {
	data := http_model.GetServiceChargeRequest{}
	data = *h.req
	res, err := service.Project.GetServiceCharge(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetServiceChargeHandler] call GetServiceCharge err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetServiceCharge fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetServiceChargeHandler) checkParam() error {
	return nil
}
