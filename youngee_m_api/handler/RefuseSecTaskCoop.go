package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapRefuseSecTaskCoopHandler(ctx *gin.Context) {
	handler := newRefuseSecTaskCoopHandler(ctx)
	BaseRun(handler)
}

type RefuseSecTaskCoop struct {
	ctx  *gin.Context
	req  *http_model.RefuseSecTaskCoopRequest
	resp *http_model.CommonResponse
}

func (c RefuseSecTaskCoop) getContext() *gin.Context {
	return c.ctx
}

func (c RefuseSecTaskCoop) getResponse() interface{} {
	return c.resp
}

func (c RefuseSecTaskCoop) getRequest() interface{} {
	return c.req
}

func (c RefuseSecTaskCoop) run() {
	data := http_model.RefuseSecTaskCoopRequest{}
	data = *c.req
	//auth := middleware.GetSessionAuth(c.ctx)
	//enterpriseID := auth.EnterpriseID
	res, err := service.SelectionTask.RefuseCoop(c.ctx, data)
	if err != nil {
		logrus.Errorf("[RefuseSecTaskCoop] call RefuseSecTaskCoop err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("RefuseSecTaskCoop fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "成功拒绝选品任务"
	c.resp.Data = res
}

func (c RefuseSecTaskCoop) checkParam() error {
	return nil
}

func newRefuseSecTaskCoopHandler(ctx *gin.Context) *RefuseSecTaskCoop {
	return &RefuseSecTaskCoop{
		ctx:  ctx,
		req:  http_model.NewRefuseSecTaskCoopRequest(),
		resp: http_model.NewRefuseSecTaskCoopResponse(),
	}
}
