package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapUpdateProjectHandler(ctx *gin.Context) {
	handler := newUpdateProjectHandler(ctx)
	BaseRun(handler)
}

func newUpdateProjectHandler(ctx *gin.Context) *UpdateProjectHandler {
	return &UpdateProjectHandler{
		req:  http_model.NewUpdateProjectRequest(),
		resp: http_model.NewUpdateProjectResponse(),
		ctx:  ctx,
	}
}

type UpdateProjectHandler struct {
	req  *http_model.UpdateProjectRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *UpdateProjectHandler) getRequest() interface{} {
	return h.req
}
func (h *UpdateProjectHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *UpdateProjectHandler) getResponse() interface{} {
	return h.resp
}
func (h *UpdateProjectHandler) run() {
	data := http_model.UpdateProjectRequest{}
	data = *h.req
	res, err := service.Project.Update(h.ctx, data, data.EnterpriseID)
	if err != nil {
		logrus.Errorf("[UpdateProjectHandler] call Create err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		logrus.Info("UpdateProject fail,req:%+v", h.req)
		return
	}
	h.resp.Message = "项目更新成功"
	h.resp.Data = res
}

func (h *UpdateProjectHandler) checkParam() error {
	return nil
}
