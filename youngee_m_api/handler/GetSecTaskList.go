package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetSecTaskListHandler(ctx *gin.Context) {
	handler := newGetSecTaskListHandler(ctx)
	BaseRun(handler)
}

type GetSecTaskList struct {
	ctx  *gin.Context
	req  *http_model.GetSecTaskListRequest
	resp *http_model.CommonResponse
}

func (c GetSecTaskList) getContext() *gin.Context {
	return c.ctx
}

func (c GetSecTaskList) getResponse() interface{} {
	return c.resp
}

func (c GetSecTaskList) getRequest() interface{} {
	return c.req
}

func (c GetSecTaskList) run() {
	data := http_model.GetSecTaskListRequest{}
	data = *c.req
	//auth := middleware.GetSessionAuth(c.ctx)
	//enterpriseID := auth.EnterpriseID
	res, err := service.SelectionTask.GetList(c.ctx, data)
	if err != nil {
		logrus.Errorf("[GetSecTaskList] call GetSecTaskList err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("GetSecTaskList fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "成功查询选品任务"
	c.resp.Data = res
}

func (c GetSecTaskList) checkParam() error {
	return nil
}

func newGetSecTaskListHandler(ctx *gin.Context) *GetSecTaskList {
	return &GetSecTaskList{
		ctx:  ctx,
		req:  http_model.NewGetSecTaskListRequest(),
		resp: http_model.NewGetSecTaskListResponse(),
	}
}
