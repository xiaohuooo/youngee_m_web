package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetLinkNumberInfoHandler(ctx *gin.Context) {
	handler := newGetLinkNumberInfoHandler(ctx)
	BaseRun(handler)
}

func newGetLinkNumberInfoHandler(ctx *gin.Context) *GetLinkNumberInfoHandler {
	return &GetLinkNumberInfoHandler{
		req:  http_model.NewGetLinkNumberInfoRequest(),
		resp: http_model.NewGetLinkNumberInfoResponse(),
		ctx:  ctx,
	}
}

type GetLinkNumberInfoHandler struct {
	req  *http_model.GetLinkNumberInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetLinkNumberInfoHandler) getRequest() interface{} {
	return h.req
}
func (h *GetLinkNumberInfoHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetLinkNumberInfoHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetLinkNumberInfoHandler) run() {
	data := http_model.GetLinkNumberInfoRequest{}
	data = *h.req
	res, err := service.Number.GetLinkNumberInfo(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetLinkNumberInfoHandler] call GetLinkNumberInfo err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetLinkNumberInfo fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetLinkNumberInfoHandler) checkParam() error {
	return nil
}
