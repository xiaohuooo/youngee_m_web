package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapCreateSecTaskLogisticsHandler(ctx *gin.Context) {
	handler := newCreateSecTaskLogisticsHandler(ctx)
	BaseRun(handler)
}

type CreateSecTaskLogistics struct {
	ctx  *gin.Context
	req  *http_model.CreateSecTaskLogisticsRequest
	resp *http_model.CommonResponse
}

func (c CreateSecTaskLogistics) getContext() *gin.Context {
	return c.ctx
}

func (c CreateSecTaskLogistics) getResponse() interface{} {
	return c.resp
}

func (c CreateSecTaskLogistics) getRequest() interface{} {
	return c.req
}

func (c CreateSecTaskLogistics) run() {
	data := http_model.CreateSecTaskLogisticsRequest{}
	data = *c.req
	res, err := service.SecLogistics.Create(c.ctx, data)
	if err != nil {
		logrus.Errorf("[CreateSecTaskLogistics] call CreateSecTaskLogistics err:%+v\n", err)
		util.HandlerPackErrorResp(c.resp, consts.ErrorInternal, "")
		logrus.Info("CreateSecTaskLogistics fail,req:%+v", c.req)
		return
	}
	c.resp.Message = "成功添加发货信息"
	c.resp.Data = res
}

func (c CreateSecTaskLogistics) checkParam() error {
	return nil
}

func newCreateSecTaskLogisticsHandler(ctx *gin.Context) *CreateSecTaskLogistics {
	return &CreateSecTaskLogistics{
		ctx:  ctx,
		req:  http_model.NewCreateSecTaskLogisticsRequest(),
		resp: http_model.NewCreateSecTaskLogisticsResponse(),
	}
}
