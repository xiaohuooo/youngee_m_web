package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapPlatformAccInfoHandler(ctx *gin.Context) {
	handler := newPlatformAccInfo(ctx)
	BaseRun(handler)
}

func newPlatformAccInfo(ctx *gin.Context) *PlatformAccInfo {
	return &PlatformAccInfo{
		req:  http_model.NewPlatformAccInfoRequest(),
		resp: http_model.NewPlatformAccInfoResponse(),
		ctx:  ctx,
	}
}

type PlatformAccInfo struct {
	req  *http_model.PlatformAccInfoRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (p PlatformAccInfo) getContext() *gin.Context {
	return p.ctx
}

func (p PlatformAccInfo) getResponse() interface{} {
	return p.resp
}

func (p PlatformAccInfo) getRequest() interface{} {
	return p.req
}

func (p PlatformAccInfo) run() {
	data, err := db.PlatformAccInfo(p.ctx, p.req.TalentId)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[PlatformAccInfoHandler] call PlatformAccInfo err:%+v\n", err)
		util.HandlerPackErrorResp(p.resp, consts.ErrorInternal, "")
		logrus.Info("PlatformAccInfoHandler fail,req:%+v", p.req)
		return
	}
	p.resp.Data = data
}

func (p PlatformAccInfo) checkParam() error {
	return nil
}
