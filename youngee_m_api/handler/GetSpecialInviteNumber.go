package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetSpecialInviteNumberHandler(ctx *gin.Context) {
	handler := newGetSpecialInviteNumberHandler(ctx)
	BaseRun(handler)
}

func newGetSpecialInviteNumberHandler(ctx *gin.Context) *GetSpecialInviteNumberHandler {
	return &GetSpecialInviteNumberHandler{
		req:  http_model.NewGetSpecialInviteNumberRequest(),
		resp: http_model.NewGetSpecialInviteNumberResponse(),
		ctx:  ctx,
	}
}

type GetSpecialInviteNumberHandler struct {
	req  *http_model.GetSpecialInviteNumberRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetSpecialInviteNumberHandler) getRequest() interface{} {
	return h.req
}
func (h *GetSpecialInviteNumberHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetSpecialInviteNumberHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetSpecialInviteNumberHandler) run() {
	data := http_model.GetSpecialInviteNumberRequest{}
	data = *h.req
	res, err := service.Number.GetSpecialInviteNumber(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetSpecialInviteNumberHandler] call GetSpecialInviteNumber err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetSpecialInviteNumber fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetSpecialInviteNumberHandler) checkParam() error {
	return nil
}
