package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetSpecialDataNumberHandler(ctx *gin.Context) {
	handler := newGetSpecialDataNumberHandler(ctx)
	BaseRun(handler)
}

func newGetSpecialDataNumberHandler(ctx *gin.Context) *GetSpecialDataNumberHandler {
	return &GetSpecialDataNumberHandler{
		req:  http_model.NewGetSpecialDataNumberRequest(),
		resp: http_model.NewGetSpecialDataNumberResponse(),
		ctx:  ctx,
	}
}

type GetSpecialDataNumberHandler struct {
	req  *http_model.GetSpecialDataNumberRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func (h *GetSpecialDataNumberHandler) getRequest() interface{} {
	return h.req
}
func (h *GetSpecialDataNumberHandler) getContext() *gin.Context {
	return h.ctx
}
func (h *GetSpecialDataNumberHandler) getResponse() interface{} {
	return h.resp
}

func (h *GetSpecialDataNumberHandler) run() {
	data := http_model.GetSpecialDataNumberRequest{}
	data = *h.req
	res, err := service.Number.GetSpecialDataNumber(h.ctx, data)
	if err != nil {
		logrus.Errorf("[GetSpecialDataNumberHandler] call GetSpecialDataNumber err:%+v\n", err)
		util.HandlerPackErrorResp(h.resp, consts.ErrorInternal, "")
		log.Info("GetSpecialDataNumber fail,req:%+v", h.req)
		return
	}
	h.resp.Message = ""
	h.resp.Data = res
}

func (h *GetSpecialDataNumberHandler) checkParam() error {
	return nil
}
