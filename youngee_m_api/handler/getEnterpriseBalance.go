package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapGetEnterpriseBalanceHandler(ctx *gin.Context) {
	handler := newGetEnterpriseBalanceHandler(ctx)
	BaseRun(handler)
}

type GetEnterpriseBalanceHandler struct {
	ctx  *gin.Context
	req  *http_model.GetEnterPriseBalanceRequest
	resp *http_model.CommonResponse
}

func (g GetEnterpriseBalanceHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetEnterpriseBalanceHandler) getResponse() interface{} {
	return g.resp
}

func (g GetEnterpriseBalanceHandler) getRequest() interface{} {
	return g.req
}

func (g GetEnterpriseBalanceHandler) run() {
	data, err := db.GetEnterpriseBalance(g.ctx, g.req.EnterPriseId)
	if err != nil {
		logrus.Errorf("[GetEnterpriseBalanceHandler] call Show err:%+v\n", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, "")
		logrus.Info("GetEnterpriseBalanceHandler fail,req:%+v", g.req)
		return
	}
	g.resp.Data = data
}

func (g GetEnterpriseBalanceHandler) checkParam() error {
	return nil
}

func newGetEnterpriseBalanceHandler(ctx *gin.Context) *GetEnterpriseBalanceHandler {
	return &GetEnterpriseBalanceHandler{
		ctx:  ctx,
		req:  http_model.NewEnterpriseBalanceRequest(),
		resp: http_model.NewGetEnterpriseBalanceResponse(),
	}
}
