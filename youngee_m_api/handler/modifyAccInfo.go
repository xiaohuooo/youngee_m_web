package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/db"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func WrapModifyAccInfoHandler(ctx *gin.Context) {
	handler := newModifyAccInfoHandler(ctx)
	BaseRun(handler)
}

type ModifyAccInfoHandler struct {
	ctx  *gin.Context
	req  *http_model.ModifyAccInfoRequest
	resp *http_model.CommonResponse
}

func (m ModifyAccInfoHandler) getContext() *gin.Context {
	return m.ctx
}

func (m ModifyAccInfoHandler) getResponse() interface{} {
	return m.resp
}

func (m ModifyAccInfoHandler) getRequest() interface{} {
	return m.req
}

func (m ModifyAccInfoHandler) run() {
	err := db.ModifyAccInfo(m.ctx, m.req)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[ModifyAccInfoHandler] call ModifyAccInfo err:%+v\n", err)
		util.HandlerPackErrorResp(m.resp, consts.ErrorInternal, "")
		logrus.Info("ModifyAccInfo fail,req:%+v", m.req)
		return
	}
	m.resp.Message = "更新成功"
}

func (m ModifyAccInfoHandler) checkParam() error {
	return nil
}

func newModifyAccInfoHandler(ctx *gin.Context) *ModifyAccInfoHandler {
	return &ModifyAccInfoHandler{
		ctx:  ctx,
		req:  http_model.NewModifyAccInfoRequest(),
		resp: http_model.NewModifyAccInfoResponse(),
	}
}
