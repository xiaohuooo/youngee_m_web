package handler

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func WrapGetWxQRCodeHandler(ctx *gin.Context) {
	handler := newGetWxQRCodeHandler(ctx)
	BaseRun(handler)
}

type GetWxQRCodeHandler struct {
	ctx  *gin.Context
	req  *http_model.GetWxQRCodeRequest
	resp *http_model.CommonResponse
}

func (q GetWxQRCodeHandler) getContext() *gin.Context {
	return q.ctx
}

func (q GetWxQRCodeHandler) getResponse() interface{} {
	return q.resp
}

func (q GetWxQRCodeHandler) getRequest() interface{} {
	return q.req
}

func (q GetWxQRCodeHandler) run() {
	req := http_model.GetWxQRCodeRequest{}
	req = *q.req
	data, err := service.QrCode.GetWxQrCode(q.ctx, req.Scene, req.Page)
	if err != nil {
		logrus.WithContext(q.ctx).Errorf("[GetWxQRCodeHandler] error GetWxQrCode, err:%+v", err)
		util.HandlerPackErrorResp(q.resp, consts.ErrorInternal, consts.DefaultToast)
		return
	}
	q.resp.Data = data
}

func (q GetWxQRCodeHandler) checkParam() error {
	return nil
}

func newGetWxQRCodeHandler(ctx *gin.Context) *GetWxQRCodeHandler {
	return &GetWxQRCodeHandler{
		ctx:  ctx,
		req:  http_model.NewGetWxQRCodeRequest(),
		resp: http_model.NewGetWxQRCodeResponse(),
	}
}
