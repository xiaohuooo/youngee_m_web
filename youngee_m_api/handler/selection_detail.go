package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapSelectionDetailHandler(ctx *gin.Context) {
	handler := newSelectionDetail(ctx)
	BaseRun(handler)
}

type SelectionDetailHandler struct {
	ctx  *gin.Context
	req  *http_model.SelectionDetailRequest
	resp *http_model.CommonResponse
}

func (s SelectionDetailHandler) getContext() *gin.Context {
	return s.ctx
}

func (s SelectionDetailHandler) getResponse() interface{} {
	return s.resp
}

func (s SelectionDetailHandler) getRequest() interface{} {
	return s.req
}

func (s SelectionDetailHandler) run() {
	//enterpriseID := middleware.GetSessionAuth(s.ctx).EnterpriseID
	res, err := service.Selection.GetSelectionDetail(s.ctx, s.req.SelectionId, s.req.EnterpriseId)
	if err != nil {
		logrus.Errorf("[GetSelectionDetail] call Show err:%+v\n", err)
		util.HandlerPackErrorResp(s.resp, consts.ErrorInternal, "")
		logrus.Info("GetSelectionDetail fail,req:%+v", s.req)
		return
	}
	s.resp.Message = "成功查询项目"
	s.resp.Data = res
}

func (s SelectionDetailHandler) checkParam() error {
	return nil
}

func newSelectionDetail(ctx *gin.Context) *SelectionDetailHandler {
	return &SelectionDetailHandler{
		ctx:  ctx,
		req:  http_model.NewSelectionDetailRequest(),
		resp: http_model.NewSelectionDetailResponse(),
	}
}
