package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
	"youngee_m_api/service"
	"youngee_m_api/util"
)

func WrapGetTaskRecordHandler(ctx *gin.Context) {
	handler := newGetTaskRecordHandler(ctx)
	BaseRun(handler)
}

type GetTaskRecordHandler struct {
	req  *http_model.GetTaskRecordRequest
	resp *http_model.CommonResponse
	ctx  *gin.Context
}

func newGetTaskRecordHandler(ctx *gin.Context) *GetTaskRecordHandler {
	return &GetTaskRecordHandler{
		req:  http_model.NewGetTaskRecordRequest(),
		resp: http_model.NewGetTaskRecordResponse(),
		ctx:  ctx,
	}
}

func (g GetTaskRecordHandler) getContext() *gin.Context {
	return g.ctx
}

func (g GetTaskRecordHandler) getResponse() interface{} {
	return g.resp
}

func (g GetTaskRecordHandler) getRequest() interface{} {
	return g.req
}

func (g GetTaskRecordHandler) run() {
	fmt.Println(g.req)
	data, err := service.User.GetTaskRecord(g.ctx, g.req.TalentId)
	if err != nil {
		// 数据库查询失败，返回5001
		logrus.Errorf("[GetTaskRecordHandler] call GetTaskRecord err:%+v\n", err)
		util.HandlerPackErrorResp(g.resp, consts.ErrorInternal, "")
		logrus.Info("GetTaskRecordHandler fail,req:%+v", g.req)
		return
	}
	g.resp.Data = data
}

func (g GetTaskRecordHandler) checkParam() error {
	return nil
}
