package consts

var projectTypeMap = map[int64]string{
	1:  "3C及电器",
	2:  "食品饮料",
	3:  "服装配饰",
	4:  "医疗",
	5:  "房地产",
	6:  "家居建材",
	7:  "教育培训",
	8:  "出行旅游",
	9:  "游戏",
	10: "互联网平台",
	11: "汽车",
	12: "文体娱乐",
	13: "影视传媒",
	14: "线下店铺",
	15: "软件服务",
	16: "美妆",
	17: "母婴宠物",
	18: "日化",
	19: "其他",
}

func GetProjectTypes(projectType int64) string {
	toast, contain := projectTypeMap[projectType]
	if contain {
		return toast
	}
	return "未知"
}
