package consts

var projectStatusMap = map[int64]string{
	1:  "创建中",
	2:  "待审核",
	3:  "审核通过",
	4:  "招募中",
	5:  "招募完毕",
	6:  "待支付",
	7:  "已支付",
	8:  "失效",
	9:  "执行中",
	10: "已结案",
}

func GetProjectStatus(status int64) string {
	toast, contain := projectStatusMap[status]
	if contain {
		return toast
	}
	return "未知"
}

var ProjectPlatformMap = map[int64]string{
	1: "红book",
	2: "抖音",
	3: "微博",
	4: "快手",
	5: "b站",
	6: "大众点评",
	7: "知乎",
}

var TaskTypeMap = map[int64]string{
	1: "实体商品寄拍",
	2: "虚拟产品测评",
	3: "线下探店打卡",
	4: "悬赏任务",
	5: "纯佣带货",
}

var ContentTypeMap = map[int]string{
	1: "图文",
	2: "视频",
	3: "直播",
}

var ReasonMap = map[int]string{
	1: "成团-新用户",
	2: "成团-老用户",
	3: "成单-申请成功",
	4: "成单-结案完毕",
}

func GetReason(status int) string {
	toast, contain := ReasonMap[status]
	if contain {
		return toast
	}
	return "未知"
}

func GetContentType(status int) string {
	toast, contain := ContentTypeMap[status]
	if contain {
		return toast
	}
	return "未知"
}

func GetTaskType(status int64) string {
	toast, contain := TaskTypeMap[status]
	if contain {
		return toast
	}
	return "未知"
}

func GetProjectPlatform(status int64) string {
	toast, contain := ProjectPlatformMap[status]
	if contain {
		return toast
	}
	return "未知"
}

func GetyoungeeType(status int64) string {
	toast, contain := ProjectPlatformMap[status]
	if contain {
		return toast
	}
	return "未知"
}

func GetTeamStage(status int64) string {
	toast, contain := ProjectPlatformMap[status]
	if contain {
		return toast
	}
	return "未知"
}

func GetPointIncome(status int64) string {
	toast, contain := ProjectPlatformMap[status]
	if contain {
		return toast
	}
	return "未知"
}

func GetMoneyIncome(status int64) string {
	toast, contain := ProjectPlatformMap[status]
	if contain {
		return toast
	}
	return "未知"
}

var ProjectFormMap = map[int64]string{
	1: "实体商品寄拍",
	2: "虚拟产品测评",
	3: "线下探店打卡",
	4: "素材微原创",
}

func GetProjectForm(status int64) string {
	toast, contain := ProjectFormMap[status]
	if contain {
		return toast
	}
	return "未知"
}

var ProjectContentTypeMap = map[int64]string{
	1: "图文",
	2: "视频",
}

func GetProjectContentType(status int64) string {
	toast, contain := ProjectContentTypeMap[status]
	if contain {
		return toast
	}
	return "未知"
}

var UserRoleTypeMap = map[string]string{
	"1": "超级管理员",
	"2": "普通管理员",
	"3": "企业用户",
}

func GetUserRoleType(status string) string {
	toast, contain := UserRoleTypeMap[status]
	if contain {
		return toast
	}
	return "未知"
}

var UserStateTypeMap = map[string]string{
	"0": "禁用",
	"1": "正常",
}

func GetUserStateType(status string) string {
	toast, contain := UserStateTypeMap[status]
	if contain {
		return toast
	}
	return "未知"
}

var CreatorIsBindAccount = map[int]string{
	0: "否",
	1: "是",
}

func GetCreatorIsBindAccountType(status int) string {
	toast, contain := CreatorIsBindAccount[status]
	if contain {
		return toast
	}
	return "未知"
}

var ProjectType = map[int64]string{
	1: "全流程项目",
	2: "专项项目",
}

func GetProjectType(project_type int64) string {
	toast, contain := ProjectType[project_type]
	if contain {
		return toast
	}
	return "未知"
}

var FeeForm = map[int64]string{
	1: "产品置换",
	2: "固定稿费",
	3: "自报价",
}

func GetFeeForm(fee_form int64) string {
	toast, contain := FeeForm[fee_form]
	if contain {
		return toast
	}
	return "未知"
}

var TaskStage = map[int]string{
	1:  "已报名",
	2:  "申请成功",
	3:  "申请失败",
	4:  "待发货",
	5:  "已发货",
	6:  "已签收",
	7:  "待传脚本",
	8:  "脚本待审",
	9:  "待传初稿",
	10: "初稿待审",
	11: "待传链接",
	12: "链接待审",
	13: "待传数据",
	14: "数据待审",
	15: "已结案",
	16: "解约",
}

func GetTaskStage(task_stage int) string {
	toast, contain := TaskStage[task_stage]
	if contain {
		return toast
	}
	return "未知"
}

var BreakType = map[int]string{
	1: "未传脚本",
	2: "未传初稿",
	3: "未传链接",
	4: "未传数据",
}

func GetBreakType(breakType int) string {
	toast, contain := BreakType[breakType]
	if contain {
		return toast
	}
	return "未知"
}

var HandleResult = map[int]string{
	4: "驳回",
	5: "解约",
}

func GetHandleResult(handleResult int) string {
	toast, contain := HandleResult[handleResult]
	if contain {
		return toast
	}
	return "未知"
}

var RechargeMethod = map[int64]string{
	1: "对公转账",
	2: "支付宝",
	3: "微信",
}

func GetRechargeMethod(method int64) string {
	toast, contain := RechargeMethod[method]
	if contain {
		return toast
	}
	return "未知"
}

var KD100Flags = map[string]string{
	"圆通":     "yuantong",
	"韵达":     "yunda",
	"申通":     "shentong",
	"中通":     "zhongtong",
	"顺丰":     "shunfeng",
	"极兔":     "jtexpress",
	"邮政":     "youzhengguonei",
	"EMS":    "ems",
	"京东":     "jd",
	"邮政标准快递": "youzhengbk",
	"丰网速运":   "fengwang",
	"德邦快递":   "debangkuaidi",
	"德邦":     "debangwuliu",
	"丹鸟":     "danniao",
	"飞豹快递":   "feibaokuaidi",
	"中通快运":   "zhongtongkuaiyun",
	"安能快运":   "annengwuliu",
	"百世快递":   "huitongkuaidi",
	"安得物流":   "annto",
	"跨越速运":   "kuayue",
	"特急送":    "lntjs",
	"宅急送":    "zhaijisong",
	"other":  "其它快递",
}

func GetKD(Kd string) string {
	toast, contain := KD100Flags[Kd]
	if contain {
		return toast
	}
	return "未知"
}

var TaskModel = map[int]string{
	1: "悬赏任务",
	2: "纯佣带货",
}

func GetTaskModel(task int) string {
	toast, contain := TaskModel[task]
	if contain {
		return toast
	}
	return "未知"
}

var SampleModel = map[int]string{
	1: "免费领样",
	2: "垫付领样",
}

func GetSampleModel(sample int) string {
	toast, contain := SampleModel[sample]
	if contain {
		return toast
	}
	return "未知"
}
