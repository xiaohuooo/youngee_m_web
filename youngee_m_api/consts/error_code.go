package consts

import "github.com/go-redis/redis/v8"

var RedisNil = redis.Nil
var errorCodeToastMap = map[int32]string{}

const DefaultToast = ""

const ErrorSuccess int32 = 0
const ErrorNotLogin int32 = 4001
const ErrorParamCheck int32 = 5001
const ErrorInternal int32 = 5001

func init() {
	errorCodeToastMap[ErrorSuccess] = "请求成功"
	errorCodeToastMap[ErrorNotLogin] = "请登录后操作"
	errorCodeToastMap[ErrorParamCheck] = "参数有误"
	errorCodeToastMap[ErrorInternal] = "请求错误"
}

func GetErrorToast(errorCode int32) string {
	toast, contain := errorCodeToastMap[errorCode]
	if contain {
		return toast
	}
	return "网络错误,请稍后再试"
}
