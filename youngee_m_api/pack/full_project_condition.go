package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpFullProjectRequestToCondition(req *http_model.FullProjectListRequest) *common_model.ProjectCondition {
	projectUpdated := conv.MustString(req.ProjectUpdated, "")
	projectName := req.ProjectName
	return &common_model.ProjectCondition{
		ProjectId:          req.ProjectId,
		ProjectName:        projectName,
		ProjectStatus:      conv.MustInt64(req.ProjectStatus, 0),
		ProjectPlatform:    conv.MustInt64(req.ProjectPlatform, 0),
		ProjectForm:        conv.MustInt64(req.ProjectForm, 0),
		ProjectContentType: conv.MustInt64(req.ProjectContentType, 0),
		ProjectUpdated:     projectUpdated,
	}
}
