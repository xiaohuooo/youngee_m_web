package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormSpecialTaskLinkInfoListToHttpSpecialTaskLinkPreviewList(gormSpecialTaskLinkInfos []*http_model.SpecialTaskLinkInfo) []*http_model.SpecialTaskLinkPreview {
	var httpProjectPreviews []*http_model.SpecialTaskLinkPreview
	for _, gormSpecialTaskLinkInfo := range gormSpecialTaskLinkInfos {
		httpSpecialTaskLinkPreview := MGormSpecialTaskLinkInfoToHttpSpecialTaskLinkPreview(gormSpecialTaskLinkInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpSpecialTaskLinkPreview)
	}
	return httpProjectPreviews
}

func MGormSpecialTaskLinkInfoToHttpSpecialTaskLinkPreview(SpecialTaskLinkInfo *http_model.SpecialTaskLinkInfo) *http_model.SpecialTaskLinkPreview {
	return &http_model.SpecialTaskLinkPreview{
		TaskID:           conv.MustString(SpecialTaskLinkInfo.TaskID, ""),
		PlatformNickname: conv.MustString(SpecialTaskLinkInfo.PlatformNickname, ""),
		FansCount:        conv.MustString(SpecialTaskLinkInfo.FansCount, ""),
		PhotoUrl:         SpecialTaskLinkInfo.PhotoUrl,
		LinkUrl:          SpecialTaskLinkInfo.LinkUrl,
		Phone:            SpecialTaskLinkInfo.Phone,
		ReviseOpinion:    SpecialTaskLinkInfo.ReviseOpinion,
		Submit:           conv.MustString(SpecialTaskLinkInfo.SubmitAt, "")[0:19],
		AgreeAt:          conv.MustString(SpecialTaskLinkInfo.AgreeAt, "")[0:19],
	}
}

func SpecialTaskLinkToTaskInfo(SpecialTaskLinks []*http_model.SpecialTaskLink) []*http_model.SpecialTaskLinkInfo {
	var SpecialTaskLinkInfos []*http_model.SpecialTaskLinkInfo
	for _, SpecialTaskLink := range SpecialTaskLinks {
		SpecialTaskLink := GetSpecialTaskLinkInfoStruct(SpecialTaskLink)
		SpecialTaskLinkInfos = append(SpecialTaskLinkInfos, SpecialTaskLink)
	}
	return SpecialTaskLinkInfos
}

func GetSpecialTaskLinkInfoStruct(SpecialTaskLink *http_model.SpecialTaskLink) *http_model.SpecialTaskLinkInfo {
	TalentPlatformInfoSnap := SpecialTaskLink.Talent.TalentPlatformInfoSnap
	TalentPersonalInfoSnap := SpecialTaskLink.Talent.TalentPersonalInfoSnap
	return &http_model.SpecialTaskLinkInfo{
		TaskID:           SpecialTaskLink.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		LinkId:           SpecialTaskLink.Link.LinkID,
		PhotoUrl:         SpecialTaskLink.Link.PhotoUrl,
		LinkUrl:          SpecialTaskLink.Link.LinkUrl,
		ReviseOpinion:    SpecialTaskLink.Link.ReviseOpinion,
		CreateAt:         SpecialTaskLink.Link.CreateAt,
		SubmitAt:         SpecialTaskLink.Link.SubmitAt,
		AgreeAt:          SpecialTaskLink.Link.AgreeAt,
		Phone:            conv.MustString(gjson.Get(TalentPersonalInfoSnap, "talent_phone_number"), ""),
		RejectAt:         SpecialTaskLink.Link.RejectAt,
		IsReview:         SpecialTaskLink.Link.IsReview,
	}
}
