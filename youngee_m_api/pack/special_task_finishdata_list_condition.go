package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpSpecialTaskFinishDataListRequestToCondition(req *http_model.SpecialTaskFinishDataListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
