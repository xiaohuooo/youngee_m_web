package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskTerminatedInfoListToHttpTaskTerminatedPreviewList(gormTaskTerminatedInfos []*http_model.TaskTerminatedInfo) []*http_model.TaskTerminatedPreview {
	var httpProjectPreviews []*http_model.TaskTerminatedPreview
	for _, gormTaskTerminatedInfo := range gormTaskTerminatedInfos {
		httpTaskTerminatedPreview := MGormTaskTerminatedInfoToHttpTaskTerminatedPreview(gormTaskTerminatedInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskTerminatedPreview)
	}
	return httpProjectPreviews
}

func MGormTaskTerminatedInfoToHttpTaskTerminatedPreview(TaskTerminatedInfo *http_model.TaskTerminatedInfo) *http_model.TaskTerminatedPreview {
	return &http_model.TaskTerminatedPreview{
		TaskID:            conv.MustString(TaskTerminatedInfo.TaskID, ""),
		ProjectID:         conv.MustString(TaskTerminatedInfo.ProjectID, ""),
		PlatformNickname:  conv.MustString(TaskTerminatedInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskTerminatedInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskTerminatedInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskTerminatedInfo.StrategyID, ""),
		AllPayment:        TaskTerminatedInfo.AllPayment,
		RealPayment:       TaskTerminatedInfo.RealPayment,
		HandleAt:          conv.MustString(TaskTerminatedInfo.HandleAt, ""),
		BreakType:         conv.MustString(TaskTerminatedInfo.BreakType, ""),
	}
}

func TaskTerminatedToTaskInfo(TaskTerminateds []*http_model.TaskTerminated) []*http_model.TaskTerminatedInfo {
	var TaskTerminatedInfos []*http_model.TaskTerminatedInfo
	for _, TaskTerminated := range TaskTerminateds {
		TaskTerminated1 := GetTerminatedInfoStruct(TaskTerminated)
		TaskTerminatedInfos = append(TaskTerminatedInfos, TaskTerminated1)
	}
	return TaskTerminatedInfos
}

func GetTerminatedInfoStruct(TaskTerminated *http_model.TaskTerminated) *http_model.TaskTerminatedInfo {
	TalentPlatformInfoSnap := TaskTerminated.Talent.TalentPlatformInfoSnap
	return &http_model.TaskTerminatedInfo{
		TaskID:           TaskTerminated.Talent.TaskId,
		ProjectID:        TaskTerminated.Talent.ProjectId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskTerminated.Talent.StrategyId,
		AllPayment:       TaskTerminated.Talent.AllPayment,
		RealPayment:      TaskTerminated.Talent.RealPayment,
		BreakType:        TaskTerminated.Default.BreakType,
		HandleAt:         conv.MustString(TaskTerminated.Default.HandleAt, "")[:19],
	}
}
