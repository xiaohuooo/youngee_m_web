package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpSpecialTaskScriptListRequestToCondition(req *http_model.SpecialTaskScriptListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		ScriptStatus:     conv.MustInt64(req.ScriptStatus, 0),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
