package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpTaskSketchListRequestToCondition(req *http_model.TaskSketchListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		SketchStatus:     conv.MustInt64(req.SketchStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
