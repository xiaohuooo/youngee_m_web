package pack

import (
	"github.com/tidwall/gjson"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"

	"github.com/caixw/lib.go/conv"
)

func GormSecTaskListToHttpSecTaskList(secTaskList []*gorm_model.YounggeeSecTaskInfo) []*http_model.SecTaskInfo {
	var resTaskTaskList []*http_model.SecTaskInfo
	for _, secTask := range secTaskList {
		secTaskH := GormSecTaskToHttpSecTask(secTask)
		resTaskTaskList = append(resTaskTaskList, secTaskH)
	}
	return resTaskTaskList
}

func GormSecTaskToHttpSecTask(secTask *gorm_model.YounggeeSecTaskInfo) *http_model.SecTaskInfo {
	TalentPlatformInfoSnap := secTask.TalentPlatformInfoSnap
	return &http_model.SecTaskInfo{
		PlatformNickname:   conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:          conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		HomePageCaptureUrl: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "home_page_capture_url"), ""),
		HomePageUrl:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "home_page_url"), ""),
		CreateDate:         secTask.CreateDate,
		SelectDate:         secTask.SelectDate,
	}
}
