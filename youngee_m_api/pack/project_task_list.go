package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

func MGormProjectTaskToHttpProjectTaskPreview(gormProjectTaskInfos []*http_model.ProjectTaskInfo) []*http_model.ProjectTaskPreview {
	var httpProjectPreviews []*http_model.ProjectTaskPreview
	for _, gormProjectTaskInfo := range gormProjectTaskInfos {
		httpProjectTaskPreview := GormFullProjectToHttpProjectTaskPreview(gormProjectTaskInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpProjectTaskPreview)
	}
	return httpProjectPreviews
}

func GormFullProjectToHttpProjectTaskPreview(projectTaskInfo *http_model.ProjectTaskInfo) *http_model.ProjectTaskPreview {
	createDate := conv.MustString(projectTaskInfo.CreateDate, "")
	createDate = createDate[0:19]
	return &http_model.ProjectTaskPreview{
		TaskId:             conv.MustString(projectTaskInfo.TaskID, ""),
		PlatformNickname:   conv.MustString(projectTaskInfo.PlatformNickname, ""),
		FansCount:          conv.MustString(projectTaskInfo.FansCount, ""),
		HomePageCaptureUrl: conv.MustString(projectTaskInfo.HomePageCaptureUrl, ""),
		StrategyId:         conv.MustString(projectTaskInfo.StrategyID, ""),
		TaskReward:         conv.MustString(projectTaskInfo.TaskReward, ""),
		AllPayment:         conv.MustString(projectTaskInfo.AllPayment, ""),
		HomePageUrl:        conv.MustString(projectTaskInfo.HomePageUrl, ""),
		TaskStatus:         conv.MustString(projectTaskInfo.TaskStatus, ""),
		CreateDate:         createDate,
	}
}

func TaskAccountToTaskInfo(taskAccounts []gorm_model.YoungeeTaskInfo) []*http_model.ProjectTaskInfo {
	var projectTasks []*http_model.ProjectTaskInfo
	for _, taskAccount := range taskAccounts {
		projectTask := GetTaskInfoStruct(taskAccount)
		projectTasks = append(projectTasks, projectTask)
	}
	return projectTasks
}

func GetTaskInfoStruct(taskAccount gorm_model.YoungeeTaskInfo) *http_model.ProjectTaskInfo {
	TalentPlatformInfoSnap := taskAccount.TalentPlatformInfoSnap
	//fmt.Printf("任务-账户关联 %+v", taskAccount)
	return &http_model.ProjectTaskInfo{
		TaskID:             taskAccount.TaskId,
		TaskReward:         taskAccount.TaskReward,
		AllPayment:         taskAccount.AllPayment,
		PlatformNickname:   conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		CreateDate:         taskAccount.CreateDate,
		HomePageUrl:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "home_page_url"), ""),
		HomePageCaptureUrl: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "home_page_capture_url"), ""),
		FansCount:          conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		TaskStatus:         string(rune(taskAccount.TaskStatus)),
		StrategyID:         taskAccount.StrategyId,
	}
}
