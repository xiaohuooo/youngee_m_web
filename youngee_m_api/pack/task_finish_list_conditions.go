package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpTaskFinishListRequestToCondition(req *http_model.TaskFinishListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:  req.ProjectId,
		StrategyId: conv.MustInt64(req.StrategyId, 0),
		// TaskId:           conv.MustString(req.TaskId),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
