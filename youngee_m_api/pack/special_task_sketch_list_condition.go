package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpSpecialTaskSketchListRequestToCondition(req *http_model.SpecialTaskSketchListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		SketchStatus:     conv.MustInt64(req.SketchStatus, 0),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
