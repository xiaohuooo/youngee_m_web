package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpSpecialTaskLinkListRequestToCondition(req *http_model.SpecialTaskLinkListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		LinkStatus:       conv.MustInt64(req.LinkStatus, 0),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
