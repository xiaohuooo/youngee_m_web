package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpTaskScriptListRequestToCondition(req *http_model.TaskScriptListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		ScriptStatus:     conv.MustInt64(req.ScriptStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
