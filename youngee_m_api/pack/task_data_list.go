package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskDataInfoListToHttpTaskDataPreviewList(gormTaskDataInfos []*http_model.TaskDataInfo) []*http_model.TaskDataPreview {
	var httpProjectPreviews []*http_model.TaskDataPreview
	for _, gormTaskDataInfo := range gormTaskDataInfos {
		httpTaskDataPreview := MGormTaskDataInfoToHttpTaskDataPreview(gormTaskDataInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskDataPreview)
	}
	return httpProjectPreviews
}

func MGormTaskDataInfoToHttpTaskDataPreview(TaskDataInfo *http_model.TaskDataInfo) *http_model.TaskDataPreview {
	//deliveryTime := conv.MustString(TaskDataInfo.DeliveryTime)
	//deliveryTime = deliveryTime[0:19]
	return &http_model.TaskDataPreview{
		TaskID:            conv.MustString(TaskDataInfo.TaskID, ""),
		PlatformNickname:  conv.MustString(TaskDataInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskDataInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskDataInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskDataInfo.StrategyID, ""),
		PlayNumber:        TaskDataInfo.PlayNumber,
		LikeNumber:        TaskDataInfo.LikeNumber,
		CommentNumber:     TaskDataInfo.CommentNumber,
		CollectNumber:     TaskDataInfo.CollectNumber,
		PhotoUrl:          TaskDataInfo.PhotoUrl,
		AllPayment:        TaskDataInfo.AllPayment,
		RealPayment:       TaskDataInfo.RealPayment,
		ReviseOpinion:     TaskDataInfo.ReviseOpinion,
		Submit:            conv.MustString(TaskDataInfo.SubmitAt, "")[0:19],
		AgreeAt:           conv.MustString(TaskDataInfo.AgreeAt, "")[0:19],
	}
}

func TaskDataToTaskInfo(TaskDatas []*http_model.TaskData) []*http_model.TaskDataInfo {
	var TaskDataInfos []*http_model.TaskDataInfo
	for _, TaskData := range TaskDatas {
		TaskData := GetDataInfoStruct(TaskData)
		TaskDataInfos = append(TaskDataInfos, TaskData)
	}
	return TaskDataInfos
}

func GetDataInfoStruct(TaskData *http_model.TaskData) *http_model.TaskDataInfo {
	TalentPlatformInfoSnap := TaskData.Talent.TalentPlatformInfoSnap
	return &http_model.TaskDataInfo{
		TaskID:           TaskData.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskData.Talent.StrategyId,
		DataId:           TaskData.Data.DataID,
		PlayNumber:       TaskData.Data.PlayNumber,
		LikeNumber:       TaskData.Data.LikeNumber,
		CommentNumber:    TaskData.Data.CommentNumber,
		CollectNumber:    TaskData.Data.CollectNumber,
		PhotoUrl:         TaskData.Data.PhotoUrl,
		AllPayment:       TaskData.Talent.AllPayment,
		RealPayment:      TaskData.Talent.RealPayment,
		ReviseOpinion:    TaskData.Data.ReviseOpinion,
		CreateAt:         TaskData.Data.CreateAt,
		SubmitAt:         TaskData.Data.SubmitAt,
		AgreeAt:          TaskData.Data.AgreeAt,
		RejectAt:         TaskData.Data.RejectAt,
		IsReview:         TaskData.Data.IsReview,
	}
}
