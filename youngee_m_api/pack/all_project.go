package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/http_model"
)

func MGormAllProjectToHttpAllProjectPreview(allProjectPreviews []*http_model.GetAllProjectPreview) []*http_model.GetAllProjectPreview {
	var httpProjectPreviews []*http_model.GetAllProjectPreview
	for _, projectInfo := range allProjectPreviews {
		httpProjectPreview := GormAllProjectToHttpAllProjectPreview(projectInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpProjectPreview)
	}
	return httpProjectPreviews
}
func GormAllProjectToHttpAllProjectPreview(projectInfo *http_model.GetAllProjectPreview) *http_model.GetAllProjectPreview {
	updatedTime := conv.MustString(projectInfo.ProjectUpdated, "")
	updatedTime = updatedTime[0:19]
	return &http_model.GetAllProjectPreview{
		EnterpriseID:   projectInfo.EnterpriseID,
		ProjectStatus:  consts.GetProjectStatus(conv.MustInt64(projectInfo.ProjectStatus, 0)),
		ProjectUpdated: updatedTime,
		ProjectId:      projectInfo.ProjectId,
		Phone:          projectInfo.Phone,
		Username:       projectInfo.Username,
	}
}
