package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpTaskDefaultReviewListRequestToCondition(req *http_model.TaskDefaultReviewListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		DefaultStatus:    conv.MustInt64(req.DefaultStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}

func HttpTaskDefaultDataListRequestToCondition(req *http_model.TaskDefaultDataListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		DefaultStatus:    conv.MustInt64(req.DefaultStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}

func HttpTaskTerminatingListRequestToCondition(req *http_model.TaskTerminatingListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		DefaultStatus:    conv.MustInt64(req.DefaultStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}

func HttpTaskTerminatedListRequestToCondition(req *http_model.TaskTerminatedListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		DefaultStatus:    conv.MustInt64(req.DefaultStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
