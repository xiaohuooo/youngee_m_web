package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

func MGormFullProjectToHttpFullProjectPreview(gormProjectInfos []*gorm_model.ProjectInfo) []*http_model.FullProjectPreview {
	var httpProjectPreviews []*http_model.FullProjectPreview
	for _, gormProjectInfo := range gormProjectInfos {
		httpProjectPreview := GormFullProjectToHttpFullProjectPreview(gormProjectInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpProjectPreview)
	}
	return httpProjectPreviews
}
func GormFullProjectToHttpFullProjectPreview(gormProjectInfo *gorm_model.ProjectInfo) *http_model.FullProjectPreview {
	updatedTime := conv.MustString(gormProjectInfo.UpdatedAt,"")
	updatedTime = updatedTime[0:19]
	return &http_model.FullProjectPreview{
		ProjectId: 			conv.MustString(gormProjectInfo.ProjectID, ""),
		ProjectName:        gormProjectInfo.ProjectName,
		EnterpriseID: 		gormProjectInfo.EnterpriseID,
		ProjectStatus:      consts.GetProjectStatus(gormProjectInfo.ProjectStatus),
		ProjectPlatform:    consts.GetProjectPlatform(gormProjectInfo.ProjectPlatform),
		ProjectForm:        consts.GetProjectForm(gormProjectInfo.ProjectForm),
		ProjectContentType: consts.GetProjectContentType(gormProjectInfo.ContentType),
		ProjectUpdated:     updatedTime,
	}
}
