package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

func MGormUserListToHttpUserListPreview(users []gorm_model.YounggeeUser) []*http_model.UserListPreview {
	var httpUserListPreviews []*http_model.UserListPreview
	for _, user := range users {
		httpUserListPreview := GormUserListToHttpUserListPreview(user)
		httpUserListPreviews = append(httpUserListPreviews, httpUserListPreview)
	}
	return httpUserListPreviews
}
func GormUserListToHttpUserListPreview(gormUser gorm_model.YounggeeUser) *http_model.UserListPreview {
	createdTime := conv.MustString(gormUser.CreatedAt, "")
	createdTime = createdTime[0:19]
	return &http_model.UserListPreview{
		User:      gormUser.User,
		Username:  gormUser.Username,
		Password:  gormUser.Password,
		Role:      consts.GetUserRoleType(gormUser.Role),
		Email:     gormUser.Email,
		Phone:     gormUser.Phone,
		UserState: consts.GetUserStateType(gormUser.UserState),
		CreatedAt: createdTime,
	}
}

func EnterpriseUserToEnterpriseUserData(enterpriseUsers []*http_model.EnterpriseUser) []*http_model.EnterpriseUserPreview {
	var enterpriseUserPreviews []*http_model.EnterpriseUserPreview
	for _, enterpriseUser := range enterpriseUsers {
		enterpriseUserPreview := GetEnterPriseUserStruct(enterpriseUser)
		enterpriseUserPreviews = append(enterpriseUserPreviews, enterpriseUserPreview)
	}
	return enterpriseUserPreviews
}

func GetEnterPriseUserStruct(enterpriseUser *http_model.EnterpriseUser) *http_model.EnterpriseUserPreview {
	createdAt := conv.MustString(enterpriseUser.YoungeeUser.CreatedAt, "")
	createdAt = createdAt[0:19]
	return &http_model.EnterpriseUserPreview{
		User:             enterpriseUser.Enterprise.EnterpriseID,
		UserID:           conv.MustString(enterpriseUser.Enterprise.UserID, ""),
		EnterpriseID:     enterpriseUser.Enterprise.EnterpriseID,
		Username:         enterpriseUser.YoungeeUser.Username,
		Balance:          enterpriseUser.Enterprise.Balance,
		AvailableBalance: enterpriseUser.Enterprise.AvailableBalance,
		FrozenBalance:    enterpriseUser.Enterprise.FrozenBalance,
		Phone:            enterpriseUser.YoungeeUser.Phone,
		CreatedAt:        createdAt,
	}
}

func TalentListToCreatorListData(talentList []gorm_model.YoungeeTalentInfo) []*http_model.CreatorListPreview {
	var CreatorListPreviews []*http_model.CreatorListPreview
	for _, talent := range talentList {
		talentPreview := GetCreatorListStruct(talent)
		CreatorListPreviews = append(CreatorListPreviews, talentPreview)
	}
	return CreatorListPreviews
}

func GetCreatorListStruct(talent gorm_model.YoungeeTalentInfo) *http_model.CreatorListPreview {
	createDate := conv.MustString(talent.CreateDate, "")
	createDate = createDate[0:19]
	return &http_model.CreatorListPreview{
		Id:               talent.ID,
		TalentWxNickname: talent.TalentWxNickname,
		IsBindAccount:    consts.GetCreatorIsBindAccountType(talent.IsBindAccount),
		CanWithDraw:      talent.Canwithdraw,
		TalentPhone:      talent.TalentPhoneNumber,
		CreateDate:       createDate,
	}
}
