package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpCreatorListRequestToCondition(req *http_model.CreatorListRequest) *common_model.CreatorListConditions {
	return &common_model.CreatorListConditions{
		Id:               req.Id,
		TalentWxNickname: req.TalentWxNickname,
		CreateDate:       req.CreateDate,
		InBlacklist:      req.InBlacklist,
	}
}

func HttpAccountInfoRequestToCondition(req *http_model.AccountInfoRequest) *common_model.AccountInfoConditions {
	return &common_model.AccountInfoConditions{
		PlatformID:       conv.MustInt64(req.PlatformID, 0),
		BindDate:         req.BindDate,
		FansLow:          req.FansLow,
		FansHigh:         req.FansHigh,
		TalentId:         req.TalentId,
		PlatformNickname: req.PlatformNickname,
	}
}
