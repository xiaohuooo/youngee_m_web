package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskLinkInfoListToHttpTaskLinkPreviewList(gormTaskLinkInfos []*http_model.TaskLinkInfo) []*http_model.TaskLinkPreview {
	var httpProjectPreviews []*http_model.TaskLinkPreview
	for _, gormTaskLinkInfo := range gormTaskLinkInfos {
		httpTaskLinkPreview := MGormTaskLinkInfoToHttpTaskLinkPreview(gormTaskLinkInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskLinkPreview)
	}
	return httpProjectPreviews
}

func MGormTaskLinkInfoToHttpTaskLinkPreview(TaskLinkInfo *http_model.TaskLinkInfo) *http_model.TaskLinkPreview {
	return &http_model.TaskLinkPreview{
		TaskID:            conv.MustString(TaskLinkInfo.TaskID, ""),
		PlatformNickname:  conv.MustString(TaskLinkInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskLinkInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskLinkInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskLinkInfo.StrategyID, ""),
		PhotoUrl:          TaskLinkInfo.PhotoUrl,
		LinkUrl:           TaskLinkInfo.LinkUrl,
		ReviseOpinion:     TaskLinkInfo.ReviseOpinion,
		Submit:            conv.MustString(TaskLinkInfo.SubmitAt, "")[0:19],
		AgreeAt:           conv.MustString(TaskLinkInfo.AgreeAt, "")[0:19],
	}
}

func TaskLinkToTaskInfo(TaskLinks []*http_model.TaskLink) []*http_model.TaskLinkInfo {
	var TaskLinkInfos []*http_model.TaskLinkInfo
	for _, TaskLink := range TaskLinks {
		TaskLink := GetLinkInfoStruct(TaskLink)
		TaskLinkInfos = append(TaskLinkInfos, TaskLink)
	}
	return TaskLinkInfos
}

func GetLinkInfoStruct(TaskLink *http_model.TaskLink) *http_model.TaskLinkInfo {
	TalentPlatformInfoSnap := TaskLink.Talent.TalentPlatformInfoSnap
	return &http_model.TaskLinkInfo{
		TaskID:           TaskLink.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskLink.Talent.StrategyId,
		LinkId:           TaskLink.Link.LinkID,
		PhotoUrl:         TaskLink.Link.PhotoUrl,
		LinkUrl:          TaskLink.Link.LinkUrl,
		ReviseOpinion:    TaskLink.Link.ReviseOpinion,
		CreateAt:         TaskLink.Link.CreateAt,
		SubmitAt:         TaskLink.Link.SubmitAt,
		AgreeAt:          TaskLink.Link.AgreeAt,
		RejectAt:         TaskLink.Link.RejectAt,
		IsReview:         TaskLink.Link.IsReview,
	}
}
