package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskFinishInfoListToHttpTaskFinishPreviewList(gormTaskFinishInfos []*http_model.TaskFinishInfo) []*http_model.TaskFinishPreview {
	var httpProjectPreviews []*http_model.TaskFinishPreview
	for _, gormTaskFinishInfo := range gormTaskFinishInfos {
		httpTaskFinishPreview := MGormTaskFinishInfoToHttpTaskFinishPreview(gormTaskFinishInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskFinishPreview)
	}
	return httpProjectPreviews
}

func MGormTaskFinishInfoToHttpTaskFinishPreview(TaskFinishInfo *http_model.TaskFinishInfo) *http_model.TaskFinishPreview {
	return &http_model.TaskFinishPreview{
		TaskID:            conv.MustString(TaskFinishInfo.TaskID, ""),
		PlatformNickname:  conv.MustString(TaskFinishInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskFinishInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskFinishInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskFinishInfo.StrategyID, ""),
		PlayNumber:        TaskFinishInfo.PlayNumber,
		LikeNumber:        TaskFinishInfo.LikeNumber,
		CommentNumber:     TaskFinishInfo.CommentNumber,
		CollectNumber:     TaskFinishInfo.CollectNumber,
		RealPayment:       TaskFinishInfo.RealPayment,
		PhotoUrl:          TaskFinishInfo.PhotoUrl,
		LinkUrl:           TaskFinishInfo.LinkUrl,
		SubmitAt:          conv.MustString(TaskFinishInfo.SubmitAt, "")[0:19],
	}
}

func TaskFinishToTaskInfo(TaskFinishs []*http_model.TaskFinish) []*http_model.TaskFinishInfo {
	var TaskFinishInfos []*http_model.TaskFinishInfo
	for _, TaskFinish := range TaskFinishs {
		TaskFinish := GetFinishInfoStruct(TaskFinish)
		TaskFinishInfos = append(TaskFinishInfos, TaskFinish)
	}
	return TaskFinishInfos
}

func GetFinishInfoStruct(TaskFinish *http_model.TaskFinish) *http_model.TaskFinishInfo {
	TalentPlatformInfoSnap := TaskFinish.Talent.TalentPlatformInfoSnap
	return &http_model.TaskFinishInfo{
		TaskID:           TaskFinish.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskFinish.Talent.StrategyId,
		PlayNumber:       TaskFinish.Data.PlayNumber,
		LikeNumber:       TaskFinish.Data.LikeNumber,
		CommentNumber:    TaskFinish.Data.CommentNumber,
		CollectNumber:    TaskFinish.Data.CollectNumber,
		RealPayment:      TaskFinish.Talent.RealPayment,
		PhotoUrl:         TaskFinish.Data.PhotoUrl,
		LinkUrl:          TaskFinish.Link.LinkUrl,
		SubmitAt:         TaskFinish.Data.SubmitAt,
	}
}
