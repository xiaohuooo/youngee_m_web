package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskDefaultReviewInfoListToHttpTaskDefaultReviewPreviewList(gormTaskDefaultReviewInfos []*http_model.TaskDefaultReviewInfo) []*http_model.TaskDefaultReviewPreview {
	var httpProjectPreviews []*http_model.TaskDefaultReviewPreview
	for _, gormTaskDefaultReviewInfo := range gormTaskDefaultReviewInfos {
		httpTaskDefaultReviewPreview := MGormTaskDefaultReviewInfoToHttpTaskDefaultReviewPreview(gormTaskDefaultReviewInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskDefaultReviewPreview)
	}
	return httpProjectPreviews
}

func MGormTaskDefaultReviewInfoToHttpTaskDefaultReviewPreview(TaskDefaultReviewInfo *http_model.TaskDefaultReviewInfo) *http_model.TaskDefaultReviewPreview {
	return &http_model.TaskDefaultReviewPreview{
		TaskID:            conv.MustString(TaskDefaultReviewInfo.TaskID, ""),
		ProjectID:         conv.MustString(TaskDefaultReviewInfo.ProjectID, ""),
		PlatformNickname:  conv.MustString(TaskDefaultReviewInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskDefaultReviewInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskDefaultReviewInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskDefaultReviewInfo.StrategyID, ""),
		AllPayment:        TaskDefaultReviewInfo.AllPayment,
		RealPayment:       TaskDefaultReviewInfo.RealPayment,
		BreakAt:           conv.MustString(TaskDefaultReviewInfo.BreakAt, "")[0:19],
	}
}

func TaskDefaultReviewToTaskInfo(TaskDefaultReviews []*http_model.TaskDefaultReview) []*http_model.TaskDefaultReviewInfo {
	var TaskDefaultReviewInfos []*http_model.TaskDefaultReviewInfo
	for _, TaskDefaultReview := range TaskDefaultReviews {
		TaskDefaultReview := GetDefaultReviewInfoStruct(TaskDefaultReview)
		TaskDefaultReviewInfos = append(TaskDefaultReviewInfos, TaskDefaultReview)
	}
	return TaskDefaultReviewInfos
}

func GetDefaultReviewInfoStruct(TaskDefaultReview *http_model.TaskDefaultReview) *http_model.TaskDefaultReviewInfo {
	TalentPlatformInfoSnap := TaskDefaultReview.Talent.TalentPlatformInfoSnap
	return &http_model.TaskDefaultReviewInfo{
		TaskID:           TaskDefaultReview.Talent.TaskId,
		ProjectID:        TaskDefaultReview.Talent.ProjectId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskDefaultReview.Talent.StrategyId,
		AllPayment:       TaskDefaultReview.Talent.AllPayment,
		RealPayment:      TaskDefaultReview.Talent.RealPayment,
		BreakAt:          TaskDefaultReview.Default.BreakAt,
	}
}
