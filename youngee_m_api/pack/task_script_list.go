package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskScriptInfoListToHttpTaskScriptPreviewList(gormTaskScriptInfos []*http_model.TaskScriptInfo) []*http_model.TaskScriptPreview {
	var httpProjectPreviews []*http_model.TaskScriptPreview
	for _, gormTaskScriptInfo := range gormTaskScriptInfos {
		httpTaskScriptPreview := MGormTaskScriptInfoToHttpTaskScriptPreview(gormTaskScriptInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskScriptPreview)
	}
	return httpProjectPreviews
}

func MGormTaskScriptInfoToHttpTaskScriptPreview(TaskScriptInfo *http_model.TaskScriptInfo) *http_model.TaskScriptPreview {
	//deliveryTime := conv.MustString(TaskScriptInfo.DeliveryTime)
	//deliveryTime = deliveryTime[0:19]
	return &http_model.TaskScriptPreview{
		TaskID:            conv.MustString(TaskScriptInfo.TaskID, ""),
		PlatformNickname:  conv.MustString(TaskScriptInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskScriptInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskScriptInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskScriptInfo.StrategyID, ""),
		Title:             TaskScriptInfo.Title,
		Content:           TaskScriptInfo.Content,
		ReviseOpinion:     TaskScriptInfo.ReviseOpinion,
		Submit:            conv.MustString(TaskScriptInfo.SubmitAt, "")[0:19],
		AgreeAt:           conv.MustString(TaskScriptInfo.AgreeAt, "")[0:19],
	}
}

func TaskScriptToTaskInfo(TaskScripts []*http_model.TaskScript) []*http_model.TaskScriptInfo {
	var TaskScriptInfos []*http_model.TaskScriptInfo
	for _, TaskScript := range TaskScripts {
		TaskScript := GetScriptInfoStruct(TaskScript)
		TaskScriptInfos = append(TaskScriptInfos, TaskScript)
	}
	return TaskScriptInfos
}

func GetScriptInfoStruct(TaskScript *http_model.TaskScript) *http_model.TaskScriptInfo {
	TalentPlatformInfoSnap := TaskScript.Talent.TalentPlatformInfoSnap
	return &http_model.TaskScriptInfo{
		TaskID:           TaskScript.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskScript.Talent.StrategyId,
		ScriptId:         TaskScript.Script.ScriptID,
		Title:            TaskScript.Script.Title,
		Content:          TaskScript.Script.Content,
		ReviseOpinion:    TaskScript.Script.ReviseOpinion,
		CreateAt:         TaskScript.Script.CreateAt,
		SubmitAt:         TaskScript.Script.SubmitAt,
		AgreeAt:          TaskScript.Script.AgreeAt,
		RejectAt:         TaskScript.Script.RejectAt,
		IsReview:         TaskScript.Script.IsReview,
	}
}
