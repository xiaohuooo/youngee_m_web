package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpTaskDataListRequestToCondition(req *http_model.TaskDataListRequest) *common_model.TalentConditions {
	//fmt.Printf("初稿转换 %+v", req)
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		DataStatus:       conv.MustInt64(req.DataStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
