package pack

import (
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"

	"github.com/caixw/lib.go/conv"
)

func HttpSpecialTaskSettleListRequestToCondition(req *http_model.SpecialTaskSettleListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		SettleStatus:     conv.MustInt64(req.SettleStatus, 0),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
