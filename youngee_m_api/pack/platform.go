package pack

import (
	"youngee_m_api/consts"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func GormPlatformToHttpPlatform(gormPlatform []*gorm_model.YoungeePlatformAccountInfo) []*http_model.PlatformAccInfoData {
	var platformAccInfos []*http_model.PlatformAccInfoData
	for _, v := range gormPlatform {
		platformAccInfo := gormPlatformToHttpPlatform(v)
		platformAccInfos = append(platformAccInfos, platformAccInfo)
	}
	return platformAccInfos
}

func gormPlatformToHttpPlatform(platformAccountInfo *gorm_model.YoungeePlatformAccountInfo) *http_model.PlatformAccInfoData {
	return &http_model.PlatformAccInfoData{
		AccountId:          platformAccountInfo.AccountID,
		Platform:           consts.GetProjectPlatform(platformAccountInfo.PlatformID),
		PlatformNickname:   platformAccountInfo.PlatformNickname,
		Fans:               util.GetNumString(platformAccountInfo.FansCount),
		HomePageCaptureUrl: platformAccountInfo.HomePageCaptureUrl,
		HomePageUrl:        platformAccountInfo.HomePageUrl,
	}
}
