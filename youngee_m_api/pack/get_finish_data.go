package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

func MGormRecruitStrategyListToHttpGetFinishDataInfoList(gormRecruitStrategys []*gorm_model.RecruitStrategy, defaultMap map[int64]int64) []*http_model.GetFinishDataInfo {
	var httpGetFinishDataInfos []*http_model.GetFinishDataInfo
	for _, gormRecruitStrategy := range gormRecruitStrategys {
		httpGetFinishDataInfo := MGormRecruitStrategyToHttpGetFinishDataInfo(gormRecruitStrategy, defaultMap)
		httpGetFinishDataInfos = append(httpGetFinishDataInfos, httpGetFinishDataInfo)
	}
	return httpGetFinishDataInfos
}

func MGormRecruitStrategyToHttpGetFinishDataInfo(RecruitStrategy *gorm_model.RecruitStrategy, defaultMap map[int64]int64) *http_model.GetFinishDataInfo {
	return &http_model.GetFinishDataInfo{
		FeeForm:         conv.MustString(RecruitStrategy.FeeForm, ""),
		StrategyID:      conv.MustString(RecruitStrategy.StrategyID, ""),
		FollowersLow:    conv.MustString(RecruitStrategy.FollowersLow, ""),
		FollowersUp:     conv.MustString(RecruitStrategy.FollowersUp, ""),
		RecruitNumber:   conv.MustString(RecruitStrategy.RecruitNumber, ""),
		Offer:           conv.MustString(RecruitStrategy.Offer, ""),
		ProjectID:       conv.MustString(RecruitStrategy.ProjectID, ""),
		ServiceCharge:   conv.MustString(RecruitStrategy.ServiceCharge, ""),
		SelectedNumber:  conv.MustString(RecruitStrategy.SelectedNumber, ""),
		WaitingNumber:   conv.MustString(RecruitStrategy.WaitingNumber, ""),
		DeliveredNumber: conv.MustString(RecruitStrategy.DeliveredNumber, ""),
		SignedNumber:    conv.MustString(RecruitStrategy.SignedNumber, ""),
		MaxOffer:        conv.MustString(RecruitStrategy.MaxOffer, ""),
		MinOffer:        conv.MustString(RecruitStrategy.MinOffer, ""),
		FanNumber:       conv.MustString(RecruitStrategy.FanNumber, ""),
		PlayNumber:      conv.MustString(RecruitStrategy.PlayNumber, ""),
		LikeNumber:      conv.MustString(RecruitStrategy.LikeNumber, ""),
		CollectNumber:   conv.MustString(RecruitStrategy.CollectNumber, ""),
		CommentNumber:   conv.MustString(RecruitStrategy.CommentNumber, ""),
		FinishNumber:    conv.MustString(RecruitStrategy.FinishNumber, ""),
		DefaultNumber:   conv.MustString(defaultMap[RecruitStrategy.StrategyID], ""),
		TotalOffer:      RecruitStrategy.TotalOffer,
	}
}
