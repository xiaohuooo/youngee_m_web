package pack

import (
	"youngee_m_api/model/http_model"

	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
)

func MGormSpecialTaskSettleInfoListToHttpSpecialTaskSettlePreviewList(gormSpecialTaskSettleInfos []*http_model.SpecialTaskSettleInfo) []*http_model.SpecialTaskSettlePreview {
	var httpProjectPreviews []*http_model.SpecialTaskSettlePreview
	for _, gormSpecialTaskSettleInfo := range gormSpecialTaskSettleInfos {
		httpSpecialTaskSettlePreview := MGormSpecialTaskSettleInfoToHttpSpecialTaskSettlePreview(gormSpecialTaskSettleInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpSpecialTaskSettlePreview)
	}
	return httpProjectPreviews
}

func MGormSpecialTaskSettleInfoToHttpSpecialTaskSettlePreview(SpecialTaskSettleInfo *http_model.SpecialTaskSettleInfo) *http_model.SpecialTaskSettlePreview {
	return &http_model.SpecialTaskSettlePreview{
		TaskID:           conv.MustString(SpecialTaskSettleInfo.TaskID, ""),
		PlatformNickname: conv.MustString(SpecialTaskSettleInfo.PlatformNickname, ""),
		FansCount:        conv.MustString(SpecialTaskSettleInfo.FansCount, ""),
		PlayNumber:       SpecialTaskSettleInfo.PlayNumber,
		LikeNumber:       SpecialTaskSettleInfo.LikeNumber,
		CommentNumber:    SpecialTaskSettleInfo.CommentNumber,
		CollectNumber:    SpecialTaskSettleInfo.CollectNumber,
		LinkUrl:          SpecialTaskSettleInfo.LinkUrl,
		PhotoUrl:         SpecialTaskSettleInfo.PhotoUrl,
		AllPayment:       SpecialTaskSettleInfo.AllPayment,
		RealPayment:      SpecialTaskSettleInfo.RealPayment,
		ReviseOpinion:    SpecialTaskSettleInfo.ReviseOpinion,
		Phone:            SpecialTaskSettleInfo.Phone,
		SubmitAt:         conv.MustString(SpecialTaskSettleInfo.SubmitAt, "")[0:19],
		AgreeAt:          conv.MustString(SpecialTaskSettleInfo.AgreeAt, "")[0:19],
		UpdateAt:         conv.MustString(SpecialTaskSettleInfo.UpdateAt, "")[0:19],
	}
}

func SpecialTaskSettleToTaskInfo(SpecialTaskSettles []*http_model.SpecialTaskSettle) []*http_model.SpecialTaskSettleInfo {
	var SpecialTaskSettleInfos []*http_model.SpecialTaskSettleInfo
	for _, SpecialTaskSettle := range SpecialTaskSettles {
		SpecialTaskSettle := GetSpecialTaskSettleInfoStruct(SpecialTaskSettle)
		SpecialTaskSettleInfos = append(SpecialTaskSettleInfos, SpecialTaskSettle)
	}
	return SpecialTaskSettleInfos
}

func GetSpecialTaskSettleInfoStruct(SpecialTaskSettle *http_model.SpecialTaskSettle) *http_model.SpecialTaskSettleInfo {
	TalentPlatformInfoSnap := SpecialTaskSettle.Talent.TalentPlatformInfoSnap
	TalentPersonalInfoSnap := SpecialTaskSettle.Talent.TalentPersonalInfoSnap
	return &http_model.SpecialTaskSettleInfo{
		TaskID:           SpecialTaskSettle.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		DataId:           SpecialTaskSettle.Data.DataID,
		PlayNumber:       SpecialTaskSettle.Data.PlayNumber,
		LikeNumber:       SpecialTaskSettle.Data.LikeNumber,
		CommentNumber:    SpecialTaskSettle.Data.CommentNumber,
		CollectNumber:    SpecialTaskSettle.Data.CollectNumber,
		LinkUrl:          SpecialTaskSettle.Link.LinkUrl,
		PhotoUrl:         SpecialTaskSettle.Data.PhotoUrl,
		AllPayment:       SpecialTaskSettle.Talent.AllPayment,
		RealPayment:      SpecialTaskSettle.Talent.RealPayment,
		ReviseOpinion:    SpecialTaskSettle.Data.ReviseOpinion,
		Phone:            conv.MustString(gjson.Get(TalentPersonalInfoSnap, "talent_phone_number"), ""),
		CreateAt:         SpecialTaskSettle.Data.CreateAt,
		SubmitAt:         SpecialTaskSettle.Data.SubmitAt,
		AgreeAt:          SpecialTaskSettle.Data.AgreeAt,
		UpdateAt:         SpecialTaskSettle.Talent.UpdateAt,
		RejectAt:         SpecialTaskSettle.Data.RejectAt,
		IsReview:         SpecialTaskSettle.Data.IsReview,
	}
}
