package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskSketchInfoListToHttpTaskSketchPreviewList(gormTaskSketchInfos []*http_model.TaskSketchInfo) []*http_model.TaskSketchPreview {
	var httpProjectPreviews []*http_model.TaskSketchPreview
	for _, gormTaskSketchInfo := range gormTaskSketchInfos {
		httpTaskSketchPreview := MGormTaskSketchInfoToHttpTaskSketchPreview(gormTaskSketchInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskSketchPreview)
	}
	return httpProjectPreviews
}

func MGormTaskSketchInfoToHttpTaskSketchPreview(TaskSketchInfo *http_model.TaskSketchInfo) *http_model.TaskSketchPreview {
	//deliveryTime := conv.MustString(TaskSketchInfo.DeliveryTime)
	//deliveryTime = deliveryTime[0:19]
	return &http_model.TaskSketchPreview{
		TaskID:            conv.MustString(TaskSketchInfo.TaskID, ""),
		SketchID:          conv.MustString(TaskSketchInfo.SketchId, ""),
		PlatformNickname:  conv.MustString(TaskSketchInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskSketchInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskSketchInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskSketchInfo.StrategyID, ""),
		Title:             TaskSketchInfo.Title,
		Content:           TaskSketchInfo.Content,
		ReviseOpinion:     TaskSketchInfo.ReviseOpinion,
		Submit:            conv.MustString(TaskSketchInfo.SubmitAt, "")[0:19],
		AgreeAt:           conv.MustString(TaskSketchInfo.AgreeAt, "")[0:19],
		//SketchPhotos:      TaskSketchInfo.SketchPhotos,
	}
}

func TaskSketchToTaskInfo(TaskSketchs []*http_model.TaskSketch) []*http_model.TaskSketchInfo {
	var TaskSketchInfos []*http_model.TaskSketchInfo
	for _, TaskSketch := range TaskSketchs {
		TaskSketch := GetSketchInfoStruct(TaskSketch)
		TaskSketchInfos = append(TaskSketchInfos, TaskSketch)
	}
	return TaskSketchInfos
}

func GetSketchInfoStruct(TaskSketch *http_model.TaskSketch) *http_model.TaskSketchInfo {
	TalentPlatformInfoSnap := TaskSketch.Talent.TalentPlatformInfoSnap
	return &http_model.TaskSketchInfo{
		TaskID:           TaskSketch.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskSketch.Talent.StrategyId,
		SketchId:         TaskSketch.Sketch.SketchID,
		Title:            TaskSketch.Sketch.Title,
		Content:          TaskSketch.Sketch.Content,
		ReviseOpinion:    TaskSketch.Sketch.ReviseOpinion,
		CreateAt:         TaskSketch.Sketch.CreateAt,
		SubmitAt:         TaskSketch.Sketch.SubmitAt,
		AgreeAt:          TaskSketch.Sketch.AgreeAt,
		RejectAt:         TaskSketch.Sketch.RejectAt,
		IsReview:         TaskSketch.Sketch.IsReview,
	}
}
