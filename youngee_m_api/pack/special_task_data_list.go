package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormSpecialTaskDataInfoListToHttpSpecialTaskDataPreviewList(gormSpecialTaskDataInfos []*http_model.SpecialTaskDataInfo) []*http_model.SpecialTaskDataPreview {
	var httpProjectPreviews []*http_model.SpecialTaskDataPreview
	for _, gormSpecialTaskDataInfo := range gormSpecialTaskDataInfos {
		httpSpecialTaskDataPreview := MGormSpecialTaskDataInfoToHttpSpecialTaskDataPreview(gormSpecialTaskDataInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpSpecialTaskDataPreview)
	}
	return httpProjectPreviews
}

func MGormSpecialTaskDataInfoToHttpSpecialTaskDataPreview(SpecialTaskDataInfo *http_model.SpecialTaskDataInfo) *http_model.SpecialTaskDataPreview {
	return &http_model.SpecialTaskDataPreview{
		TaskID:           conv.MustString(SpecialTaskDataInfo.TaskID, ""),
		PlatformNickname: conv.MustString(SpecialTaskDataInfo.PlatformNickname, ""),
		FansCount:        conv.MustString(SpecialTaskDataInfo.FansCount, ""),
		PlayNumber:       SpecialTaskDataInfo.PlayNumber,
		LikeNumber:       SpecialTaskDataInfo.LikeNumber,
		CommentNumber:    SpecialTaskDataInfo.CommentNumber,
		CollectNumber:    SpecialTaskDataInfo.CollectNumber,
		LinkUrl:          SpecialTaskDataInfo.LinkUrl,
		PhotoUrl:         SpecialTaskDataInfo.PhotoUrl,
		AllPayment:       SpecialTaskDataInfo.AllPayment,
		RealPayment:      SpecialTaskDataInfo.RealPayment,
		ReviseOpinion:    SpecialTaskDataInfo.ReviseOpinion,
		Phone:            SpecialTaskDataInfo.Phone,
		SubmitAt:         conv.MustString(SpecialTaskDataInfo.SubmitAt, "")[0:19],
		AgreeAt:          conv.MustString(SpecialTaskDataInfo.AgreeAt, "")[0:19],
	}
}

func SpecialTaskDataToTaskInfo(SpecialTaskDatas []*http_model.SpecialTaskData) []*http_model.SpecialTaskDataInfo {
	var SpecialTaskDataInfos []*http_model.SpecialTaskDataInfo
	for _, SpecialTaskData := range SpecialTaskDatas {
		SpecialTaskData := GetSpecialTaskDataInfoStruct(SpecialTaskData)
		SpecialTaskDataInfos = append(SpecialTaskDataInfos, SpecialTaskData)
	}
	return SpecialTaskDataInfos
}

func GetSpecialTaskDataInfoStruct(SpecialTaskData *http_model.SpecialTaskData) *http_model.SpecialTaskDataInfo {
	TalentPlatformInfoSnap := SpecialTaskData.Talent.TalentPlatformInfoSnap
	TalentPersonalInfoSnap := SpecialTaskData.Talent.TalentPersonalInfoSnap
	return &http_model.SpecialTaskDataInfo{
		TaskID:           SpecialTaskData.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		DataId:           SpecialTaskData.Data.DataID,
		PlayNumber:       SpecialTaskData.Data.PlayNumber,
		LikeNumber:       SpecialTaskData.Data.LikeNumber,
		CommentNumber:    SpecialTaskData.Data.CommentNumber,
		CollectNumber:    SpecialTaskData.Data.CollectNumber,
		LinkUrl:          SpecialTaskData.Link.LinkUrl,
		PhotoUrl:         SpecialTaskData.Data.PhotoUrl,
		AllPayment:       SpecialTaskData.Talent.AllPayment,
		RealPayment:      SpecialTaskData.Talent.RealPayment,
		ReviseOpinion:    SpecialTaskData.Data.ReviseOpinion,
		Phone:            conv.MustString(gjson.Get(TalentPersonalInfoSnap, "talent_phone_number"), ""),
		CreateAt:         SpecialTaskData.Data.CreateAt,
		SubmitAt:         SpecialTaskData.Data.SubmitAt,
		AgreeAt:          SpecialTaskData.Data.AgreeAt,
		RejectAt:         SpecialTaskData.Data.RejectAt,
		IsReview:         SpecialTaskData.Data.IsReview,
	}
}
