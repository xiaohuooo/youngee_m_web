package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

func MGormSpecialTaskInviteInfoListToHttpSpecialTaskInvitePreviewList(gormSpecialTaskInviteInfos []*http_model.SpecialTaskInviteInfo) []*http_model.SpecialTaskInvitePreview {
	var httpProjectPreviews []*http_model.SpecialTaskInvitePreview
	for _, gormSpecialTaskInviteInfo := range gormSpecialTaskInviteInfos {
		httpSpecialTaskInvitePreview := GormFullProjectToHttpSpecialTaskInvitePreview(gormSpecialTaskInviteInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpSpecialTaskInvitePreview)
	}
	return httpProjectPreviews
}

func GormFullProjectToHttpSpecialTaskInvitePreview(projectTaskInfo *http_model.SpecialTaskInviteInfo) *http_model.SpecialTaskInvitePreview {
	createDate := conv.MustString(projectTaskInfo.CreateDate, "")
	createDate = createDate[0:19]
	return &http_model.SpecialTaskInvitePreview{
		TaskId:             conv.MustString(projectTaskInfo.TaskID, ""),
		PlatformNickname:   conv.MustString(projectTaskInfo.PlatformNickname, ""),
		FansCount:          conv.MustString(projectTaskInfo.FansCount, ""),
		HomePageCaptureUrl: conv.MustString(projectTaskInfo.HomePageCaptureUrl, ""),
		AllPayment:         conv.MustString(projectTaskInfo.AllPayment, ""),
		TaskReward:         conv.MustString(projectTaskInfo.TaskReward, ""),
		HomePageUrl:        conv.MustString(projectTaskInfo.HomePageUrl, ""),
		TaskStatus:         conv.MustString(projectTaskInfo.TaskStatus, ""),
		CreateDate:         createDate,
		Phone:              projectTaskInfo.Phone,
	}
}

func YoungeeTaskInfoToSpecialTaskInviteInfo(taskAccounts []gorm_model.YoungeeTaskInfo) []*http_model.SpecialTaskInviteInfo {
	var projectTasks []*http_model.SpecialTaskInviteInfo
	for _, taskAccount := range taskAccounts {
		projectTask := GetSpecialTaskInviteInfoStruct(taskAccount)
		projectTasks = append(projectTasks, projectTask)
	}
	return projectTasks
}

func GetSpecialTaskInviteInfoStruct(taskAccount gorm_model.YoungeeTaskInfo) *http_model.SpecialTaskInviteInfo {
	TalentPlatformInfoSnap := taskAccount.TalentPlatformInfoSnap
	TalentPersonalInfoSnap := taskAccount.TalentPersonalInfoSnap
	//fmt.Printf("任务-账户关联 %+v", taskAccount)
	return &http_model.SpecialTaskInviteInfo{
		TaskID:             taskAccount.TaskId,
		TaskReward:         taskAccount.TaskReward,
		AllPayment:         taskAccount.AllPayment,
		PlatformNickname:   conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		CreateDate:         taskAccount.CreateDate,
		HomePageUrl:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "home_page_url"), ""),
		HomePageCaptureUrl: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "home_page_capture_url"), ""),
		FansCount:          conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		TaskStatus:         string(rune(taskAccount.TaskStatus)),
		Phone:              conv.MustString(gjson.Get(TalentPersonalInfoSnap, "talent_phone_number"), ""),
	}
}
