package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpTaskLogisticsListRequestToCondition(req *http_model.TaskLogisticsListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		LogisticsStatus:  conv.MustInt64(req.LogisticsStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}

func HttpSpecialProjectTaskRequestToCondition(req *http_model.GetSpecialLogisticListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		//ProjectId:        req.ProjectId,
		LogisticsStatus:  conv.MustInt64(req.LogisticsStatus, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
