package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormSpecialTaskFinishDataInfoListToHttpSpecialTaskFinishDataPreviewList(gormSpecialTaskFinishDataInfos []*http_model.SpecialTaskFinishDataInfo) []*http_model.SpecialTaskFinishDataPreview {
	var httpProjectPreviews []*http_model.SpecialTaskFinishDataPreview
	for _, gormSpecialTaskFinishDataInfo := range gormSpecialTaskFinishDataInfos {
		httpSpecialTaskFinishDataPreview := MGormSpecialTaskFinishDataInfoToHttpSpecialTaskFinishDataPreview(gormSpecialTaskFinishDataInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpSpecialTaskFinishDataPreview)
	}
	return httpProjectPreviews
}

func MGormSpecialTaskFinishDataInfoToHttpSpecialTaskFinishDataPreview(SpecialTaskFinishDataInfo *http_model.SpecialTaskFinishDataInfo) *http_model.SpecialTaskFinishDataPreview {
	return &http_model.SpecialTaskFinishDataPreview{
		TaskID:           conv.MustString(SpecialTaskFinishDataInfo.TaskID, ""),
		PlatformNickname: conv.MustString(SpecialTaskFinishDataInfo.PlatformNickname, ""),
		FansCount:        conv.MustString(SpecialTaskFinishDataInfo.FansCount, ""),
		PlayNumber:       SpecialTaskFinishDataInfo.PlayNumber,
		LikeNumber:       SpecialTaskFinishDataInfo.LikeNumber,
		CommentNumber:    SpecialTaskFinishDataInfo.CommentNumber,
		CollectNumber:    SpecialTaskFinishDataInfo.CollectNumber,
		RealPayment:      SpecialTaskFinishDataInfo.RealPayment,
		PhotoUrl:         SpecialTaskFinishDataInfo.PhotoUrl,
		LinkUrl:          SpecialTaskFinishDataInfo.LinkUrl,
		Phone:            SpecialTaskFinishDataInfo.Phone,
		SubmitAt:         conv.MustString(SpecialTaskFinishDataInfo.SubmitAt, "")[0:19],
	}
}

func SpecialTaskFinishDataToTaskInfo(SpecialTaskFinishDatas []*http_model.SpecialTaskFinishData) []*http_model.SpecialTaskFinishDataInfo {
	var SpecialTaskFinishDataInfos []*http_model.SpecialTaskFinishDataInfo
	for _, SpecialTaskFinishData := range SpecialTaskFinishDatas {
		SpecialTaskFinishData := GetSpecialTaskFinishDataInfoStruct(SpecialTaskFinishData)
		SpecialTaskFinishDataInfos = append(SpecialTaskFinishDataInfos, SpecialTaskFinishData)
	}
	return SpecialTaskFinishDataInfos
}

func GetSpecialTaskFinishDataInfoStruct(SpecialTaskFinishData *http_model.SpecialTaskFinishData) *http_model.SpecialTaskFinishDataInfo {
	TalentPlatformInfoSnap := SpecialTaskFinishData.Talent.TalentPlatformInfoSnap
	TalentPersonalInfoSnap := SpecialTaskFinishData.Talent.TalentPersonalInfoSnap
	return &http_model.SpecialTaskFinishDataInfo{
		TaskID:           SpecialTaskFinishData.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		PlayNumber:       SpecialTaskFinishData.Data.PlayNumber,
		LikeNumber:       SpecialTaskFinishData.Data.LikeNumber,
		CommentNumber:    SpecialTaskFinishData.Data.CommentNumber,
		CollectNumber:    SpecialTaskFinishData.Data.CollectNumber,
		RealPayment:      SpecialTaskFinishData.Talent.RealPayment,
		PhotoUrl:         SpecialTaskFinishData.Data.PhotoUrl,
		Phone:            conv.MustString(gjson.Get(TalentPersonalInfoSnap, "talent_phone_number"), ""),
		LinkUrl:          SpecialTaskFinishData.Link.LinkUrl,
		SubmitAt:         SpecialTaskFinishData.Data.SubmitAt,
	}
}
