package pack

import (
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
)

func HttpFindAllSelectionRequestToCondition(req *http_model.FindAllSelectionRequest) *common_model.SelectionConditions {
	return &common_model.SelectionConditions{
		SelectionStatus: req.SelectionStatus,
		Platform:        req.Platform,
		SampleMode:      req.SampleMode,
		ContentType:     req.ContentType,
		TaskMode:        req.TaskMode,
		SearchValue:     req.SearchValue,
		SubmitAt:        req.SubmitAt,
		TaskDdl:         req.TaskDdl,
	}
}

func MGormSelectionToHttpSelectionPreview(gormSelectionInfos []*gorm_model.YounggeeSelectionInfo) []*gorm_model.YounggeeSelectionInfo {
	var httpSelectionPreviews []*gorm_model.YounggeeSelectionInfo
	for _, gormSelectionInfo := range gormSelectionInfos {
		gormSelectionInfo := GormSelectionToHttpSelectionPreview(gormSelectionInfo)
		httpSelectionPreviews = append(httpSelectionPreviews, gormSelectionInfo)
	}
	return httpSelectionPreviews
}

func GormSelectionToHttpSelectionPreview(selectionInfo *gorm_model.YounggeeSelectionInfo) *gorm_model.YounggeeSelectionInfo {
	return &gorm_model.YounggeeSelectionInfo{
		SelectionID:      selectionInfo.SelectionID,
		SelectionName:    selectionInfo.SelectionName,
		EnterpriseID:     selectionInfo.EnterpriseID,
		ProductID:        selectionInfo.ProductID,
		ContentType:      selectionInfo.ContentType,
		SelectionStatus:  selectionInfo.SelectionStatus,
		Platform:         selectionInfo.Platform,
		ProductUrl:       selectionInfo.ProductUrl,
		RemainNum:        selectionInfo.RemainNum,
		EstimatedCost:    selectionInfo.EstimatedCost,
		TaskReward:       selectionInfo.TaskReward,
		SampleCondition:  selectionInfo.SampleCondition,
		RewardCondition:  selectionInfo.RewardCondition,
		SettlementAmount: selectionInfo.SettlementAmount,
		TaskDdl:          selectionInfo.TaskDdl,
		Detail:           selectionInfo.Detail,
		ProductSnap:      selectionInfo.ProductSnap,
		ProductPhotoSnap: selectionInfo.ProductPhotoSnap,
		CreatedAt:        selectionInfo.CreatedAt,
		UpdatedAt:        selectionInfo.UpdatedAt,
		SubmitAt:         selectionInfo.SubmitAt,
		PassAt:           selectionInfo.PassAt,
		FailReason:       selectionInfo.FailReason,
		PayAt:            selectionInfo.PayAt,
		FinishAt:         selectionInfo.FinishAt,
		IsRead:           selectionInfo.IsRead,
		TaskMode:         selectionInfo.TaskMode,
	}
}
