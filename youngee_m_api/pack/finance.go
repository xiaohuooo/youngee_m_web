package pack

import (
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpWithdrawRecordsRequestToCondition(request *http_model.WithdrawalRecordsRequest) *common_model.WithdrawRecordsCondition {
	return &common_model.WithdrawRecordsCondition{
		TalentId:   request.TalentId,
		SubmitAt:   request.SubmitAt,
		Status:     request.Status,
		WithdrawAt: request.WithdrawAt,
	}
}

func HttpInvoiceRecordsRequestToCondition(request *http_model.InvoiceRecordsRequest) *common_model.InvoiceRecordsCondition {
	return &common_model.InvoiceRecordsCondition{
		SubmitAt:  request.SubmitAt,
		BillingAt: request.BillingAt,
	}
}

func HttpRechargeRecordsRequestToCondition(request *http_model.GetRechargeRecordsRequest) *common_model.RechargeRecordsCondition {
	return &common_model.RechargeRecordsCondition{
		CommitAt:  request.CommitAt,
		ConfirmAt: request.ConfirmAt,
	}
}
