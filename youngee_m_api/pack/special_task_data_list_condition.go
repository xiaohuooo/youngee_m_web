package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpSpecialTaskDataListRequestToCondition(req *http_model.SpecialTaskDataListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		DataStatus:       conv.MustInt64(req.DataStatus, 0),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
