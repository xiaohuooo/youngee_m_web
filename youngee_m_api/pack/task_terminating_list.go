package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskTerminatingInfoListToHttpTaskTerminatingPreviewList(gormTaskTerminatingInfos []*http_model.TaskTerminatingInfo) []*http_model.TaskTerminatingPreview {
	var httpProjectPreviews []*http_model.TaskTerminatingPreview
	for _, gormTaskTerminatingInfo := range gormTaskTerminatingInfos {
		httpTaskTerminatingPreview := MGormTaskTerminatingInfoToHttpTaskTerminatingPreview(gormTaskTerminatingInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskTerminatingPreview)
	}
	return httpProjectPreviews
}

func MGormTaskTerminatingInfoToHttpTaskTerminatingPreview(TaskTerminatingInfo *http_model.TaskTerminatingInfo) *http_model.TaskTerminatingPreview {
	return &http_model.TaskTerminatingPreview{
		TaskID:            conv.MustString(TaskTerminatingInfo.TaskID, ""),
		ProjectID:         conv.MustString(TaskTerminatingInfo.ProjectID, ""),
		PlatformNickname:  conv.MustString(TaskTerminatingInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskTerminatingInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskTerminatingInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskTerminatingInfo.StrategyID, ""),
		AllPayment:        TaskTerminatingInfo.AllPayment,
		RealPayment:       TaskTerminatingInfo.RealPayment,
		TerminateAt:       conv.MustString(TaskTerminatingInfo.TerminateAt, "")[0:19],
		BreakType:         conv.MustString(TaskTerminatingInfo.BreakType, ""),
	}
}

func TaskTerminatingToTaskInfo(TaskTerminatings []*http_model.TaskTerminating) []*http_model.TaskTerminatingInfo {
	var TaskTerminatingInfos []*http_model.TaskTerminatingInfo
	for _, TaskTerminating := range TaskTerminatings {
		TaskTerminating := GetTerminatingInfoStruct(TaskTerminating)
		TaskTerminatingInfos = append(TaskTerminatingInfos, TaskTerminating)
	}
	return TaskTerminatingInfos
}

func GetTerminatingInfoStruct(TaskTerminating *http_model.TaskTerminating) *http_model.TaskTerminatingInfo {
	TalentPlatformInfoSnap := TaskTerminating.Talent.TalentPlatformInfoSnap
	return &http_model.TaskTerminatingInfo{
		TaskID:           TaskTerminating.Talent.TaskId,
		ProjectID:        TaskTerminating.Talent.ProjectId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskTerminating.Talent.StrategyId,
		AllPayment:       TaskTerminating.Talent.AllPayment,
		RealPayment:      TaskTerminating.Talent.RealPayment,
		TerminateAt:      TaskTerminating.Default.TerminateAt,
		BreakType:        TaskTerminating.Default.BreakType,
	}
}
