package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormSpecialTaskSketchInfoListToHttpSpecialTaskSketchPreviewList(gormSpecialTaskSketchInfos []*http_model.SpecialTaskSketchInfo) []*http_model.SpecialTaskSketchPreview {
	var httpProjectPreviews []*http_model.SpecialTaskSketchPreview
	for _, gormSpecialTaskSketchInfo := range gormSpecialTaskSketchInfos {
		httpSpecialTaskSketchPreview := MGormSpecialTaskSketchInfoToHttpSpecialTaskSketchPreview(gormSpecialTaskSketchInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpSpecialTaskSketchPreview)
	}
	return httpProjectPreviews
}

func MGormSpecialTaskSketchInfoToHttpSpecialTaskSketchPreview(SpecialTaskSketchInfo *http_model.SpecialTaskSketchInfo) *http_model.SpecialTaskSketchPreview {
	return &http_model.SpecialTaskSketchPreview{
		TaskID:           conv.MustString(SpecialTaskSketchInfo.TaskID, ""),
		SketchID:         conv.MustString(SpecialTaskSketchInfo.SketchId, ""),
		PlatformNickname: conv.MustString(SpecialTaskSketchInfo.PlatformNickname, ""),
		FansCount:        conv.MustString(SpecialTaskSketchInfo.FansCount, ""),
		Title:            SpecialTaskSketchInfo.Title,
		Content:          SpecialTaskSketchInfo.Content,
		ReviseOpinion:    SpecialTaskSketchInfo.ReviseOpinion,
		Phone:            SpecialTaskSketchInfo.Phone,
		Submit:           conv.MustString(SpecialTaskSketchInfo.SubmitAt, "")[0:19],
		AgreeAt:          conv.MustString(SpecialTaskSketchInfo.AgreeAt, "")[0:19],
	}
}

func SpecialTaskSketchToTaskInfo(SpecialTaskSketchs []*http_model.SpecialTaskSketch) []*http_model.SpecialTaskSketchInfo {
	var SpecialTaskSketchInfos []*http_model.SpecialTaskSketchInfo
	for _, SpecialTaskSketch := range SpecialTaskSketchs {
		SpecialTaskSketch := GetSpecialTaskSketchInfoStruct(SpecialTaskSketch)
		SpecialTaskSketchInfos = append(SpecialTaskSketchInfos, SpecialTaskSketch)
	}
	return SpecialTaskSketchInfos
}

func GetSpecialTaskSketchInfoStruct(SpecialTaskSketch *http_model.SpecialTaskSketch) *http_model.SpecialTaskSketchInfo {
	TalentPlatformInfoSnap := SpecialTaskSketch.Talent.TalentPlatformInfoSnap
	TalentPersonalInfoSnap := SpecialTaskSketch.Talent.TalentPersonalInfoSnap
	return &http_model.SpecialTaskSketchInfo{
		TaskID:           SpecialTaskSketch.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		SketchId:         SpecialTaskSketch.Sketch.SketchID,
		Title:            SpecialTaskSketch.Sketch.Title,
		Content:          SpecialTaskSketch.Sketch.Content,
		ReviseOpinion:    SpecialTaskSketch.Sketch.ReviseOpinion,
		CreateAt:         SpecialTaskSketch.Sketch.CreateAt,
		Phone:            conv.MustString(gjson.Get(TalentPersonalInfoSnap, "talent_phone_number"), ""),
		SubmitAt:         SpecialTaskSketch.Sketch.SubmitAt,
		AgreeAt:          SpecialTaskSketch.Sketch.AgreeAt,
		RejectAt:         SpecialTaskSketch.Sketch.RejectAt,
		IsReview:         SpecialTaskSketch.Sketch.IsReview,
	}
}
