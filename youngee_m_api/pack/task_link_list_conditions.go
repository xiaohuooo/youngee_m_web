package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpTaskLinkListRequestToCondition(req *http_model.TaskLinkListRequest) *common_model.TalentConditions {
	//fmt.Printf("链接转换 %+v", req)
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		LinkStatus:       conv.MustInt64(req.LinkStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
