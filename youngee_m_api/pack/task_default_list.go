package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormTaskDefaultDataInfoListToHttpTaskDefaultDataPreviewList(gormTaskDefaultDataInfos []*http_model.TaskDefaultDataInfo) []*http_model.TaskDefaultDataPreview {
	var httpProjectPreviews []*http_model.TaskDefaultDataPreview
	for _, gormTaskDefaultDataInfo := range gormTaskDefaultDataInfos {
		httpTaskDefaultDataPreview := MGormTaskDefaultDataInfoToHttpTaskDefaultDataPreview(gormTaskDefaultDataInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskDefaultDataPreview)
	}
	return httpProjectPreviews
}

func MGormTaskDefaultDataInfoToHttpTaskDefaultDataPreview(TaskDefaultDataInfo *http_model.TaskDefaultDataInfo) *http_model.TaskDefaultDataPreview {
	return &http_model.TaskDefaultDataPreview{
		TaskID:            conv.MustString(TaskDefaultDataInfo.TaskID, ""),
		ProjectID:         conv.MustString(TaskDefaultDataInfo.ProjectID, ""),
		PlatformNickname:  conv.MustString(TaskDefaultDataInfo.PlatformNickname, ""),
		FansCount:         conv.MustString(TaskDefaultDataInfo.FansCount, ""),
		RecruitStrategyID: conv.MustString(TaskDefaultDataInfo.RecruitStrategyID, ""),
		StrategyID:        conv.MustString(TaskDefaultDataInfo.StrategyID, ""),
		AllPayment:        TaskDefaultDataInfo.AllPayment,
		RealPayment:       TaskDefaultDataInfo.RealPayment,
		BreakAt:           conv.MustString(TaskDefaultDataInfo.BreakAt, "")[0:19],
		LinkUrl:           TaskDefaultDataInfo.LinkUrl,
	}
}

func TaskDefaultDataToTaskInfo(TaskDefaultDatas []*http_model.TaskDefaultData) []*http_model.TaskDefaultDataInfo {
	var TaskDefaultDataInfos []*http_model.TaskDefaultDataInfo
	for _, TaskDefaultData := range TaskDefaultDatas {
		TaskDefaultData := GetDefaultDataInfoStruct(TaskDefaultData)
		TaskDefaultDataInfos = append(TaskDefaultDataInfos, TaskDefaultData)
	}
	return TaskDefaultDataInfos
}

func GetDefaultDataInfoStruct(TaskDefaultData *http_model.TaskDefaultData) *http_model.TaskDefaultDataInfo {
	TalentPlatformInfoSnap := TaskDefaultData.Talent.TalentPlatformInfoSnap
	return &http_model.TaskDefaultDataInfo{
		TaskID:           TaskDefaultData.Talent.TaskId,
		ProjectID:        TaskDefaultData.Talent.ProjectId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:       TaskDefaultData.Talent.StrategyId,
		AllPayment:       TaskDefaultData.Talent.AllPayment,
		RealPayment:      TaskDefaultData.Talent.RealPayment,
		BreakAt:          TaskDefaultData.Default.BreakAt,
		LinkUrl:          TaskDefaultData.Link.LinkUrl,
	}
}
