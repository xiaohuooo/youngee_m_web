package pack

import (
	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
	"youngee_m_api/model/http_model"
)

func MGormSpecialTaskScriptInfoListToHttpSpecialTaskScriptPreviewList(gormSpecialTaskScriptInfos []*http_model.SpecialTaskScriptInfo) []*http_model.SpecialTaskScriptPreview {
	var httpProjectPreviews []*http_model.SpecialTaskScriptPreview
	for _, gormSpecialTaskScriptInfo := range gormSpecialTaskScriptInfos {
		httpSpecialTaskScriptPreview := MGormSpecialTaskScriptInfoToHttpSpecialTaskScriptPreview(gormSpecialTaskScriptInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpSpecialTaskScriptPreview)
	}
	return httpProjectPreviews
}

func MGormSpecialTaskScriptInfoToHttpSpecialTaskScriptPreview(SpecialTaskScriptInfo *http_model.SpecialTaskScriptInfo) *http_model.SpecialTaskScriptPreview {
	return &http_model.SpecialTaskScriptPreview{
		TaskID:           conv.MustString(SpecialTaskScriptInfo.TaskID, ""),
		PlatformNickname: conv.MustString(SpecialTaskScriptInfo.PlatformNickname, ""),
		FansCount:        conv.MustString(SpecialTaskScriptInfo.FansCount, ""),
		Title:            SpecialTaskScriptInfo.Title,
		Content:          SpecialTaskScriptInfo.Content,
		ReviseOpinion:    SpecialTaskScriptInfo.ReviseOpinion,
		Phone:            SpecialTaskScriptInfo.Phone,
		Submit:           conv.MustString(SpecialTaskScriptInfo.SubmitAt, "")[0:19],
		AgreeAt:          conv.MustString(SpecialTaskScriptInfo.AgreeAt, "")[0:19],
	}
}

func SpecialTaskScriptToTaskInfo(SpecialTaskScripts []*http_model.SpecialTaskScript) []*http_model.SpecialTaskScriptInfo {
	var SpecialTaskScriptInfos []*http_model.SpecialTaskScriptInfo
	for _, SpecialTaskScript := range SpecialTaskScripts {
		SpecialTaskScript := GetSpecialTaskScriptInfoStruct(SpecialTaskScript)
		SpecialTaskScriptInfos = append(SpecialTaskScriptInfos, SpecialTaskScript)
	}
	return SpecialTaskScriptInfos
}

func GetSpecialTaskScriptInfoStruct(SpecialTaskScript *http_model.SpecialTaskScript) *http_model.SpecialTaskScriptInfo {
	TalentPlatformInfoSnap := SpecialTaskScript.Talent.TalentPlatformInfoSnap
	TalentPersonalInfoSnap := SpecialTaskScript.Talent.TalentPersonalInfoSnap
	return &http_model.SpecialTaskScriptInfo{
		TaskID:           SpecialTaskScript.Talent.TaskId,
		PlatformNickname: conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:        conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		ScriptId:         SpecialTaskScript.Script.ScriptID,
		Title:            SpecialTaskScript.Script.Title,
		Content:          SpecialTaskScript.Script.Content,
		ReviseOpinion:    SpecialTaskScript.Script.ReviseOpinion,
		CreateAt:         SpecialTaskScript.Script.CreateAt,
		SubmitAt:         SpecialTaskScript.Script.SubmitAt,
		AgreeAt:          SpecialTaskScript.Script.AgreeAt,
		Phone:            conv.MustString(gjson.Get(TalentPersonalInfoSnap, "talent_phone_number"), ""),
		RejectAt:         SpecialTaskScript.Script.RejectAt,
		IsReview:         SpecialTaskScript.Script.IsReview,
	}
}
