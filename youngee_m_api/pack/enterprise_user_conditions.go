package pack

import (
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpEnterpriseUserRequestToCondition(req *http_model.EnterpriseUserRequest) *common_model.EnterpriseUserConditions {
	return &common_model.EnterpriseUserConditions{
		User:      req.User,
		Username:  req.Username,
		CreatedAt: req.CreatedAt,
	}
}
