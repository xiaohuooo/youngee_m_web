package pack

import (
	"youngee_m_api/model/http_model"

	"github.com/caixw/lib.go/conv"
	"github.com/tidwall/gjson"
)

func MGormTaskLogisticsInfoListToHttpTaskLogisticsPreviewList(gormTaskLogisticsInfos []*http_model.TaskLogisticsInfo) []*http_model.TaskLogisticsPreview {
	var httpProjectPreviews []*http_model.TaskLogisticsPreview
	for _, gormTaskLogisticsInfo := range gormTaskLogisticsInfos {
		httpTaskLogisticsPreview := MGormTaskLogisticsInfoToHttpTaskLogisticsPreview(gormTaskLogisticsInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskLogisticsPreview)
	}
	return httpProjectPreviews
}

func MGormTaskLogisticsInfoToHttpTaskLogisticsPreview(TaskLogisticsInfo *http_model.TaskLogisticsInfo) *http_model.TaskLogisticsPreview {
	deliveryTime := conv.MustString(TaskLogisticsInfo.DeliveryTime, "")
	deliveryTime = deliveryTime[0:19]
	SignedTime := ""
	if TaskLogisticsInfo.SignedTime == "" {
		SignedTime = ""
	} else {
		SignedTime = TaskLogisticsInfo.SignedTime[:19]
	}
	return &http_model.TaskLogisticsPreview{
		TaskID:                conv.MustString(TaskLogisticsInfo.TaskID, ""),
		PlatformNickname:      conv.MustString(TaskLogisticsInfo.PlatformNickname, ""),
		FansCount:             conv.MustString(TaskLogisticsInfo.FansCount, ""),
		RecruitStrategyID:     conv.MustString(TaskLogisticsInfo.RecruitStrategyID, ""),
		StrategyID:            conv.MustString(TaskLogisticsInfo.StrategyID, ""),
		DetailAddr:            conv.MustString(TaskLogisticsInfo.DetailAddr, ""),
		CompanyName:           conv.MustString(TaskLogisticsInfo.CompanyName, ""),
		LogisticsNumber:       conv.MustString(TaskLogisticsInfo.LogisticsNumber, ""),
		DeliveryTime:          deliveryTime,
		ExplorestoreStarttime: TaskLogisticsInfo.ExplorestoreStarttime,
		ExplorestoreEndtime:   TaskLogisticsInfo.ExplorestoreEndtime,
		ExplorestorePeriod:    conv.MustString(TaskLogisticsInfo.ExplorestorePeriod, ""),
		CouponCode:            conv.MustString(TaskLogisticsInfo.CouponCode, ""),
		SignedTime:            SignedTime,
	}
}

func TaskLogisticsToTaskInfo(TaskLogisticss []*http_model.TaskLogistics) []*http_model.TaskLogisticsInfo {
	var TaskLogisticsInfos []*http_model.TaskLogisticsInfo
	for _, TaskLogistics := range TaskLogisticss {
		TaskLogistics := GetTalentInfoStruct(TaskLogistics)
		TaskLogisticsInfos = append(TaskLogisticsInfos, TaskLogistics)
	}
	return TaskLogisticsInfos
}

func GetTalentInfoStruct(TaskLogistics *http_model.TaskLogistics) *http_model.TaskLogisticsInfo {
	TalentPlatformInfoSnap := TaskLogistics.Talent.TalentPlatformInfoSnap
	TalentPostAddrSnap := TaskLogistics.Talent.TalentPostAddrSnap
	SignedTime := ""
	if TaskLogistics.Logistics.SignedTime == nil {
		SignedTime = ""
	} else {
		SignedTime = conv.MustString(TaskLogistics.Logistics.SignedTime, "")
	}
	return &http_model.TaskLogisticsInfo{
		TaskID:                TaskLogistics.Talent.TaskId,
		PlatformNickname:      conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:             conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		StrategyID:            TaskLogistics.Talent.StrategyId,
		DetailAddr:            conv.MustString(gjson.Get(TalentPostAddrSnap, "detail_addr"), "") + TaskLogistics.Region,
		CompanyName:           TaskLogistics.Logistics.CompanyName,
		LogisticsNumber:       TaskLogistics.Logistics.LogisticsNumber,
		DeliveryTime:          conv.MustString(TaskLogistics.Logistics.DeliveryTime, ""),
		ExplorestoreStarttime: TaskLogistics.Logistics.ExplorestoreStarttime,
		ExplorestoreEndtime:   TaskLogistics.Logistics.ExplorestoreEndtime,
		ExplorestorePeriod:    TaskLogistics.Logistics.ExplorestorePeriod,
		CouponCode:            TaskLogistics.Logistics.CouponCodeInformation,
		SignedTime:            SignedTime,
	}
}

func MGormSpecialTaskLogisticsInfoListToHttpTaskLogisticsPreviewList(gormTaskLogisticsInfos []*http_model.SpecialTaskLogisticsInfo) []*http_model.SpecialTaskLogisticsPreview {
	var httpProjectPreviews []*http_model.SpecialTaskLogisticsPreview
	for _, gormTaskLogisticsInfo := range gormTaskLogisticsInfos {
		httpTaskLogisticsPreview := MGormSpecialTaskLogisticsInfoToHttpTaskLogisticsPreview(gormTaskLogisticsInfo)
		httpProjectPreviews = append(httpProjectPreviews, httpTaskLogisticsPreview)
	}
	return httpProjectPreviews
}

func MGormSpecialTaskLogisticsInfoToHttpTaskLogisticsPreview(TaskLogisticsInfo *http_model.SpecialTaskLogisticsInfo) *http_model.SpecialTaskLogisticsPreview {
	deliveryTime := conv.MustString(TaskLogisticsInfo.DeliveryTime, "")
	deliveryTime = deliveryTime[0:19]
	SignedTime := ""
	if TaskLogisticsInfo.SignedTime == "" {
		SignedTime = ""
	} else {
		SignedTime = TaskLogisticsInfo.SignedTime[:19]
	}
	return &http_model.SpecialTaskLogisticsPreview{
		TaskID:                conv.MustString(TaskLogisticsInfo.TaskID, ""),
		PlatformNickname:      conv.MustString(TaskLogisticsInfo.PlatformNickname, ""),
		FansCount:             conv.MustString(TaskLogisticsInfo.FansCount, ""),
		DetailAddr:            conv.MustString(TaskLogisticsInfo.DetailAddr, ""),
		CompanyName:           conv.MustString(TaskLogisticsInfo.CompanyName, ""),
		LogisticsNumber:       conv.MustString(TaskLogisticsInfo.LogisticsNumber, ""),
		DeliveryTime:          deliveryTime,
		ExplorestoreStarttime: TaskLogisticsInfo.ExplorestoreStarttime,
		ExplorestoreEndtime:   TaskLogisticsInfo.ExplorestoreEndtime,
		ExplorestorePeriod:    conv.MustString(TaskLogisticsInfo.ExplorestorePeriod, ""),
		CouponCode:            conv.MustString(TaskLogisticsInfo.CouponCode, ""),
		SignedTime:            SignedTime,
	}
}

func SpecialTaskLogisticsToTaskInfo(TaskLogisticss []*http_model.SpecialTaskLogistics) []*http_model.SpecialTaskLogisticsInfo {
	var TaskLogisticsInfos []*http_model.SpecialTaskLogisticsInfo
	for _, TaskLogistics := range TaskLogisticss {
		TaskLogistics := GetSpecialTalentInfoStruct(TaskLogistics)
		TaskLogisticsInfos = append(TaskLogisticsInfos, TaskLogistics)
	}
	return TaskLogisticsInfos
}

func GetSpecialTalentInfoStruct(TaskLogistics *http_model.SpecialTaskLogistics) *http_model.SpecialTaskLogisticsInfo {
	TalentPlatformInfoSnap := TaskLogistics.Talent.TalentPlatformInfoSnap
	TalentPostAddrSnap := TaskLogistics.Talent.TalentPostAddrSnap
	SignedTime := ""
	if TaskLogistics.Logistics.SignedTime == nil {
		SignedTime = ""
	} else {
		SignedTime = conv.MustString(TaskLogistics.Logistics.SignedTime, "")
	}
	return &http_model.SpecialTaskLogisticsInfo{
		TaskID:                TaskLogistics.Talent.TaskId,
		PlatformNickname:      conv.MustString(gjson.Get(TalentPlatformInfoSnap, "platform_nickname"), ""),
		FansCount:             conv.MustString(gjson.Get(TalentPlatformInfoSnap, "fans_count"), ""),
		DetailAddr:            conv.MustString(gjson.Get(TalentPostAddrSnap, "detail_addr"), "") + TaskLogistics.Region,
		CompanyName:           TaskLogistics.Logistics.CompanyName,
		LogisticsNumber:       TaskLogistics.Logistics.LogisticsNumber,
		DeliveryTime:          conv.MustString(TaskLogistics.Logistics.DeliveryTime, ""),
		ExplorestoreStarttime: TaskLogistics.Logistics.ExplorestoreStarttime,
		ExplorestoreEndtime:   TaskLogistics.Logistics.ExplorestoreEndtime,
		ExplorestorePeriod:    TaskLogistics.Logistics.ExplorestorePeriod,
		CouponCode:            TaskLogistics.Logistics.CouponCodeInformation,
		SignedTime:            SignedTime,
	}
}
