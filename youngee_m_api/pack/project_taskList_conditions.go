package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpProjectTaskRequestToCondition(req *http_model.ProjectTaskListRequest) *common_model.TaskConditions {
	return &common_model.TaskConditions{
		ProjectId:        req.ProjectId,
		TaskStatus:       conv.MustInt64(req.TaskStatus, 0),
		StrategyId:       conv.MustInt64(req.StrategyId, 0),
		TaskId:           conv.MustString(req.TaskId, ""),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
