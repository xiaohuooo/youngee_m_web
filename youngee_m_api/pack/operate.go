package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/consts"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/gorm_model"
	"youngee_m_api/model/http_model"
	"youngee_m_api/util"
)

func HttpPricingRequestToConditions(req *http_model.SearchPricingRequest) *common_model.PricingConditions {
	pricingUpdated := conv.MustString(req.UpdateAt, "")
	return &common_model.PricingConditions{
		ProjectType: conv.MustInt64(req.ProjectType, 0),
		FeeForm:     conv.MustInt64(req.FeeForm, 0),
		Platform:    conv.MustInt64(req.Platform, 0),
		UpdateAt:    pricingUpdated,
	}
}

func HttpYoungeeRequestToConditions(req *http_model.SearchYoungeeRequest) *common_model.YoungeeConditions {
	return &common_model.YoungeeConditions{
		ProjectType: conv.MustInt64(req.ProjectType, 0),
		TaskType:    conv.MustInt64(req.TaskType, 0),
		Platform:    conv.MustInt64(req.Platform, 0),
		ContentType: conv.MustInt64(req.ContentType, 0),
	}
}

func GormSearchPricingDataToHttpData(gormPricings []*gorm_model.InfoPricingStrategy) []*http_model.SearchPricingPreview {
	var httpPricingPreviews []*http_model.SearchPricingPreview
	for _, gormPricing := range gormPricings {
		httpPricingPreview := MGromSearchPricingDataToHttpData(gormPricing)
		httpPricingPreviews = append(httpPricingPreviews, httpPricingPreview)
	}
	return httpPricingPreviews
}

func GormSearchYoungeeDataToHttpData(gormYoungees []*gorm_model.InfoYoungeeStrategy) []*http_model.SearchYoungeePreview {
	var httpYoungeePreviews []*http_model.SearchYoungeePreview
	for _, gormYoungee := range gormYoungees {
		httpYoungeePreview := MGromSearchYoungeeDataToHttpData(gormYoungee)
		httpYoungeePreviews = append(httpYoungeePreviews, httpYoungeePreview)
	}
	return httpYoungeePreviews
}

func MGromSearchYoungeeDataToHttpData(gormYoungee *gorm_model.InfoYoungeeStrategy) *http_model.SearchYoungeePreview {
	updatedTime := conv.MustString(gormYoungee.CreateAt, "")
	updatedTime = updatedTime[0:19]
	//if gormYoungee.FansLow == 0 && gormYoungee.FansUp == 0 {
	//	fans = "不限"
	//} else if gormYoungee.FansLow == 0 && gormYoungee.FansUp != 0 {
	//	fans = util.GetNumString(gormYoungee.FansUp) + "以下"
	//} else if gormYoungee.FansLow != 0 && gormYoungee.FansUp != 0 {
	//	fans = util.GetNumString(gormYoungee.FansLow) + "-" + util.GetNumString(gormYoungee.FansUp)
	//} else if gormYoungee.FansLow != 0 && gormYoungee.FansUp == 0 {
	//	fans = util.GetNumString(gormYoungee.FansLow) + "以上"
	//}
	return &http_model.SearchYoungeePreview{
		ID:          conv.MustString(gormYoungee.ID, ""),
		StrategyId:  gormYoungee.StrategyId,
		ProjectType: consts.GetProjectType(gormYoungee.ProjectType),
		Platform:    consts.GetProjectPlatform(gormYoungee.Platform),
		TaskType:    consts.GetTaskType(gormYoungee.TaskType),
		ContentType: consts.GetContentType(gormYoungee.ContentType),
		Reason:      consts.GetReason(gormYoungee.Reason),
		Points:      conv.MustString(gormYoungee.Points, ""),
		Cash:        conv.MustString(gormYoungee.Cash/1, "") + "%",
		Createat:    updatedTime,
	}
}

func MGromSearchPricingDataToHttpData(gormPricing *gorm_model.InfoPricingStrategy) *http_model.SearchPricingPreview {
	updatedTime := conv.MustString(gormPricing.UpdateAt, "")
	updatedTime = updatedTime[0:19]
	fans := ""
	//if gormPricing.FansLow == 0 && gormPricing.FansUp == 0 {
	//	fans = "不限"
	//} else if gormPricing.FansLow == 0 && gormPricing.FansUp != 0 {
	//	fans = util.GetNumString(gormPricing.FansUp) + "以下"
	//} else if gormPricing.FansLow != 0 && gormPricing.FansUp != 0 {
	//	fans = util.GetNumString(gormPricing.FansLow) + "-" + util.GetNumString(gormPricing.FansUp)
	//} else if gormPricing.FansLow != 0 && gormPricing.FansUp == 0 {
	//	fans = util.GetNumString(gormPricing.FansLow) + "以上"
	//}
	fans = util.GetNumString(gormPricing.FansLow) + "-" + util.GetNumString(gormPricing.FansUp)
	serviceCharge := ""
	if gormPricing.ServiceCharge == 0 {
		serviceCharge = "不限"
	} else {
		serviceCharge = conv.MustString(gormPricing.ServiceCharge, "")
	}
	return &http_model.SearchPricingPreview{
		ID:             conv.MustString(gormPricing.ID, ""),
		StrategyId:     gormPricing.StrategyId,
		ProjectType:    consts.GetProjectType(gormPricing.ProjectType),
		Platform:       consts.GetProjectPlatform(gormPricing.Platform),
		ManuscriptForm: consts.GetFeeForm(gormPricing.FeeForm),
		Fans:           fans,
		ServiceCharge:  serviceCharge,
		ServiceRate:    conv.MustString(gormPricing.ServiceRate/10, "") + "%",
		UpdateTime:     updatedTime,
	}
}

func HttpBreachHandledRequestToConditions(request *http_model.BreachHandledRequest) *common_model.BreachHandledConditions {
	return &common_model.BreachHandledConditions{
		HandleResult:    conv.MustInt32(request.HandleResult, 0),
		TerminateReason: conv.MustInt32(request.TerminateReason, 0),
	}
}
