package pack

import (
	"github.com/caixw/lib.go/conv"
	"youngee_m_api/model/common_model"
	"youngee_m_api/model/http_model"
)

func HttpSpecialTaskInviteListRequestToCondition(req *http_model.SpecialTaskInviteListRequest) *common_model.TalentConditions {
	return &common_model.TalentConditions{
		ProjectId:        req.ProjectId,
		TaskStatus:       conv.MustInt64(req.TaskStatus, 0),
		PlatformNickname: conv.MustString(req.PlatformNickname, ""),
	}
}
