# youngee_m_web

> 本系统采用一个极简的vue admin 管理后台 vue-admin-template

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration