import Vue from 'vue'
import Router from 'vue-router'
import Cookies from 'js-cookie'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/',
    redirect: '/dashboard/dashboard',
    component: () => import('@/views/dashboard/index'),
    hidden: true
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/dashboard',
    component: Layout,
    children: [{
      path: 'dashboard',
      name: '工作台',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '工作台', icon: 'el-icon-s-home' }
    }]
  },

  {
    path: '/product',
    component: Layout,
    redirect: '/product/wholeProcessProject',
    name: 'Product',
    meta: { title: '项目中心', icon: 'el-icon-s-grid' },
    children: [
      {
        path: 'wholeProcessProject',
        name: 'WholeProcessProject',
        component: () => import('@/views/product/wholeProcessProject/index'),
        meta: { title: '全流程项目', icon: '' }
      },
      {
        path: 'specialImplementationProject',
        name: 'SpecialImplementationProject',
        component: () => import('@/views/product/specialImplementationProject/index'),
        meta: { title: '专项执行项目', icon: '' }
      },
      {
        path: '/viewHome',
        name: 'ViewHome',
        component: () => import('@/views/product/viewHome/index')
        // meta: { title: '项目详情', icon: '' },
      },
      {
        path: '/projectDetail',
        name: 'ProjectDetail',
        component: () => import('@/components/Project/projectDetail/index')
        // meta: { title: '项目详情', icon: '' },
      },
      {
        path: '/recruitManage',
        name: 'RecruitManage',
        component: () => import('@/components/Project/recruitManage/index')
        // meta: { title: '招募管理', icon: '' },
      }
    ]
  },
  
//添加选品广场
  // {
  //   path: '/selection',
  //   component: Layout,
  //   meta: { title: '选品广场', icon: 'el-icon-s-shop' },
  //   redirect: '/selection',
  //   children: [{
  //     path: 'selection',
  //     name: '选品广场',
  //     component: () => import('@/views/selection/index'),
  //     meta: { title: '选品广场', icon: '' }
  //   },
  //   {
  //     path: '/detail',
  //     name: '选品详细',
  //     component: () => import('@/views/selection/detail/index'),
  //     // meta: { title: '选品详情', icon: '' },
  //   },
  // ]
  // },

  
  {
    path: '/selection',
    component: Layout,
    children: [
      {
        path: 'selection',
        name: '选品广场',
        component: () => import('@/views/SelectionCenter/SelectionSquare/SelectionSquare.vue'),
        meta: { title: '选品广场', icon: 'el-icon-s-shop' }
      },
   
    // {
    //   path: '/detail',
    //   name: '选品详细',
    //   component: () => import('@/views/SelectionCenter/SelectionSquare/SelectionDetail'),
    //   hidden: true,
    //   meta: { title: '选品详情', icon: '' }
    // },
    ]
  },

 


    // //BI中心
    // {
    //   path: '/bi',
    //   component: Layout,
    //   children: [{
    //     path: 'bi',
    //     name: 'BI中心',
    //     component: () => import('@/views/bi/index'),
    //     meta: { title: ' BI中心', icon: 'el-icon-data-line' }
    //   }]
    // },



  {
    path: '/operation',
    component: Layout,
    redirect: '/operation/pricingManagement',
    name: 'Operation',
    meta: { title: '运营中心', icon: 'el-icon-s-cooperation' },
    children: [
      {
        path: 'pricingManagement',
        name: 'PricingManagement',
        component: () => import('@/views/operation/pricingManagement/index'),
        meta: { title: '定价管理', icon: '' }
      },
      {
        path: 'automaticProcessingSettings',
        name: 'AutomaticProcessingSettings',
        component: () => import('@/views/operation/automaticProcessingSettings/index'),
        meta: { title: '自动处理设置', icon: '' }
      },
      {
        path: 'defaultDeductionSettings',
        name: 'DefaultDeductionSettings',
        component: () => import('@/views/operation/defaultDeductionSettings/index'),
        meta: { title: '违约扣款设置', icon: '' }
      },
      {
        path: 'breachOfContract',
        name: 'BreachOfContract',
        component: () => import('@/views/operation/breachOfContract/index'),
        meta: { title: '违约处理', icon: '' }
      },
      {
        path: 'youngeefans',
        name: 'Youngeefans',
        component: () => import('@/views/operation/youngeefans/index'),
        meta: { title: 'YOUNGEE之团设置', icon: '' }
      }
    ]
  },

  {
    path: '/user',
    component: Layout,
    redirect: '/user/enterpriseUserManagement',
    name: 'User',
    meta: { title: '用户中心', icon: 'el-icon-user-solid' },
    children: [
      {
        path: 'enterpriseUserManagement',
        name: 'EnterpriseUserManagement',
        component: () => import('@/views/user/enterpriseUserManagement/index'),
        meta: { title: '商家用户管理', icon: '' }
      },
      {
        path: 'creatorManagement',
        name: 'CreatorManagement',
        component: () => import('@/views/user/creatorManagement/index'),
        meta: { title: '创作者管理', icon: '' }
        // children: [
        //   {
        //     path: '/creatorDetail',
        //     name: 'CreatorDetail',
        //     component: () => import('@/views/user/creatorDetail/index'),
        //     hidden: true,
        //     meta: { title: '创作者详情', icon: '' }
        //   }
        // ]
      },
      {
        path: '/creatorDetail',
        name: 'CreatorDetail',
        component: () => import('@/views/user/creatorDetail/index'),
        hidden: true,
        meta: { title: '创作者详情', icon: '' }
      },
      {
        path: 'employeeAccountManagement',
        name: 'EmployeeAccountManagement',
        component: () => import('@/views/user/employeeAccountManagement/index'),
        meta: { title: '员工账号管理', icon: '' }
      },
      {
        path: '/enterpriseUserDetail',
        name: 'EnterpriseUserDetail',
        component: () => import('@/views/user/enterpriseUserDetail/index'),
        hidden: true,
        meta: { title: '企业用户详情', icon: '' }
      }
    ]
  },
  {
    path: '/finance',
    component: Layout,
    redirect: '/finance/rechargeManagement',
    name: 'Finance',
    meta: { title: '财务中心', icon: 'el-icon-s-finance' },
    children: [
      {
        path: 'rechargeManagement',
        name: 'RechargeManagement',
        component: () => import('@/views/finance/rechargeManagement/index'),
        meta: { title: '充值管理', icon: '' }
      },
      {
        path: 'invoiceManagement',
        name: 'InvoiceManagement',
        component: () => import('@/views/finance/invoiceManagement/index'),
        meta: { title: '发票管理', icon: '' }
      },
      {
        path: 'withdrawalManagement',
        name: 'WithdrawalManagement',
        component: () => import('@/views/finance/withdrawalManagement/index'),
        meta: { title: '提现管理', icon: '' }
      }
    ]
  },

  {
    path: '/accountWarehouse',
    component: Layout,
    children: [
      {
        path: 'accountWarehouse',
        name: 'AccountWarehouse',
        component: () => import('@/views/accountWarehouse/index'),
        meta: { title: '账号仓库', icon: 'form' }
      }
    ]
  },

  //BI中心
  {
    path: '/bi',
    component: Layout,
    children: [{
      path: 'bi',
      name: 'BI中心',
      component: () => import('@/views/bi/index'),
      meta: { title: ' BI中心', icon: 'el-icon-data-line' }
    }]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

// import { Message } from 'element-ui'
import store from '@/store'
router.beforeEach((to, from, next) => {
  // 获取用户token
  const token = Cookies.get('token_m')
  if (token) {
    store.state.isLogin = true
    next()
  } else {
    store.state.isLogin = false
    next()
    // 如果用户token不存在则跳转到login页面
    if (to.path === '/dashboard/dashboard' || to.path === '/' || from.path === '/') {
      next()
    } else {
      // Message.error('请先登录！')
      this.$router.replace(`/login`)
    }
  }
})
export default router
