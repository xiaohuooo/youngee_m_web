import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    settings,
    user,
    isLogin: false,
    project: {
      enterprise_id: '',
      project_name: '',
      project_type: '',
      project_platform: '',
      project_form: '',
      talent_type: [],
      content_type: '',
      recruit_ddl: '',
      project_detail: '',
      recruit_strategys: [],
      project_photos: [],
      product_id: '',
      project_id: '',
      create_at: ''
    },
    isCreating: true,
    username: ''
  },
  getters
})

export default store
