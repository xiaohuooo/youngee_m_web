import defaultSettings from '@/settings'

const title = defaultSettings.title || 'younggee_m_web'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
