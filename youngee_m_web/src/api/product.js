import request from '@/utils/request'

export function getProductList(params) {
  return request({
    url: '/youngee/m/product/list',
    method: 'post',
    data: params
  })
}

export function getProjectShow(params) {
  return request({
    url: '/youngee/m/project/show',
    method: 'post',
    data: params
  })
}
