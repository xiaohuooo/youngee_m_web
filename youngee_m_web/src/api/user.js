import request from '@/utils/request'
import Cookies from 'js-cookie'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/userInfo',
    method: 'post',
    params: { token }
  })
}

export function logout() {
  return Cookies.remove('token')
}
